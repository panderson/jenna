import fs                               from "fs";
import Path                             from "path";
import {readdirRecursiveSync, rx, when} from "./lib";

let files = (root = '') => when(
	readdirRecursiveSync(Path.join(__dirname, "..", root), {withFileTypes: true}),
	ent => !(ent.isDirectory() && ["test", "tests", "node_modules", ".git"].includes(ent.name))
);

const paths = new Map<string, string>();

for (const file of files()) {
	if (file.name !== "tsconfig.json")
		continue;

	const config = eval(`(${fs.readFileSync(file.path, "utf-8")})`);
	const options = config.compilerOptions;

	if (!options)
		continue;
	const aliases = options.paths;
	if (!aliases)
		continue;
	const baseUrl = options.baseUrl;
	if (!baseUrl)
		continue;

	for (const [alias, values] of Object.entries(aliases) as [string, string[]][]) {
		if (values.length !== 1) {
			throw new Error(
				`Don't know how to handle an alias with ${values.length} values:\n    "${
					alias
				}": ${values}
			`);
		}

		const value = `.${Path.sep}${Path.join(baseUrl, values[0])}`;
		if (paths.has(alias)) {
			throw new Error(
				`Alias ${alias} already found:\n    existing : "${alias}": ${
					paths.get(alias)
				}\n    new      : "${alias}": ${value}`
			);
		}

		if (alias.endsWith("/*")) {
			if (!value.endsWith("/*"))
				throw new Error(`Don't know how to handle alias "${alias}" for "${value}"`);
			paths.set(alias, value.slice(0, -2));
		} else {
			if (value.endsWith("/index"))
				paths.set(alias, value.slice(0, -6));
			else
				paths.set(alias, value);
		}
	}
}

function unalias(alias: string, relativeTo: string) {
	const path = paths.get(alias);
	if (!path) {
		throw new Error(
			`Alias "${alias}" not found in paths: ${JSON.stringify(paths, null, "    ")}`
		);
	}
	const result = Path.relative(relativeTo, Path.resolve(__dirname, "..", "dist", path));
	if (result.startsWith('.'))
		return result;
	return `./${result}`;
}

const rxRequire = rx`/
	require\(
		# Either kind of quote.
		(["'])

		(
			# My mapped imports all start with either '@' or '#'.
			[@#]
			# Match the first part of the import path, anything other than a closing quote
			# or either kind of slash.
			[^\1\/\\]+
		)

		# Match the rest of the import path, if present.
		([\/\\][^\1]*)?

		# A closing quote to match the opening one.
		\1
	\)
/gx`;
const rxImport  = rx`/
	(
		(?:im|ex)port\b\s*
		(?:
			(?:
				\*
				(?:\s+as\s+\w+)?
			|
				\{.+\}
			)
			\s+from\s+
		|
			\s*\(
		)?
	|
		declare\s+module\s*
	)
	(["'])
	([@\#][^"/\\]+)
	([/\\][^"]*)?
	\2
/gx`;


function fixRef(
	dir   : string,
	pre   : string,
	post  : string,
	quote : string,
	alias : string,
	path  : string | undefined
): string {
	if (path) {
		const root = unalias(`${alias}/*`, dir);
		console.log(`    ${alias}${path} => ${root}${path}`);
		return pre + quote + root + path + quote + post;
	}
	const root = unalias(alias, dir);
	console.log(`    ${alias} => ${root}`);
	return pre + quote + root + quote + post;
}

for (const file of files("dist")) {
	if (!file.isFile())
		continue;

	const dir = Path.dirname(file.path);

	if (file.name.endsWith(".js")) {
		const content = fs.readFileSync(file.path, "utf-8");
		if (rxRequire.test(content)) {
			console.log(file.path);
			const replaced = content.replace(
				rxRequire,
				(_, quote: string, alias: string, path: string | undefined) =>
					fixRef(dir, "require(", ")", quote, alias, path)
			);
			console.log("    saving...")
			fs.writeFileSync(file.path, replaced, {encoding: "utf-8"});
			console.log();
		}
	} else if(file.name.endsWith(".d.ts")) {
		const content = fs.readFileSync(file.path, "utf-8");
		if (rxImport.test(content)) {
			console.log(file.path);
			const replaced = content.replace(
				rxImport,
				(_, pre: string, quote: string, alias: string, path: string | undefined) =>
					fixRef(dir, pre, '', quote, alias, path)
			);
			console.log("    saving...")
			fs.writeFileSync(file.path, replaced, {encoding: "utf-8"});
			console.log();
		}
	}
}