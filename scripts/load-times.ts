import {execFileSync} from "child_process";
import {readdirSync}  from "fs";
import Path           from "path";
import {performance}  from "perf_hooks";

export function loadTime(pkg: string, paths?: string[]) {
	const path   = require.resolve(pkg, {paths});
	const start  = performance.now();
	const module = require(path);
	const end    = performance.now();

	return {
		package  : pkg,
		duration : end - start,
		module,
	};
}

const notNull = <T>(o: T | null | undefined): o is T => o !== null && o !== undefined;

export const loadTimes = (root: string, paths?: string[] | undefined) =>
	readdirSync(Path.join(root, "node_modules"))
	.sort(() => Math.random() - 0.5)
	.map((f, index) => {
		try {
			return {...loadTime(f, paths), index};
		} catch {
			return undefined;
		}
	})
	.filter(notNull);

const getRemoteResults = (args: string[]) => JSON.parse(
	execFileSync("node", args, {encoding: "utf-8"})
) as Record<string, {time: number, index: number}>;

export async function *averageLoadTimes(paths: (s: string) => string[] | null) {
	const args = [__filename, process.cwd()];
	const p    = paths('');
	if (p)
		args.push(p.join(Path.delimiter));

	while (true) {
		const results = getRemoteResults(args);
		const list    = Object.values(results).map(x => x.time);
		const total   = list.reduce((prev, x) => prev + x, 0);
		const sorted  = list.sort((a, b) => a - b);
		const max     = sorted[list.length - 1];
		const min     = sorted[0];
		const median  = sorted[Math.round(list.length / 2)];
		const mean    = total / list.length;
		const jenna   = results.jenna;

		yield {count: list.length, total, max, min, median, mean, ...jenna};
	}
}

if (module === require.main) {
	const paths = process.argv.length > 3 ?
		process.argv[3].split(Path.delimiter) :
		require.resolve.paths('') ?? undefined;

	const root = process.argv.length > 2 ? process.argv[2] : '.';

	console.log(JSON.stringify(Object.fromEntries(
		loadTimes(root, paths)
		.map(r => [r.package, {time: r.duration, index: r.index}])
	)));
}