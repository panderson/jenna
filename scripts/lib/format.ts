export function friendlyDuration(ms: number): string {
	let duration = ms;
	let scale;

	if (duration > 1_000) {
		duration /= 1_000;
		scale = 's';
	} else if(duration > 1)
		scale = "ms";
	else if(duration * 1_000 > 1) {
		duration *= 1_000;
		scale = "µs";
	} else {
		duration *= 1_000_000;
		scale = "ns";
	}

	return `${
		duration.toLocaleString(undefined, {useGrouping: true})
	} ${scale}`
}