import {escape} from "./utils";

export class Params {
	constructor(readonly pattern: string, readonly flags: Set<string>) {}
}

function embed(arg: unknown, flags: Set<string>): string {
	switch (typeof arg) {
	case "undefined" : return "";
	case "number"    :
	case "bigint"    :
	case "boolean"   : return String(arg);
	case "string"    : return escape(arg);
	case "symbol"    : return arg.description ?? arg.toString();
	case "object":
		if (arg === null)
			return "";

		if (arg instanceof RegExp || arg instanceof Params) {
			for (const flag of arg.flags)
				flags.add(flag);
			return arg instanceof Params ? arg.pattern : arg.source;
		}

		if (arg instanceof Date)
			return arg.toISOString();

		if (Object.getPrototypeOf(arg) === Object.prototype)
			return escape(JSON.stringify(arg));

		break; // Fall out and perform the default action.
	}
	return escape(String(arg));
}

export function params(template: TemplateStringsArray, args: any[]): Params {
	const flags   = new Set<string>();
	let   pattern = template.raw.flatMap((s, i) => [s, embed(args[i], flags)]).join('');
	const match   = /^\/(.+)\/([a-z]*)$/is.exec(pattern);

	if (match) {
		pattern = match[1];
		for (const flag of match[2])
			flags.add(flag.toLowerCase());
	}

	return new Params(pattern, flags);
}