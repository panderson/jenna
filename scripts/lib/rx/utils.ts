export const escape = (regex: string) => regex.replace(/[-\/\\^$*+?.()|[\]{}]/g, '\\$&');

export function condense(pattern: string): string {
	return pattern
		// Anything on a line following and including an unescaped '#' is a comment.  Remove it.
		.replace(/(?<=(?<!\\)(?:\\\\)*)#.*$/gm, '')

		// Remove any unescaped white space (including newlines).
		.replace(/(?<=(?<!\\)(?:\\\\)*)\s/g, '')

		// Remove the escapes before desired white space and '#' characters, which are now unnecessary.
		.replace(/(?<=(?:\\\\)*)\\(\s|#)/g, "$1");
}