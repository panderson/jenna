// This is copied and condensed from my separate `rx` project, to remove the dependency here on that
// project.

import {params} from "./params";
import {condense} from "./utils";

function toRegExp(pattern: string, flags?: string) {
	if (flags?.includes('x'))
		return new RegExp(condense(pattern), flags.replace('x', ''));

	return new RegExp(pattern, flags);
}

export function rx(template: TemplateStringsArray, ...args: any[]): RegExp {
	const {pattern, flags} = params(template, args);
	return toRegExp(pattern, Array.from(flags).join(''));
}