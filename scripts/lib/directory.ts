import {
	Dirent as BaseDirent,
	ObjectEncodingOptions,
	PathLike,
	readdirSync
}           from "fs";
import Path from "path";

type Dirent = BaseDirent & {path: string};

// @ts-ignore
export function readdirRecursiveSync(
	path     : PathLike,
	options? :
		| {encoding: BufferEncoding | null; withFileTypes?: false}
		| BufferEncoding
		| null
): Generator<string, void, boolean | undefined>;

// @ts-ignore
export function readdirRecursiveSync(
	path     : PathLike,
	options? :
		| {encoding: "buffer"; withFileTypes?: false}
		| "buffer"
): Generator<Buffer, void, boolean | undefined>;

// @ts-ignore
export function readdirRecursiveSync(
	path     : PathLike,
	options? :
		| ObjectEncodingOptions & {withFileTypes?: false}
		| BufferEncoding
		| null
): Generator<string, void, boolean | undefined> | Generator<Buffer, void, boolean | undefined>;

// @ts-ignore
export function readdirRecursiveSync(
	path     : PathLike,
	options? : ObjectEncodingOptions & {withFileTypes: true}
): Generator<Dirent, void, boolean | undefined>;

export function *readdirRecursiveSync(
	path     : PathLike,
	options? :
		| {encoding?: BufferEncoding | "buffer"; withFileTypes?: boolean}
		| BufferEncoding
		| "buffer"
		| null
): Generator<string | Buffer | Dirent, void, boolean | undefined> {
	const stringPath = typeof path === "string" ? path : path.toString("utf-8");

	const encoding: BufferEncoding | "buffer" =
		!options || options === "utf8" ?
			"utf-8" :
		typeof options === "string" ?
			options :
			options.encoding ?? "utf-8";

	const realOptions = {
		encoding      : encoding as BufferEncoding | null,
		withFileTypes : true as const
	};

	const toString: (name: string | Buffer) => string =
		encoding === "utf-8" ?
			name => name as string :
		encoding === "buffer" ?
			name => name.toString("utf-8") :
		name => Buffer.from(name as string, encoding).toString("utf-8");

	const toEncoding: (name: string) => string | Buffer =
		encoding === "utf-8" ?
			name => name :
		encoding === "buffer" ?
			name => Buffer.from(name) :
		name => Buffer.from(name).toString(encoding);

	const toOutput: (ent: BaseDirent) => string | Buffer | Dirent =
		options && typeof options === "object" && options.withFileTypes
			? ent => Object.assign(
				Object.create(ent),
				{path: toEncoding(Path.join(stringPath, toString(ent.name)))}
			) : ent => toEncoding(Path.join(stringPath, toString(ent.name)));

	for (const ent of readdirSync(path, realOptions)) {
		const skip = yield toOutput(ent);
		if (skip !== false && ent.isDirectory()) {
			yield *readdirRecursiveSync(
				Path.join(stringPath, toString(ent.name)),
				options as any
			);
		}
	}
}