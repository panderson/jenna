// This is stolen from the main source.
export function *when<T, N>(
	iterable : Iterable<T>,
	selector : (value: T) => N
): Generator<T, any, N | undefined> {
	const it = iterable[Symbol.iterator]();
	let step = it.next();
	while (!step.done) {
		yield step.value;
		step = it.next(selector(step.value) as any);
	}
	return step.value;
}