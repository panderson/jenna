#!/bin/bash -e

tsc -p src --outDir dist
rm dist/tsconfig.tsbuildinfo

ts-node scripts/unalias-paths &&

jq --tab 'del(.scripts) | del(.private)' package.json >dist/package.json

mkdir -p builds
mv -f *.tgz builds/ || true