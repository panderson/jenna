const {readdirSync} = require("fs");
const Path          = require("path");
const {performance} = require("perf_hooks");

function loadTime(pkg, paths){
	const path = require.resolve(pkg, {paths});
	const start = performance.now();
	const module = require(path);
	const end = performance.now();

	let duration = end - start;
	let scale;

	if(duration > 1_000){
		duration /= 1_000;
		scale = 's';
	}else if(duration > 1)
		scale = "ms";
	else if(duration * 1_000 > 1){
		duration *= 1_000;
		scale = "µs";
	}else{
		duration *= 1_000_000;
		scale = "ns";
	}

	return {
		package: pkg,
		duration : end - start,
		friendly :`${
			duration.toLocaleString(undefined, {useGrouping: true})
			} ${scale}`,
		module,
	};
}

function loadTimes(root, paths){
	const results = readdirSync(Path.join(root, "node_modules")).map(f => {
		try{
			return loadTime(f, paths);
		}catch{}
	}).filter(r => r);

	const maxLength = Math.max.apply(null, results.map(r => r.package.length));

	for(const r of results.sort((a, b) => a.duration - b.duration))
		console.log(`${r.package.padEnd(maxLength)} : ${r?.friendly}`);
}

module.exports = {loadTime, loadTimes};