import {basic}       from "./basic";
import {empty}       from "./empty";
import {extended}    from "./extended";
import {matchCounts} from "./match-counts";
import {passesIndex} from "./passes-index";
import {protocol}    from "./protocol";

describe("basic usage",  basic);
describe("passes index", passesIndex);
describe("match counts", matchCounts);
describe("empty",        empty);
describe("extended",     extended);
describe("protocol",     protocol);