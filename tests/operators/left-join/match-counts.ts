import {runTests} from "lib";
import * as data  from "./data";

export const matchCounts = () => runTests("leftJoin", {data}, ({leftJoin, data}) => {
	const {indexed, alphabet} = data;

	test("includes items with no match", indexed, alphabet, async (a, b) => {
		const result = await leftJoin(
			a(),
			b(),
			({index}) => index,
			word => word.length,
			({index}) => index,
		).toArray();

		for (const item of indexed)
			expect(result).toContain(item.index);
	});

	test("unmatched items are undefined", indexed, alphabet, async (a, b) => {
		const result = await leftJoin(
			a(),
			b(),
			({index}) => index,
			word => word.length,
			({index}, word) => ({index, word}),
		).toArray();

		for (const index of [0, 1, 8, 9, 10])
			expect(result).toContainEqual({index, word: undefined});
	});

	test("matched items include matches", indexed, alphabet, async (a, b) => {
		const result = await leftJoin(
			a(),
			b(),
			({index}) => index,
			word => word.length,
			({index}, word) => ({index, word}),
		).toArray();

		expect(result).toContainEqual({index: 2, word: "pi"});
		expect(result).toContainEqual({index: 3, word: "rho"});
		expect(result).toContainEqual({index: 5, word: "alpha"});
		expect(result).toContainEqual({index: 6, word: "lambda"});
		expect(result).toContainEqual({index: 7, word: "omicron"});
	});

	test("returns multiple matches", indexed, alphabet, async (a, b) => {
		const result = await leftJoin(
			a(),
			b(),
			({index}) => index,
			word => word.length,
		).toArray();

		const two   = result.filter(([{word}]) => word === "two")  .map(([, letter]) => letter);
		const three = result.filter(([{word}]) => word === "three").map(([, letter]) => letter);
		const five  = result.filter(([{word}]) => word === "five") .map(([, letter]) => letter);
		const six   = result.filter(([{word}]) => word === "six")  .map(([, letter]) => letter);
		const seven = result.filter(([{word}]) => word === "seven").map(([, letter]) => letter);

		expect(two)  .toEqual("mu nu xi pi".split(' '));
		expect(three).toEqual("eta rho tau phi chi psi".split(' '));
		expect(five) .toEqual("alpha gamma delta theta kappa sigma omega".split(' '));
		expect(six)  .toEqual(["lambda"]);
		expect(seven).toEqual("epsilon omicron upsilon".split(' '));
	});
});