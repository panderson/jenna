import {runTests} from "lib";

export const extended = () => runTests("leftJoin", ({AsyncIterable, leftJoin}) =>
	test("is extended", () =>
		expect(leftJoin("abc", [1, 2, 3], c => c.charCodeAt(0) - 95, i => i))
			.toBeInstanceOf(AsyncIterable)
	)
);