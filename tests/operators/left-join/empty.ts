import {runTests} from "lib";
import {numbers}  from "./data";

export const empty = () => runTests("leftJoin", {numbers}, ({leftJoin, empty, numbers}) => {
		test("right", numbers, async input => {
			const expected = [
				["zero",  undefined, 4],
				["one",   undefined, 3],
				["two",   undefined, 3],
				["three", undefined, 5],
				["four",  undefined, 4],
				["five",  undefined, 4],
				["six",   undefined, 3],
				["seven", undefined, 5],
				["eight", undefined, 5],
				["nine",  undefined, 4],
				["ten",   undefined, 3],
			];

			for (const [, value] of empty) {
				const result = await leftJoin(
					input(),
					value,
					word => word.length,
					() => 3,
				).toArray();

				expect(result).toStrictEqual(expected);
			}
		});

		test("left", numbers, async input => {
			for (const [, value] of empty) {
				expect(
					await leftJoin(value, input(), () => 3, word => word.length).count()
				).toBe(0);
			}
		});
	}
);