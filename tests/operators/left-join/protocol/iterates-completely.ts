import {runTests} from "lib";

export const iteratesCompletely = () => runTests("leftJoin", ({leftJoin, protocol}) => {
	const handlers = [protocol.createHandlers(), protocol.createHandlers()];

	beforeEach(() => jest.resetAllMocks());

	describe("iterates completely", () => {
		describe("left empty", () => {
			const left = protocol('', handlers[0]);

			test("right empty", async () => {
				await leftJoin(left, protocol('', handlers[1]), () => 1, () => 1).count();
				expect(handlers[0].onDone).toHaveBeenCalled();
				expect(handlers[1].onDone).toHaveBeenCalled();
			});

			test("right not empty", async () => {
				await leftJoin(left, protocol("def", handlers[1]), () => 1, () => 1).count();
				expect(handlers[0].onDone).toHaveBeenCalled();
				expect(handlers[1].onDone).toHaveBeenCalled();
			});
		});

		describe("left not empty", () => {
			const left = protocol("abc", handlers[0]);

			test("right empty", async () => {
				await leftJoin(left, protocol('', handlers[1]), () => 1, () => 1).count();
				expect(handlers[0].onDone).toHaveBeenCalled();
				expect(handlers[1].onDone).toHaveBeenCalled();
			});

			test("right not empty", async () => {
				await leftJoin(left, protocol("def", handlers[1]), () => 1, () => 1).count();
				expect(handlers[0].onDone).toHaveBeenCalled();
				expect(handlers[1].onDone).toHaveBeenCalled();
			});
		});
	});
});