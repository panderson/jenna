import {runTests} from "lib";

export const doesNotCallThrow = () => runTests("leftJoin", ({leftJoin, protocol, error}) => {
	const handlers = [protocol.createHandlers(), protocol.createHandlers()];

	beforeEach(() => jest.resetAllMocks());

	const left  = protocol("abc", handlers[0]);
	const right = protocol("def", handlers[1]);

	describe("doesn't call iterator.throw", () => {
		test("left", async () => {
			await expect(() => leftJoin(left, right, error, () => 1).count())
				.rejects.toThrow(error);
			expect(handlers[0].onThrow).not.toHaveBeenCalled();
			expect(handlers[1].onThrow).not.toHaveBeenCalled();
		});
		test("right", async () => {
			await expect(() => leftJoin(left, right, () => 1, error).count())
				.rejects.toThrow(error);
			expect(handlers[0].onThrow).not.toHaveBeenCalled();
			expect(handlers[1].onThrow).not.toHaveBeenCalled();
		});
	});
});