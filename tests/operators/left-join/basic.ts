import {runTests} from "lib";
import * as data  from "./data";

export const basic = () => runTests("leftJoin", {data}, ({leftJoin, data}) => {
	const {numbers, indexed} = data;

	test("with result selector", numbers, indexed, async (a, b) => {
		const result = await leftJoin(
			a(),
			b(),
			word => word.length,
			({index}) => index,
			(word, x) => `"${word}" is ${x ? numbers[x.index] : word.length} letters long`,
		).toArray();

		const expected = [
			'"zero" is 4 letters long',
			'"one" is three letters long',
			'"two" is three letters long',
			'"three" is five letters long',
			'"four" is 4 letters long',
			'"five" is 4 letters long',
			'"six" is three letters long',
			'"seven" is five letters long',
			'"eight" is five letters long',
			'"nine" is 4 letters long',
			'"ten" is three letters long',
		];

		expect(result).toStrictEqual(expected);
	});

	test("default result selector", numbers, indexed, async (a, b) => {
		const result = await leftJoin(
			a(),
			b(),
			word => word.length,
			({index}) => index,
		).toArray();

		const expected = [
			["zero",  undefined,                 4],
			["one",   {word: "three", index: 3}, 3],
			["two",   {word: "three", index: 3}, 3],
			["three", {word: "five",  index: 5}, 5],
			["four",  undefined,                 4],
			["five",  undefined,                 4],
			["six",   {word: "three", index: 3}, 3],
			["seven", {word: "five",  index: 5}, 5],
			["eight", {word: "five",  index: 5}, 5],
			["nine",  undefined,                 4],
			["ten",   {word: "three", index: 3}, 3],
		];

		expect(result).toStrictEqual(expected);
	});
});