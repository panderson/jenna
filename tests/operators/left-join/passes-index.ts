import {runTests} from "lib";
import * as data  from "./data";

export const passesIndex = () => runTests("leftJoin", {data}, ({leftJoin, data}) => {
	const {numbers, indexed, alphabet} = data;

	test("left key selector", numbers, indexed, async (a, b) => {
		const result = await leftJoin(
			a(),
			b(),
			(_, index) => index,
			({index}) => index,
		).toArray();

		const expected = [
			["zero",  {word: "zero",  index:  0},  0],
			["one",   {word: "one",   index:  1},  1],
			["two",   {word: "two",   index:  2},  2],
			["three", {word: "three", index:  3},  3],
			["four",  undefined,                   4],
			["five",  {word: "five",  index:  5},  5],
			["six",   {word: "six",   index:  6},  6],
			["seven", {word: "seven", index:  7},  7],
			["eight", {word: "eight", index:  8},  8],
			["nine",  {word: "nine",  index:  9},  9],
			["ten",   {word: "ten",   index: 10}, 10],
		];

		expect(result).toStrictEqual(expected);
	});

	test("right key selector", numbers, alphabet, async (a, b) => {
		const result = await leftJoin(
			a(),
			b(),
			word => word.length,
			(_, index) => index,
		).toArray();

		const expected = [
			["zero",  "epsilon", 4],
			["one",   "delta",   3],
			["two",   "delta",   3],
			["three", "zeta",    5],
			["four",  "epsilon", 4],
			["five",  "epsilon", 4],
			["six",   "delta",   3],
			["seven", "zeta",    5],
			["eight", "zeta",    5],
			["nine",  "epsilon", 4],
			["ten",   "delta",   3],
		];

		expect(result).toStrictEqual(expected);
	});
});