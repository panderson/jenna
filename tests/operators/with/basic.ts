import {runTests} from "lib";

export const basic = () => runTests("with", ({with: with_}) => {
	const data = Array.from("abcdefgh");

	describe.verbose("positive", test => {
		for (let length = 1; length < data.length; length++) {
			const source = data.slice(0, length);

			test(source.join(''), source, async input => {
				for (let index = 0; index < length; index++) {
					const result = await with_(input(), index, 'z').toArray();
					expect(result.length).toBe(length);

					for (let i = 0; i < length; i++)
						expect(result[i]).toBe(i === index ? 'z' : data[i]);
				}
			});
		}
	});

	describe.verbose("negative", test => {
		for (let length = 1; length < data.length; length++) {
			const source = data.slice(0, length);

			test(source.join(''), source, async input => {
				for (let index = -1; index >= -length; index--) {
					const returnValue = with_(input(), index, 'z');
					expect(returnValue).not.toBeInstanceOf(Array);
					const result = await returnValue.toArray();

					expect(result.length).toBe(length);
					for (let i = 0; i < length; i++)
						expect(result[i]).toBe(i === length + index ? 'z' : data[i]);
				}
			});
		}
	});

	describe("out of range", () => {
		test("empty", [], async input => {
			for (let i = -2; i < 3; i++)
				expect(() => with_(input(), i, 'z').toArray()).rejects.toThrowError(RangeError)
		});

		test("positive", [1, 2, 3], input => {
			expect(() => with_(input(), 3, 4).toArray()).rejects.toThrowError(RangeError);
			expect(() => with_(input(), 4, 4).toArray()).rejects.toThrowError(RangeError);
			expect(() => with_(input(), 6, 4).toArray()).rejects.toThrowError(RangeError);
		});

		test("negative", [1, 2, 3], input => {
			expect(() => with_(input(), -4, 4).toArray()).rejects.toThrowError(RangeError);
			expect(() => with_(input(), -5, 4).toArray()).rejects.toThrowError(RangeError);
			expect(() => with_(input(), -6, 4).toArray()).rejects.toThrowError(RangeError);
			expect(() => with_(input(), -7, 4).toArray()).rejects.toThrowError(RangeError);
		});
	});
});