import {runTests} from "lib";

export const passesParameter = () => runTests("with", ({with: with_, protocol}) => {
	const handlers = protocol.createHandlers();

	beforeEach(() => jest.resetAllMocks());

	const source   = "abcdef";
	const iterable = protocol(source, handlers);

	test("passes parameter to next", async () => {
		// Positive only!
		const it   = with_(iterable, 1, 'z');
		let   step = await it.next("ignored");

		while (!step.done)
			step = await it.next(step.value.toUpperCase());

		expect(handlers.onNext).toHaveBeenCalledTimes(source.length);
		for (let i = 0; i < source.length; i++) {
			expect(handlers.onNext).toHaveBeenNthCalledWith(
				i + 1,
				source[i],
				i === 1 ? 'Z' : source[i].toUpperCase(),
			);
		}
	});
});