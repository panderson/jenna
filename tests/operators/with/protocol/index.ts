import {iteratesCompletely} from "./iterates-completely";
import {passesParameter}    from "./passes-parameter";
import {callsReturn}        from "./return";
import {returnsValue}       from "./returns-value";
import {doesNotCallThrow}   from "./throw";

export function protocol() {
	describe("iterates completely",                iteratesCompletely);
	describe("calls iterator.return exactly once", callsReturn);
	describe("doesn't call iterator.throw",        doesNotCallThrow);
	passesParameter();
	describe("returns iterator return value",      returnsValue);
}