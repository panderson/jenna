import {runTests} from "lib";

export const returnsValue = () => runTests("with", ({with: with_, protocol}) => {
	const handlers = protocol.createHandlers();

	beforeEach(() => jest.resetAllMocks());

	const source   = "abcdef";
	const rtn      = Symbol();
	const iterable = protocol(source, rtn, handlers);

	test("positive", async () => {
		const it   = with_(iterable, 1, 'z');
		let   step = await it.next();

		while (!step.done)
			step = await it.next();

		expect(step.value).toBe(rtn);
	});
	test("negative", async () => {
		for (let i = 1; i <= source.length; i++) {
			const it   = with_(iterable, -i, 'z');
			let   step = await it.next();

			while (!step.done)
				step = await it.next();

			expect(step.value).toBe(rtn);
		}
	});
});