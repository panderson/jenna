import {runTests} from "lib";

export const iteratesCompletely = () => runTests("with", ({with: with_, protocol}) => {
	const handlers = protocol.createHandlers();

	beforeEach(() => jest.resetAllMocks());

	const source   = "abcdef";
	const iterable = protocol(source, handlers);

	test("positive", async () => {
		for (let i = 0; i < source.length; i++) {
			jest.resetAllMocks();
			await with_(iterable, i, 'z').count();
			expect(handlers.onDone).toHaveBeenCalled();
		}
	});
	test("negative", async () => {
		for (let i = -1; i >= -source.length; i--) {
			jest.resetAllMocks();
			await with_(iterable, i, 'z').count();
			expect(handlers.onDone).toHaveBeenCalled();
		}
	});
});