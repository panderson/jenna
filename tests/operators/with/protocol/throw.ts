import {runTests} from "lib";

export const doesNotCallThrow = () => runTests("with", ({with: with_, protocol, error}) => {
	const handlers = protocol.createHandlers();

	beforeEach(() => jest.resetAllMocks());

	const source   = "abcdef";
	const iterable = protocol(source, handlers);

	describe("out of range", () => {
		test("positive", async () => {
			for (let i = source.length; i < source.length + 3; i++) {
				jest.resetAllMocks();
				await expect(() => with_(iterable, i, 'z').count()).rejects.toThrow(RangeError);
				expect(handlers.onThrow).not.toHaveBeenCalled();
			}
		});
		test("negative", async () => {
			for (let i = -source.length - 1; i > -source.length - 4; i--) {
				jest.resetAllMocks();
				await expect(() => with_(iterable, i, 'z').count()).rejects.toThrow(RangeError);
				expect(handlers.onThrow).not.toHaveBeenCalled();
			}
		});
	});
	describe("error", () => {
		test("positive", async () => {
			for (let i = 0; i < source.length; i++) {
				jest.resetAllMocks();
				await expect(() => with_(iterable, i, error).count()).rejects.toThrow(error);
				expect(handlers.onThrow).not.toHaveBeenCalled();
			}
		});
		test("negative", async () => {
			for (let i = -source.length - 1; i < -source.length - 4; i++) {
				jest.resetAllMocks();
				await expect(() => with_(iterable, i, error).count()).rejects.toThrow(error);
				expect(handlers.onThrow).not.toHaveBeenCalled();
			}
		});
	});
});