import {runTests} from "lib";

export const extended = () => runTests("with", ({AsyncIterable, with: with_, protocol}) =>
	test("is extended", () => {
		expect(with_("abc", 1, 'z')).toBeInstanceOf(AsyncIterable);
		expect(with_("abc", -2, 'z')).toBeInstanceOf(AsyncIterable);
		expect(with_(['a', 'b', 'c'], 1, 'z')).toBeInstanceOf(AsyncIterable);
		expect(with_(['a', 'b', 'c'], -2, 'z')).toBeInstanceOf(AsyncIterable);
		expect(with_(new Set("abc"), 1, 'z')).toBeInstanceOf(AsyncIterable);
		expect(with_(new Set("abc"), -2, 'z')).toBeInstanceOf(AsyncIterable);
		expect(with_(protocol("abc"), 1, 'z')).toBeInstanceOf(AsyncIterable);
		expect(with_(protocol("abc"), -2, 'z')).toBeInstanceOf(AsyncIterable);
	})
);