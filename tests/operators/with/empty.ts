import {runTests} from "lib";

export const empty = () => runTests("with", ({with: with_, empty}) => {
	for (const [kind, value] of empty) {
		test(kind, () => {
			expect(async () => await with_(value, 0, '').count()).rejects.toThrowError(RangeError);
			expect(async () => await with_(value, -1, '').count()).rejects.toThrowError(RangeError);
		});
	}
});