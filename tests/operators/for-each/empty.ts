import {runTests} from "lib";

export const empty = () => runTests("forEach", ({forEach, empty}) => {
	const callback = jest.fn();

	beforeEach(() => callback.mockReset());

	for (const [kind, value] of empty) {
		test(kind, async () => {
			await forEach(value, callback);
			expect(callback).not.toHaveBeenCalled();
		});
	}
});