import {runTests} from "lib";

export const protocol = () => runTests("forEach", ({forEach, protocol, error}) => {
	const handlers = protocol.createHandlers();
	beforeEach(() => jest.resetAllMocks());

	const rtn      = Symbol();
	const iterable = protocol("abc", rtn, handlers);

	describe("iterates completely", () => {
		test("non-empty", async () => {
			await forEach(iterable, () => {});
			expect(handlers.onDone).toHaveBeenCalled();
		});
		test("empty", async () => {
			await forEach(protocol('', handlers), () => {});
			expect(handlers.onDone).toHaveBeenCalled();
		});
	});

	describe("calls iterator.return exactly once", () => {
		test("success", async () => {
			await forEach(iterable, () => {});
			expect(handlers.onReturn).toHaveBeenCalledTimes(1);
		});
		test("empty", async () => {
			await forEach(protocol('', handlers), () => {});
			expect(handlers.onReturn).toHaveBeenCalledTimes(1);
		});
		test("error", async () => {
			await expect(() => forEach(iterable, error)).rejects.toThrow(error);
			expect(handlers.onReturn).toHaveBeenCalledTimes(1);
		});
	});

	test("doesn't call iterator.throw", async () => {
		await expect(() => forEach(iterable, error)).rejects.toThrow(error);
		expect(handlers.onThrow).not.toHaveBeenCalled();
	});

	test("passes parameter to next", async () => {
		await forEach(iterable, c => c.toUpperCase());
		expect(handlers.onNext).toHaveBeenCalledTimes(3);
		expect(handlers.onNext).toHaveBeenNthCalledWith(1, 'a', 'A');
		expect(handlers.onNext).toHaveBeenNthCalledWith(2, 'b', 'B');
		expect(handlers.onNext).toHaveBeenNthCalledWith(3, 'c', 'C');
	});

	test("returns iterator return value", async () =>
		expect(await forEach(iterable, () => {})).toBe(rtn)
	);
});