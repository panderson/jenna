import {runTests} from "lib";

export const basic = () => runTests("forEach", ({forEach}) => {
	test("returns nothing", "abc", async input =>
		expect(await forEach(input(), c => c)).toBeUndefined()
	);

	test("invokes callback", "abc", async input => {
		const callback = jest.fn();
		await forEach(input(), callback);
		expect(callback).toHaveBeenCalledTimes(3);
	});
});