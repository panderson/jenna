import {runTests} from "lib";

export const extended = () => runTests("pick", ({AsyncIterable, pick}) =>
	test("is extended", "abc", 'c', async (a, b) =>
		expect(pick(a(), c => c, b())).toBeInstanceOf(AsyncIterable)
	)
);