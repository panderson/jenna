import {runTests} from "lib";

export const empty = () => runTests("pick", ({pick, empty}) => {
	describe("left", () => {
		for (const [kind, value] of empty)
			test(kind, async () => expect(await pick(value, () => 'a', "abc").count()).toBe(0));
	});

	describe("right", () => {
		for (const [kind, value] of empty)
			test(kind, async () => expect(await pick("abc", c => c, value).count()).toBe(0));
	});
});