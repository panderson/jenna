export const numbers = [
	{id: 0, name: "zero"},
	{id: 1, name: "one"},
	{id: 2, name: "two"},
	{id: 3, name: "three"},
	{id: 4, name: "four"},
	{id: 5, name: "five"},
	{id: 6, name: "six"},
	{id: 7, name: "seven"},
	{id: 8, name: "eight"},
	{id: 9, name: "nine"},
	{id: 10, name: "ten"},
];

export const evens = [0, 2, 4, 6, 8, 10];
export const odds  = [1, 3, 5, 7, 9];