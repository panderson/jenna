import {runTests} from "lib";

export const iteratesCompletely = () => runTests("pick", ({pick, protocol}) => {
	const handlers = [protocol.createHandlers(), protocol.createHandlers()];

	beforeEach(() => jest.resetAllMocks());

	describe("iterates completely", () => {
		describe("left empty", () => {
			const left = protocol('', handlers[0]);

			test("right empty", async () => {
				await pick(left, x => x, protocol('', handlers[1])).count();
				expect(handlers[0].onDone).toHaveBeenCalled();
				expect(handlers[1].onDone).toHaveBeenCalled();
			});

			test("right not empty", async () => {
				await pick(left, x => x, protocol("def", handlers[1])).count();
				expect(handlers[0].onDone).toHaveBeenCalled();
				expect(handlers[1].onDone).toHaveBeenCalled();
			});
		});

		describe("left not empty", () => {
			const left = protocol("abc", handlers[0]);

			test("right empty", async () => {
				await pick(left, x => x, protocol('', handlers[1])).count();
				expect(handlers[0].onDone).toHaveBeenCalled();
				expect(handlers[1].onDone).toHaveBeenCalled();
			});

			test("right not empty", async () => {
				await pick(left, x => x, protocol("def", handlers[1])).count();
				expect(handlers[0].onDone).toHaveBeenCalled();
				expect(handlers[1].onDone).toHaveBeenCalled();
			});
		});
	});
});