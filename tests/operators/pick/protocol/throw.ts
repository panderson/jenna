import {runTests} from "lib";

export const doesNotCallThrow = () => runTests("pick", ({pick, protocol, error}) => {
	const handlers = [protocol.createHandlers(), protocol.createHandlers()];

	beforeEach(() => jest.resetAllMocks());

	const left  = protocol("abc", handlers[0]);
	const right = protocol("def", handlers[1]);

	test("doesn't call iterator.throw", async () => {
		await expect(() => pick(left, error, right).count()).rejects.toThrow(error);
		expect(handlers[0].onThrow).not.toHaveBeenCalled();
		expect(handlers[1].onThrow).not.toHaveBeenCalled();
	});
});