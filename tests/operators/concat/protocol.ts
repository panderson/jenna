import {runTests} from "lib";

export const protocol = () => runTests("concat", ({concat, protocol}) => {
	const handlers = [
		protocol.createHandlers(),
		protocol.createHandlers(),
		protocol.createHandlers()
	];

	beforeEach(() => jest.resetAllMocks());

	const iterables = [
		protocol("abc", "(ABC)", handlers[0]),
		protocol("def", "(DEF)", handlers[1]),
		protocol("ghi", "(GHI)", handlers[2]),
	] as const;

	test("iterates completely", async () => {
		await concat(iterables[0], iterables[1], iterables[2]).count();
		expect(handlers[0].onDone).toHaveBeenCalled();
		expect(handlers[1].onDone).toHaveBeenCalled();
		expect(handlers[2].onDone).toHaveBeenCalled();
	});

	describe("calls iterator.return exactly once", () => {
		for (const error of [false, true]) {
			describe(error ? "error" : "abort", () => {
				for (let i = 0; i < 10; i++) {
					test(String(i), async () => {
						let count = 0;
						try {
							for await (const _ of concat(...iterables)) {
								if (count++ === i) {
									if (error)
										throw new Error();
									break;
								}
							}
						} catch {}

						expect(handlers[0].onReturn).toHaveBeenCalledTimes(1);

						if (i < 3)
							expect(handlers[1].onStart).not.toHaveBeenCalled();
						else
							expect(handlers[1].onReturn).toHaveBeenCalledTimes(1);

						if (i < 6)
							expect(handlers[2].onStart).not.toHaveBeenCalled();
						else
							expect(handlers[2].onReturn).toHaveBeenCalledTimes(1);
					});
				}
			});
		}
	});

	test("passes parameter to next", async () => {
		const it   = concat(...iterables);
		let   step = await it.next("ignored");

		while (!step.done)
			step = await it.next(step.value.toUpperCase());

		expect(handlers[0].onNext).toHaveBeenCalledTimes(3);
		expect(handlers[1].onNext).toHaveBeenCalledTimes(3);
		expect(handlers[2].onNext).toHaveBeenCalledTimes(3);

		expect(handlers[0].onNext).toHaveBeenNthCalledWith(1, 'a', 'A');
		expect(handlers[0].onNext).toHaveBeenNthCalledWith(2, 'b', 'B');
		expect(handlers[0].onNext).toHaveBeenNthCalledWith(3, 'c', 'C');

		expect(handlers[1].onNext).toHaveBeenNthCalledWith(1, 'd', 'D');
		expect(handlers[1].onNext).toHaveBeenNthCalledWith(2, 'e', 'E');
		expect(handlers[1].onNext).toHaveBeenNthCalledWith(3, 'f', 'F');

		expect(handlers[2].onNext).toHaveBeenNthCalledWith(1, 'g', 'G');
		expect(handlers[2].onNext).toHaveBeenNthCalledWith(2, 'h', 'H');
		expect(handlers[2].onNext).toHaveBeenNthCalledWith(3, 'i', 'I');
	});

	test("returns iterator return value", async () => {
		const it   = concat(...iterables);
		let   step = await it.next();

		while (!step.done)
			step = await it.next();

		expect(step.value).toBe("(GHI)");
	});
});