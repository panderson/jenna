import {runTests} from "lib";

export const extended = () => runTests("concat", ({AsyncIterable, concat}) =>
	test("is extended", "abcdef", [1, 2, 3], (a, b) =>
		expect(concat(a(), b())).toBeInstanceOf(AsyncIterable)
	)
);