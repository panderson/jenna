import {runTests} from "lib";

export const basic = () => runTests("concat", ({concat}) => {
	const first  = "abcdef";
	const second = [1, 2, 3];

	test("simple iteration", first, second, async (a, b) => {
		let index = 0;
		for await (const item of concat(a(), b())) {
			expect(index).toBeLessThan(first.length + second.length);
			expect(item).toBe(index < first.length ? first[index] : second[index - first.length]);
			index++;
		}
	});

	test("one input", first, async a => expect(await concat(a()).join()).toBe(first));

	test("three inputs", "abc", 'd', "ef", async (a, b, c) =>
		expect(await concat(a(), b(), c()).join()).toBe("abcdef")
	);
});