import {runTests} from "lib";

export const empty = () => runTests("concat", ({concat, empty}) => test("empty", async () =>
	expect(
		await concat(
			empty.string,
			empty.array,
			empty.set,
			empty.map,
			empty.iterable,
			empty.generator
		).count()
	).toBe(0)
));