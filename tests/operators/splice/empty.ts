import {runTests} from "lib";

export const empty = () => runTests("splice", ({splice, empty}) => {
	const quick   = !!process.env["QUICK"];
	const numbers = [
		[-Infinity, undefined, -2, -1, 0, 1, Infinity, NaN],
		[undefined, -1, 0, Infinity]
	][Number(quick)];

	for (const [kind, value] of empty) {
		describe(kind, () => {
			test("no arguments", async () => expect(await splice(value).count()).toBe(0));

			test("start only", async () => {
				for (const start of numbers)
					expect(await splice(value, start).count()).toBe(0);
			});

			test("start and deleteCount", async () => {
				for (const start of numbers) {
					for (const deleteCount of numbers)
						expect(await splice(value, start, deleteCount).count()).toBe(0);
				}
			});

			test("start, deleteCount, and newItems", async () => {
				for (const start of numbers) {
					for (const deleteCount of numbers)
						expect(await splice(value, start, deleteCount, 1, 2, 3).count()).toBe(3);
				}
			});
		});
	}
});