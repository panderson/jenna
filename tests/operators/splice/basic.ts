import {runTests} from "lib";

export const basic = () => runTests("splice", ({splice}) => {
	const quick   = !!process.env["QUICK"];
	const source  = "abcdef";
	const numbers = [
		[Infinity, undefined, 0, -10, -6, -3, -1, 0, 1, 3, 6, 10, -Infinity, NaN],
		[undefined, 0, -6, -1, 0, 1, 6, Infinity],
	][Number(quick)];

	async function check(
		input   : () => any,
		...args : [start?: number, deleteCount?: number, ...newItems: any[]]
	): Promise<void> {
		const actual   = await splice(input(), ...args).join();
		const expected = [...source];
		expected.splice.apply(expected, args as any);
		expect(actual).toBe(expected.join(''));
	}

	test("no arguments", source, input => check(input));

	test("start only", source, async input => {
		for (const start of numbers)
			await check(input, start);
	});

	test("start and deleteCount", source, async input => {
		for (const start of numbers) {
			for (const deleteCount of numbers.filter(i => !i || i >= 0))
				await check(input, start, deleteCount);
		}
	});

	test("start, deleteCount, and newItems", source, async input => {
		for (const start of numbers) {
			for (const deleteCount of numbers.filter(i => !i || i >= 0))
				await check(input, start, deleteCount, 1, 2, 3);
		}
	});
});