import {runTests} from "lib";

export const passesParameter = () => runTests("splice", ({splice, protocol}) => {
	const quick    = !!process.env["QUICK"];
	const handlers = protocol.createHandlers();

	beforeEach(() => jest.resetAllMocks());

	const source   = "abcdef";
	const iterable = protocol(source, handlers);

	async function iterateAll(
		...args: [start?: number, deleteCount?: number, ...items: any[]]
	): Promise<void> {
		jest.resetAllMocks();

		const it   = splice(iterable, ...args);
		let   step = await it.next("ignored");

		while (!step.done)
			step = await it.next(`<${step.value}>`);
	}

	test("no arguments", async () => {
		await iterateAll();
		expect(handlers.onNext).toHaveBeenCalledTimes(source.length);
		for (let i = 0; i < source.length; i++)
			expect(handlers.onNext).nthCalledWith(i + 1, source[i], `<${source[i]}>`);
	});

	test("start only", async () => {
		const starts = [[undefined, 0, 1, 3, 5, Infinity], [undefined, 0, 3]][Number(quick)];
		for (const start of starts) {
			await iterateAll(start);
			expect(handlers.onNext).toHaveBeenCalledTimes(source.length);

			if (start) {
				for (let i = 0; i < start && i < source.length; i++)
					expect(handlers.onNext).nthCalledWith(i + 1, source[i], `<${source[i]}>`);
			}

			for (let i = start ?? 0; i < source.length; i++)
				expect(handlers.onNext).nthCalledWith(i + 1, source[i], undefined);
		}
	});

	test("start and deleteCount", async () => {
		const starts       = [[undefined, Infinity, 0, 1, 3, 5], [undefined, 0, 3]][Number(quick)];
		const deleteCounts = [
			[undefined, Infinity, 0, 1, 3, 5, 6],
			[undefined, 0, 3, Infinity]
		][Number(quick)];

		for (const start of starts) {
			for (const deleteCount of deleteCounts) {
				await iterateAll(start, deleteCount);
				expect(handlers.onNext).toHaveBeenCalledTimes(source.length);

				if (start) {
					for (let i = 0; i < start && i < source.length; i++)
						expect(handlers.onNext).nthCalledWith(i + 1, source[i], `<${source[i]}>`);
				}

				for (let i = 0, j = start ?? 0; i < (deleteCount ?? 0) && j < source.length; i++, j++)
					expect(handlers.onNext).nthCalledWith(j + 1, source[j], undefined);

				for (let i = (start ?? 0) + (deleteCount ?? 0); i < source.length; i++)
					expect(handlers.onNext).nthCalledWith(i + 1, source[i], `<${source[i]}>`);
			}
		}
	});

	test("start, deleteCount, and newItems", async () => {
		const starts       = [[undefined, Infinity, 0, 1, 3, 5], [undefined, 0, 3]][Number(quick)];
		const deleteCounts = [
			[undefined, Infinity, 0, 1, 3, 5, 6],
			[undefined, 0, 3, Infinity]
		][Number(quick)];

		for (const start of starts) {
			for (const deleteCount of deleteCounts) {
				await iterateAll(start, deleteCount, 1, 2, 3);
				expect(handlers.onNext).toHaveBeenCalledTimes(source.length);

				if (start) {
					for (let i = 0; i < start && i < source.length; i++)
						expect(handlers.onNext).nthCalledWith(i + 1, source[i], `<${source[i]}>`);
				}

				for (let i = 0, j = start ?? 0; i < (deleteCount ?? 0) && j < source.length; i++, j++)
					expect(handlers.onNext).nthCalledWith(j + 1, source[j], undefined);

				for (let i = (start ?? 0) + (deleteCount ?? 0); i < source.length; i++)
					expect(handlers.onNext).nthCalledWith(i + 1, source[i], `<${source[i]}>`);
			}
		}
	});
});