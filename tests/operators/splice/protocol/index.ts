import {passesParameter} from "./passes-parameter";
import {callsReturn}     from "./return";

export function protocol() {
	describe("calls iterator.return exactly once", callsReturn);
	describe("passes parameter to next",           passesParameter);
}