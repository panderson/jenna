import {runTests} from "lib";

export const callsReturn = () => runTests("splice", ({splice, protocol}) => {
	const quick   = !!process.env["QUICK"];
	const numbers = [
		[-Infinity, undefined, -2, -1, 0, 1, Infinity, NaN],
		[undefined, -1, 0, Infinity]
	][Number(quick)];

	const handlers = protocol.createHandlers();

	beforeEach(() => jest.resetAllMocks());

	const source   = "abcdef";
	const iterable = protocol(source, handlers);

	test("no arguments", async () => {
		await splice(iterable).count();
		expect(handlers.onReturn).toHaveBeenCalledTimes(1);
	});

	test("start only", async () => {
		for (const start of numbers) {
			jest.resetAllMocks();
			await splice(iterable, start).count();
			expect(handlers.onReturn).toHaveBeenCalledTimes(1);
		}
	});

	test("start and deleteCount", async () => {
		for (const start of numbers) {
			for (const deleteCount of numbers) {
				jest.resetAllMocks();
				await splice(iterable, start, deleteCount).count();
				expect(handlers.onReturn).toHaveBeenCalledTimes(1);
			}
		}
	});

	test("start, deleteCount, and newItems", async () => {
		for (const start of numbers) {
			for (const deleteCount of numbers) {
				jest.resetAllMocks();
				await splice(iterable, start, deleteCount, 1, 2, 3).count();
				expect(handlers.onReturn).toHaveBeenCalledTimes(1);
			}
		}
	});
});