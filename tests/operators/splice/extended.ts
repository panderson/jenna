import {runTests} from "lib";

export const extended = () => runTests("splice", ({splice, AsyncIterable}) => {
	const quick   = !!process.env["QUICK"];
	const source  = "abcdef";
	const numbers = [
		[Infinity, undefined, 0, -10, -6, -3, -1, 0, 1, 3, 6, 10, -Infinity, NaN],
		[undefined, 0, -6, -1, 0, 1, 3, 6],
	][Number(quick)];

	test("is extended", source, input => {
		expect(splice(input())).toBeInstanceOf(AsyncIterable);

		for (const start of numbers) {
			expect(splice(input(), start)).toBeInstanceOf(AsyncIterable);

			for (const deleteCount of numbers) {
				expect(splice(input(), start, deleteCount)).toBeInstanceOf(AsyncIterable);
				expect(splice(input(), start, deleteCount, 1, 2, 3)).toBeInstanceOf(AsyncIterable);
			}
		}
	});
});