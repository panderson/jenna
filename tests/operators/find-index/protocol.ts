import {runTests} from "lib";

export const protocol = () => runTests("findIndex", ({findIndex, protocol, error}) => {
	const handlers = protocol.createHandlers();

	beforeEach(() => jest.resetAllMocks());

	const iterable = protocol("abc", handlers);

	describe("calls iterator.return exactly once", () => {
		test("found", async () => {
			await findIndex(iterable, () => true);
			expect(handlers.onReturn).toHaveBeenCalledTimes(1);
		});

		test("not found", async () => {
			await findIndex(iterable, () => false);
			expect(handlers.onReturn).toHaveBeenCalledTimes(1);
		});

		test("exception", async () => {
			await expect(() => findIndex(iterable, error)).rejects.toThrow(error);
			expect(handlers.onReturn).toHaveBeenCalledTimes(1);
		});
	});

	test("doesn't call iterator.throw", async () => {
		await expect(() => findIndex(iterable, error)).rejects.toThrow(error);
		expect(handlers.onThrow).not.toHaveBeenCalled();
	});

	test("doesn't iterate completely", async () => {
		await findIndex(iterable, () => true);
		expect(handlers.onDone).not.toHaveBeenCalled();
	});

	test("iterates completely", async () => {
		await findIndex(iterable, () => false);
		expect(handlers.onDone).toHaveBeenCalled();
	});
});