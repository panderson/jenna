import {runTests} from "lib";

export const empty = () => runTests("findIndex", ({findIndex, empty}) => {
	for (const [kind, value] of empty)
		test(kind, async () => expect(await findIndex(value, () => true)).toBe(-1));
});