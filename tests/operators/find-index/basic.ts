import {runTests} from "lib";

export const basic = () => runTests("findIndex", ({findIndex}) => {
	const source = "abcdef";

	for (let i = 0; i < source.length; i++) {
		test(source[i], source, async input =>
			expect(await findIndex(input(), v => v === source[i])).toBe(i)
		);
	}

	test('z', source, async input => expect(await findIndex(input(), v => v === 'z')).toBe(-1));
});