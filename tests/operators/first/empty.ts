import {runTests} from "lib";

export const empty = () => runTests("first", ({first, empty}) => {
	for (const [kind, value] of empty)
		test(kind, async () => expect(await first(value)).toBe(undefined));
});