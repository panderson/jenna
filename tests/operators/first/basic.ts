import {runTests} from "lib";

export const basic = () => runTests("first", ({first, protocol}) => {
	test("string", async () => expect(await first("abc")).toBe('a'));
	test("array",  async () => expect(await first([1, 2, 3])).toBe(1));
	test("set",    async () => expect(await first(new Set("abc"))).toBe('a'));
	test("map",    async () =>
		expect(await first(new Map([['a', 1], ['b', 2], ['c', 3]]))).toEqual(['a', 1])
	);

	test("generator", async () => expect(await first(protocol("abc"))).toBe('a'));
});