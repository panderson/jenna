import {runTests} from "lib";

export const protocol = () => runTests("first", ({first, protocol}) => {
	const handlers = protocol.createHandlers();

	beforeEach(() => jest.resetAllMocks());

	describe("doesn't iterate completely", () => {
		test("three items", async () => {
			await first(protocol("abc", handlers));
			expect(handlers.onDone).not.toHaveBeenCalled();
		});

		test("one item", async () => {
			await first(protocol('a', handlers));
			expect(handlers.onDone).not.toHaveBeenCalled();
		});
	});

	test("iterates empty completely", async () => {
		await first(protocol('', handlers));
		expect(handlers.onDone).toHaveBeenCalled();
	});

	describe("calls iterator.return exactly once", () => {
		test("three items", async () => {
			await first(protocol("abc", handlers));
			expect(handlers.onReturn).toHaveBeenCalledTimes(1);
		});

		test("one item", async () => {
			await first(protocol('a', handlers));
			expect(handlers.onReturn).toHaveBeenCalledTimes(1);
		});

		test("empty", async () => {
			await first(protocol('', handlers));
			expect(handlers.onReturn).toHaveBeenCalledTimes(1);
		});
	});
});