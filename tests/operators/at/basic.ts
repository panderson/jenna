import {runTests} from "lib";

export const basic = () => runTests("at", ({at}) => {
	const source = "abcdefghijk";
	test("positive", source, async input => {
		for (let i = 0; i < source.length; i++)
			expect(await at(input(), i)).toBe(source[i]);
	});

	test("negative", source, async input => {
		for (let i = -1; i >= -source.length; i--)
			expect(await at(input(), i)).toBe(source[source.length + i]);
	});

	test(">= length", source, async input => {
		expect(await at(input(), source.length)).toBe(undefined);
		expect(await at(input(), source.length * 2)).toBe(undefined);
	});

	test("< -length", source, async input =>
		expect(await at(input(), -source.length * 2)).toBe(undefined)
	);
});