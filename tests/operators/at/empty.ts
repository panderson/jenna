import {runTests} from "lib";

export const empty = () => runTests("at", ({at, empty}) => {
	for (const [kind, value] of empty)
		test(kind, async () => expect(await at(value, 0)).toBe(undefined));
});