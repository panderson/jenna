import {runTests} from "lib";

export const protocol = () => runTests("at", ({at, protocol}) => {
	const {onDone, onReturn} = protocol.createHandlers();

	beforeEach(() => jest.resetAllMocks());

	const iterable = protocol([1, 2, 3, 4, 5], {onDone, onReturn});

	test("calls iterator.return exactly once", async () => {
		await at(iterable, 3);
		expect(onReturn).toHaveBeenCalledTimes(1);
	});

	test("doesn't iterate completely", async () => {
		await at(iterable, 3);
		expect(onDone).not.toHaveBeenCalled();
	});

	test("iterates completely", async () => {
		await at(iterable, -3);
		expect(onDone).toHaveBeenCalled();
	});
});