import {runTests} from "lib";

export const protocol = () => runTests("present", ({present, protocol}) => {
	const handlers = protocol.createHandlers();

	beforeEach(() => jest.resetAllMocks());

	const rtn     = Symbol();
	const object  = {};
	const iterable = protocol([1, 2, 3, undefined, 'a', 'b', 'c', null, object], rtn, handlers);

	test("iterates completely", async () => {
		await present(iterable).count();
		expect(handlers.onDone).toHaveBeenCalled();
	});

	test("calls iterator.return exactly once", async () => {
		await present(iterable).count();
		expect(handlers.onReturn).toHaveBeenCalledTimes(1);
	});

	test("passes parameter to next", async () => {
		const it   = present(iterable);
		let   step = await it.next("ignored");

		while (!step.done)
			step = await it.next(`<${step.value}>`);

		expect(handlers.onNext).toHaveBeenCalledTimes(9);
		expect(handlers.onNext).toHaveBeenNthCalledWith(1, 1,         "<1>");
		expect(handlers.onNext).toHaveBeenNthCalledWith(2, 2,         "<2>");
		expect(handlers.onNext).toHaveBeenNthCalledWith(3, 3,         "<3>");
		expect(handlers.onNext).toHaveBeenNthCalledWith(4, undefined, undefined);
		expect(handlers.onNext).toHaveBeenNthCalledWith(5, 'a',       "<a>");
		expect(handlers.onNext).toHaveBeenNthCalledWith(6, 'b',       "<b>");
		expect(handlers.onNext).toHaveBeenNthCalledWith(7, 'c',       "<c>");
		expect(handlers.onNext).toHaveBeenNthCalledWith(8, null,      undefined);
		expect(handlers.onNext).toHaveBeenNthCalledWith(9, object,    "<[object Object]>");
	});

	test("returns iterator return value", async () => {
		const it   = present(iterable);
		let   step = await it.next();

		while (!step.done)
			step = await it.next();

		expect(step.value).toBe(rtn);
	});
});