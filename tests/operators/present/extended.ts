import {runTests} from "lib";

export const extended = () => runTests("present", ({AsyncIterable, present}) =>
	test("is extended", "abc", async input =>
		expect(present(input())).toBeInstanceOf(AsyncIterable)
	)
);