import {runTests} from "lib";

export const empty = () => runTests("present", ({present, empty}) => {
	for (const [kind, value] of empty)
		test(kind, async () => expect(await(present(value)).count()).toBe(0));
})