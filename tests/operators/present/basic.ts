import {runTests} from "lib";

export const basic = () => runTests("present", ({present}) => {
	const now      = new Date();
	const object   = {
		a: 'a',
		0: 0,
		empty: '',
		null: null,
		undefined: undefined,
	};
	const array    = ['a', 0, '', null, undefined];
	const source   = [
		0, 1, undefined, undefined, 2, 3,
		"four", null, "five", "six", null, "",
		true, false,
		now, object, array,
	];
	const expected = [0, 1, 2, 3, "four", "five", "six", '', true, false, now, object, array];

	test("removes null and undefined (only)", source, async input =>
		expect(await present(input()).toArray()).toEqual(expected)
	);

	test("doesn't remove anything if nothing is nullish", "abcdef", async input =>
		expect(await present(input()).join()).toEqual("abcdef")
	);
});