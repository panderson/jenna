import {runTests} from "lib";

export const empty = () => runTests("flatten", ({flatten, empty}) => {
	for (const [kind, value] of empty)
		test(kind, async () => expect(await flatten(value).count()).toBe(0));
});