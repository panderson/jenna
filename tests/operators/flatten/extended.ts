import {runTests} from "lib";

export const extended = () => runTests("flatten", ({AsyncIterable, flatten}) => {
	const words = ["the", "quick", "brown", "fox", "jumps", "over", "the", "lazy", "dog"];
	test("is extended", new Set(words), input =>
		expect(flatten(input())).toBeInstanceOf(AsyncIterable)
	);
});