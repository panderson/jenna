import {runTests} from "lib";

export const protocol = () => runTests("flatten", ({flatten, protocol, error}) => {
	const handlers = protocol.createHandlers();

	beforeEach(() => jest.resetAllMocks());

	const rtn      = Symbol();
	const iterable = protocol(["abc", "def"], rtn, handlers);

	describe("iterates complete", () => {
		test("self", async () => {
			await flatten(iterable).count();
			expect(handlers.onDone).toHaveBeenCalled();
		});
		test("children", async () => {
			await flatten("abc", () => iterable).count();
			expect(handlers.onDone).toHaveBeenCalledTimes(3);
		})
	});

	describe("calls iterator.return exactly once", () => {
		test("success", async () => {
			await flatten(iterable).count();
			expect(handlers.onReturn).toHaveBeenCalledTimes(1);
		});
		test("error", async () => {
			await expect(() => flatten(iterable, error).count()).rejects.toThrow(error);
			expect(handlers.onReturn).toHaveBeenCalledTimes(1);
		});
		test("of children", async () => {
			await flatten("abc", () => iterable).count();
			expect(handlers.onReturn).toHaveBeenCalledTimes(3);
		});
	});

	test("doesn't call iterator.throw", async () => {
		await expect(() => flatten(iterable, error).count()).rejects.toThrow(error);
		expect(handlers.onThrow).not.toHaveBeenCalled();
	});

	describe("passes parameter to next", () => {
		test("children", async () => {
			const it   = flatten("abc", () => iterable);
			let   step = await it.next("ignored");
			let   i    = 0;

			while (!step.done)
				step = await it.next(step.value.toUpperCase() + ++i);

			expect(handlers.onNext).toHaveBeenCalledTimes(6);
			expect(handlers.onNext).toHaveBeenNthCalledWith(1, "abc", "ABC1");
			expect(handlers.onNext).toHaveBeenNthCalledWith(2, "def", "DEF2");
			expect(handlers.onNext).toHaveBeenNthCalledWith(3, "abc", "ABC3");
			expect(handlers.onNext).toHaveBeenNthCalledWith(4, "def", "DEF4");
			expect(handlers.onNext).toHaveBeenNthCalledWith(5, "abc", "ABC5");
			expect(handlers.onNext).toHaveBeenNthCalledWith(6, "def", "DEF6");
		});

		test("parent", async () => {
			const it   = flatten(iterable, () => [10, 20, 30]);
			let   step = await it.next("ignored");
			let   i    = 0;

			while (!step.done)
				step = await it.next(step.value * ++i);

			expect(handlers.onNext).toHaveBeenCalledTimes(2);
			expect(handlers.onNext).toHaveBeenNthCalledWith(1, "abc", 90);
			expect(handlers.onNext).toHaveBeenNthCalledWith(2, "def", 180);
		});
	});

	test("returns iterator return value", async () => {
		const it   = flatten(iterable, () => [10, 20, 30]);
		let   step = await it.next();

		while (!step.done)
			step = await it.next();

		expect(step.value).toBe(rtn);
	});
});