import {runTests} from "lib";

export const basic = () => runTests("flatten", ({flatten, protocol}) => {
	const expected = "the quick brown fox jumps over the lazy dog";
	const words    = expected.split(/(?<= )|(?= )/);
	const arrays   = words.map(s => Array.from(s));
	const sets     = words.map(s => new Set(s));

	describe("self iterable", () => {
		test("words",  words , async input => expect(await flatten(input()).join()).toBe(expected));
		test("arrays", arrays, async input => expect(await flatten(input()).join()).toBe(expected));
		test("sets",   sets  , async input => expect(await flatten(input()).join()).toBe(expected));
	});

	describe("selector", () => {
		test("empty item", words, async input =>
			expect(await flatten(input(), w => w === "jumps" ? '' : w).join())
			.toBe(expected.replace("jumps", ''))
		);

		test("lookup", words.map((_, i) => i), async input =>
			expect(await flatten(input(), i => words[i]).join()).toBe(expected)
		);

		test("map", async () => {
			const lookup = new Map(words.map((word, index) => [index, word]));
			expect(await flatten(lookup, ([, word]) => word).join()).toBe(expected);
			expect(await flatten(lookup, ([, word]) => protocol(word)).join()).toBe(expected);
		});
	});
});