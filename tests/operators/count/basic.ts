import {runTests} from "lib";

export const basic = () => runTests("count", ({count, protocol}) => {
	const string = "abcdef";

	const iterable: AsyncIterable<string> & Iterable<string> = {
		[Symbol.asyncIterator](): AsyncIterator<string> {
			let index = 0;

			return {
				async next(): Promise<IteratorResult<string>> {
					if (index >= string.length)
						return {done: true, value: undefined};

					return {value: string[index++]};
				},
			};
		},
		[Symbol.iterator](): Iterator<string> {
			let index = 0;

			return {
				next(): IteratorResult<string> {
					if (index >= string.length)
						return {done: true, value: undefined};

					return {value: string[index++]};
				},
			};
		},
	};

	describe("basic usage", () => {
		test("string",    async () => expect(await count(string)).toBe(string.length));
		test("array",     async () => expect(await count(['a', 'b', 'c'])).toBe(3));
		test("set",       async () => expect(await count(new Set("abc"))).toBe(3));
		test("map",       async () => expect(await count(new Map([['a', 1], ['b', 2]]))).toBe(2));
		test("iterable",  async () => expect(await count(iterable)).toBe(string.length));
		test("generator", async () => expect(await count(protocol("abcde"))).toBe(5));
	});
});