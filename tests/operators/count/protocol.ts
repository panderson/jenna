import {runTests} from "lib";

export const protocol = () => runTests("count", ({count, protocol}) => {
	const handlers = protocol.createHandlers();

	const iterable = protocol("abc", handlers);

	beforeEach(() => jest.resetAllMocks());

	test("iterates completely", async () => {
		await count(iterable);
		expect(handlers.onDone).toHaveBeenCalled();
	});

	test("calls iterator.return exactly once", async () => {
		await count(iterable);
		expect(handlers.onReturn).toHaveBeenCalledTimes(1);
	});
});