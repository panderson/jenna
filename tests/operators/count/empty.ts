import {runTests} from "lib";

export const empty = () => runTests("count", ({count, empty}) => {
	for (const [kind, value] of empty)
		test(kind, async () => expect(await count(value)).toBe(0));
});