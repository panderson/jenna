import {runTests} from "lib";

export const protocol = () => runTests("mean", ({mean, protocol}) => {
	const handlers = protocol.createHandlers();

	const iterable = protocol("abc", handlers);

	beforeEach(() => jest.resetAllMocks());

	test("iterates completely", async () => {
		await mean(iterable);
		expect(handlers.onDone).toHaveBeenCalled();
	});

	test("calls iterator.return exactly once", async () => {
		await mean(iterable);
		expect(handlers.onReturn).toHaveBeenCalledTimes(1);
	});
});