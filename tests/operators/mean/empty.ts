import {runTests} from "lib";

export const empty = () => runTests("mean", ({mean, empty}) => {
	for (const [kind, value] of empty)
		test(kind, async () => expect(await mean(value)).toBe(undefined));
});