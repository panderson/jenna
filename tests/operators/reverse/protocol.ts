import {runTests} from "lib";

export const protocol = () => runTests("reverse", ({reverse, protocol}) => {
	const handlers = protocol.createHandlers();

	beforeEach(() => jest.resetAllMocks());

	const iterable = protocol("abcdef", handlers);

	test("iterates completely", async () => {
		await reverse(iterable);
		expect(handlers.onDone).toHaveBeenCalled();
	});

	test("calls iterator.return exactly once", async () => {
		await reverse(iterable);
		expect(handlers.onReturn).toHaveBeenCalledTimes(1);
	});
});