import {runTests} from "lib";

export const basic = () => runTests("reverse", ({reverse}) => {
	test("returns an array", "abcdef", async input =>
		expect(await reverse(input())).toBeInstanceOf(Array)
	);

	describe.verbose("reverses its input", test => {
		test("text", "abcdef", async input =>
			expect((await reverse(input())).join('')).toBe("fedcba")
		);

		test("assorted values", [0, 1, 2, 3, 4, 5, 6, undefined, true, false, null, {}], async input =>
			expect(await reverse(input()))
				.toEqual([{}, null, false, true, undefined, 6, 5, 4, 3, 2, 1, 0])
		);
	});
});