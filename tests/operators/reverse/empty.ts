import {runTests} from "lib";

export const empty = () => runTests("reverse", ({reverse, empty}) => {
	for (const [kind, value] of empty)
		test(kind, async () => expect(await reverse(value)).toEqual([]));
});