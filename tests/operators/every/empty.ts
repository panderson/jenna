import {runTests} from "lib";

export const empty = () => runTests("every", ({every, empty}) => {
	for (const [kind, value] of empty) {
		test(kind, async () => {
			expect(await every(value, () => false)).toBe(true);
			expect(await every(value, () => true)).toBe(true);
		});
	}
});