import {runTests} from "lib";

export const basic = () => runTests("every", ({every}) => {
	test("predicate returns true", "abc", async input =>
		expect(await every(input(), () => true)).toBe(true)
	);

	test("predicate returns false", "abc", async input =>
		expect(await every(input(), () => false)).toBe(false)
	);

	test("evaluates to truthy", [1, 3, 5], async input =>
		expect(await every(input(), i => (i & 1) as any)).toBe(true)
	);

	// This is the only test different from `some`.
	test("one item is not", [1, 3, 5, 0], async input =>
		expect(await every(input(), i => i as any)).toBe(false)
	);
});