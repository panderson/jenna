import {runTests} from "lib";

export const protocol = () => runTests("includes", ({includes, protocol}) => {
	const handlers = protocol.createHandlers();

	beforeEach(() => jest.resetAllMocks());

	const generator = () => protocol("abc", handlers);

	describe("calls iterator.return exactly once", () => {
		test("found", async () => {
			await includes(generator(), 'a');
			expect(handlers.onReturn).toHaveBeenCalledTimes(1);
		});

		test("not found", async () => {
			await includes(generator(), 'z');
			expect(handlers.onReturn).toHaveBeenCalledTimes(1);
		});
	});

	test("doesn't iterate completely", async () => {
		await includes(generator(), 'a');
		expect(handlers.onDone).not.toHaveBeenCalled();
	});

	test("iterates completely", async () => {
		await includes(generator(), 'z');
		expect(handlers.onDone).toHaveBeenCalled();
	});
});