import {runTests} from "lib";

export const basic = () => runTests("includes", ({includes}) => {
	const source = "abcdef";

	test("without minIndex", source, async input => {
		for (let i = 0; i < source.length; i++)
			expect(await includes(input(), source[i])).toBe(true);

		expect(await includes(input(), 'z')).toBe(false);
	});

	test("with minIndex", source, async input => {
		expect(await includes(input(), 'a', -1)).toBe(true);
		expect(await includes(input(), 'a', 0)).toBe(true);

		for (let i = 1; i <= source.length; i++)
			expect(await includes(input(), 'a', i)).toBe(false);

		expect(await includes(input(), 'a', source.length * 2)).toBe(false);
	});
});