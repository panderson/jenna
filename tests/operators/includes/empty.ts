import {runTests} from "lib";

export const empty = () => runTests("includes", ({includes, empty}) => {
	for (const [kind, value] of empty)
		test(kind, async () => expect(await includes(value, '')).toBe(false));
});