import {runTests} from "lib";

export const extended = () => runTests("extend", ({AsyncIterable, extend}) =>
	test("is extended", "abcdef", a => expect(extend(a(), 1, 2, 3)).toBeInstanceOf(AsyncIterable))
);