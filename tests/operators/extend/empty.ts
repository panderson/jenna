import {runTests} from "lib";

export const empty = () => runTests("extend", ({extend, empty}) => {
	for (const [kind, value] of empty)
		test(kind, async () => expect(await extend(value, 'a').join()).toBe('a'));
});