import {runTests} from "lib";

export const basic = () => runTests("extend", ({extend}) => {
	const first  = "abcdef";
	const second = [1, 2, 3];

	test("simple iteration", first, async a => {
		let index = 0;

		for await (const item of extend(a(), ...second)) {
			expect(index).toBeLessThan(first.length + second.length);
			expect(item).toBe(index < first.length ? first[index] : second[index - first.length]);
			index++;
		}
	});

	test("one input",  first, async a => expect(await extend(a()).join()).toBe(first));
	test("two inputs", "abc", async a => expect(await extend(a(), 1).join()).toBe("abc1"));
});