import {runTests} from "lib";

export const protocol = () => runTests("extend", ({extend, protocol}) => {
	const handlers = protocol.createHandlers();

	beforeEach(() => jest.resetAllMocks());

	const iterable = protocol("abc", handlers);

	test("iterates completely", async () => {
		await extend(iterable, 1).count();
		expect(handlers.onDone).toHaveBeenCalled();
	});

	describe("calls iterator.return exactly once", () => {
		for (const error of [false, true]) {
			describe(error ? "error" : "abort", () => {
				for (let i = 0; i < 5; i++) {
					test(String(i), async () => {
						let count = 0;
						try {
							for await (const _ of extend(iterable, 1)) {
								if (count++ === i) {
									if (error)
										throw new Error();
									break;
								}
							}
						} catch {}

						expect(handlers.onReturn).toHaveBeenCalledTimes(1);
					});
				}
			});
		}
	});

	test("passes parameter to next", async () => {
		const it   = extend(iterable, 1);
		let   step = await it.next("ignored");

		while (!step.done)
			step = await it.next(`<${step.value}>`);

		expect(handlers.onNext).toHaveBeenCalledTimes(3);
		expect(handlers.onNext).toHaveBeenNthCalledWith(1, 'a', "<a>");
		expect(handlers.onNext).toHaveBeenNthCalledWith(2, 'b', "<b>");
		expect(handlers.onNext).toHaveBeenNthCalledWith(3, 'c', "<c>");
	});
});