import {basic}   from "./basic";
import {empty}   from "./empty";
import {extended} from "./extended";
import {protocol} from "./protocol";

describe("basic usage", basic);
describe("extended",    extended);
describe("empty",       empty);
describe("protocol",    protocol);