import {runTests} from "lib";

export const protocol = () => runTests("lastIndexOf", ({lastIndexOf, protocol}) => {
	const handlers = protocol.createHandlers();

	beforeEach(() => jest.resetAllMocks());

	const iterable = protocol("abc", handlers);

	describe("calls iterator.return exactly once", () => {
		test("found", async () => {
			await lastIndexOf(iterable, 'a');
			expect(handlers.onReturn).toHaveBeenCalledTimes(1);
		});

		test("not found", async () => {
			await lastIndexOf(iterable, 'z');
			expect(handlers.onReturn).toHaveBeenCalledTimes(1);
		});
	});

	describe("iterates completely", () => {
		test("found", async () => {
			await lastIndexOf(iterable, 'a');
			expect(handlers.onDone).toHaveBeenCalled();
		});

		test("not found", async () => {
			await lastIndexOf(iterable, 'z');
			expect(handlers.onDone).toHaveBeenCalled();
		});
	});
});