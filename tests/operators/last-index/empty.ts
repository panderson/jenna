import {runTests} from "lib";

export const empty = () => runTests("lastIndexOf", ({lastIndexOf, empty}) => {
	for (const [kind, value] of empty)
		test(kind, async () => expect(await lastIndexOf(value, undefined)).toBe(-1));
});