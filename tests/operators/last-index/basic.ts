import {runTests} from "lib";

export const basic = () => runTests("lastIndexOf", ({lastIndexOf}) => {
	const source = "abcdef";

	test("without maxIndex", source, async input => {
		for (let i = 0; i < source.length; i++)
			expect(await lastIndexOf(input(), source[i])).toBe(i);

		expect(await lastIndexOf(input(), 'z')).toBe(-1);
	});


	test("with maxIndex", source, async input => {
		for (const letter of source)
			expect(await lastIndexOf(input(), letter, -1)).toBe(-1);

		for (let i = 0; i <= source.length; i++) {
			expect(await lastIndexOf(input(), 'a', i)).toBe(0);
			expect(await lastIndexOf(input(), 'b', i)).toBe(i < 1 ? -1 : 1);
			expect(await lastIndexOf(input(), 'c', i)).toBe(i < 2 ? -1 : 2);
			expect(await lastIndexOf(input(), 'd', i)).toBe(i < 3 ? -1 : 3);
			expect(await lastIndexOf(input(), 'e', i)).toBe(i < 4 ? -1 : 4);
			expect(await lastIndexOf(input(), 'f', i)).toBe(i < 5 ? -1 : 5);
		}

		for (let i = 0; i < source.length; i++)
			expect(await lastIndexOf(input(), source[i], source.length * 2)).toBe(i);
	});

	describe("finds last index", () => {
		test("without maxIndex", source + source, async input => {
			for (let i = 0; i < source.length; i++)
				expect(await lastIndexOf(input(), source[i])).toBe(source.length + i);
		});

		test("with maxIndex", source + source + source, async input => {
			for (let i = 0; i < source.length; i++) {
				expect(await lastIndexOf(
					input(),
					source[i],
					source.length * 2 + i - 1
				)).toBe(source.length + i)
			}
		});
	});
});