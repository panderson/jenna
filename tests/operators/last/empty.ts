import {runTests} from "lib";

export const empty = () => runTests("last", ({last, empty}) => {
	for (const [kind, value] of empty)
		test(kind, async () => expect(await last(value)).toBe(undefined));
});