import {runTests} from "lib";

export const basic = () => runTests("last", ({last, protocol}) => {
	test("string", async () => expect(await last("abc")).toBe('c'));
	test("array",  async () => expect(await last([1, 2, 3])).toBe(3));
	test("set",    async () => expect(await last(new Set("abc"))).toBe('c'));
	test("map",    async () =>
		expect(await last(new Map([['a', 1], ['b', 2], ['c', 3]]))).toEqual(['c', 3])
	);

	test("generator", async () => expect(await last(protocol("abc"))).toBe('c'));
});