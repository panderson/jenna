import {runTests} from "lib";

export const protocol = () => runTests("last", ({last, protocol}) => {
	const handlers = protocol.createHandlers();

	beforeEach(() => jest.resetAllMocks());

	describe("iterates completely", () => {
		test("empty", async () => {
			await last(protocol('', handlers));
			expect(handlers.onDone).toHaveBeenCalled();
		});
		test("one item", async () => {
			await last(protocol('a', handlers));
			expect(handlers.onDone).toHaveBeenCalled();
		});
		test("three items", async () => {
			await last(protocol("abc", handlers));
			expect(handlers.onDone).toHaveBeenCalled();
		});
	});

	describe("calls iterator.return exactly once", () => {
		test("empty", async () => {
			await last(protocol('', handlers));
			expect(handlers.onReturn).toHaveBeenCalledTimes(1);
		});
		test("one item", async () => {
			await last(protocol('a', handlers));
			expect(handlers.onReturn).toHaveBeenCalledTimes(1);
		});
		test("three items", async () => {
			await last(protocol("abc", handlers));
			expect(handlers.onReturn).toHaveBeenCalledTimes(1);
		});
	});
});