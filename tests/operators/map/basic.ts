import {runTests} from "lib";

export const basic = () => runTests("map", ({map}) => {
	const source = "abcdef";

	test("basic usage", source, async input => {
		const result   = await map(input(), c => c.charCodeAt(0)).toArray();
		const expected = [97, 98, 99, 100, 101, 102];
		expect(result).toEqual(expected);
	});

	test("passes index", source, async input => {
		const result   = await map(input(), (_, i) => i).toArray();
		const expected = [0, 1, 2, 3, 4, 5];
		expect(result).toEqual(expected);
	});
});