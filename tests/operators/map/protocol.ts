import {runTests} from "lib";

export const protocol = () => runTests("map", ({map, protocol, error}) => {
	const handlers = protocol.createHandlers();

	beforeEach(() => jest.resetAllMocks());

	const rtn      = Symbol();
	const iterable = protocol("abc", rtn, handlers);

	test("iterates completely", async () => {
		await map(iterable, c => c).count();
		expect(handlers.onDone).toHaveBeenCalled();
	});

	describe("calls iterator.return exactly once", () => {
		test("success", async () => {
			await map(iterable, c => c).count();
			expect(handlers.onReturn).toHaveBeenCalledTimes(1);
		});

		test("error", async () => {
			await expect(() => map(iterable, error).count()).rejects.toThrow(error);
			expect(handlers.onReturn).toHaveBeenCalledTimes(1);
		});
	});

	test("doesn't call iterator.throw", async () => {
		await expect(() => map(iterable, error).count()).rejects.toThrow(error);
		expect(handlers.onThrow).not.toHaveBeenCalled();
	});

	test("passes parameter to next", async () => {
		const it   = map(iterable, c => c.toUpperCase());
		let   step = await it.next("ignored");

		while (!step.done)
			step = await it.next(`<${step.value}>`);

		expect(handlers.onNext).toHaveBeenCalledTimes(3);
		expect(handlers.onNext).toHaveBeenNthCalledWith(1, 'a', "<A>");
		expect(handlers.onNext).toHaveBeenNthCalledWith(2, 'b', "<B>");
		expect(handlers.onNext).toHaveBeenNthCalledWith(3, 'c', "<C>");
	});

	test("returns iterator return value", async () => {
		const it   = map(iterable, c => c.toUpperCase());
		let   step = await it.next();

		while (!step.done)
			step = await it.next();

		expect(step.value).toBe(rtn);
	});
});