import {runTests} from "lib";

export const empty = () => runTests("map", ({map, empty}) => {
	for (const [kind, value] of empty)
		test(kind, async () => expect(await map(value, c => c).count()).toBe(0))
});