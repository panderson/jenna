import {runTests} from "lib";

export const extended = () => runTests("map", ({AsyncIterable, map}) =>
	test("is extended", "abcdef", input =>
		expect(map(input(), c => c)).toBeInstanceOf(AsyncIterable)
	)
);