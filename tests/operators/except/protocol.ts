import {runTests} from "lib";

export const protocol = () => runTests("except", ({except, protocol}) => {
	const handlers = [protocol.createHandlers(), protocol.createHandlers()];

	beforeEach(() => jest.resetAllMocks());

	describe("iterates completely", () => {
		describe("left empty", () => {
			const left = protocol('', handlers[0]);

			test("right empty", async () => {
				await except(left, protocol('', handlers[1])).count();
				expect(handlers[0].onDone).toHaveBeenCalled();
				expect(handlers[1].onDone).toHaveBeenCalled();
			});

			test("right not empty", async () => {
				await except(left, protocol("abc", handlers[1])).count();
				expect(handlers[0].onDone).toHaveBeenCalled();
				expect(handlers[1].onDone).toHaveBeenCalled();
			});
		});

		describe("left not empty", () => {
			const left = protocol("abc", handlers[0]);

			test("right empty", async () => {
				await except(left, protocol('', handlers[1])).count();
				expect(handlers[0].onDone).toHaveBeenCalled();
				expect(handlers[1].onDone).toHaveBeenCalled();
			});

			test("right not empty", async () => {
				await except(left, protocol("abc", handlers[1])).count();
				expect(handlers[0].onDone).toHaveBeenCalled();
				expect(handlers[1].onDone).toHaveBeenCalled();
			});
		});
	});

	describe("calls iterator.return exactly once", () => {
		describe("left empty", () => {
			const left = protocol('', handlers[0]);

			test("right empty", async () => {
				await except(left, protocol('', handlers[1])).count();
				expect(handlers[0].onReturn).toHaveBeenCalledTimes(1);
				expect(handlers[1].onReturn).toHaveBeenCalledTimes(1);
			});

			test("right not empty", async () => {
				await except(left, protocol("abc", handlers[1])).count();
				expect(handlers[0].onReturn).toHaveBeenCalledTimes(1);
				expect(handlers[1].onReturn).toHaveBeenCalledTimes(1);
			});
		});

		describe("left not empty", () => {
			const left = protocol("abc", handlers[0]);

			test("right empty", async () => {
				await except(left, protocol('', handlers[1])).count();
				expect(handlers[0].onReturn).toHaveBeenCalledTimes(1);
				expect(handlers[1].onReturn).toHaveBeenCalledTimes(1);
			});

			test("right not empty", async () => {
				await except(left, protocol("abc", handlers[1])).count();
				expect(handlers[0].onReturn).toHaveBeenCalledTimes(1);
				expect(handlers[1].onReturn).toHaveBeenCalledTimes(1);
			});
		});
	});

	test("passes parameter to next", async () => {
		const it   = except(protocol("abcdef", handlers[0]), "cd");
		let   step = await it.next("ignored");

		while (!step.done)
			step = await it.next(step.value.toUpperCase());

		expect(handlers[0].onNext).toHaveBeenCalledTimes(6);
		expect(handlers[0].onNext).toHaveBeenNthCalledWith(1, 'a', 'A');
		expect(handlers[0].onNext).toHaveBeenNthCalledWith(2, 'b', 'B');
		expect(handlers[0].onNext).toHaveBeenNthCalledWith(3, 'c', undefined);
		expect(handlers[0].onNext).toHaveBeenNthCalledWith(4, 'd', undefined);
		expect(handlers[0].onNext).toHaveBeenNthCalledWith(5, 'e', 'E');
		expect(handlers[0].onNext).toHaveBeenNthCalledWith(6, 'f', 'F');
	});

	test("returns iterator return value", async () => {
		const rtn  = Symbol();
		const it   = except(protocol("abcdef", rtn, {}), "cd");
		let   step = await it.next();

		while (!step.done)
			step = await it.next();

		expect(step.value).toBe(rtn);
	});
});