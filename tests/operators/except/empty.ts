import {runTests} from "lib";

export const empty = () => runTests("except", ({except, empty}) => {
	describe("left", () => {
		for (const [kind, value] of empty)
			test(kind, async () => expect(await except(value, 'x').count()).toBe(0));
	});

	describe("right", () => {
		for (const [kind, value] of empty)
			test(kind, async () => expect(await except("abc", value).join()).toBe("abc"));
	});
});