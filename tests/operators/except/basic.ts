import {runTests} from "lib";

export const basic = () => runTests("except", ({except}) => {
	test('a',   "abc", async s => expect(await except(s(), 'a').join()).toBe("bc"));
	test('b',   "abc", async s => expect(await except(s(), 'b').join()).toBe("ac"));
	test('c',   "abc", async s => expect(await except(s(), 'c').join()).toBe("ab"));
	test("ab",  "abc", async s => expect(await except(s(), "ab").join()).toBe('c'));
	test("ac",  "abc", async s => expect(await except(s(), "ac").join()).toBe('b'));
	test("bc",  "abc", async s => expect(await except(s(), "bc").join()).toBe('a'));
	test("abc", "abc", async s => expect(await except(s(), "abc").join()).toBe(''));
	test("d",   "abc", async s => expect(await except(s(), 'd').join()).toBe("abc"));
});