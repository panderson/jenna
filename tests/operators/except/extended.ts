import {runTests} from "lib";

export const extended = () => runTests("except", ({AsyncIterable, except}) =>
	test("is extended", "abc", 'a', (a, b) =>
		expect(except(a(), b())).toBeInstanceOf(AsyncIterable)
	)
);