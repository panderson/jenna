import {iteratesCompletely} from "./iterates-completely";
import {callsReturn}        from "./return";
import {doesNotCallThrow}   from "./throw";

export function protocol() {
	iteratesCompletely();
	callsReturn();
	doesNotCallThrow();
};