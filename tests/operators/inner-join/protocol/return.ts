import {runTests} from "lib";

export const callsReturn = () => runTests("innerJoin", ({innerJoin, protocol, error}) => {
	const handlers = [protocol.createHandlers(), protocol.createHandlers()];

	beforeEach(() => jest.resetAllMocks());

	const left  = protocol("abc", handlers[0]);
	const right = protocol("def", handlers[1]);
	const empty = [protocol('', handlers[0]), protocol('', handlers[1])];

	describe("calls iterator.return exactly once", () => {
		describe("left empty", () => {
			const left = empty[0];

			test("right empty", async () => {
				await innerJoin(left, empty[1], () => 1, () => 1).count();
				expect(handlers[0].onReturn).toHaveBeenCalledTimes(1);
				expect(handlers[1].onReturn).toHaveBeenCalledTimes(1);
			});

			test("right not empty", async () => {
				await innerJoin(left, right, () => 1, () => 1).count();
				expect(handlers[0].onReturn).toHaveBeenCalledTimes(1);
				expect(handlers[1].onReturn).toHaveBeenCalledTimes(1);
			});

			test("right error", async () => {
				await expect(() => innerJoin(left, right, () => 1, error).count())
					.rejects.toThrow(error);
				// Because it builds a lookup from the right sequence first, if it throws, the left
				// sequence never iterates.
				expect(handlers[0].onStart).not.toHaveBeenCalled();
				expect(handlers[1].onReturn).toHaveBeenCalledTimes(1);
			});
		});

		describe("left not empty", () => {
			test("right empty", async () => {
				await innerJoin(left, empty[1], () => 1, () => 1).count();
				expect(handlers[0].onReturn).toHaveBeenCalledTimes(1);
				expect(handlers[1].onReturn).toHaveBeenCalledTimes(1);
			});

			test("right not empty", async () => {
				await innerJoin(left, right, () => 1, () => 1).count();
				expect(handlers[0].onReturn).toHaveBeenCalledTimes(1);
				expect(handlers[1].onReturn).toHaveBeenCalledTimes(1);
			});

			test("right error", async () => {
				await expect(() => innerJoin(left, right, () => 1, error).count())
					.rejects.toThrow(error);
				expect(handlers[0].onStart).not.toHaveBeenCalled();
				expect(handlers[1].onReturn).toHaveBeenCalledTimes(1);
			});
		});

		describe("left error", () => {
			test("right empty", async () => {
				await expect(() => innerJoin(left, empty[1], error, () => 1).count())
					.rejects.toThrow(error);
				expect(handlers[0].onReturn).toHaveBeenCalledTimes(1);
				expect(handlers[1].onReturn).toHaveBeenCalledTimes(1);
			});

			test("right not empty", async () => {
				await expect(() => innerJoin(left, right, error, () => 1).count())
					.rejects.toThrow(error);
				expect(handlers[0].onReturn).toHaveBeenCalledTimes(1);
				expect(handlers[1].onReturn).toHaveBeenCalledTimes(1);
			});

			test("right error", async () => {
				await expect(() => innerJoin(left, right, error, error).count())
					.rejects.toThrow(error);
				expect(handlers[0].onStart).not.toHaveBeenCalled();
				expect(handlers[1].onReturn).toHaveBeenCalledTimes(1);
			});
		});
	});
});