import {runTests} from "lib";

export const extended = () => runTests("innerJoin", ({AsyncIterable, innerJoin}) =>
	test("is extended", () =>
		expect(innerJoin("abc", [1, 2, 3], c => c.charCodeAt(0) - 95, i => i))
			.toBeInstanceOf(AsyncIterable)
	)
);