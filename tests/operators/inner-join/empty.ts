import {runTests} from "lib";
import {numbers}  from "./data";

export const empty = () => runTests("innerJoin", {numbers}, ({innerJoin, empty, numbers}) => {
	test("right", numbers, async input => {
		for (const [, value] of empty)
			expect(await innerJoin(input(), value, word => word.length, () => 3).count()).toBe(0);
	});

	test("left", numbers, async input => {
		for (const [, value] of empty)
			expect(await innerJoin(value, input(), () => 3, word => word.length).count()).toBe(0);
	});
});