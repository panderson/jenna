import {runTests} from "lib";
import * as data  from "./data";

export const basic = () => runTests("innerJoin", {data}, ({innerJoin, data}) => {
	const {numbers, indexed} = data;

	test("with result selector", numbers, indexed, async (a, b) => {
		const result = await innerJoin(
			a(),
			b(),
			word => word.length,
			({index}) => index,
			(word, {index}) => `"${word}" is ${numbers[index]} letters long`,
		).toArray();

		const expected = [
			'"zero" is four letters long',
			'"one" is three letters long',
			'"two" is three letters long',
			'"three" is five letters long',
			'"four" is four letters long',
			'"five" is four letters long',
			'"six" is three letters long',
			'"seven" is five letters long',
			'"eight" is five letters long',
			'"nine" is four letters long',
			'"ten" is three letters long',
		];

		expect(result).toStrictEqual(expected);
	});

	test("default result selector", numbers, indexed, async (a, b) => {
		const result = await innerJoin(
			a(),
			b(),
			word => word.length,
			({index}) => index,
		).toArray();

		const expected = [
			["zero",  {word: "four",  index: 4}, 4],
			["one",   {word: "three", index: 3}, 3],
			["two",   {word: "three", index: 3}, 3],
			["three", {word: "five",  index: 5}, 5],
			["four",  {word: "four",  index: 4}, 4],
			["five",  {word: "four",  index: 4}, 4],
			["six",   {word: "three", index: 3}, 3],
			["seven", {word: "five",  index: 5}, 5],
			["eight", {word: "five",  index: 5}, 5],
			["nine",  {word: "four",  index: 4}, 4],
			["ten",   {word: "three", index: 3}, 3],
		];

		expect(result).toStrictEqual(expected);
	});
});