import {runTests} from "lib";
import * as data  from "./data";

export const matchCounts = () => runTests("innerJoin", {data}, ({innerJoin, data}) => {
	const {indexed, alphabet} = data;

	test("omits items with no match", indexed, alphabet, async (a, b) => {
		const result = await innerJoin(
			a(),
			b(),
			({index}) => index,
			word => word.length,
			({index}) => index,
		).toArray();

		expect(result).not.toContain(0);
		expect(result).not.toContain(1);
		expect(result).not.toContain(8);
		expect(result).not.toContain(9);
		expect(result).not.toContain(10);

		// But make sure it worked at all.
		expect(result).toContain(2);
		expect(result).toContain(3);
		expect(result).toContain(4);
		expect(result).toContain(5);
		expect(result).toContain(6);
		expect(result).toContain(7);
	});

	test("returns multiple matches", indexed, alphabet, async (a, b) => {
		const result = await innerJoin(
			a(),
			b(),
			({index}) => index,
			word => word.length,
		).toArray();

		const two   = result.filter(([{word}]) => word === "two")  .map(([, letter]) => letter);
		const three = result.filter(([{word}]) => word === "three").map(([, letter]) => letter);
		const four  = result.filter(([{word}]) => word === "four") .map(([, letter]) => letter);
		const five  = result.filter(([{word}]) => word === "five") .map(([, letter]) => letter);
		const six   = result.filter(([{word}]) => word === "six")  .map(([, letter]) => letter);
		const seven = result.filter(([{word}]) => word === "seven").map(([, letter]) => letter);

		expect(two)  .toEqual("mu nu xi pi".split(' '));
		expect(three).toEqual("eta rho tau phi chi psi".split(' '));
		expect(four) .toEqual("beta zeta iota".split(' '));
		expect(five) .toEqual("alpha gamma delta theta kappa sigma omega".split(' '));
		expect(six)  .toEqual(["lambda"]);
		expect(seven).toEqual("epsilon omicron upsilon".split(' '));
	});
});