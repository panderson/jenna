export const numbers  = "zero one two three four five six seven eight nine ten".split(' ');
export const indexed  = numbers.map((word, index) => ({word, index}));
export const alphabet = [
	"alpha", "beta", "gamma", "delta", "epsilon", "zeta", "eta", "theta", "iota", "kappa", "lambda",
	"mu", "nu", "xi", "omicron", "pi", "rho", "sigma", "tau", "upsilon", "phi", "chi", "psi", "omega"
];