import {runTests} from "lib";

export const basic = () => runTests("groupBy", ({groupBy}) => {
	const words = "now is the time for all good men to come to the aid of their country".split(' ');
	const expected = Object.entries({
		n: ["now"],
		i: ["is"],
		t: ["the", "time", "to", "to", "the", "their"],
		f: ["for"],
		a: ["all", "aid"],
		g: ["good"],
		m: ["men"],
		c: ["come", "country"],
		o: ["of"],
	});

	test("returns a map", words, async input => {
		const result = await groupBy(input(), word => word[0]);
		expect(result).toBeInstanceOf(Map);
	});

	test("groups words correctly", words, async input => {
		const result = await groupBy(input(), word => word[0]);
		expect(result.size).toBe(expected.length);
		expect(Array.from(result.keys()).join('')).toBe("nitfagmco");
		for (const [key, value] of expected)
			expect(result.get(key)).toStrictEqual(value);
	});

	test("value selector works", words, async input => {
		const result = await groupBy(
			input(),
			word => word[0],
			word => word.toUpperCase()
		);
		for (const [key, value] of expected)
			expect(result.get(key)).toStrictEqual(value.map(word => word.toUpperCase()));
	});

	test("passes index to key selector", words, async input => {
		const result = await groupBy(input(), (_, index) => index);
		expect(result.size).toBe(words.length);
		for (let i = 0; i < words.length; i++)
			expect(result.get(i)).toStrictEqual([words[i]]);
	});

	test("passes index to value selector", words, async input => {
		const result = await groupBy(input(), word => word[0], (_, index) => index);
		const expected = Object.entries({
			n: [0],
			i: [1],
			t: [2, 3, 8, 10, 11, 14],
			f: [4],
			a: [5, 12],
			g: [6],
			m: [7],
			c: [9, 15],
			o: [13],
		});
		expect(result.size).toBe(expected.length);
		for (const [key, value] of expected)
			expect(result.get(key)).toStrictEqual(value);
	});

	test("passes key to value selector", words, async input => {
		const result = await groupBy(input(), word => word[0], (_, __, key) => key);
		expect(result.size).toBe(expected.length)
		for (const [key, value] of expected)
			expect(result.get(key)).toStrictEqual(value.map(() => key));
	});
});