import {runTests} from "lib";

export const protocol = () => runTests("groupBy", ({groupBy, protocol, error}) => {
	const handlers = protocol.createHandlers();

	beforeEach(() => jest.resetAllMocks());

	const iterable  = protocol("abc", handlers);
	const empty     = protocol('', handlers);

	describe("iterates completely", () => {
		test("empty", async () => {
			await groupBy(empty, () => {});
			expect(handlers.onDone).toHaveBeenCalled();
		});
		test("not empty", async () => {
			await groupBy(iterable, () => {});
			expect(handlers.onDone).toHaveBeenCalled();
		});
	});
	describe("runs iterator.return exactly once", () => {
		test("empty", async () => {
			await groupBy(empty, () => {});
			expect(handlers.onReturn).toHaveBeenCalledTimes(1);
		});
		test("not empty", async () => {
			await groupBy(iterable, () => {});
			expect(handlers.onReturn).toHaveBeenCalledTimes(1);
		});
		describe("error", () => {
			test("key selector", async () => {
				await expect(() => groupBy(iterable, error)).rejects.toThrow(error);
				expect(handlers.onReturn).toHaveBeenCalledTimes(1);
			});
			test("value selector", async () => {
				await expect(() => groupBy(iterable, error)).rejects.toThrow(error);
				expect(handlers.onReturn).toHaveBeenCalledTimes(1);
			});
		});
	});

	describe("doesn't call iterator.throw", () => {
		test("key selector", async () => {
			await expect(() => groupBy(iterable, error)).rejects.toThrow(error);
			expect(handlers.onThrow).not.toHaveBeenCalled();
		});
		test("value selector", async () => {
			await expect(() => groupBy(iterable, error)).rejects.toThrow(error);
			expect(handlers.onThrow).not.toHaveBeenCalled();
		});
	});
});