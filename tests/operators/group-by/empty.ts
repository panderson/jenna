import {runTests} from "lib";

export const empty = () => runTests("groupBy", ({groupBy, empty}) => {
	for (const [kind, value] of empty) {
		test(kind, async () => {
			const result = await groupBy(value, () => {throw "shouldn't get here"});
			expect(result).toBeInstanceOf(Map);
			expect(result.size).toBe(0);
		});
	}
});