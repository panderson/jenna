import {runTests} from "lib";

export const empty = () => runTests("max", ({max, empty}) => {
	for (const [kind, value] of empty)
		test(kind, async () => expect(await max(value)).toBe(undefined));
});