import {runTests} from "lib";

export const basic = () => runTests("max", ({max}) => {
	const numbers = [-10, -9, -8, -7, -6, -5, -4, -3, -2, -1, 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10];
	const dates   = [
		new Date("1980-01-01T00:00:00"),
		new Date("2020-02-02T02:02:02"),
		new Date("1999-12-31T23:59:59.999"),
	];

	const randomize = <T>(array: T[]): T[] => array.sort(() => Math.random() - 0.5);

	test("works in any order", numbers, async iterate => {
		for (let i = 0; i < 10; i++)
			expect(await max(iterate(randomize([...numbers])))).toBe(10);
	});

	describe.verbose("compares strings lexically", test => {
		test("numbers",    numbers.map(String),      async input => expect(await max(input())).toBe('9'));
		test("lower case", ["abc", "abcde", "abcd"], async input => expect(await max(input())).toBe("abcde"));
		test("mixed case", ["abc", "ABC", "DEF"],    async input => expect(await max(input())).toBe("abc"));
	});

	test("works with dates", dates, async input => {
		const result = await max(input());
		expect(result).toBeInstanceOf(Date);
		expect(result).toEqual(new Date("2020-02-02T02:02:02"));
	});
});