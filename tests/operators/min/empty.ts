import {runTests} from "lib";

export const empty = () => runTests("min", ({min, empty}) => {
	for (const [kind, value] of empty)
		test(kind, async () => expect(await min(value)).toBe(undefined));
});