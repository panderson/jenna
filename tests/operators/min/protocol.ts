import {runTests} from "lib";

export const protocol = () => runTests("min", ({min, protocol}) => {
	const handlers = protocol.createHandlers();

	const iterable = protocol("abc", handlers);

	beforeEach(() => jest.resetAllMocks());

	test("iterates completely", async () => {
		await min(iterable);
		expect(handlers.onDone).toHaveBeenCalled();
	});

	test("calls iterator.return exactly once", async () => {
		await min(iterable);
		expect(handlers.onReturn).toHaveBeenCalledTimes(1);
	});
});