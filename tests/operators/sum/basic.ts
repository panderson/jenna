import {runTests} from "lib";

export const basic = () => runTests("sum", ({sum}) => {
	const numbers = [-10, -9, -8, -7, -6, -5, -4, -3, -2, -1, 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10];

	const randomize = <T>(array: T[]): T[] => array.sort(() => Math.random() - 0.5);

	describe.verbose("works in any order", test => {
		test("positive", numbers, async iterate => {
			for (let i = 0; i < 10; i++)
				expect(await sum(iterate(randomize(numbers.slice(10))))).toBe(55);
		});

		test("negative", numbers, async iterate => {
			for (let i = 0; i < 10; i++)
				expect(await sum(iterate(randomize(numbers.slice(0, 11))))).toBe(-55);
		});

		test("all", numbers, async iterate => {
			for (let i = 0; i < 10; i++)
				expect(await sum(iterate(randomize([...numbers])))).toBe(0);
		});
	});
});