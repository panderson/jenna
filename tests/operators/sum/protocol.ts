import {runTests} from "lib";

export const protocol = () => runTests("sum", ({sum, protocol}) => {
	const handlers = protocol.createHandlers();

	const iterable = protocol("abc", handlers);

	beforeEach(() => jest.resetAllMocks());

	test("iterates completely", async () => {
		await sum(iterable);
		expect(handlers.onDone).toHaveBeenCalled();
	});

	test("calls iterator.return exactly once", async () => {
		await sum(iterable);
		expect(handlers.onReturn).toHaveBeenCalledTimes(1);
	});
});