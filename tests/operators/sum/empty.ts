import {runTests} from "lib";

export const empty = () => runTests("sum", ({sum, empty}) => {
	for (const [kind, value] of empty)
		test(kind, async () => expect(await sum(value)).toBe(undefined));
});