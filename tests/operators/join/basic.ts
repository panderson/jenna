import {runTests} from "lib";

export const basic = () => runTests("join", ({join}) => {
	describe("strings", () => {
		const source = ["abc", "def", "ghi", "jkl"];

		test("no joiner", source, async input => expect(await join(input())).toBe("abcdefghijkl"));

		test("dashed", source, async input =>
			expect(await join(input(), '-')).toBe("abc-def-ghi-jkl")
		);

		test("multiple characters", source, async input =>
			expect(await join(input(), " / ")).toBe("abc / def / ghi / jkl")
		);

		test("null", source, async input =>
			expect(await join(input(), null as any)).toBe("abcnulldefnullghinulljkl")
		);

		test("undefined", source, async input =>
			expect(await join(input(), undefined)).toBe("abcdefghijkl")
		);
	});

	describe("numbers", () => {
		const source = [1, 2, 3, 4, 5];

		test("no joiner", source, async input => expect(await join(input())).toBe("12345"));
		test("dashed", source, async input => expect(await join(input(), '-')).toBe("1-2-3-4-5"));

		test("multiple characters", source, async input =>
			expect(await join(input(), " / ")).toBe("1 / 2 / 3 / 4 / 5")
		);

		test("null", source, async input =>
			expect(await join(input(), null as any)).toBe("1null2null3null4null5")
		);

		test("undefined", source, async input =>
			expect(await join(input(), undefined)).toBe("12345")
		);
	});
});