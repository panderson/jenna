import {runTests} from "lib";

export const protocol = () => runTests("join", ({join, protocol}) => {
	const handlers = protocol.createHandlers();

	beforeEach(() => jest.resetAllMocks());

	const iterable = protocol(["abc", "def", "ghi", "jkl"], handlers);

	test("iterates completely", async () => {
		await join(iterable);
		expect(handlers.onDone).toHaveBeenCalled();
	});

	test("calls iterator.return exactly once", async () => {
		await join(iterable);
		expect(handlers.onReturn).toHaveBeenCalledTimes(1);
	});
});