import {runTests} from "lib";

export const empty = () => runTests("join", ({join, empty}) => {
	for (const [kind, value] of empty)
		test(kind, async () => expect(await join(value)).toBe(''));
});