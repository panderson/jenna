import {basic}    from "./basic";
import {empty}    from "./empty";
import {protocol} from "./protocol";

describe("basic usage", basic);
describe("empty",       empty);
describe("protocol",    protocol);