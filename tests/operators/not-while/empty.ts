import {runTests} from "lib";

export const empty = () => runTests("notWhile", ({notWhile, empty}) => {
	for (const [kind, value] of empty) {
		test(kind, async () => {
			expect(await notWhile(value, () => true).count()).toBe(0);
			expect(await notWhile(value, () => false).count()).toBe(0);
		});
	}
});