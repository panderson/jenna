import {runTests} from "lib";

export const extended = () => runTests("notWhile", ({AsyncIterable, notWhile, protocol}) =>
	test("is extended", () => {
		expect(notWhile("abc", () => true)).toBeInstanceOf(AsyncIterable);
		expect(notWhile("abc", () => false)).toBeInstanceOf(AsyncIterable);
		expect(notWhile(["abc"], () => true)).toBeInstanceOf(AsyncIterable);
		expect(notWhile(["abc"], () => false)).toBeInstanceOf(AsyncIterable);
		expect(notWhile(new Set("abc"), () => true)).toBeInstanceOf(AsyncIterable);
		expect(notWhile(new Set("abc"), () => false)).toBeInstanceOf(AsyncIterable);
		expect(notWhile(protocol("abc"), () => true)).toBeInstanceOf(AsyncIterable);
		expect(notWhile(protocol("abc"), () => false)).toBeInstanceOf(AsyncIterable);
	})
);