import {runTests} from "lib";

export const basic = () => runTests("notWhile", ({notWhile}) => {
	const source = "abcdef";

	test("basic usage", source, async input =>
		expect(await notWhile(input(), c => c !== 'd').join()).toBe("def")
	);

	test("true",  source, async input =>
		expect(await notWhile(input(), () => true).count()).toBe(0)
	);
	test("false", source, async input =>
		expect(await notWhile(input(), () => false).join()).toBe(source)
	);

	test("stops calling predicate after test returns false", source, async input => {
		const predicate = jest.fn((c: string) => c !== 'd');
		await notWhile(input(), predicate).count();
		expect(predicate).toHaveBeenCalledTimes(4);
		expect(predicate).toHaveBeenNthCalledWith(1, 'a');
		expect(predicate).toHaveBeenNthCalledWith(2, 'b');
		expect(predicate).toHaveBeenNthCalledWith(3, 'c');
		expect(predicate).toHaveBeenNthCalledWith(4, 'd');
	});
});