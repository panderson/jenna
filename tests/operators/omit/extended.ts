import {runTests} from "lib";

export const extended = () => runTests("omit", ({AsyncIterable, omit}) =>
	test("is extended", "abc", 'c', async (a, b) =>
		expect(omit(a(), c => c, b())).toBeInstanceOf(AsyncIterable)
	)
);