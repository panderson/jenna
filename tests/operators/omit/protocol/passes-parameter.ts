import {runTests} from "lib";
import * as data  from "../data";

export const passesParameter = () => runTests("omit", {data}, ({omit, protocol, data}) => {
	const {numbers, evens} = data;

	const handlers = protocol.createHandlers();

	beforeEach(() => jest.resetAllMocks());

	test("passes parameter to next", async () => {
		const it   = omit(protocol(numbers, handlers), ({id}) => id, evens);
		let   step = await it.next("ignored");

		while (!step.done)
			step = await it.next(step.value.name.toUpperCase());

		expect(handlers.onNext).toHaveBeenCalledTimes(numbers.length);

		for (let i = 0; i < numbers.length; i++) {
			expect(handlers.onNext).toHaveBeenNthCalledWith(
				i + 1,
				numbers[i],
				numbers[i].id & 1 ? numbers[i].name.toUpperCase() : undefined,
			);
		}
	});
});