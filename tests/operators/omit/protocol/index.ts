import {iteratesCompletely} from "./iterates-completely";
import {passesParameter}    from "./passes-parameter";
import {callsReturn}        from "./return";
import {returnsValue}       from "./returns-value";
import {doesNotCallThrow}   from "./throw";

export function protocol() {
	iteratesCompletely();
	callsReturn();
	doesNotCallThrow();
	passesParameter();
	returnsValue();
}