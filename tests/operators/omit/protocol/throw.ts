import {runTests} from "lib";

export const doesNotCallThrow = () => runTests("omit", ({omit, protocol, error}) => {
	const handlers = [protocol.createHandlers(), protocol.createHandlers()];

	beforeEach(() => jest.resetAllMocks());

	const left  = protocol("abc", handlers[0]);
	const right = protocol("abc", handlers[1]);

	test("doesn't call iterator.throw", async () => {
		await expect(() => omit(left, error, right).count()).rejects.toThrow(error);
		expect(handlers[0].onThrow).not.toHaveBeenCalled();
		expect(handlers[1].onThrow).not.toHaveBeenCalled();
	});
});