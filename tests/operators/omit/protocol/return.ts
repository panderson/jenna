import {runTests} from "lib";

export const callsReturn = () => runTests("omit", ({omit, protocol, error}) => {
	const handlers = [protocol.createHandlers(), protocol.createHandlers()];

	beforeEach(() => jest.resetAllMocks());

	const left  = protocol("abc", handlers[0]);
	const right = protocol("abc", handlers[1]);
	const empty = [protocol('', handlers[0]), protocol('', handlers[1])];

	describe("calls iterator.return exactly once", () => {
		describe("left empty", () => {
			const left = empty[0];

			test("right empty", async () => {
				await omit(left, x => x, empty[1]).count();
				expect(handlers[0].onReturn).toHaveBeenCalledTimes(1);
				expect(handlers[1].onReturn).toHaveBeenCalledTimes(1);
			});

			test("right not empty", async () => {
				await omit(left, x => x, right).count();
				expect(handlers[0].onReturn).toHaveBeenCalledTimes(1);
				expect(handlers[1].onReturn).toHaveBeenCalledTimes(1);
			});
		});

		describe("left not empty", () => {
			test("right empty", async () => {
				await omit(left, x => x, empty[1]).count();
				expect(handlers[0].onReturn).toHaveBeenCalledTimes(1);
				expect(handlers[1].onReturn).toHaveBeenCalledTimes(1);
			});

			test("right not empty", async () => {
				await omit(left, x => x, right).count();
				expect(handlers[0].onReturn).toHaveBeenCalledTimes(1);
				expect(handlers[1].onReturn).toHaveBeenCalledTimes(1);
			});
		});

		describe("left error", () => {
			test("right empty", async () => {
				await expect(() => omit(left, error, empty[1]).count()).rejects.toThrow(error);
				expect(handlers[0].onReturn).toHaveBeenCalledTimes(1);
				expect(handlers[1].onReturn).toHaveBeenCalledTimes(1);
			});

			test("right not empty", async () => {
				await expect(() => omit(left, error, right).count()).rejects.toThrow(error);
				expect(handlers[0].onReturn).toHaveBeenCalledTimes(1);
				expect(handlers[1].onReturn).toHaveBeenCalledTimes(1);
			});
		});
	});
});