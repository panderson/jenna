import {runTests} from "lib";
import * as data  from "../data";

export const returnsValue = () => runTests("omit", {data}, ({omit, protocol, data}) => {
	const {numbers, evens} = data;

	test("returns iterator return value", async () => {
		const rtn  = Symbol();
		const it   = omit(protocol(numbers, rtn, {}), ({id}) => id, evens);
		let   step = await it.next();

		while (!step.done)
			step = await it.next();

		expect(step.value).toBe(rtn);
	});
});