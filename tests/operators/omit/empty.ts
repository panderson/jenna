import {runTests} from "lib";

export const empty = () => runTests("omit", ({omit, empty}) => {
	describe("left", () => {
		for (const [kind, value] of empty)
			test(kind, async () => expect(await omit(value, x => x, 'x').count()).toBe(0));
	});

	describe("right", () => {
		for (const [kind, value] of empty)
			test(kind, async () => expect(await omit("abc", c => c, value).join()).toBe("abc"));
	});
});