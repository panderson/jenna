import {runTests} from "lib";
import * as data  from "./data";

export const basic = () => runTests("omit", {data}, ({omit, data}) => {
	const {numbers, odds, evens} = data;

	test("basic usage", numbers, evens, async (a, b) => {
		const result = await omit(a(), ({id}) => id, b()).toArray();
		expect(result.length).toBe(5);

		for (const index of evens)
			expect(result).not.toContain(numbers[index]);

		for (const index of odds)
			expect(result).toContain(numbers[index]);

		expect(result.map(({id}) => id)).toStrictEqual(odds);
	});

	test("right order doesn't matter", numbers, [...evens].reverse(), async (a, b) =>
		expect(
			await omit(a(), ({id}) => id, b()).toArray()
		).toStrictEqual([numbers[1], numbers[3], numbers[5], numbers[7], numbers[9]])
	);

	test("right can have duplicates", numbers, [...evens, ...evens], async (a, b) =>
		expect(
			await omit(a(), ({id}) => id, b()).toArray()
		).toStrictEqual([numbers[1], numbers[3], numbers[5], numbers[7], numbers[9]])
	);

	test("duplicates in left cause duplicates in result", [...numbers, ...numbers], evens,
		async (a, b) => expect(await omit(a(), ({id}) => id, b()).toArray()).toStrictEqual([
			numbers[1], numbers[3], numbers[5], numbers[7], numbers[9],
			numbers[1], numbers[3], numbers[5], numbers[7], numbers[9],
		])
	);
});