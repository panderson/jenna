import {runTests} from "lib";

export const basic = () => runTests("intersect", ({intersect}) => {
	const left     = "abcghilmnopxyz";
	const right    = "cdefgjklmnpqrsyz";
	const expected = "cglmnpyz";

	test("basic usage", left, right, async (a, b) =>
		expect(await intersect(a(), b()).join('')).toBe(expected)
	);

	test("right order doesn't matter", left, Array.from(right).reverse(), async (a, b) =>
		expect(await intersect(a(), b()).join('')).toBe(expected)
	);

	test("right can have duplicates", left, right + right, async (a, b) =>
		expect(await intersect(a(), b()).join('')).toBe(expected)
	);

	test("duplicates in left cause duplicates in result", left + left, right, async (a, b) =>
		expect(await intersect(a(), b()).join('')).toBe(expected + expected)
	);
});