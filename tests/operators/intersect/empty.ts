import {runTests} from "lib";

export const empty = () => runTests("intersect", ({intersect, empty}) => {
	describe("left", () => {
		for (const [kind, value] of empty)
			test(kind, async () => expect(await intersect(value, "abc").count()).toBe(0));
	});

	describe("right", () => {
		for (const [kind, value] of empty)
			test(kind, async () => expect(await intersect("abc", value).count()).toBe(0));
	});
});