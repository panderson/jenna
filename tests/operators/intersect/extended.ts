import {runTests} from "lib";

export const extended = () => runTests("intersect", ({AsyncIterable, intersect}) =>
	test("is extended", "abc", 'a', (a, b) =>
		expect(intersect(a(), b())).toBeInstanceOf(AsyncIterable)
	)
);