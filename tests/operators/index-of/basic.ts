import {runTests} from "lib";

export const basic = () => runTests("indexOf", ({indexOf}) => {
	const source = "abcdef";

	test("without minIndex", source, async input => {
		for (let i = 0; i < source.length; i++)
			expect(await indexOf(input(), source[i])).toBe(i);

		expect(await indexOf(input(), 'z')).toBe(-1);
	});

	test("with minIndex", source, async input => {
		expect(await indexOf(input(), 'a', -1)).toBe(0);
		expect(await indexOf(input(), 'a', 0)).toBe(0);

		for (let i = 1; i <= source.length; i++) {
			expect(await indexOf(input(), 'a', i)).toBe(-1);
			expect(await indexOf(input(), 'b', i)).toBe(i < 2 ? 1 : -1);
			expect(await indexOf(input(), 'c', i)).toBe(i < 3 ? 2 : -1);
			expect(await indexOf(input(), 'd', i)).toBe(i < 4 ? 3 : -1);
			expect(await indexOf(input(), 'e', i)).toBe(i < 5 ? 4 : -1);
			expect(await indexOf(input(), 'f', i)).toBe(i < 6 ? 5 : -1);
		}

		for (const letter of source)
			expect(await indexOf(input(), letter, source.length * 2)).toBe(-1);
	});

	describe("finds first index", () => {
		test("without minIndex", source + source, async input => {
			for (let i = 0; i < source.length; i++)
				expect(await indexOf(input(), source[i])).toBe(i);
		});

		test("with minIndex", source + source + source, async input => {
			for (let i = 0; i < source.length; i++)
				expect(await indexOf(input(), source[i], i + 1)).toBe(source.length + i);
		});
	});
});