import {runTests} from "lib";

export const protocol = () => runTests("indexOf", ({indexOf, protocol}) => {
	const handlers = protocol.createHandlers();

	beforeEach(() => jest.resetAllMocks());

	const generator = () => protocol("abc", handlers);

	describe("calls iterator.return exactly once", () => {
		test("found", async () => {
			await indexOf(generator(), 'a');
			expect(handlers.onReturn).toHaveBeenCalledTimes(1);
		});

		test("not found", async () => {
			await indexOf(generator(), 'z');
			expect(handlers.onReturn).toHaveBeenCalledTimes(1);
		});
	});
	test("doesn't iterate completely", async () => {
		await indexOf(generator(), 'a');
		expect(handlers.onDone).not.toHaveBeenCalled();
	});

	test("iterates completely", async () => {
		await indexOf(generator(), 'z');
		expect(handlers.onDone).toHaveBeenCalled();
	});
});