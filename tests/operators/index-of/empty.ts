import {runTests} from "lib";

export const empty = () => runTests("indexOf", ({indexOf, empty}) => {
	for (const [kind, value] of empty)
		test(kind, async () => expect(await indexOf(value, undefined)).toBe(-1));
});