import {runTests} from "lib";

export const overload07 = () => runTests("reduce", ({reduce}) => {
	// params: Reducer<I, R> & InitialValue<R> & Finalizer<R, V>

	const numbers = [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10];
	const string  = "abcdef";

	test("add numbers", numbers, async input =>
		expect(await reduce(
			input(),
			{
				reduce       : (a, b) => a + b,
				initialValue : -10,
				finalize     : (x: number) => x * 10,
			}
		)).toBe(450)
	);

	test("add strings", string, async input =>
		expect(await reduce(
			input(),
			{
				reduce       : (a, b) => a + b,
				initialValue : ">>",
				finalize     : (s: string) => `[${s}]`
			}
		)).toBe(`[>>${string}]`)
	);

	test("build array", string, async input => {
		const seed: string[] = [];
		expect(await reduce(
			input(),
			{
				reduce(a: string[], b) {
					a.push(b);
					return a;
				},
				initialValue: seed,
				finalize(result: string[]) {
					result.push('z');
					return result;
				},
			}
		)).toBe(seed);
		expect(seed).toStrictEqual(Array.from(string + 'z'));
	});

	test("build new array", string, async input => {
		const seed: string[] = [];
		const result = await reduce(
			input(),
			{
				reduce       : (a: string[], b) => [...a, b],
				initialValue : seed,
				finalize     : (x: string[]) => [...x, 'z'],
			}
		);
		expect(result).not.toBe(seed);
		expect(seed.length).toBe(0);
		expect(result).toStrictEqual(Array.from(string + 'z'));
	});

	test("called length times", numbers, async input => {
		const callback = jest.fn((a: number, b: number) => a + b);
		await reduce(
			input(),
			{
				reduce       : callback,
				initialValue : -10,
				finalize     : (x: number) => x * 10,
			}
		);
		expect(callback).toHaveBeenCalledTimes(numbers.length);
	});

	test("finalizer called exactly once with result", string, async input => {
		const finalize = jest.fn((result: string) => `<${result}>`);
		await reduce(
			input(),
			{
				reduce       : (a, b) => a + b,
				initialValue : ">>",
				finalize,
			}
		);
		expect(finalize).toHaveBeenCalledTimes(1);
		expect(finalize).toHaveBeenCalledWith(`>>${string}`);
	});

	test("parameters", string, async input => {
		let i = 0;
		await reduce(
			input(),
			{
				reduce(prev: string, current: string, index: number) {
					expect(index).toBe(i++);
					expect(current).toBe(string[index]);
					expect(prev).toBe(`>>${string.slice(0, index)}`);
					return prev + current;
				},
				initialValue : ">>",
				finalize     : (x: string) => `<${x}>`
			},
		);
	});
});