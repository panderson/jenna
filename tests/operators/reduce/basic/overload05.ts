import {runTests} from "lib";

export const overload05 = () => runTests("reduce", ({reduce}) => {
	// params: Reducer<I, R> & Initializer<I, R>

	const numbers = [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10];
	const string  = "abcdef";

	test("add numbers", numbers, async input =>
		expect(await reduce(
			input(),
			{reduce: (a, b) => a + b, initialize: (x: number) => x - 10}
		)).toBe(45)
	);

	test("add strings", string, async input =>
		expect(await reduce(
			input(),
			{reduce: (a, b) => a + b, initialize: (s: string) => `<${s}>`}
		)).toBe(`<${string[0]}>${string.slice(1)}`)
	);

	test("build array", string, async input => {
		const seed: string[] = [];
		expect(await reduce(
			input(),
			{
				reduce(a: string[], b) {
					a.push(b);
					return a;
				},
				initialize(firstItem: string) {
					seed.push(firstItem);
					return seed;
				},
			}
		)).toBe(seed);
		expect(seed).toStrictEqual(Array.from(string));
	});

	test("build new array", string, async input => {
		const result = await reduce(
			input(),
			{reduce: (a: string[], b) => [...a, b], initialize: (a: string) => [a]}
		);
		expect(result).toStrictEqual(Array.from(string));
	});

	test("called length - 1 times", numbers, async input => {
		const callback = jest.fn((a: number, b: number) => a + b);
		await reduce(input(), {reduce: callback, initialize: (a: number) => a - 10});
		expect(callback).toHaveBeenCalledTimes(numbers.length - 1);
	});

	test("initializer called exactly once with first item", string, async input => {
		const initialize = jest.fn((firstItem: string) => `<${firstItem}>`);
		await reduce(input(), {reduce: (a, b) => a + b, initialize});
		expect(initialize).toHaveBeenCalledTimes(1);
		expect(initialize).toHaveBeenCalledWith('a');
	});

	test("parameters", string, async input => {
		let i = 1;
		await reduce(
			input(),
			{
				reduce(prev: string, current: string, index: number) {
					expect(index).toBe(i++);
					expect(current).toBe(string[index]);
					expect(prev).toBe(`<${string[0]}>${string.slice(1, index)}`);
					return prev + current;
				},
				initialize: (a: string) => `<${a}>`,
			},
		);
	});
});