import {runTests} from "lib";

export const overload03 = () => runTests('reduce', ({reduce}) => {
	// params: Reducer<I, I>

	const numbers = [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10];
	const string  = "abcdef";

	test("add numbers", numbers, async input =>
		expect(await reduce(input(), {reduce: (a, b) => a + b})).toBe(55)
	);

	test("add strings", string, async input =>
		expect(await reduce(input(), {reduce: (a, b) => a + b})).toBe(string)
	);

	test("called length - 1 times", numbers, async input => {
		const callback = jest.fn((a: number, b: number) => a + b);
		await reduce(input(), {reduce: callback});
		expect(callback).toHaveBeenCalledTimes(numbers.length - 1);
	});

	test("parameters", string, async input => {
		let i = 1;
		await reduce(input(), {reduce(prev, current, index) {
			expect(index).toBe(i++);
			expect(current).toBe(string[index]);
			expect(prev).toBe(string.slice(0, index));
			return prev + current;
		}});
	});
});