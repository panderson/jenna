import {runTests} from "lib";

export const overload06 = () => runTests("reduce", ({reduce}) => {
	// params: Reducer<I, I> & Finalizer<I | undefined, R>

	const numbers = [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10];
	const string  = "abcdef";

	test("add numbers", numbers, async input =>
		expect(await reduce(
			input(),
			{reduce: (a, b) => a + b, finalize: (x: number | undefined) => x ? x * 10 : NaN}
		)).toBe(550)
	);

	test("add strings", string, async input =>
		expect(await reduce(
			input(),
			{reduce: (a, b) => a + b, finalize: (s: string | undefined) => `<${s}>`}
		)).toBe(`<${string}>`)
	);

	test("called length - 1 times", numbers, async input => {
		const callback = jest.fn((a: number, b: number) => a + b);
		await reduce(
			input(),
			{reduce: callback, finalize: (x: number | undefined) => x ? x * 10 : NaN}
		);
		expect(callback).toHaveBeenCalledTimes(numbers.length - 1);
	});

	test("finalizer called exactly once with result", string, async input => {
		const finalize = jest.fn((result: string | undefined) => `<${result}>`);
		await reduce(input(), {reduce: (a, b) => a + b, finalize});
		expect(finalize).toHaveBeenCalledTimes(1);
		expect(finalize).toHaveBeenCalledWith(string);
	});

	test("parameters", string, async input => {
		let i = 1;
		await reduce(
			input(),
			{
				reduce(prev: string, current: string, index: number) {
					expect(index).toBe(i++);
					expect(current).toBe(string[index]);
					expect(prev).toBe(string.slice(0, index));
					return prev + current;
				},
				finalize: (a: string | undefined) => `<${a}>`,
			},
		);
	});
});