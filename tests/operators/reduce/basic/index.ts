import {overload01} from "./overload01";
import {overload02} from "./overload02";
import {overload03} from "./overload03";
import {overload04} from "./overload04";
import {overload05} from "./overload05";
import {overload06} from "./overload06";
import {overload07} from "./overload07";
import {overload08} from "./overload08";
import {overload09} from "./overload09";
import {overload10} from "./overload10";

export function basic() {
	describe("overload 01", overload01);
	describe("overload 02", overload02);
	describe("overload 03", overload03);
	describe("overload 04", overload04);
	describe("overload 05", overload05);
	describe("overload 06", overload06);
	describe("overload 07", overload07);
	describe("overload 08", overload08);
	describe("overload 09", overload09);
	describe("overload 10", overload10);
}