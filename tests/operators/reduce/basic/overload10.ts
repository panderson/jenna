import {runTests} from "lib";

export const overload10 = () => runTests("reduce", ({reduce}) => {
	// initialValue : R,
	// reduce       : Reduce<I, R>,
	// finalize     : Finalize<R, V>,

	const numbers = [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10];
	const string  = "abcdef";

	test("add numbers", numbers, async input =>
		expect(await reduce(input(), -10, (a, b) => a + b, x => x * 10)).toBe(450)
	);

	test("add strings", string, async input =>
		expect(await reduce(input(), ">>", (a, b) => a + b, s => `[${s}]`)).toBe(`[>>${string}]`)
	);

	test("build array", string, async input => {
		const seed: string[] = [];
		expect(await reduce(
			input(),
			seed,
			(a, b) => {
				a.push(b);
				return a;
			},
			result => {
				result.push('z');
				return result;
			},
		)).toBe(seed);
		expect(seed).toStrictEqual(Array.from(string + 'z'));
	});

	test("build new array", string, async input => {
		const seed: string[] = [];
		const result = await reduce(input(), seed, (a, b) => [...a, b], x => [...x, 'z']);
		expect(result).not.toBe(seed);
		expect(seed.length).toBe(0);
		expect(result).toStrictEqual(Array.from(string + 'z'));
	});

	test("called length times", numbers, async input => {
		const callback = jest.fn((a: number, b: number) => a + b);
		await reduce(input(), -10, callback, x => x * 10);
		expect(callback).toHaveBeenCalledTimes(numbers.length);
	});

	test("finalizer called exactly once with result", string, async input => {
		const finalize = jest.fn((result: string) => `<${result}>`);
		await reduce(input(), ">>", (a: string, b) => a + b, finalize);
		expect(finalize).toHaveBeenCalledTimes(1);
		expect(finalize).toHaveBeenCalledWith(`>>${string}`);
	});

	test("parameters", string, async input => {
		let i = 0;
		await reduce(
			input(),
			">>",
			(prev, current, index) => {
				expect(index).toBe(i++);
				expect(current).toBe(string[index]);
				expect(prev).toBe(`>>${string.slice(0, index)}`);
				return prev + current;
			},
			x => `<${x}>`,
		);
	});
});