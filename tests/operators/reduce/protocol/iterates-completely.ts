import {runTests} from "lib";

export const iteratesCompletely = () => runTests("reduce", ({reduce, protocol}) => {
	const handlers = protocol.createHandlers();

	beforeEach(() => jest.resetAllMocks());

	const iterable = protocol("abcdef", handlers);

	test("overload 01", async () => {
		// reduce: Reduce<I, I>,
		await reduce(iterable, (a, b) => a + b);
		expect(handlers.onDone).toHaveBeenCalled();
	});

	test("overload 02", async () => {
		// reduce       : Reduce<I, R>,
		// initialValue : R,
		await reduce(iterable, (a, b) => a + b, ">>");
		expect(handlers.onDone).toHaveBeenCalled();
	});

	test("overload 03", async () => {
		// params: Reducer<I, I>
		await reduce(iterable, {reduce: (a, b) => a + b});
		expect(handlers.onDone).toHaveBeenCalled();
	});

	test("overload 04", async () => {
		// params: Reducer<I, R> & InitialValue<R>
		await reduce(iterable, {reduce: (a, b) => a + b, initialValue: ">>"});
		expect(handlers.onDone).toHaveBeenCalled();
	});

	test("overload 05", async () => {
		// params: Reducer<I, R> & Initializer<I, R>
		await reduce(iterable, {reduce: (a, b) => a + b, initialize: (a: string) => `<${a}>`});
		expect(handlers.onDone).toHaveBeenCalled();
	});

	test("overload 06", async () => {
		// params: Reducer<I, I> & Finalizer<I | undefined, R>
		await reduce(
			iterable,
			{reduce: (a, b) => a + b, finalize: (x: string | undefined) => `<${x}>`}
		);
		expect(handlers.onDone).toHaveBeenCalled();
	});

	test("overload 07", async () => {
		// params: Reducer<I, R> & InitialValue<R> & Finalizer<R, V>
		await reduce(
			iterable,
			{reduce: (a, b) => a + b, initialValue: ">>", finalize: (x: string) => `<${x}>`}
		);
		expect(handlers.onDone).toHaveBeenCalled();
	});

	test("overload 08", async () => {
		// params: Reducer<I, R> & Initializer<I, R> & Finalizer<R | undefined, V>
		await reduce(
			iterable,
			{
				reduce     : (a, b) => a + b,
				initialize : (a: string) => `[${a}]`,
				finalize   : (x: string | undefined) => `<${x}>`,
			}
		);
		expect(handlers.onDone).toHaveBeenCalled();
	});

	test("overload 09", async () => {
		// initialValue : R,
		// reduce       : Reduce<I, R>,
		await reduce(iterable, ">>", (a, b) => a + b);
		expect(handlers.onDone).toHaveBeenCalled();
	});

	test("overload 10", async () => {
		// initialValue : R,
		// reduce       : Reduce<I, R>,
		// finalize     : Finalize<R, V>,
		await reduce(iterable, ">>", (a, b) => a + b, x => `<${x}>`);
		expect(handlers.onDone).toHaveBeenCalled();
	});
});