import {runTests} from "lib";

export const doesNotCallThrow = () => runTests("reduce", ({reduce, protocol, error}) => {
	const handlers = protocol.createHandlers();

	beforeEach(() => jest.resetAllMocks());

	const iterable     = protocol("abcdef", handlers);
	const reducer      = (a: string, b: string) => a + b;
	const initialValue = ">>";
	const initialize   = (a: string) => `[${a}]`;
	const finalize     = (x: string | undefined) => `<${x}>`;

	test("overload 01", async () => {
		// reduce: Reduce<I, I>,
		await expect(() => reduce(iterable, error)).rejects.toThrow(error);
		expect(handlers.onThrow).not.toHaveBeenCalled();
	});

	test("overload 02", async () => {
		// reduce       : Reduce<I, R>,
		// initialValue : R,
		await expect(() => reduce(iterable, error, ">>")).rejects.toThrow(error);
		expect(handlers.onThrow).not.toHaveBeenCalled();
	});

	test("overload 03", async () => {
		// params: Reducer<I, I>
		await expect(() => reduce(iterable, {reduce: error})).rejects.toThrow(error);
		expect(handlers.onThrow).not.toHaveBeenCalled();
	});

	test("overload 04", async () => {
		// params: Reducer<I, R> & InitialValue<R>
		await expect(() => reduce(iterable, {reduce: error, initialValue})).rejects.toThrow(error);
		expect(handlers.onThrow).not.toHaveBeenCalled();
	});

	describe("overload 05", () => {
		// params: Reducer<I, R> & Initializer<I, R>
		test("reduce", async () => {
			await expect(() => reduce(iterable, {reduce: error, initialize})).rejects.toThrow(error);
			expect(handlers.onThrow).not.toHaveBeenCalled();
		});
		test("initialize", async () => {
			await expect(() => reduce(iterable, {reduce: reducer, initialize: error})).rejects.toThrow(error);
			expect(handlers.onThrow).not.toHaveBeenCalled();
		});
	});

	describe("overload 06", () => {
		// params: Reducer<I, I> & Finalizer<I | undefined, R>
		test("reduce", async () => {
			await expect(() => reduce(iterable, {reduce: error, finalize})).rejects.toThrow(error);
			expect(handlers.onThrow).not.toHaveBeenCalled();
		});
		test("finalize", async () => {
			await expect(() => reduce(iterable, {reduce: reducer, finalize: error})).rejects.toThrow(error);
			expect(handlers.onThrow).not.toHaveBeenCalled();
		});
	});

	describe("overload 07", () => {
		// params: Reducer<I, R> & InitialValue<R> & Finalizer<R, V>
		test("reduce", async () => {
			await expect(() => reduce(iterable, {reduce: error, initialValue, finalize})).rejects.toThrow(error);
			expect(handlers.onThrow).not.toHaveBeenCalled();
		});
		test("finalize", async () => {
			await expect(() => reduce(iterable, {reduce: reducer, initialValue, finalize: error})).rejects.toThrow(error);
			expect(handlers.onThrow).not.toHaveBeenCalled();
		});
	});

	describe("overload 08", () => {
		// params: Reducer<I, R> & Initializer<I, R> & Finalizer<R | undefined, V>
		test("reduce", async () => {
			await expect(() => reduce(iterable, {reduce: error, initialize, finalize})).rejects.toThrow(error);
			expect(handlers.onThrow).not.toHaveBeenCalled();
		});
		test("initialize", async () => {
			await expect(() => reduce(iterable, {reduce: reducer, initialize: error, finalize})).rejects.toThrow(error);
			expect(handlers.onThrow).not.toHaveBeenCalled();
		});
		test("finalize", async () => {
			await expect(() => reduce(iterable, {reduce: reducer, initialize, finalize: error})).rejects.toThrow(error);
			expect(handlers.onThrow).not.toHaveBeenCalled();
		});
	});

	test("overload 09", async () => {
		// initialValue : R,
		// reduce       : Reduce<I, R>,
		await expect(() => reduce(iterable, ">>", error)).rejects.toThrow(error);
		expect(handlers.onThrow).not.toHaveBeenCalled();
	});

	describe("overload 10", () => {
		// initialValue : R,
		// reduce       : Reduce<I, R>,
		// finalize     : Finalize<R, V>,
		test("reducer", async () => {
			await expect(() => reduce(iterable, ">>", error, finalize)).rejects.toThrow(error);
			expect(handlers.onThrow).not.toHaveBeenCalled();
		});
		test("finalizer", async () => {
			await expect(() => reduce(iterable, ">>", reducer, error)).rejects.toThrow(error);
			expect(handlers.onThrow).not.toHaveBeenCalled();
		});
	});
});