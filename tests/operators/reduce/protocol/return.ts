import {runTests} from "lib";

export const callsReturn = () => runTests("reduce", ({reduce, protocol, error}) => {
	const handlers = protocol.createHandlers();

	beforeEach(() => jest.resetAllMocks());

	const iterable     = protocol("abcdef", handlers);
	const reducer      = (a: string, b: string) => a + b;
	const initialValue = ">>";
	const initialize   = (a: string) => `[${a}]`;
	const finalize     = (x: string | undefined) => `<${x}>`;

	describe("overload 01", () => {
		// reduce: Reduce<I, I>,
		test("success", async () => {
			await reduce(iterable, reducer);
			expect(handlers.onReturn).toHaveBeenCalledTimes(1);
		});
		test("error", async () => {
			await expect(() => reduce(iterable, error)).rejects.toThrow(error);
			expect(handlers.onReturn).toHaveBeenCalledTimes(1);
		});
	});

	describe("overload 02", () => {
		// reduce       : Reduce<I, R>,
		// initialValue : R,
		test("success", async () => {
			await reduce(iterable, reducer, ">>");
			expect(handlers.onReturn).toHaveBeenCalledTimes(1);
		});
		test("error", async () => {
			await expect(() => reduce(iterable, error, ">>")).rejects.toThrow(error);
			expect(handlers.onReturn).toHaveBeenCalledTimes(1);
		});
	});

	describe("overload 03", () => {
		// params: Reducer<I, I>
		test("success", async () => {
			await reduce(iterable, {reduce: reducer});
			expect(handlers.onReturn).toHaveBeenCalledTimes(1);
		});
		test("error", async () => {
			await expect(() => reduce(iterable, {reduce: error})).rejects.toThrow(error);
			expect(handlers.onReturn).toHaveBeenCalledTimes(1);
		});
	});

	describe("overload 04", () => {
		// params: Reducer<I, R> & InitialValue<R>
		test("success", async () => {
			await reduce(iterable, {reduce: reducer, initialValue});
			expect(handlers.onReturn).toHaveBeenCalledTimes(1);
		});
		test("error", async () => {
			await expect(() => reduce(iterable, {reduce: error, initialValue})).rejects.toThrow(error);
			expect(handlers.onReturn).toHaveBeenCalledTimes(1);
		});
	});

	describe("overload 05", () => {
		// params: Reducer<I, R> & Initializer<I, R>
		test("success", async () => {
			await reduce(iterable, {reduce: reducer, initialize});
			expect(handlers.onReturn).toHaveBeenCalledTimes(1);
		});
		describe("error", () => {
			test("reduce", async () => {
				await expect(() => reduce(iterable, {reduce: error, initialize})).rejects.toThrow(error);
				expect(handlers.onReturn).toHaveBeenCalledTimes(1);
			});
			test("initialize", async () => {
				await expect(() => reduce(iterable, {reduce: reducer, initialize: error})).rejects.toThrow(error);
				expect(handlers.onReturn).toHaveBeenCalledTimes(1);
			});
		});
	});

	describe("overload 06", () => {
		// params: Reducer<I, I> & Finalizer<I | undefined, R>
		test("success", async () => {
			await reduce(iterable, {reduce: reducer, finalize});
			expect(handlers.onReturn).toHaveBeenCalledTimes(1);
		});
		describe("error", () => {
			test("reduce", async () => {
				await expect(() => reduce(iterable, {reduce: error, finalize})).rejects.toThrow(error);
				expect(handlers.onReturn).toHaveBeenCalledTimes(1);
			});
			test("finalize", async () => {
				await expect(() => reduce(iterable, {reduce: reducer, finalize: error})).rejects.toThrow(error);
				expect(handlers.onReturn).toHaveBeenCalledTimes(1);
			});
		});
	});

	describe("overload 07", () => {
		// params: Reducer<I, R> & InitialValue<R> & Finalizer<R, V>
		test("success", async () => {
			await reduce(iterable, {reduce: reducer, initialValue, finalize});
			expect(handlers.onReturn).toHaveBeenCalledTimes(1);
		});
		describe("error", () => {
			test("reduce", async () => {
				await expect(() => reduce(iterable, {reduce: error, initialValue, finalize})).rejects.toThrow(error);
				expect(handlers.onReturn).toHaveBeenCalledTimes(1);
			});
			test("finalize", async () => {
				await expect(() => reduce(iterable, {reduce: reducer, initialValue, finalize: error})).rejects.toThrow(error);
				expect(handlers.onReturn).toHaveBeenCalledTimes(1);
			});
		});
	});

	describe("overload 08", () => {
		// params: Reducer<I, R> & Initializer<I, R> & Finalizer<R | undefined, V>
		test("success", async () => {
			await reduce(iterable, {reduce: reducer, initialize, finalize});
			expect(handlers.onReturn).toHaveBeenCalledTimes(1);
		});
		describe("error", () => {
			test("reduce", async () => {
				await expect(() => reduce(iterable, {reduce: error, initialize, finalize})).rejects.toThrow(error);
				expect(handlers.onReturn).toHaveBeenCalledTimes(1);
			});
			test("initialize", async () => {
				await expect(() => reduce(iterable, {reduce: reducer, initialize: error, finalize})).rejects.toThrow(error);
				expect(handlers.onReturn).toHaveBeenCalledTimes(1);
			});
			test("finalize", async () => {
				await expect(() => reduce(iterable, {reduce: reducer, initialize, finalize: error})).rejects.toThrow(error);
				expect(handlers.onReturn).toHaveBeenCalledTimes(1);
			});
		});
	});

	describe("overload 09", () => {
		test("success", async () => {
			await reduce(iterable, ">>", reducer);
			expect(handlers.onReturn).toHaveBeenCalledTimes(1);
		});
		test("error", async () => {
			// initialValue : R,
			// reduce       : Reduce<I, R>,
			await expect(() => reduce(iterable, ">>", error)).rejects.toThrow(error);
			expect(handlers.onReturn).toHaveBeenCalledTimes(1);
		});
	});

	describe("overload 10", () => {
		// initialValue : R,
		// reduce       : Reduce<I, R>,
		// finalize     : Finalize<R, V>,
		test("success", async () => {
			await reduce(iterable, ">>", reducer, finalize);
			expect(handlers.onReturn).toHaveBeenCalledTimes(1);
		});
		describe("error", () => {
			test("reducer", async () => {
				await expect(() => reduce(iterable, ">>", error, finalize)).rejects.toThrow(error);
				expect(handlers.onReturn).toHaveBeenCalledTimes(1);
			});
			test("finalizer", async () => {
				await expect(() => reduce(iterable, ">>", reducer, error)).rejects.toThrow(error);
				expect(handlers.onReturn).toHaveBeenCalledTimes(1);
			});
		});
	});
});