import {iteratesCompletely} from "./iterates-completely";
import {callsReturn}        from "./return";
import {doesNotCallThrow}   from "./throw";

export function protocol() {
	describe("iterates completely",                iteratesCompletely);
	describe("calls iterator.return exactly once", callsReturn);
	describe("does not call iterator.throw",       doesNotCallThrow);
}