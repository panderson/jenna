import {runTests} from "lib";

export const empty = () => runTests("reduce", ({reduce, empty}) => {
	test("overload 01", async () => {
		// reduce<I>(
		//     iterable : Tagged.Iterable<T, I>,
		//     reduce   : Reduce<I, I>,
		// ): Tagged.Promise<T, I | undefined>;
		for (const [, value] of empty)
			expect(await reduce(value, (a, b) => a + b)).toBe(undefined);
	});

	test("overload 02", async () => {
		// reduce<I, R>(
		//     iterable     : Tagged.Iterable<T, I>,
		//     reduce       : Reduce<I, R>,
		//     initialValue : R,
		// ): Tagged.Promise<T, I | R>;
		for (const [, value] of empty)
			expect(await reduce(value, (a, b) => a + b, 99)).toBe(99);
	});

	test("overload 03", async () => {
		// reduce<I>(
		//     iterable : Tagged.Iterable<T, I>,
		//     params   : Reducer<I, I>
		// ): Tagged.Promise<T, I | undefined>;
		for (const [, value] of empty)
			expect(await reduce(value, {reduce: (a, b) => a + b})).toBe(undefined);
	});

	test("overload 04", async () => {
		// reduce<I, R>(
		//     iterable : Tagged.Iterable<T, I>,
		//     params   : Reducer<I, R> & InitialValue<R>
		// ): Tagged.Promise<T, R>;
		for (const [, value] of empty)
			expect(await reduce(value, {reduce: (a, b) => a + b, initialValue: 99})).toBe(99);
	});

	test("overload 05", async () => {
		// reduce<I, R>(
		//     iterable : Tagged.Iterable<T, I>,
		//     params   : Reducer<I, R> & Initializer<I, R>
		// ): Tagged.Promise<T, R | undefined>;
		for (const [, value] of empty) {
			expect(
				await reduce(value, {reduce: (a, b) => a + b, initialize: (a: unknown) => `<${a}>`})
			).toBe(undefined);
		}
	});

	test("overload 06", async () => {
		// reduce<I, R>(
		//     iterable : Tagged.Iterable<T, I>,
		//     params   : Reducer<I, I> & Finalizer<I | undefined, R>
		// ): Tagged.Promise<T, R>;
		for (const [, value] of empty) {
			expect(
				await reduce(value, {reduce: (a, b) => a + b, finalize: (x: unknown) => `<${x}>`})
			).toBe("<undefined>");
		}
	});

	test("overload 07", async () => {
		// reduce<I, R, V>(
		//     iterable : Tagged.Iterable<T, I>,
		//     params   : Reducer<I, R> & InitialValue<R> & Finalizer<R, V>
		// ): Tagged.Promise<T, V>;
		for (const [, value] of empty) {
			expect(await reduce(
				value,
				{reduce: (a, b) => a + b, initialValue: 99, finalize: (x: unknown) => `<${x}>`}
			)).toBe("<99>");
		}
	});

	test("overload 08", async () => {
		// reduce<I, R, V>(
		//     iterable : Tagged.Iterable<T, I>,
		//     params   : Reducer<I, R> & Initializer<I, R> & Finalizer<R | undefined, V>
		// ): Tagged.Promise<T, V>;
		for (const [, value] of empty) {
			expect(await reduce(
				value,
				{
					reduce     : (a, b) => a + b,
					initialize : (a: unknown) => `[${a}]`,
					finalize   : (x: unknown) => `<${x}>`,
				}
			)).toBe("<undefined>");
		}
	});

	test("overload 09", async () => {
		// reduce<I, R>(
		//     iterable     : Tagged.Iterable<T, I>,
		//     initialValue : R,
		//     reduce       : Reduce<I, R>,
		// ): Tagged.Promise<T, I | R>;
		for (const [, value] of empty)
			expect(await reduce(value, 99, (a, b) => a + b)).toBe(99);
	});

	test("overload 10", async () => {
		// reduce<I, R, V>(
		//     iterable     : Tagged.Iterable<T, I>,
		//     initialValue : R,
		//     reduce       : Reduce<I, R>,
		//     finalize     : Finalize<R, V>,
		// ): Tagged.Promise<T, V>;
		for (const [, value] of empty)
			expect(await reduce(value, 99, (a, b) => a + b, x => `<${x}>`)).toBe("<99>");
	});
});