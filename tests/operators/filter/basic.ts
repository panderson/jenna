import {runTests} from "lib";

export const basic = () => runTests("filter", ({filter}) => {
	const source = "abcdef";

	for (const letter of source) {
		test(letter, source, async input =>
			expect(await filter(input(), c => c === letter).join()).toBe(letter)
		);
	}

	for (const letter of source) {
		test(`!${letter}`, source, async input => {
			const actual = await filter(input(), c => c !== letter).join();
			expect(actual.length).toBe(source.length - 1);
			expect(actual).not.toContain(letter);
		});
	}

	for (let i = 0; i < source.length; i++) {
		test(String(i), source, async input =>
			expect(await filter(input(), (_, index) => index === i).join()).toBe(source[i])
		);
	}
});