import {runTests} from "lib";

export const protocol = () => runTests("filter", ({filter, protocol, error}) => {
	const handlers = protocol.createHandlers();

	beforeEach(() => jest.resetAllMocks());

	const rtn      = Symbol();
	const iterable = protocol("abc", rtn, handlers);

	describe("iterates completely", () => {
		test("true", async () => {
			await filter(iterable, () => true).count();
			expect(handlers.onDone).toHaveBeenCalled();
		});
		test("false", async () => {
			await filter(iterable, () => false).count();
			expect(handlers.onDone).toHaveBeenCalled();
		});
	});

	describe("calls iterator.return exactly once", () => {
		test("true", async () => {
			await filter(iterable, () => true).count();
			expect(handlers.onReturn).toHaveBeenCalledTimes(1);
		});

		test("false", async () => {
			await filter(iterable, () => false).count();
			expect(handlers.onReturn).toHaveBeenCalledTimes(1);
		});

		test("error", async () => {
			await expect(() => filter(iterable, error).count()).rejects.toThrow(error);
			expect(handlers.onReturn).toHaveBeenCalledTimes(1);
		});
	});

	test("doesn't call iterator.throw", async () => {
		await expect(() => filter(iterable, error).count()).rejects.toThrow(error);
		expect(handlers.onThrow).not.toHaveBeenCalled();
	});

	describe("passes parameter to next", () => {
		test("() => true", async () => {
			const it   = filter(iterable, () => true);
			let   step = await it.next("ignored");

			while (!step.done)
				step = await it.next(step.value.toUpperCase());

			expect(handlers.onNext).toHaveBeenCalledTimes(3);
			expect(handlers.onNext).toHaveBeenNthCalledWith(1, 'a', 'A');
			expect(handlers.onNext).toHaveBeenNthCalledWith(2, 'b', 'B');
			expect(handlers.onNext).toHaveBeenNthCalledWith(3, 'c', 'C');
		});

		test("() => false", async () => {
			const it   = filter(iterable, () => false);
			let   step = await it.next("ignored");
			expect(step.done).toBe(true);
			expect(handlers.onNext).toHaveBeenCalledTimes(3);
			expect(handlers.onNext).toHaveBeenNthCalledWith(1, 'a', undefined);
			expect(handlers.onNext).toHaveBeenNthCalledWith(2, 'b', undefined);
			expect(handlers.onNext).toHaveBeenNthCalledWith(3, 'c', undefined);
		});
	});

	describe("returns iterator return value", () => {
		for (const pass of [true, false]) {
			test(`() => ${pass}`, async () => {
				const it   = filter(iterable, () => pass);
				let   step = await it.next();

				while (!step.done)
					step = await it.next();

				expect(step.value).toBe(rtn);
			});
		}
	});
});