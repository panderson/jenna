import {runTests} from "lib";

export const extended = () => runTests("filter", ({AsyncIterable, filter}) =>
	test("is extended", "abcdef", input =>
		expect(filter(input(), () => true)).toBeInstanceOf(AsyncIterable)
	)
);