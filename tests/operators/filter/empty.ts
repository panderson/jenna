import {runTests} from "lib";

export const empty = () => runTests("filter", ({filter, empty}) => {
	for (const [kind, value] of empty)
		test(kind, async () => expect(await filter(value, () => true).count()).toBe(0));
});