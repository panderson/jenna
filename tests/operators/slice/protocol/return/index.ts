import {negative} from "./negative";
import {positive} from "./positive";

export function callsReturn() {
	describe("positive", positive);
	describe("negative", negative);
}