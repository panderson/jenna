import {runTests} from "lib";

export const positive = () => runTests("slice", ({slice, protocol}) => {
	const quick    = !!process.env["QUICK"];
	const handlers = protocol.createHandlers();

	beforeEach(() => jest.resetAllMocks());

	const source   = "abcdef";
	const iterable = protocol(source, handlers);

	test("no parameters / undefined", async () => {
		await slice(iterable).count();
		expect(handlers.onReturn).toHaveBeenCalledTimes(1);
		jest.resetAllMocks();
		await slice(iterable, undefined).count();
		expect(handlers.onReturn).toHaveBeenCalledTimes(1);
		jest.resetAllMocks();
		await slice(iterable, undefined, undefined).count();
		expect(handlers.onReturn).toHaveBeenCalledTimes(1);
	});

	test("end = undefined", async () => {
		const starts = [[0, 1, 3, 5], [0, 3]][Number(quick)];
		for (const start of starts) {
			jest.resetAllMocks();
			await slice(iterable, start).count();
			expect(handlers.onReturn).toHaveBeenCalledTimes(1);
		}
	});

	test("end <= start", async () => {
		const starts = [[0, 1, 3, 5], [0, 3]][Number(quick)];
		for (const start of starts) {
			for (let end = 0; end <= start; end++) {
				jest.resetAllMocks();
				await slice(iterable, start, end).count();
				// It doesn't iterate at all in this case.
				expect(handlers.onStart).not.toHaveBeenCalled();
				expect(handlers.onReturn).not.toHaveBeenCalled();
			}
		}
	});

	test("start >= length", async () => {
		const starts = [[0, 1, 3, 5, 6, 7, 9], [0, 3, 6, 9]][Number(quick)];
		for (const start of starts) {
			for (const end of [undefined, start + 1]) {
				jest.resetAllMocks();
				await slice(iterable, start, end).count();
				expect(handlers.onReturn).toHaveBeenCalledTimes(1);
			}
		}
	});

	test("end > length", async () => {
		const starts = [[0, 1, 3, 5], [0, 3]][Number(quick)];
		for (const start of starts) {
			for (const end of [source.length + 1, source.length + 3]) {
				jest.resetAllMocks();
				await slice(iterable, start, end).count();
				expect(handlers.onReturn).toHaveBeenCalledTimes(1);
			}
		}
	});
});