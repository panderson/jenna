import {runTests} from "lib";

export const negative = () => runTests("slice", ({slice, protocol}) => {
	const quick    = !!process.env["QUICK"];
	const handlers = protocol.createHandlers();

	beforeEach(() => jest.resetAllMocks());

	const source   = "abcdef";
	const iterable = protocol(source, handlers);

	test("no end", async () => {
		const starts = [[1, 3, 5, 6, 7, 9], [3, 6, 9]][Number(quick)];
		for (const start of starts) {
			jest.resetAllMocks();
			await slice(iterable, -start).count();
			expect(handlers.onReturn).toHaveBeenCalledTimes(1);
		}
	});

	test("start < 0", async () => {
		const starts = [[1, 3, 5, 6, 7, 9], [3, 6, 9]][Number(quick)];
		const ends   = [[0, 1, 3, 5, 6, 7, 9], [0, 3, 6, 9]][Number(quick)];
		for (const start of starts) {
			for (const end of ends) {
				jest.resetAllMocks();
				await slice(iterable, -start, end).count();
				expect(handlers.onReturn).toHaveBeenCalledTimes(1);
			}
		}
	});

	test("end < start", async () => {
		const starts = [[1, 3, 5], [3]][Number(quick)];
		for (const start of starts) {
			const ends   = [start + 1, source.length, source.length + 2];
			for (const end of ends) {
				jest.resetAllMocks();
				await slice(iterable, -start, -end).count();
				// It doesn't iterate at all in this case.
				expect(handlers.onStart).not.toHaveBeenCalled();
				expect(handlers.onReturn).not.toHaveBeenCalled();
			}
		}
	});

	test("end < 0", async () => {
		const starts = [[0, 1, 3, 5, 6, 7, 9], [0, 3, 6, 9]][Number(quick)];
		const ends   = [[1, 3, 5, 6, 7, 9], [3, 6, 9]][Number(quick)];
		for (const start of starts) {
			for (const end of ends) {
				jest.resetAllMocks();
				await slice(iterable, start, -end).count();
				expect(handlers.onReturn).toHaveBeenCalledTimes(1);
			}
		}
	});

	test("both < 0", async () => {
		const starts = [[1, 3, 5, 6, 7, 9], [3, 6, 9]][Number(quick)];
		const ends   = [[1, 3, 5, 6, 7, 9], [3, 6, 9]][Number(quick)];
		for (const start of starts) {
			for (const end of ends) {
				jest.resetAllMocks();
				await slice(iterable, -start, -end).count();
				if (-end > -start)
					expect(handlers.onReturn).toHaveBeenCalledTimes(1);
				else {
					expect(handlers.onStart).not.toHaveBeenCalled();
					expect(handlers.onReturn).not.toHaveBeenCalled();
				}
			}
		}
	});
});