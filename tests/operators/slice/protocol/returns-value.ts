import {runTests} from "lib";

export const returnsValue = () => runTests("slice", ({slice, protocol}) => {
	const quick    = !!process.env["QUICK"];
	const rtn      = Symbol();
	const source   = "abcdef";
	const iterable = protocol(source, rtn, {});

	async function checkReturn(...args: [start?: number, end?: number]): Promise<void> {
		const it   = slice(iterable, ...args);
		let   step = await it.next();

		while (!step.done)
			step = await it.next();

		expect(step.value).toBe(rtn);
	}

	describe("positive", () => {
		test("no parameters / undefined", async () => {
			await checkReturn();
			await checkReturn(undefined);
			await checkReturn(undefined, undefined);
		});

		test("end = undefined", async () => {
			const starts = [[0, 1, 3, 5], [0, 3]][Number(quick)];
			for (const start of starts)
				await checkReturn(start);
		});

		test("start >= length", async () => {
			for (const start of [source.length, source.length + 2])
				await checkReturn(start, start + 1);
		});

		test("end > length", async () => {
			const starts = [[0, 1, 3, 5], [0, 3]][Number(quick)];
			for (const start of starts)
				await checkReturn(start, source.length + 1);
		});
	});

	describe("negative", () => {
		test("no end", async () => {
			const starts = [[1, 3, 5, 6, 7, 9], [1, 3, 6, 9]][Number(quick)];
			for (const start of starts)
				await checkReturn(-start);
		});

		test("start < 0", async () => {
			const starts = [[1, 3, 5], [1, 3]][Number(quick)];
			const ends   = [source.length, source.length + 2];
			for (const start of starts) {
				for (const end of ends)
					await checkReturn(-start, end);
			}
		});

		test("end < 0", async () => {
			const starts = [[0, 1, 3, 5], [0, 3]][Number(quick)];
			const ends   = [[1, 3, 5, 6, 7, 9], [1, 3, 6, 9]][Number(quick)];
			for (const start of starts) {
				for (const end of ends)
					await checkReturn(start, -end);
			}
		});

		test("both < 0", async () => {
			const starts = [[2, 3, 5, 6, 7, 9], [2, 3, 6, 9]][Number(quick)];
			for (const start of starts) {
				const ends = quick
					? start === 2 ? [1] : [1, start - 1]
					: Array.from({length: start - 1}).map((_, i) => i + 1);

				for (const end of ends)
					await checkReturn(-start, -end);
			}
		});
	});
});