import {passesParameter} from "./passes-parameter";
import {callsReturn}     from "./return";
import {returnsValue}    from "./returns-value";

export function protocol() {
	describe("calls iterator.return exactly once", callsReturn);
	describe("passes parameter to next",           passesParameter);
	describe("returns iterator return value",      returnsValue);
}