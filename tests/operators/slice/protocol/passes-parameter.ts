import {runTests} from "lib";

export const passesParameter = () => runTests("slice", ({slice, protocol}) => {
	const quick    = !!process.env["QUICK"];
	const handlers = protocol.createHandlers();

	beforeEach(() => jest.resetAllMocks());

	const source   = "abcdef";
	const iterable = protocol(source, handlers);

	async function iterateAll(...args: [start?: number, end?: number]): Promise<void> {
		jest.resetAllMocks();

		const it   = slice(iterable, ...args);
		let   step = await it.next("ignored");

		while (!step.done)
			step = await it.next(step.value.toUpperCase());
	}

	// Positive only
	test("no parameters / undefined", async () => {
		for (const args of [[], [undefined], [undefined, undefined]]) {
			await iterateAll(...args);
			expect(handlers.onNext).toHaveBeenCalledTimes(source.length);
			for (let i = 0; i < source.length; i++)
				expect(handlers.onNext).nthCalledWith(i + 1, source[i], source[i].toUpperCase());
		}
	});

	test("end = undefined", async () => {
		const starts = [[0, 1, 3, 5], [0, 3]][Number(quick)];
		for (const start of starts) {
			await iterateAll(start);
			expect(handlers.onNext).toHaveBeenCalledTimes(source.length);

			for (let i = 0; i < start; i++)
				expect(handlers.onNext).nthCalledWith(i + 1, source[i], undefined);

			for (let i = start; i < source.length; i++)
				expect(handlers.onNext).nthCalledWith(i + 1, source[i], source[i].toUpperCase());
		}
	});

	test("start and end", async () => {
		const starts = [[0, 1, 3, 5], [0, 3]][Number(quick)];
		for (const start of starts) {
			const ends = quick
				? [start + 1, source.length]
				: Array.from({length: source.length - start}).map((_, i) => start + i + 1);

			for (const end of ends) {
				await iterateAll(start, end);
				expect(handlers.onNext).toHaveBeenCalledTimes(end - 1);

				for (let i = 0; i < start; i++)
					expect(handlers.onNext).nthCalledWith(i + 1, source[i], undefined);

				for (let i = start; i < end - 1; i++)
					expect(handlers.onNext).nthCalledWith(i + 1, source[i], source[i].toUpperCase());
			}
		}
	});
});