import {runTests} from "lib";

export const positive = () => runTests("slice", ({slice}) => {
	const quick   = !!process.env["QUICK"];
	const source  = "abcdef";

	test("no parameters / undefined", source, async input => {
		expect(await slice(input()).join()).toBe(source);
		expect(await slice(input(), undefined).join()).toBe(source);
		expect(await slice(input(), undefined, undefined).join()).toBe(source);
	});

	test("end = undefined", source, async input => {
		const starts = [[0, 1, 3, 6, 7, 10], [0, 1, 5, 10]][Number(quick)];
		for (const start of starts)
			expect(await slice(input(), start).join()).toBe(source.slice(start));
	});

	test("end < length", source, async input => {
		const starts = [[0, 1, 3, 5], [0, 1, 5]][Number(quick)];

		for (const start of starts) {
			const ends = quick
				? [start + 1, source.length]
				: Array.from({length: source.length - start}).map((_, i) => start + i + 1);

			for (const end of ends)
				expect(await slice(input(), start, end).join()).toBe(source.slice(start, end));
		}
	});

	test("end <= start", source, async input => {
		for (let start = 1; start < source.length; start++) {
			for (let end = 0; end <= start; end++)
				expect(await slice(input(), start, end).count()).toBe(0);
		}
	});

	test("start >= length", source, async input => {
		for (let start = source.length; start < source.length + 3; start++) {
			for (const end of [undefined, start - 1, start, start + 1])
				expect(await slice(input(), start, end).count()).toBe(0);
		}
	});

	test("end > length", source, async input => {
		const starts = [[0, 1, 3, 5], [0, 1, 5]][Number(quick)];

		for (const start of starts) {
			for (let end = source.length + 1; end < source.length + 3; end++)
				expect(await slice(input(), start, end).join()).toBe(source.slice(start));
		}
	});
});