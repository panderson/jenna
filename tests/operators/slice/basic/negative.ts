import {runTests} from "lib";

export const negative = () => runTests("slice", ({slice}) => {
	const quick  = !!process.env["QUICK"];
	const source = "abcdef";

	test("no end", source, async input => {
		const starts = [[1, 3, 5, 6, 7, 9], [1, 3, 6, 9]][Number(quick)];
		for (const start of starts)
			expect(await slice(input(), -start).join()).toBe(source.slice(-start));
	});

	test("start < 0", source, async input => {
		const starts = [[1, 3, 5, 6, 7, 9], [1, 3, 6, 9]][Number(quick)];
		for (const start of starts) {
			const ends = [[0, 1, 3, 5, 6, 7, 9], [0, 1, 6, 9]][Number(quick)];
			for (const end of ends)
				expect(await slice(input(), -start, end).join()).toBe(source.slice(-start, end));
		}
	});

	test("end < 0", source, async input => {
		const starts = [[0, 1, 3, 5, 6, 7, 9], [0, 1, 6, 9]][Number(quick)];
		const ends   = [[1, 3, 5, 6, 7, 9], [1, 6, 9]][Number(quick)];
		for (const start of starts) {
			for (const end of ends)
				expect(await slice(input(), start, -end).join()).toBe(source.slice(start, -end));
		}
	});

	test("both < 0", source, async input => {
		const indices = [[1, 3, 5, 6, 7, 9], [1, 6, 9]][Number(quick)];
		for (const start of indices) {
			for (const end of indices)
				expect(await slice(input(), -start, -end).join()).toBe(source.slice(-start, -end));
		}
	});
});