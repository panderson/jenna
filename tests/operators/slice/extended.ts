import {runTests} from "lib";

export const extended = () => runTests("slice", ({slice, AsyncIterable}) =>
	test("is extended", "abcdef", input => {
		expect(slice(input())).toBeInstanceOf(AsyncIterable);

		for (const start of [0, 1, 5, 6, 7, 10]) {
			expect(slice(input(), start)).toBeInstanceOf(AsyncIterable);
			if (start)
				expect(slice(input(), -start)).toBeInstanceOf(AsyncIterable);

			for (const end of [0, 1, 5, 6, 7, 10]) {
				expect(slice(input(), start, end)).toBeInstanceOf(AsyncIterable);
				if (start) {
					expect(slice(input(), -start, end)).toBeInstanceOf(AsyncIterable);
					if (end)
						expect(slice(input(), -start, -end)).toBeInstanceOf(AsyncIterable);
				} else if (end)
					expect(slice(input(), start, -end)).toBeInstanceOf(AsyncIterable);
			}
		}
	})
);