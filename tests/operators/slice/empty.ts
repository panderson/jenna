import {runTests} from "lib";

export const empty = () => runTests("slice", ({slice, empty}) => {
	for (const [kind, value] of empty) {
		describe(kind, () => {
			test("no parameters / undefined", async () => {
				expect(await slice(value).count()).toBe(0);
				expect(await slice(value, undefined).count()).toBe(0);
				expect(await slice(value, undefined, undefined).count()).toBe(0);
			})

			describe("positive", () => {
				test("end = undefined", async () => {
					for (const start of [0, 1])
						expect(await slice(value, start).count()).toBe(0);
				});

				test("end <= start", async () => {
					for (const start of [0, 1]) {
						for (const end of [start - 1, start])
							expect(await slice(value, start, end).count()).toBe(0);
					}
				});

				test("end > start", async () => {
					for (const start of [0, 1])
						expect(await slice(value, start, start + 1).count()).toBe(0);
				});
			});

			describe("negative", () => {
				test("end = undefined", async () => expect(await slice(value, -1).count()).toBe(0));

				test("end <= start", async () => {
					for (const end of [-2, -1])
						expect(await slice(value, -1, end).count()).toBe(0);
				});

				test("end > start", async () => {
					for (const start of [-3, -2])
						expect(await slice(value, start, -1).count()).toBe(0);
				});

				test("start > 0", async () => {
					for (const start of [0, 1])
						expect(await slice(value, start, -1).count()).toBe(0);
				});

				test("end > 0", async () => {
					for (const end of [0, 1])
						expect(await slice(value, -1, end).count()).toBe(0);
				});
			});
		});
	}
});