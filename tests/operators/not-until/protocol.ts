import {runTests} from "lib";

export const protocol = () => runTests("notUntil", ({notUntil, protocol, error}) => {
	const handlers = protocol.createHandlers();

	beforeEach(() => jest.resetAllMocks());

	const rtn      = Symbol();
	const iterable = protocol("abcdef", rtn, handlers);

	describe("iterates completely", () => {
		test("true", async () => {
			await notUntil(iterable, () => true).count();
			expect(handlers.onDone).toHaveBeenCalled();
		});

		test("false", async () => {
			await notUntil(iterable, () => false).count();
			expect(handlers.onDone).toHaveBeenCalled();
		});
	});

	describe("calls iterator.return exactly once", () => {
		test("true", async () => {
			await notUntil(iterable, () => true).count();
			expect(handlers.onReturn).toHaveBeenCalledTimes(1);
		});

		test("false", async () => {
			await notUntil(iterable, () => false).count();
			expect(handlers.onReturn).toHaveBeenCalledTimes(1);
		});

		test("error", async () => {
			await expect(() => notUntil(iterable, error).count()).rejects.toThrow(error);
			expect(handlers.onReturn).toHaveBeenCalledTimes(1);
		});
	});

	test("doesn't call iterator.throw", async () => {
		await expect(() => notUntil(iterable, error).count()).rejects.toThrow(error);
		expect(handlers.onThrow).not.toHaveBeenCalled();
	});

	test("passes parameter to next", async () => {
		const it   = notUntil(iterable, c => c === 'd');
		let   step = await it.next("ignored") ;

		while (!step.done)
			step = await it.next(step.value.toUpperCase());

		expect(handlers.onNext).toHaveBeenCalledTimes(6);
		expect(handlers.onNext).toHaveBeenNthCalledWith(1, 'a', undefined);
		expect(handlers.onNext).toHaveBeenNthCalledWith(2, 'b', undefined);
		expect(handlers.onNext).toHaveBeenNthCalledWith(3, 'c', undefined);
		expect(handlers.onNext).toHaveBeenNthCalledWith(4, 'd', 'D');
		expect(handlers.onNext).toHaveBeenNthCalledWith(5, 'e', 'E');
		expect(handlers.onNext).toHaveBeenNthCalledWith(6, 'f', 'F');
	});

	describe("returns iterator return value", () => {
		for (const pass of [true, false]) {
			test(`() => ${pass}`, async () => {
				const it   = notUntil(iterable, () => pass);
				let   step = await it.next();

				while (!step.done)
					step = await it.next();

				expect(step.value).toBe(rtn);
			});
		}
	});
});