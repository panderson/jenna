import {runTests} from "lib";

export const empty = () => runTests("notUntil", ({notUntil, empty}) => {
	for (const [kind, value] of empty) {
		test(kind, async () => {
			expect(await notUntil(value, () => true).count()).toBe(0);
			expect(await notUntil(value, () => false).count()).toBe(0);
		});
	}
});