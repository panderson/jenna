import {runTests} from "lib";

export const extended = () => runTests("notUntil", ({AsyncIterable, notUntil, protocol}) =>
	test("is extended", () => {
		expect(notUntil("abc", () => true)).toBeInstanceOf(AsyncIterable);
		expect(notUntil("abc", () => false)).toBeInstanceOf(AsyncIterable);
		expect(notUntil(["abc"], () => true)).toBeInstanceOf(AsyncIterable);
		expect(notUntil(["abc"], () => false)).toBeInstanceOf(AsyncIterable);
		expect(notUntil(new Set("abc"), () => true)).toBeInstanceOf(AsyncIterable);
		expect(notUntil(new Set("abc"), () => false)).toBeInstanceOf(AsyncIterable);
		expect(notUntil(protocol("abc"), () => true)).toBeInstanceOf(AsyncIterable);
		expect(notUntil(protocol("abc"), () => false)).toBeInstanceOf(AsyncIterable);
	})
);