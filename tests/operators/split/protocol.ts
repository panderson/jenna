import {runTests} from "lib";

export const protocol = () => runTests("split", ({split, protocol, error}) => {
	const handlers = protocol.createHandlers();

	beforeEach(() => jest.resetAllMocks());

	const rtn      = Symbol();
	const iterable = protocol("abc-def-ghi", rtn, handlers);

	describe("calls iterator.return exactly once", () => {
		test("full iteration", async () => {
			await split(iterable, '-').count();
			expect(handlers.onReturn).toHaveBeenCalledTimes(1);
		});

		test("limit 1", async () => {
			await split(iterable, {separator: '-', limit: 1}).count();
			expect(handlers.onReturn).toHaveBeenCalledTimes(1);
		});

		test("error", async () => {
			await expect(() => split(iterable, error).count()).rejects.toThrow(error);
			expect(handlers.onReturn).toHaveBeenCalledTimes(1);
		});
	});

	test("doesn't call iterator.throw", async () => {
		await expect(() => split(iterable, error).count()).rejects.toThrow(error);
		expect(handlers.onThrow).not.toHaveBeenCalled();
	});

	test("doesn't iterate at all", async () => {
		await split(iterable, {separator: '-', limit: 0}).count();
		expect(handlers.onStart).not.toHaveBeenCalled();
	});

	test("doesn't iterate completely", async () => {
		await split(iterable, {separator: '-', limit: 1}).count();
		expect(handlers.onDone).not.toHaveBeenCalled();
	});

	test("iterates completely", async () => {
		await split(iterable, '-').count();
		expect(handlers.onDone).toHaveBeenCalled();
	});

	test("passes parameter to next", async () => {
		const it   = split(iterable, '-');
		let   step = await it.next("ignored");

		while (!step.done)
			step = await it.next(step.value.join().toUpperCase());

		expect(handlers.onNext).toHaveBeenCalledTimes(11);
		expect(handlers.onNext).toHaveBeenNthCalledWith( 1, 'a', undefined);
		expect(handlers.onNext).toHaveBeenNthCalledWith( 2, 'b', undefined);
		expect(handlers.onNext).toHaveBeenNthCalledWith( 3, 'c', undefined);
		expect(handlers.onNext).toHaveBeenNthCalledWith( 4, '-', "A,B,C");
		expect(handlers.onNext).toHaveBeenNthCalledWith( 5, 'd', undefined);
		expect(handlers.onNext).toHaveBeenNthCalledWith( 6, 'e', undefined);
		expect(handlers.onNext).toHaveBeenNthCalledWith( 7, 'f', undefined);
		expect(handlers.onNext).toHaveBeenNthCalledWith( 8, '-', "D,E,F");
		expect(handlers.onNext).toHaveBeenNthCalledWith( 9, 'g', undefined);
		expect(handlers.onNext).toHaveBeenNthCalledWith(10, 'h', undefined);
		expect(handlers.onNext).toHaveBeenNthCalledWith(11, 'i', undefined);
	});

	test("returns iterator return value", async () => {
		const it   = split(iterable, '-');
		let   step = await it.next();

		while (!step.done)
			step = await it.next();

		expect(step.value).toBe(rtn);
	});
});