import {runTests} from "lib";

export const options = () => runTests("split", ({split}) => {
	const source = "abcd-efg-h-ij-klmnop";
	const parts  = {
		keepSeparator: {
			false : ["abcd", "efg", "h", "ij", "klmnop"],
			true  : ["abcd", "-", "efg", "-", "h", "-", "ij", "-", "klmnop"],
			start : ["abcd", "-efg", "-h", "-ij", "-klmnop"],
			end   : ["abcd-", "efg-", "h-", "ij-", "klmnop"],
		},
	};
	const expected = parts.keepSeparator.false;

	describe("limit", () => {
		for (let i = 0; i < expected.length; i++) {
			test(String(i), source, async input => {
				const actual = await split(input(), {separator: '-', limit: i}).toArray();

				expect(actual.length).toBe(i);
				for (let j = 0; j < i; j++)
					expect(actual[j].join('')).toBe(expected[j]);
			});
		}

		test(`>= ${expected.length}`, source, async input => {
			const actual = await split(
				input(),
				{separator: '-', limit: expected.length * 2}
			).toArray();

			expect(actual.length).toBe(expected.length);

			for (let i = 0; i < actual.length; i++)
				expect(actual[i].join('')).toBe(expected[i]);
		});

		test("< 0", source, async input => {
			const actual = await split(input(), {separator: '-', limit: -1}).toArray();

			expect(actual.length).toBe(expected.length);

			for (let i = 0; i < actual.length; i++)
				expect(actual[i].join('')).toBe(expected[i]);
		});
	});

	describe("keep separator", () => {
		for (const param of [true, "start", "end"] as const) {
			test(String(param), source, async input => {
				const expected = parts.keepSeparator[String(param) as "true"];
				const actual   = (
					await split(input(), {separator: '-', keepSeparator: param}).toArray()
				).map(p => p.join(''));

				expect(actual).toEqual(expected);
			});
		}
	});
});