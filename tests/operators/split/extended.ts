import {runTests} from "lib";

export const extended = () => runTests("split", ({AsyncIterable, split, protocol}) => {
	const string = "abc-def";
	test("string",    () => expect(split(string, '-')).toBeInstanceOf(AsyncIterable));
	test("array",     () => expect(split(Array.from(string), '-')).toBeInstanceOf(AsyncIterable));
	test("set",       () => expect(split(new Set(string), '-')).toBeInstanceOf(AsyncIterable));
	test("generator", () => expect(split(protocol(string), '-')).toBeInstanceOf(AsyncIterable));
});