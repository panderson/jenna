import {runTests} from "lib";

export const withEmptyParts = () => runTests("split", ({split}) => {
	const source = "-abcd--efg-h-ij-klmnop--";
	const parts  = {
		keepSeparator: {
			false : ["", "abcd", "", "efg", "h", "ij", "klmnop", "", ""],
			true  : ["", "-", "abcd", "-", "", "-", "efg", "-", "h", "-", "ij", "-", "klmnop", "-", "", "-", ""],
			start : ["", "-abcd", "-", "-efg", "-h", "-ij", "-klmnop", "-", "-"],
			end   : ["-", "abcd-", "-", "efg-", "h-", "ij-", "klmnop-", "-", ""],
		},
		omitEmpty: {
			keepSeparator: {
				false : ["abcd", "efg", "h", "ij", "klmnop"],
				true  : ["-", "abcd", "-", "-", "efg", "-", "h", "-", "ij", "-", "klmnop", "-", "-"],
				start : ["-abcd", "-", "-efg", "-h", "-ij", "-klmnop", "-", "-"],
				end   : ["-", "abcd-", "-", "efg-", "h-", "ij-", "klmnop-", "-"],
			},
		},
	};

	describe("keep separator", () => {
		for (const param of [false, true, "start", "end"] as const) {
			test(String(param), source, async input => {
				const expected = parts.keepSeparator[String(param) as "true"];
				const actual   = (
					await split(input(), {separator: '-', keepSeparator: param}).toArray()
				).map(p => p.join(''));

				expect(actual).toEqual(expected);
			});
		}
	});

	describe("omit empty", () => {
		describe("keep separator", () => {
			for (const param of [false, true, "start", "end"] as const) {
				test(String(param), source, async input => {
					const expected = parts.omitEmpty.keepSeparator[String(param) as "true"];
					const actual   = (
						await split(
							input(),
							{separator: '-', omitEmpty: true, keepSeparator: param}
						).toArray()
					).map(p => p.join(''));

					expect(actual).toEqual(expected);
				});
			}
		});
	})
});