import {basic}          from "./basic";
import {extended}       from "./extended";
import {options}        from "./options";
import {protocol}       from "./protocol";
import {withEmptyParts} from "./with-empty-parts";

describe("basic usage",      basic);
describe("options",          options);
describe("with empty parts", withEmptyParts);
describe("is extended",      extended);
describe("protocol",         protocol);