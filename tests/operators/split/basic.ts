import {runTests} from "lib";

export const basic = () => runTests("split", ({split}) => {
	const source = "abcd-efg-h-ij-klmnop";
	const parts  = {
		keepSeparator: {
			false : ["abcd", "efg", "h", "ij", "klmnop"],
			true  : ["abcd", "-", "efg", "-", "h", "-", "ij", "-", "klmnop"],
			start : ["abcd", "-efg", "-h", "-ij", "-klmnop"],
			end   : ["abcd-", "efg-", "h-", "ij-", "klmnop"],
		},
	};
	const expected = parts.keepSeparator.false;

	test("simple iteration", source, async input => {
		let index = 0;

		for await (const part of split(input(), '-')) {
			expect(index).toBeLessThan(expected.length);
			expect(part.join('')).toBe(expected[index++]);
		}

		expect(index).toBe(expected.length);
	});

	test("iterate later", source, async input => {
		const actual = await split(input(), '-').toArray();

		expect(actual.length).toBe(expected.length);

		for (let i = 0; i < actual.length; i++)
			expect(actual[i].join('')).toBe(expected[i]);
	});

	test("`shouldSplit` function", source, async input => {
		const actual = await split(input(), {shouldSplit: c => c === '-'}).toArray();

		expect(actual.length).toBe(expected.length);

		for (let i = 0; i < actual.length; i++)
			expect(actual[i].join('')).toBe(expected[i]);
	});
});