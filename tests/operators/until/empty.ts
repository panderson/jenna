import {runTests} from "lib";

export const empty = () => runTests("until", ({until, empty}) => describe("empty", () => {
	for (const [kind, value] of empty) {
		test(kind, async () => {
			expect(await until(value, () => true).count()).toBe(0);
			expect(await until(value, () => false).count()).toBe(0);
		});
	}
}));