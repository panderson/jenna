import {runTests} from "lib";

export const extended = () => runTests("until", ({AsyncIterable, until, protocol}) =>
	test("is extended", () => {
		expect(until("abc", () => true)).toBeInstanceOf(AsyncIterable);
		expect(until("abc", () => false)).toBeInstanceOf(AsyncIterable);
		expect(until(["abc"], () => true)).toBeInstanceOf(AsyncIterable);
		expect(until(["abc"], () => false)).toBeInstanceOf(AsyncIterable);
		expect(until(new Set("abc"), () => true)).toBeInstanceOf(AsyncIterable);
		expect(until(new Set("abc"), () => false)).toBeInstanceOf(AsyncIterable);
		expect(until(protocol("abc"), () => true)).toBeInstanceOf(AsyncIterable);
		expect(until(protocol("abc"), () => false)).toBeInstanceOf(AsyncIterable);
	})
);