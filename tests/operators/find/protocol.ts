import {runTests} from "lib";

export const protocol = () => runTests("find", ({find, protocol, error}) => {
	const handlers = protocol.createHandlers();

	beforeEach(() => jest.resetAllMocks());

	const iterable = protocol("abc", handlers);

	describe("calls iterator.return exactly once", () => {
		test("found", async () => {
			await find(iterable, () => true);
			expect(handlers.onReturn).toHaveBeenCalledTimes(1);
		});

		test("not found", async () => {
			await find(iterable, () => false);
			expect(handlers.onReturn).toHaveBeenCalledTimes(1);
		});

		test("exception", async () => {
			await expect(() => find(iterable, error)).rejects.toThrow(error);
			expect(handlers.onReturn).toHaveBeenCalledTimes(1);
		});
	});

	test("doesn't call iterator.throw", async () => {
		await expect(() => find(iterable, error)).rejects.toThrow(error);
		expect(handlers.onThrow).not.toHaveBeenCalled();
	});

	test("doesn't iterate completely", async () => {
		await find(iterable, () => true);
		expect(handlers.onDone).not.toHaveBeenCalled();
	});

	test("iterates completely", async () => {
		await find(iterable, () => false);
		expect(handlers.onDone).toHaveBeenCalled();
	});
});