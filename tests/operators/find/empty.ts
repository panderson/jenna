import {runTests} from "lib";

export const empty = () => runTests("find", ({find, empty}) => {
	for (const [kind, value] of empty)
		test(kind, async () => expect(await find(value, () => true)).toBe(undefined));
});