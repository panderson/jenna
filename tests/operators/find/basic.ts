import {runTests} from "lib";

export const basic = () => runTests("find", ({find}) => {
	const source = "abcdef";

	for (let i = 0; i < source.length; i++) {
		test(source[i], source, async input =>
			expect(await find(input(), v => v === source[i])).toBe(source[i])
		);
	}

	test('z', source, async input => expect(await find(input(), v => v === 'z')).toBe(undefined));
});