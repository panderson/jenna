import {runTests} from "lib";

export const empty = () => runTests("while", ({while: while_, empty}) => {
	for (const [kind, value] of empty) {
		test(kind, async () => {
			expect(await while_(value, () => true).count()).toBe(0);
			expect(await while_(value, () => false).count()).toBe(0);
		});
	}
});