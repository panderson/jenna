import {runTests} from "lib";

export const protocol = () => runTests("while", ({while: while_, protocol, error}) => {
	const handlers = protocol.createHandlers();

	beforeEach(() => jest.resetAllMocks());

	const rtn      = Symbol();
	const iterable = protocol("abcdef", rtn, handlers);

	test("iterates completely", async () => {
		await while_(iterable, () => true).count();
		expect(handlers.onDone).toHaveBeenCalled();
	});

	test("doesn't iterate completely", async () => {
		await while_(iterable, () => false).count();
		expect(handlers.onStart).toHaveBeenCalled();
		expect(handlers.onDone).not.toHaveBeenCalled();
	});

	describe("calls iterator.return exactly once", () => {
		test("true", async () => {
			await while_(iterable, () => true).count();
			expect(handlers.onReturn).toHaveBeenCalledTimes(1);
		});

		test("false", async () => {
			await while_(iterable, () => false).count();
			expect(handlers.onReturn).toHaveBeenCalledTimes(1);
		});

		test("error", async () => {
			await expect(() => while_(iterable, error).count()).rejects.toThrow(error);
			expect(handlers.onReturn).toHaveBeenCalledTimes(1);
		});
	});

	test("doesn't call iterator.throw", async () => {
		await expect(() => while_(iterable, error).count()).rejects.toThrow(error);
		expect(handlers.onThrow).not.toHaveBeenCalled();
	});

	test("passes parameter to next", async () => {
		const it   = while_(iterable, c => c !== 'd');
		let   step = await it.next("ignored");

		while (!step.done)
			step = await it.next(step.value.toUpperCase());

		expect(handlers.onNext).toHaveBeenCalledTimes(3);
		expect(handlers.onNext).toHaveBeenNthCalledWith(1, 'a', 'A');
		expect(handlers.onNext).toHaveBeenNthCalledWith(2, 'b', 'B');
		expect(handlers.onNext).toHaveBeenNthCalledWith(3, 'c', 'C');
	});

	test("returns iterator return value", async () => {
		const it   = while_(iterable, () => true); // Only when the predicate never returns false.
		let   step = await it.next();

		while (!step.done)
			step = await it.next();

		expect(step.value).toBe(rtn);
	});
});