import {runTests} from "lib";

export const extended = () => runTests("while", ({AsyncIterable, while: while_, protocol}) =>
	test("is extended", () => {
		expect(while_("abc", () => true)).toBeInstanceOf(AsyncIterable);
		expect(while_("abc", () => false)).toBeInstanceOf(AsyncIterable);
		expect(while_(["abc"], () => true)).toBeInstanceOf(AsyncIterable);
		expect(while_(["abc"], () => false)).toBeInstanceOf(AsyncIterable);
		expect(while_(new Set("abc"), () => true)).toBeInstanceOf(AsyncIterable);
		expect(while_(new Set("abc"), () => false)).toBeInstanceOf(AsyncIterable);
		expect(while_(protocol("abc"), () => true)).toBeInstanceOf(AsyncIterable);
		expect(while_(protocol("abc"), () => false)).toBeInstanceOf(AsyncIterable);
	})
);