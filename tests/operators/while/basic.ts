import {runTests} from "lib";

export const basic = () => runTests("while", ({while: while_}) => {
	const source = "abcdef";

	test("basic usage", source, async input =>
		expect(await while_(input(), c => c !== 'd').join()).toBe("abc")
	);

	test("true",  source, async input => expect(await while_(input(), () => true).join()).toBe(source));
	test("false", source, async input => expect(await while_(input(), () => false).count()).toBe(0));

	test("stops calling predicate after test returns false", source, async input => {
		const predicate = jest.fn((c: string) => c !== 'd');
		await while_(input(), predicate).count();
		expect(predicate).toHaveBeenCalledTimes(4);
		expect(predicate).toHaveBeenNthCalledWith(1, 'a');
		expect(predicate).toHaveBeenNthCalledWith(2, 'b');
		expect(predicate).toHaveBeenNthCalledWith(3, 'c');
		expect(predicate).toHaveBeenNthCalledWith(4, 'd');
	});
});