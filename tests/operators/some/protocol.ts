import {runTests} from "lib";

export const protocol = () => runTests("some", ({some, protocol, error}) => {
	const handlers = protocol.createHandlers();

	beforeEach(() => jest.resetAllMocks());

	const iterable = protocol([1, 2, 3, 4, 5], handlers);

	describe("calls iterator.return exactly once", () => {
		test("true", async () => {
			await some(iterable, () => true);
			expect(handlers.onReturn).toHaveBeenCalledTimes(1);
		});

		test("false", async () => {
			await some(iterable, () => false);
			expect(handlers.onReturn).toHaveBeenCalledTimes(1);
		});

		test("exception", async () => {
			await expect(() => some(iterable, error)).rejects.toThrow(error);
			expect(handlers.onReturn).toHaveBeenCalledTimes(1);
		});
	});

	test("doesn't call iterator.throw", async () => {
		await expect(() => some(iterable, error)).rejects.toThrow(error);
		expect(handlers.onThrow).not.toHaveBeenCalled();
	});

	test("doesn't iterate completely", async () => {
		await some(iterable, () => true);
		expect(handlers.onDone).not.toHaveBeenCalled();
	});

	test("iterates completely", async () => {
		await some(iterable, () => false);
		expect(handlers.onDone).toHaveBeenCalled();
	});
});