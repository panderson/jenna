import {runTests} from "lib";

export const empty = () => runTests("some", ({some, empty}) => {
	for (const [kind, value] of empty) {
		test(kind, async () => {
			expect(await some(value, () => false)).toBe(false);
			expect(await some(value, () => true)).toBe(false);
		});
	}
});