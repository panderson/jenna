import {runTests} from "lib";

export const basic = () => runTests("some", ({some}) => {
	test("predicate returns true", "abc", async input =>
		expect(await some(input(), () => true)).toBe(true)
	);

	test("predicate returns false", "abc", async input =>
		expect(await some(input(), () => false)).toBe(false)
	);

	test("evaluates to truthy", [1, 3, 5], async input =>
		expect(await some(input(), i => (i & 1) as any)).toBe(true)
	);

	// This is the only test different from `every`.
	test("one item is not", [1, 3, 5, 0], async input =>
		expect(await some(input(), i => i as any)).toBe(true)
	);
});