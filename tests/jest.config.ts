import fs                                                   from "fs";
import * as JSONC                                           from "jsonc-parser";
import Path                                                 from "path";
import {type JestConfigWithTsJest, pathsToModuleNameMapper} from "ts-jest";

const {compilerOptions} = JSONC.parse(
	fs.readFileSync(Path.join(__dirname, "tsconfig.json"), "utf-8")
);

const jestConfig: JestConfigWithTsJest = {
	preset           : "ts-jest",
	testEnvironment  : "node",
	transform        : {"^.+\\.tsx?$": ["ts-jest", {isolatedModules: true}]},
	roots            : [__dirname],
	modulePaths      : [compilerOptions.baseUrl],
	moduleNameMapper : pathsToModuleNameMapper(compilerOptions.paths),
	setupFiles       : [Path.join(__dirname, "setup.ts")],
};

export default jestConfig;