export function extractContext<T, U>(
	context: T | {async: T, sync: U} | undefined
): {asyncContext: T, syncContext: U} {
	if (!context)
		return {asyncContext: undefined as any, syncContext: undefined as any};

	if (typeof context === "object" && context && "async" in context && "sync" in context)
		return {asyncContext: context.async, syncContext: context.sync};

	return {asyncContext: context, syncContext: context as any};
}

type AsyncContext<T extends object> = {
	[K in keyof T]: T[K] extends {async: infer A, sync: any} ? A : T[K];
};

type SyncContext<T extends object> = {
	[K in keyof T]: T[K] extends {async: any, sync: infer S} ? S : T[K];
};

interface Context<T extends object> {
	async : AsyncContext<T>;
	sync  : SyncContext<T>;
};

export function context<T extends object>(context: T): Context<T> {
	const async = {} as any;
	const sync  = {} as any;

	for (const [key, value] of Object.entries(context)) {
		const {asyncContext, syncContext} = extractContext(value);

		async[key] = asyncContext;
		sync [key] = syncContext;
	}

	return {async, sync};
}