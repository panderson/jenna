// The nonsense below effectively equates to this:
//
//     export class TestError extends Error {}
//
// But the difference is the below can be called without `new`, and will create an instance and
// throw it.

function TestError() {
	if (!new.target) {
		// @ts-ignore
		throw new TestError(...arguments);
	}
	return Reflect.construct(Error, arguments, TestError);
}

Object.setPrototypeOf(TestError, Error);
Object.setPrototypeOf(TestError.prototype, Error.prototype);

const error = TestError as typeof TestError & {new (...args: any[]): Error};

export {error as TestError};