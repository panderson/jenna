export interface Handlers {
	onStart?  ()                                   : void;
	onNext?   (lastYield: any, nextValue: unknown) : void;
	onDone?   ()                                   : void;
	onReturn? (completed?: true)                   : void;
	onThrow?  (error: any)                         : void;
}

export interface MockHandlers {
	onStart  : jest.Mock<void, []>;
	onNext   : jest.Mock<void, [lastYield: any, nextValue: unknown]>;
	onDone   : jest.Mock<void, []>;
	onReturn : jest.Mock<void, [completed?: true]>;
	onThrow  : jest.Mock<void, [error: any]>;
}