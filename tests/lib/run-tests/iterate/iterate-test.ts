import {AsyncAction, Source, SyncAction} from "./types";
import {IterateTypes, iterateTypes}      from "./iterate-types";

type Async = <A extends readonly Source[]>(
	name    : string,
	...args : [...A, AsyncAction<A>]
) => void | Promise<void>;

type Sync = <A extends readonly Source[]>(
	name    : string,
	...args : [...A, SyncAction<A>]
) => void;

export interface IterateTest {
	async : Async;
	sync  : Sync;
}

const constructor = <I extends IterateTypes[keyof IterateTypes]>(
	verbose      : boolean,
	iterateTypes : I,
): I extends IterateTypes["async"] ? Async : Sync => function test<A extends readonly Source[]>(
	name    : string,
	...args : [...A, I extends IterateTypes["async"] ? AsyncAction<A> : SyncAction<A>]
) {
	if (args.length < 2)
		it(name, args[0] as any);
	else if (verbose) {
		// We shouldn't return anything to `describe`.
		describe(name, () => {iterateTypes(...args as any)});
	} else {
		// But in this case, we should return iterateTypes' promise.
		it(name, () => iterateTypes(...args as any));
	}
} as any;

const verbose = !!process.env["VERBOSE"];

export const iterateTest: IterateTest = {
	async : constructor(verbose, iterateTypes.async),
	sync  : constructor(verbose, iterateTypes.sync),
};