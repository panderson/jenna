type AsyncIterableTypes<T> = readonly T[] | AsyncIterable<T> | AsyncGenerator<T>;
type SyncIterableTypes<T>  = readonly T[] | Iterable<T> | Generator<T>;

export type IterableType = "string" | "array" | "set" | "iterable" | "generator";

export type Source = string | Set<any> | readonly any[];

export type AsyncIteratedType<T extends Source> =
	T extends string ? string | AsyncIterableTypes<string> :
	T extends Set<infer U> ? Set<U> | AsyncIterableTypes<U> :
	T extends ReadonlyArray<infer U> ? AsyncIterableTypes<U> :
	never;

export type SyncIteratedType<T extends Source> =
	T extends string ? string | SyncIterableTypes<string> :
	T extends Set<infer U> ? Set<U> | SyncIterableTypes<U> :
	T extends ReadonlyArray<infer U> ? SyncIterableTypes<U> :
	never;

export type AsyncActionArgs<A extends readonly Source[]> = {
	[i in keyof A]: (source?: A[i]) => AsyncIteratedType<A[i]>;
} & {length: A["length"]};

export type SyncActionArgs<A extends readonly Source[]> = {
	[i in keyof A]: (source?: A[i]) => SyncIteratedType<A[i]>;
} & {length: A["length"]};

export type AsyncAction<A extends readonly Source[]> =
	(...args: AsyncActionArgs<A>) => void | Promise<void>;

export type SyncAction<A extends readonly Source[]> =
	(...args: SyncActionArgs<A>) => void;

export type AsyncIterateTest = <A extends readonly Source[]>(
	name: string,
	...args: [...A, AsyncAction<A>]
) => void;
export type SyncIterateTest  = <A extends readonly Source[]>(
	name: string,
	...args: [...A, SyncAction<A>]
) => void;