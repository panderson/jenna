import {AsyncIteratedType, IterableType, Source, SyncIteratedType} from "./types";

const asyncTimeout = (ms: number) => new Promise(resolve => setTimeout(resolve, ms));

const constructor = (quick: boolean) => function *iterableTypes<T extends Source>(
	source: T
): Generator<[IterableType, (source?: T) => AsyncIteratedType<T>]> {
	let array       : any[] | string;
	let checkSource : (newSource: T | undefined) => T;

	function toArray(newSource: T | undefined) {
		newSource = checkSource(newSource);
		if (newSource === source)
			return array;
		return Array.from(newSource);
	}

	if (typeof source === "string") {
		checkSource = newSource => {
			if (newSource === undefined)
				return source;
			if (typeof newSource === "string")
				return newSource;
			throw new Error("New source must also be a string.");
		};

		yield ["string", source => checkSource(source) as any];

		if (!quick)
			yield ["array", source => Array.from(checkSource(source)) as any];

		array  = source;
	} else if (source instanceof Set) {
		checkSource = newSource => {
			if (newSource === undefined)
				return source;
			if (newSource instanceof Set)
				return newSource;
			throw new Error("New source must also be a set.");
		};

		if (!quick)
			yield ["set", source => checkSource(source) as any];

		array = Array.from(source);
		yield ["array", toArray as any];
	} else {
		checkSource = newSource => {
			if (newSource === undefined)
				return source;
			if (Array.isArray(newSource))
				return newSource;
			throw new Error("New source must also be an array.");
		};

		yield ["array", source => checkSource(source) as any];
		array = source as any;
	}

	if (!quick) {
		yield [
			"iterable",
			source => {
				const array = toArray(source);
				return {
					[Symbol.asyncIterator](): AsyncIterator<any> {
						const {length} = array;
						let i = 0;
						return {
							async next(): Promise<IteratorResult<any>> {
								await asyncTimeout(0);
								if (i === length)
									return {done: true, value: undefined};
								return {done: false, value: array[i++]};
							},
						};
					},
				} as any;
			},
		];
	}

	yield [
		"generator",
		async function * (source?: T) {
			const array = toArray(source);
			for (let i = 0; i < array.length; i++) {
				await asyncTimeout(0);
				yield array[i];
			}
		} as any
	];
};

const quick = !!process.env["QUICK"];

export const iterableTypes = {
	async : constructor(quick),
	sync  : Function(
		`"use strict";return ${
			constructor.toString()
			.replace("asyncIterator", "iterator")
			.replace(/await asyncTimeout\(0\);?\s*|async\s*/g, '')
		}`
	)()(quick) as <T extends Source>(
		source: T
	) => Generator<[IterableType, (source?: T) => SyncIteratedType<T>]>,
};