import {iterableTypes}                   from "./iterable-types";
import {AsyncAction, Source, SyncAction} from "./types";

type IterableTypes = typeof iterableTypes.async;

type Async = <A extends readonly Source[]>(...args: [...A, AsyncAction<A>]) => Promise<void>;
type Sync  = <A extends readonly Source[]>(...args: [...A,  SyncAction<A>]) => void;

export interface IterateTypes {
	async : Async;
	sync  : Sync;
}

const constructor = (
	verbose       : boolean,
	iterableTypes : IterableTypes,
): Async => async function(...args) {
	const action  = args.pop() as AsyncAction<any>;
	const types   = (args as Source[]).map(source => Array.from(iterableTypes(source)));
	const indices = types.map(() => 0);

	while (true) {
		const inputs = indices.map((j, i) => types[i][j][1]);

		if (verbose) {
			const name = indices.map((j, i) => types[i][j][0]).join(", ");
			it(name, () => action(...inputs));
		} else
			await action(...inputs);

		for (let i = 0; i < indices.length; i++) {
			if (++indices[i] < types[i].length)
				break;
			indices[i] = 0;
		}
		if (!indices.some(j => j))
			break;
	}
};

const verbose = !!process.env["VERBOSE"];

export const iterateTypes: IterateTypes = {
	async : constructor(verbose, iterableTypes.async),
	sync  : Function(`"use strict";return ${
		constructor.toString().replace(/\ba(?:sync|wait)\s*/g, '')
	}`)()(verbose, iterableTypes.sync) as Sync,
};