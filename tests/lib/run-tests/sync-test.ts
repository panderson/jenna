import {AsyncTest, SyncTest} from "./types";

export function syncTest<T extends object = {}>(asyncTest: AsyncTest<T>): SyncTest<T> {
	const fnText = asyncTest.toString().replace(/\ba(?:sync|wait)\s*|\.rejects\b/g, '');
	return Function(`"use strict";return ${fnText}`)() as SyncTest<T>;
}