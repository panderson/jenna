import {AsyncIterable, Iterable}                             from "#types";
import {extractContext}                                      from "../context";
import {defaultAsyncContext, defaultSyncContext}             from "./context";
import {IterateTest, iterateTest, iterateTypes}              from "./iterate";
import {syncTest}                                            from "./sync-test";
import {AsyncIterableClass, AsyncOperatorContext, AsyncTest} from "./types";

type IterateTestAsync = IterateTest["async"];
type IterateTestSync  = IterateTest["sync"];

type DescribeVerboseCallbackAsync = (test: IterateTestAsync) => void | Promise<void>;
type DescribeVerboseCallbackSync  = (test: IterateTestSync)  => void;

type DescribeVerboseAsync = (description: string, fn: DescribeVerboseCallbackAsync) => void;
type DescribeVerboseSync  = (description: string, fn: DescribeVerboseCallbackSync)  => void;

declare global {
	namespace jest {
		interface Describe {
			verbose: DescribeVerboseAsync;
		}

		interface It extends IterateTestAsync {}
	}
}

const verbose = !!process.env["VERBOSE"];

export function runTests(asyncTest: AsyncTest): void;
export function runTests<O extends keyof AsyncIterableClass>(
	operator  : O,
	asyncTest : AsyncTest<AsyncOperatorContext<O>>,
): void;
export function runTests<T extends object, U extends object>(
	context   : T | {async: T, sync: U},
	asyncTest : AsyncTest<T>,
): void;
export function runTests<O extends keyof AsyncIterableClass, T extends object, U extends object>(
	operator  : O,
	context   : T | {async: T, sync: U},
	asyncTest : AsyncTest<T & AsyncOperatorContext<O>>,
): void;

export function runTests<O extends keyof AsyncIterableClass, T extends object, U extends object>(
	...args:
		| [AsyncTest]
		| [O, AsyncTest]
		| [T | {async: T, sync: U}, AsyncTest]
		| [O, T | {async: T, sync: U}, AsyncTest]
): void {
	const {test}           = global;
	const asyncIterateTest = Object.assign(iterateTest.async, test);
	const syncIterateTest  = Object.assign(iterateTest.sync,  test);

	const [asyncDescribeVerbose, syncDescribeVerbose]: [DescribeVerboseAsync, DescribeVerboseSync] =
		verbose ? [
			// For these, don't return whatever `fn` does.
			(description, fn) => describe(description, () => { fn(iterateTest.async) }),
			(description, fn) => describe(description, () => { fn(iterateTest.sync)  }),
		] : [
			// But for these, return the function's promise, if applicable.
			(name, fn) => test(name, () => fn((_, ...args) => iterateTypes.async(...args))),
			(name, fn) => test(name, () => fn((_, ...args) => iterateTypes.sync (...args))),
		];

	function async(fn: () => void): void {
		global.test = asyncIterateTest;
		describe.verbose = asyncDescribeVerbose;
		describe("async", () => { fn() }); // Don't return its promise, if applicable.
	}

	function sync(fn: () => void): void {
		global.test = syncIterateTest;
		describe.verbose = syncDescribeVerbose as any;
		describe("sync", () => { fn() }); // Don't return its promise, if applicable.
	}

	switch (args.length) {
	case 1: {
		const [asyncTest] = args;

		async(() => asyncTest(defaultAsyncContext));
		sync (() => syncTest(asyncTest)(defaultSyncContext));
		break;
	}
	case 2: {
		const [context, asyncTest] = args;
		if (typeof context === "string") {
			const operator = context;

			async(() => asyncTest({...defaultAsyncContext, [operator]: AsyncIterable[operator]}));
			sync (() => syncTest(asyncTest)({...defaultSyncContext, [operator]: Iterable[operator]}));
		} else {
			const {asyncContext, syncContext} = extractContext(context);

			async(() => asyncTest({...defaultAsyncContext, ...asyncContext}));
			sync (() => syncTest(asyncTest)({...defaultSyncContext, ...syncContext}));
		}
		break;
	}
	case 3: {
		const [operator, context, asyncTest] = args;
		const {asyncContext, syncContext}    = extractContext(context);

		async(() => asyncTest({...defaultAsyncContext, [operator]: AsyncIterable[operator], ...asyncContext}));
		sync (() => syncTest(asyncTest)({...defaultSyncContext, [operator]: Iterable[operator], ...syncContext}));
		break;
	}}

	global.test = test;
}