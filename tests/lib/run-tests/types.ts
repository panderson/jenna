import {AsyncIterable, Iterable}     from "#types";
import {empty}                       from "../empty";
import {AsyncProtocol, SyncProtocol} from "../protocol";
import {TestError}                   from "../test-error";

export type AsyncIterableClass = typeof AsyncIterable;
export type IterableClass      = typeof Iterable;
export type TestErrorClass     = typeof TestError;

type AsyncEmpty = typeof empty.async;
type SyncEmpty  = typeof empty.sync;

export type AsyncContext<T extends object = {}> = T & {
	AsyncIterable : AsyncIterableClass;
	error         : TestErrorClass;
	protocol      : AsyncProtocol;
	empty         : AsyncEmpty;
};
export type AsyncOperatorContext<O extends keyof AsyncIterableClass> = {
	[K in O]: AsyncIterableClass[K];
};

export type SyncContext<T extends object = {}> = T & {
	AsyncIterable : IterableClass,
	error         : TestErrorClass;
	protocol      : SyncProtocol;
	empty         : SyncEmpty;
};
export type SyncOperatorContext<O extends keyof IterableClass> = {
	[K in O]: IterableClass[K];
};

export type AsyncTest<T extends object = {}> = (context: AsyncContext<T>) => void;
export type SyncTest <T extends object = {}> = (context: SyncContext<T>)  => void;