import {AsyncIterable, Iterable} from "#types";
import {empty}                   from "../empty";
import {protocol}                from "../protocol";
import {TestError}               from "../test-error";
import {AsyncTest, SyncTest}     from "./types";

export const defaultAsyncContext: Parameters<AsyncTest>[0] = {
	AsyncIterable,
	error    : TestError,
	protocol : protocol.async,
	empty    : empty.async,
};
export const defaultSyncContext: Parameters<SyncTest>[0] = {
	AsyncIterable : Iterable,
	error         : TestError,
	protocol      : protocol.sync,
	empty         : empty.sync,
};