import {Handlers, MockHandlers} from "./handlers";

export type AsyncProtocol = {
	(string: string, handlers?: Handlers): AsyncIterable<string>;
	(string: string, returnValue: any, handlers: Handlers): AsyncIterable<string>;
	<T>(arrayLike: ArrayLike<T>, handlers?: Handlers): AsyncIterable<T>;
	<T>(arrayLike: ArrayLike<T>, returnValue: any, handlers: Handlers): AsyncIterable<T>;

	createHandlers(): MockHandlers;
};

export type SyncProtocol = {
	(string: string, handlers?: Handlers): Iterable<string>;
	(string: string, returnValue: any, handlers: Handlers): Iterable<string>;
	<T>(arrayLike: ArrayLike<T>, handlers?: Handlers): Iterable<T>;
	<T>(arrayLike: ArrayLike<T>, returnValue: any, handlers: Handlers): Iterable<T>;

	createHandlers(): MockHandlers;
};

type ElementType<T extends string | ArrayLike<any>> =
	T extends string ? string : T extends ArrayLike<infer U> ? U : never;

const asyncTimeout = (ms: number) => new Promise(resolve => setTimeout(resolve, ms));

function asyncProtocol(string: string, handlers?: Handlers): AsyncIterable<string>;
function asyncProtocol(string: string, returnValue: any, handlers: Handlers): AsyncIterable<string>;
function asyncProtocol<T>(arrayLike: ArrayLike<T>, handlers?: Handlers): AsyncIterable<T>;
function asyncProtocol<T>(arrayLike: ArrayLike<T>, returnValue: any, handlers: Handlers): AsyncIterable<string>;

function asyncProtocol<T extends string | ArrayLike<any>>(
	iterable : T,
	...args  : [handlers?: Handlers] | [returnValue: any, handlers: Handlers]
): AsyncIterable<ElementType<T>> {
	const length = iterable.length;
	const [returnValue, handlers] = args.length === 1 ? [undefined, args[0]] : args;

	return {
		[Symbol.asyncIterator]() {
			let i = 0;

			return {
				async next(value?: any): Promise<IteratorResult<ElementType<T>>> {
					await asyncTimeout(0);
					if (!i)
						handlers?.onStart?.();
					else
						handlers?.onNext?.(iterable[i - 1], value);

					if (i === length) {
						handlers?.onDone?.();
						handlers?.onReturn?.(true);
						return {done: true, value: returnValue};
					}

					if (i > length)
						return {done: true, value: returnValue};

					return {done: false, value: iterable[i++]};
				},

				async return(value?: any): Promise<IteratorResult<ElementType<T>>> {
					await asyncTimeout(0);
					i = length + 1;
					handlers?.onReturn?.();
					return {done: true, value};
				},

				async throw(E?: any): Promise<IteratorResult<ElementType<T>>> {
					await asyncTimeout(0);
					i = length + 1;
					handlers?.onThrow?.(E);
					throw E;
				},
			};
		},
	};
}

asyncProtocol.createHandlers = () => ({
	onStart  : jest.fn(),
	onNext   : jest.fn(),
	onDone   : jest.fn(),
	onReturn : jest.fn(),
	onThrow  : jest.fn(),
});

const syncProtocol: SyncProtocol = Function(`"use strict";return ${
	asyncProtocol.toString()
	.replace(/await asyncTimeout\(0\)|async /g, '')
	.replace("asyncIterator", "iterator")
}`)() as any;

syncProtocol.createHandlers = asyncProtocol.createHandlers;

export const protocol: {async: AsyncProtocol, sync: SyncProtocol} =
	{async: asyncProtocol, sync: syncProtocol};