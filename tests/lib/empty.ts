const quick = !!process.env["QUICK"];

class EmptySet extends Set {
	add(_value: any): this {
		throw new TypeError("This set is empty and immutable.");
	}
}

class EmptyMap extends Map {
	set(_key: any, _value: any): this {
		throw new TypeError("This map is empty and immutable.");
	}
}

const emptyIteratorResult: IteratorResult<never> = Object.freeze({done: true, value: undefined});

const _empty = Object.freeze({
	string : "",
	array  : Object.freeze([]),
	set    : new EmptySet(),
	map    : new EmptyMap(),
});

export const empty = Object.freeze({
	async: {
		..._empty,

		iterable: Object.freeze({
			[Symbol.asyncIterator]: () => ({next: async () => emptyIteratorResult}),
		}) as AsyncIterable<never>,

		get generator() {
			async function *generator(): AsyncGenerator<never> {};
			return generator();
		},

		*[Symbol.iterator]() {
			if (!quick)
				yield ["string",    this.string]    as const;

			yield ["array",     this.array]     as const;

			if (!quick) {
				yield ["set",       this.set]       as const;
				yield ["map",       this.map]       as const;
				yield ["iterable",  this.iterable]  as const;
			}

			yield ["generator", this.generator] as const;
		},
	},
	sync: {
		..._empty,

		iterable: Object.freeze({
			[Symbol.iterator]: () => ({next: () => emptyIteratorResult}),
		}) as Iterable<never>,

		get generator() {
			function *generator(): Generator<never> {};
			return generator();
		},

		*[Symbol.iterator]() {
			if (!quick)
				yield ["string",    this.string]    as const;

			yield ["array",     this.array]     as const;

			if (!quick) {
				yield ["set",       this.set]       as const;
				yield ["map",       this.map]       as const;
				yield ["iterable",  this.iterable]  as const;
			}

			yield ["generator", this.generator] as const;
		},
	},
});