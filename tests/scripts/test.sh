#!/bin/bash

for arg do
	shift
	case $arg in
		all|--all|-a)
			set -- "$@" --watch-all
			;;
		quick|--quick|-q)
			export QUICK=true
			;;
		verbose|--verbose|-v)
			export VERBOSE=true
			;;
		watch|--watch|-w)
			set -- "$@" --watch
			;;
		-[aqvw]*)
			if [[ $arg = *a* ]]; then
				set -- "$@" --watch-all
				arg=${arg//a}
			fi
			if [[ $arg = *q* ]]; then
				export QUICK=true
				arg=${arg//q}
			fi
			if [[ $arg = *v* ]]; then
				export VERBOSE=true
				arg=${arg//v}
			fi
			if [[ $arg = *w* ]]; then
				set -- "$@" --watch
				arg=${arg//w}
			fi
			if [[ $arg != - ]]; then
				echo >&2 "Unrecognized option '$arg'"
				exit 1
			fi
			;;
		*)
			set -- "$@" "$arg"
			;;
	esac
done

jest "$@"