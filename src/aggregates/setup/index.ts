import {Tag}                 from "#tags";
import {OperatorKey}         from "@operators/setup";
import {syncFunc}            from "@operators/setup/generate";
import {buildImplementation} from "@operators/setup/utils";
import {
	Aggregate,
	AggregateKey,
	Builder,
	Definitions,
	Implementation,
	MapType,
	Options,
	Template,
} from "./types";

export function aggregate<K extends AggregateKey>(
	aggregate : K,
	template  : Template<K>,
): Aggregate<K>;

export function aggregate<
	K extends AggregateKey,
	I extends boolean                = false,
	A extends readonly any[]         = [],
	O extends readonly OperatorKey[] = [],
>(
	aggregate : K,
	options   : Options<I, A, O>,
	build     : Builder<K, I, A, O>,
): Aggregate<K>;

export function aggregate<
	K extends AggregateKey,
	I extends boolean                = false,
	A extends readonly any[]         = [],
	O extends readonly OperatorKey[] = [],
>(
	_aggregate : K,
	...args    : [Template<K>] | [Options<I, A, O>, Builder<K, I, A, O>]
): Aggregate<K> {
	let template : Template<K>;
	let async    : Implementation<K, Tag.InstanceAsync>;
	let sync     : Implementation<K, Tag.InstanceSync>;

	if (args.length === 1) {
		[template] = args;
	} else {
		const [options, build] = args;
		template = buildImplementation(options, build);
	}

	if (typeof template === "object" && "async" in template) {
		async = buildAsync(template.async);
		sync  = buildSync(template.sync, template.async);
	} else {
		async = buildAsync(template);
		sync  = buildSync(undefined, template);
	}

	return {
		[Tag.InstanceSync]  : sync,
		[Tag.InstanceAsync] : async,
	};
}

function buildAsync<K extends AggregateKey>(
	template: Definitions<K>["async"]
): Implementation<K, Tag.InstanceAsync> {
	if (typeof template === "function")
		return template;

	const fn = template.map;

	return async function(
		this    : ThisParameterType<Implementation<K, Tag.InstanceAsync>>,
		...args : Parameters<Implementation<K, Tag.InstanceAsync>>
	): Promise<MapType<Implementation<K, Tag.InstanceAsync>>> {
		const map = new Map() as MapType<Implementation<K, Tag.InstanceAsync>>;
		await fn.call(this, map, ...args);
		return map;
	} as any;
}

function buildSync<K extends AggregateKey>(
	template : Definitions<K>["sync"] | undefined,
	async    : Definitions<K>["async"],
): Implementation<K, Tag.InstanceSync> {
	if (!template) {
		if (typeof async === "function")
			return syncFunc(async) as any;

		return buildSync({map: syncFunc(async.map) as any}, async);
	}

	if (typeof template === "function")
		return template;

	const fn = template.map;

	return function (
		this    : ThisParameterType<Implementation<K, Tag.InstanceSync>>,
		...args : Parameters<Implementation<K, Tag.InstanceSync>>
	): MapType<Implementation<K, Tag.InstanceSync>> {
		const map = new Map() as MapType<Implementation<K, Tag.InstanceSync>>;
		fn.call(this, map, ...args);
		return map;
	} as any;
}