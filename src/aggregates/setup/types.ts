import {Tag}                                from "#tags";
import {Operator, OperatorKey, OpFuncs}     from "@operators/setup";
import {Aggregates}                         from "@aggregates";
import {Func, IsTrue, Matched, UnasyncArgs} from "@utils";

export type AggregateKey = Exclude<keyof Aggregates<Tag.Instance, unknown, unknown>, "items">;

export type Implementation<K extends AggregateKey, T extends Tag.Instance> =
	Extract<Aggregates<T, unknown, unknown>[K], Function>;

export type MapType<F extends Func> =
	ReturnType<F> extends Map<infer K, infer V> | Promise<Map<infer K, infer V>>
		? Map<K, V> : never;

interface WithMap<F extends Func> {
	map(
		this    : ThisParameterType<F>,
		map     : MapType<F>,
		...rest : Parameters<F>
	): ReturnType<F> extends Promise<any> ? Promise<void> : void;
}

type FuncDefinition<F extends Func> = F | WithMap<F>;

export interface Definitions<K extends AggregateKey> {
	sync  : FuncDefinition<Implementation<K, Tag.InstanceSync>>;
	async : FuncDefinition<Implementation<K, Tag.InstanceAsync>>;
}

export type Template<K extends AggregateKey> =
	Definitions<K> | FuncDefinition<Implementation<K, Tag.InstanceAsync>>;

export type Aggregate<K extends AggregateKey> = {
	[T in Tag.Instance]: Implementation<K, T>;
};

type IteratorParam<I extends boolean> = IsTrue<I, [typeof Matched.iterator], []>;
type Args<I extends boolean, A extends readonly any[], O extends readonly OperatorKey[]> =
	[...IteratorParam<I>, ...A, ...OpFuncs<O>];

export type Builder<
	K extends AggregateKey,
	I extends boolean                = false,
	A extends readonly any[]         = [],
	O extends readonly OperatorKey[] = [],
> = (...args: Args<I, A, O>) => Template<K>;

export interface Options<
	I extends boolean                = false,
	A extends readonly any[]         = [],
	O extends readonly OperatorKey[] = [],
> {
	ops?      : {[N in keyof O]: Operator<O[N]>};
	iterator? : I;
	closure?  : A | {
		async : A;
		sync  : UnasyncArgs<A>;
	};
}