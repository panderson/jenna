import {Tag, Tagged} from "#tags";
import {Aggregates}  from ".";

export interface Mean<T extends Tag.Instance, K, I> {
	/** Returns the mean value of the elements associated with each key. */
	mean(this: Aggregates<T, K, I>): Tagged.Promise<T, Map<K, I>>;
}