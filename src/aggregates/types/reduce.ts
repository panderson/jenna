import {Tag, Tagged} from "#tags";
import {Aggregates}  from ".";

interface Initializer<K, I, R = I>        {(key: K, firstItem: I): R};
interface Reducer    <K, I, R = I>        {(key: K, prev: R, current: I, index: number): R};
interface Finalizer  <K, R, F = R>        {(key: K, result: R): F};
interface Params     <K, I, R = I, F = R> {
	reduce      : Reducer    <K, I, R>,
	initialize? : Initializer<K, I, R> | undefined,
	finalize?   : Finalizer  <K, R, F> | undefined,
};

export interface ReduceDocs {
	/**
	 * Calls the specified callback for all the elements in each keyed group.  The return value of
	 * the callback is the accumulated result, and is provided as an argument in the next call for
	 * an element with the given key.
	 */
	reduce: any;
}

export interface Reduce<T extends Tag.Instance, K, I> extends ReduceDocs {
	/**
	 * @param reduce A function that accepts up to four arguments.  It's called one time for each
	 * element in the iterable, except for the first element, which serves as the initial value of
	 * the accumulation.
	 * @returns The accumulated result, or `undefined` if the iterable is empty.
	 */
	reduce<R = I>(
		this   : Aggregates<T, K, I>,
		reduce : Reducer<K, I, R>,
	): Tagged.Promise<T, Map<K, R>>;

	/**
	 * @param params An object with the following properties:
	 *
	 *   * `reduce`: A function that accepts up to four arguments.  This function is required, and
	 *     is called one time for each element in the iterable.
	 *
	 *   * `initialize`: A function called with the first item in each keyed group of the iterable.
	 *     This function is optional.  If present, its return value becomes the initial value of the
	 *     accumulator.
	 *
	 *   * `finalize`: A function called with the final accumulated result of each group.  This
	 *     function is optional.  If present, its return value will be the final result.
	 */
	reduce<R = I, F = R>(
		this     : Aggregates<T, K, I>,
		params   : Params<K, I, R, F>,
	): Tagged.Promise<T, Map<K, F>>;
}