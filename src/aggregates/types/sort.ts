import {Tag, Tagged}   from "#tags";
import {AggregateSort} from "@utils";
import {Aggregates}    from ".";

export interface Sort<T extends Tag.Instance, K, I> {
	/**
	 * Returns a map containing sorted groups of elements matching each key in the iterable.  Groups
	 * are sorted using the default comparer, which uses `>` and `<` operators to compare items, and
	 * treats `null` as lower than all other values except `undefined`, which is considered even
	 * lower.
	 * @param reverse Whether to sort in reverse order.  The default value is `false`.
	 */
	sort(this: Aggregates<T, K, I>, reverse?: boolean | undefined): Tagged.Promise<T, Map<K, I[]>>;

	/**
	 * Returns a map containing sorted groups of elements matching each key in the iterable.
	 * @param comparer A function used to determine the order of the elements in each group.  It
	 * is expected to return a negative value if its second argument is less than its third
	 * argument, zero if they're equal, and a positive value otherwise.
	 */
	sort(
		this     : Aggregates<T, K, I>,
		comparer : AggregateSort.Comparer<K, I>,
	): Tagged.Promise<T, Map<K, I[]>>;
}