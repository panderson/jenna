import {Tag, Tagged} from "#tags";
import {Aggregates}  from ".";

export interface Sum<T extends Tag.Instance, K, I> {
	/** Returns the sum of the elements associated with each key. */
	sum(this: Aggregates<T, K, I>): Tagged.Promise<T, Map<K, I>>;
}