import {Tag, Tagged} from "#tags";
import {Aggregates}  from ".";

export interface Some<T extends Tag.Instance, K, I> {
	/**
	 * For each keyed group of members of the iterable, determine if any of them satisfy the
	 * specified test.
	 *
	 * @param predicate A function that accepts up to two arguments.  The `some` function calls the
	 * predicate for each element in the original iterable, passing the element and its key value.
	 * If the predicate returns a value coercible to the Boolean value `true` for any element
	 * associated with a given key, then the resulting map will have the value `true` associated
	 * with that key.  Otherwise, it will have the value `false`.
	 */
	some(
		this      : Aggregates<T, K, I>,
		predicate : (value: I, key: K) => boolean
	): Tagged.Promise<T, Map<K, boolean>>;
}