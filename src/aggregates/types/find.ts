import {Tag, Tagged} from "#tags";
import {Aggregates}  from ".";

interface FindDocs {
	/**
	 * For each keyed group in the sequence, finds the first element where `predicate` returns
	 * `true`, associating that element with given key in the resultant map.  If no such element is
	 * found, `undefined` is associated with the given key.
	 */
	find: any;
}

export interface Find<T extends Tag.Instance, K, I> extends FindDocs {
	/**
	 * @param predicate A function that accepts up to two arguments.  The `find` function calls the
	 * predicate for each element in the original iterable, passing the element and its key value.
	 * If the predicate returns a value coercible to `true`, the element will be associated with the
	 * given key, and it will be assumed to be the given type.
	 */
	find<J extends I>(
		this      : Aggregates<T, K, I>,
		predicate : (value: I, key: K) => value is J
	): Tagged.Promise<T, Map<K, J>>;

	/**
	 * @param predicate A function that accepts up to two arguments.  The `find` function calls the
	 * predicate for each element in the original iterable, passing the element and its key value.
	 * If the predicate returns a value coercible to `true`, the element will be associated with the
	 * given key.
	 */
	 find(
		this      : Aggregates<T, K, I>,
		predicate : (value: I, key: K) => boolean
	): Tagged.Promise<T, Map<K, I>>;
}