import {Tag, Tagged} from "#tags";
import {Aggregates}  from ".";

export interface Count<T extends Tag.Instance, K, I> {
	/** Returns a map containing the count of elements matching each key in the iterable. */
	count(this: Aggregates<T, K, I>): Tagged.Promise<T, Map<K, number>>;
}