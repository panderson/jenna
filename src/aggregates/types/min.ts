import {Tag, Tagged} from "#tags";
import {Aggregates}  from ".";

export interface Min<T extends Tag.Instance, K, I> {
	/** Returns the minimum valued element associated with each key. */
	min(this: Aggregates<T, K, I>): Tagged.Promise<T, Map<K, I>>;
}