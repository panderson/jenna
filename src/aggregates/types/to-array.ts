import {Tag, Tagged} from "#tags";
import {Aggregates}  from ".";

export interface ToArray<T extends Tag.Instance, K, I> {
	/** Materializes the elements associated with each key to arrays. */
	toArray(this: Aggregates<T, K, I>): Tagged.Promise<T, Map<K, I[]>>;
}