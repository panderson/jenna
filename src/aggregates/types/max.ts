import {Tag, Tagged} from "#tags";
import {Aggregates}  from ".";

export interface Max<T extends Tag.Instance, K, I> {
	/** Returns the maximum valued element associated with each key. */
	max(this: Aggregates<T, K, I>): Tagged.Promise<T, Map<K, I>>;
}