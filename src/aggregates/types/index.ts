import {Tag, Tagged} from "#tags";

import {Count}    from "./count";
import {Every}    from "./every";
import {Find}     from "./find";
import {First}    from "./first";
import {Flatten}  from "./flatten";
import {Join}     from "./join";
import {Last}     from "./last";
import {Max}      from "./max";
import {Mean}     from "./mean";
import {Min}      from "./min";
import {Reduce}   from "./reduce";
import {Some}     from "./some";
import {Sort}     from "./sort";
import {SortBy}   from "./sort-by";
import {Sum}      from "./sum";
import {ToArray}  from "./to-array";
import {ToMap}    from "./to-map";
import {ToRecord} from "./to-record";
import {ToSet}    from "./to-set";

export interface Aggregates<T extends Tag.Instance, K, I> {
	items: Tagged.Iterable<T, [K, I]>;

	count    : Count   <T, K, I>["count"];
	every    : Every   <T, K, I>["every"];
	find     : Find    <T, K, I>["find"];
	first    : First   <T, K, I>["first"];
	flatten  : Flatten <T, K, I>["flatten"];
	join     : Join    <T, K, I>["join"];
	last     : Last    <T, K, I>["last"];
	max      : Max     <T, K, I>["max"];
	mean     : Mean    <T, K, I>["mean"];
	min      : Min     <T, K, I>["min"];
	reduce   : Reduce  <T, K, I>["reduce"];
	some     : Some    <T, K, I>["some"];
	sort     : Sort    <T, K, I>["sort"];
	sortBy   : SortBy  <T, K, I>["sortBy"];
	sum      : Sum     <T, K, I>["sum"];
	toArray  : ToArray <T, K, I>["toArray"];
	toMap    : ToMap   <T, K, I>["toMap"];
	toRecord : ToRecord<T, K, I>["toRecord"];
	toSet	 : ToSet   <T, K, I>["toSet"];
}

export type AggregateBy<T extends Tag, K, I> =
	T extends Tag.Sync  ? Aggregates<Tag.InstanceSync,  K, I> :
	T extends Tag.Async ? Aggregates<Tag.InstanceAsync, K, I> :
	never;