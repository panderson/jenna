import {Tag, Tagged} from "#tags";
import {Aggregates}  from ".";

export interface Last<T extends Tag.Instance, K, I> {
	/** Returns the last element associated with each key. */
	last(this: Aggregates<T, K, I>): Tagged.Promise<T, Map<K, I>>;
}