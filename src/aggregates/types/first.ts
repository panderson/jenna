import {Tag, Tagged} from "#tags";
import {Aggregates}  from ".";

export interface First<T extends Tag.Instance, K, I> {
	/** Returns the first element associated with each key. */
	first(this: Aggregates<T, K, I>): Tagged.Promise<T, Map<K, I>>;
}