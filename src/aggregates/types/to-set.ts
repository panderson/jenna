import {Tag, Tagged} from "#tags";
import {Aggregates}  from ".";

export interface ToSet<T extends Tag.Instance, K, I> {
	/** Materializes the elements associated with each key to sets. */
	toSet(this: Aggregates<T, K, I>): Tagged.Promise<T, Map<K, Set<I>>>;
}