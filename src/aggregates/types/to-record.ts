import {Tag, Tagged} from "#tags";
import {Aggregates}  from ".";

interface ToRecordDocs {
	/** Materializes the elements associated with each key to maps. */
	toRecord: any;
}

export interface ToRecord<T extends Tag.Instance, K, I> extends ToRecordDocs {
	toRecord<K2 extends keyof any>(
		this        : Aggregates<T, K, I>,
		keySelector : (item: I, groupKey: K) => K2
	): Tagged.Promise<T, Map<K, Record<K2, I>>>;

	toRecord<R extends Record<any, any>>(
		this          : Aggregates<T, K, I>,
		keySelector   : (item: I, groupKey: K) => keyof R,
		valueSelector : <K2 extends keyof R>(item: I, groupKey: K, key: K2) => R[K],
	): Tagged.Promise<T, Map<K, R>>;
}