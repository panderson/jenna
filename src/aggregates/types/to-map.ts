import {Tag, Tagged} from "#tags";
import {Aggregates}  from ".";

interface ToMapDocs {
	/** Materializes the elements associated with each key to maps. */
	toMap: any;
}

export interface ToMap<T extends Tag.Instance, K, I> extends ToMapDocs {
	toMap<K2>(
		this        : Aggregates<T, K, I>,
		keySelector : (item: I, groupKey: K) => K2
	): Tagged.Promise<T, Map<K, Map<K2, I>>>;

	toMap<K2, V>(
		this          : Aggregates<T, K, I>,
		keySelector   : (item: I, groupKey: K) => K2,
		valueSelector : (item: I, groupKey: K, key: K2) => V,
	): Tagged.Promise<T, Map<K, Map<K2, V>>>;
}