import {Tag, Tagged} from "#tags";
import {Aggregates}  from ".";

export interface Join<T extends Tag.Instance, K, I> {
	/**
	 * Concatenates all of the elements in each keyed group to a string, separated by the specified
	 * separator string, if given.
	 * @param separator A string used to separate one element of the iterable from the next in the
	 * resulting string.  If omitted, the elements are joined without a separator.
	 */
	join(
		this       : Aggregates<T, K, I>,
		separator? : string,
	): Tagged.Promise<T, Map<K, string>>;
}