import {Tag, Tagged}   from "#tags";
import {AggregateSort} from "@utils";
import {Aggregates}    from ".";

interface SortByDocs {
	/** Returns a map containing sorted groups of elements matching each key in the iterable. */
	sortBy: any;
}

export interface SortBy<T extends Tag.Instance, K, I> extends SortByDocs {
	/**
	 * @param keySelector A function used to select a key from each item.  The iterable is
	 * sorted based on a strict ordered comparison of these keys, using the default comparer as
	 * described for the `sort` method.
	 * @param reverse Whether to sort in reverse order.  The default value is `false`.
	 */
	sortBy<K2>(
		this        : Aggregates<T, K, I>,
		keySelector : (groupKey: K, item: I) => K2,
		reverse?    : boolean | undefined,
	): Tagged.Promise<T, Map<K, I[]>>;

	/**
	 * @param keySelector A function used to select a key from each item.  The iterable is
	 * sorted based on a strict ordered comparison of these keys.
	 * @param comparer A function used to compare keys, similar to the item compare function in
	 * the `sort` method, except that it receives keys instead of the original items.
	 * @param reverse Whether to sort in reverse order.  The default value is `false`.
	 */
	sortBy<K2>(
		this        : Aggregates<T, K, I>,
		keySelector : (groupKey: K, item: I) => K2,
		comparer    : AggregateSort.Comparer<K, K2>,
		reverse?    : boolean | undefined,
	): Tagged.Promise<T, Map<K, I[]>>;
}