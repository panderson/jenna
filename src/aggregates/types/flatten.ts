import {Tag, Tagged} from "#tags";
import {Aggregates}  from ".";

type Elements<T extends Tag, I, Else = never> =
	I extends Tagged.Iterable<T, infer J> ? J : Else;

interface FlattenDocs {
	/**
	 * For each keyed group of members of an iterable, flatten the iterables selected from their
	 * elements into a new one-dimensional array.
	 */
	flatten: any;
}

interface FlattenIterables<
	T extends Tag.Instance,
	K,
	I extends Tagged.Iterable<T, any>
> extends FlattenDocs {
	/**
	 * @param selector A function that takes up to two arguments and returns an iterable object.
	 * The `flatten` function calls this selector for each element in the original iterable, passing
	 * the element and its key value, and then yields each element in each resulting iterable.
	 */
	flatten<J = Elements<T, I, any>>(
		this      : Aggregates<T, K, I>,
		selector? : (item: I, key: K) => Tagged.Iterable<T, J>
	): Tagged.Promise<T, Map<K, J[]>>;
}

interface FlattenSelected<T extends Tag.Instance, K, I> extends FlattenDocs {
	/**
	 * @param selector A function that takes up to two arguments and returns an iterable object.
	 * The `flatten` function calls this selector for each element in the original iterable, passing
	 * the element and its key value, and then yields each element in each resulting iterable.
	 */
	 flatten<J>(
		this     : Aggregates<T, K, I>,
		selector : (item: I, key: K) => Tagged.Iterable<T, J>
	): Tagged.Promise<T, Map<K, J[]>>;
}

export type Flatten<T extends Tag.Instance, K, I> =
	I extends Tagged.Iterable<T, any>
		? FlattenIterables<T, K, I>
		: FlattenSelected<T, K, I>;