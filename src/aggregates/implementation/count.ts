import {aggregate} from "../setup";

export const count = aggregate("count", {async map(map) {
	for await (const [key] of this.items)
		map.set(key, (map.get(key) ?? 0) + 1);
}});