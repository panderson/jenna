import {aggregate} from "../setup";

export const join = aggregate("join", {async map(map, separator) {
	for await (const [key, value] of this.items) {
		const current = map.get(key);
		if (current === undefined)
			map.set(key, String(value));
		else
			map.set(key, current + separator + String(value));
	}
}});