import {aggregate} from "../setup";

export const reduce = aggregate("reduce", {async map(map, params) {
	if (typeof params === "function")
		params = {reduce: params};

	if (!params.initialize)
		params.initialize = (_, i) => i;

	const {reduce, initialize, finalize} = params;

	const indices = new Map<unknown, number>();

	for await (const [key, value] of this.items) {
		const index = indices.default(key, 0);
		if (!index)
			map.set(key, initialize(key, value));
		else
			map.set(key, reduce(key, map.get(key), value, index));
		indices.set(key, index + 1);
	}

	if (finalize) {
		for (const [key, value] of map)
			map.set(key, finalize(key, value));
	}
}});