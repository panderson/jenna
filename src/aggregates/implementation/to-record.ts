import {aggregate} from "../setup";

export const toRecord = aggregate("toRecord", {
	async map(map, keySelector, valueSelector?) {
		for await (const [groupKey, item] of this.items) {
			const key   = keySelector(item, groupKey);
			const value = valueSelector ? valueSelector(item, groupKey, key) : item;
			let   obj   = map.get(groupKey);

			if (obj)
				obj[key] = value;
			else
				map.set(groupKey, {[key]: value});
		}
	}
});