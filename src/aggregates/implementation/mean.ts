import {aggregate} from "../setup";

export const mean = aggregate("mean", {async map(map) {
	const sums = new Map<unknown, [number, unknown]>();
	for await (const [key, value] of this.items) {
		const current = sums.get(key);
		if (current) {
			current[0]++;
			(current[1] as any) += value;
		} else
			sums.set(key, [1, value]);
	}

	for (const [key, [count, sum]] of sums.entries())
		map.set(key, (sum as any) / count);
}});