import {aggregate} from "../setup";

export const find = aggregate("find", {async map(map, predicate) {
	for await (const [key, value] of this.items) {
		if (map.has(key)) {
			if (map.get(key) !== undefined && predicate(value, key))
				map.set(key, value);
		} else if (predicate(value, key))
			map.set(key, value);
	}
}});