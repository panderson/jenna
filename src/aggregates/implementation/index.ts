import {Tag}        from "#tags";
import {Aggregates} from "../types";
import * as all     from "./@all";

type AggregateImplementations = {
	[$ in Tag.Instance]: Aggregates<$, unknown, unknown>;
};

export const aggregates: AggregateImplementations =
	Object.fromEntries(
		Tag.Instance.map(tag => [
			tag,
			Object.fromEntries(
				Object.entries(all).map(([name, aggregate]) => [name, aggregate[tag]])
			)
		])
	) as any;