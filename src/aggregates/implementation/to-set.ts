import {aggregate} from "../setup";

export const toSet = aggregate("toSet", {async map(map) {
	for await (const [key, value] of this.items) {
		const set = map.get(key);
		if (set)
			set.add(value);
		else
			map.set(key, new Set([value]));
	}
}});