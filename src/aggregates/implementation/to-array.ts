import {aggregate} from "../setup";

export const toArray = aggregate("toArray", {async map(map) {
	for await (const [key, value] of this.items) {
		const list = map.get(key);
		if (list)
			list.push(value);
		else
			map.set(key, [value]);
	}
}});