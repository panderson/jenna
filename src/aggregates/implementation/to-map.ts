import {aggregate} from "../setup";

export const toMap = aggregate("toMap", {async map(map, keySelector, valueSelector?) {
	for await (const [groupKey, item] of this.items) {
		const key      = keySelector(item, groupKey);
		const value    = valueSelector ? valueSelector(item, groupKey, key) : item;
		let   groupMap = map.get(groupKey);

		if (groupMap)
			groupMap.set(key, value);
		else
			map.set(groupKey, new Map([[key, value]]));
	}
}});