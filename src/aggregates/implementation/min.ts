import {aggregate} from "../setup";

export const min = aggregate("min", {async map(map) {
	for await (const [key, value] of this.items) {
		const current = map.get(key);
		if (current === undefined || current as any > (value as any))
			map.set(key, value);
	}
}});