import {aggregate} from "../setup";

export const some = aggregate("some", {async map(map, predicate) {
	for await (const [key, value] of this.items) {
		const current = map.get(key);
		if (!current) {
			if (predicate(value, key))
				map.set(key, true);
			else if (current === undefined)
				map.set(key, false);
		}
	}
}});