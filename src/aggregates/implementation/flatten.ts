import {toArray}   from "@operators/implementation/to-array";
import {aggregate} from "../setup";

export const flatten = aggregate("flatten", {ops: [toArray]}, toArray => ({
	async map(map, selector?) {
		if (!selector)
			selector = x => x as any;

		for await (const [key, value] of this.items) {
			const values = selector(value, key);
			const list = map.get(key);

			if (list) {
				for await (const j of values)
					list.push(j);
			} else
				map.set(key, await toArray.call(null, values));
		}
	}
}));