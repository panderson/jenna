import {aggregate} from "../setup";

export const sum = aggregate("sum", {async map(map) {
	for await (const [key, value] of this.items) {
		const current = map.get(key);
		map.set(key, current === undefined ? value : current as any + value);
	}
}});