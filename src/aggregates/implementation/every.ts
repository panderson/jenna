import {aggregate} from "../setup";

export const every = aggregate("every", {async map(map, predicate) {
	for await (const [key, value] of this.items) {
		const current = map.get(key);
		if (current !== false) {
			if (predicate(value, key)) {
				if (!current)
					map.set(key, true);
			} else
				map.set(key, false);
		}
	}
}});