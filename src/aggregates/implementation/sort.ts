import {AggregateSort} from "@utils";
import {aggregate}     from "../setup";

export const sort = aggregate("sort", {closure: [AggregateSort.sortArray]}, sortArray => ({
	async map(map, param) {
		for await (const [key, value] of this.items) {
			if (map.has(key))
				map.get(key)!.push(value);
			else
				map.set(key, [value]);
		}

		for (const [key, items] of map)
			sortArray(key, items, param);
	}
}));