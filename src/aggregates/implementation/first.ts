import {aggregate} from "../setup";

export const first = aggregate("first", {async map(map) {
	for await (const [key, value] of this.items) {
		if (!map.has(key))
			map.set(key, value);
	}
}});