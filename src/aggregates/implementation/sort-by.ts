import {Tag}           from "#tags";
import {AggregateSort} from "@utils";
import {aggregate}     from "../setup";
import {sort}          from "./sort";

const async   = [sort[Tag.InstanceAsync], AggregateSort.keySorter] as const;
const sync    = [sort[Tag.InstanceSync],  AggregateSort.keySorter] as const;
const closure = {async, sync};

export const sortBy = aggregate("sortBy", {closure}, (sort, keySorter) =>
	async function(keySelector, ...args) {
		const [comparer, reverse] = args.length === 2 || typeof args[0] !== "boolean"
			? args : [undefined, args[0]];

		return sort.call(
			this,
			keySorter(keySelector, comparer as any, reverse as any)
		);
	}
);