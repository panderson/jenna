import {aggregate} from "../setup";

export const last = aggregate("last", {async map(map) {
	for await (const [key, value] of this.items)
		map.set(key, value);
}});