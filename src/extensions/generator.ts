import {addMethods, Interfaces} from "#types/iterable/setup";
import {KeyOf, Methods}         from "@utils";

export const GeneratorFunction: GeneratorFunctionConstructor =
	Object.getPrototypeOf(function*(){}).constructor;
export const Generator: GeneratorFunction = GeneratorFunction.prototype;

declare global {
	interface Generator<T = unknown, TReturn = any, TNext = unknown> extends Interfaces {
		cast<T, R = any, N = unknown>(): Generator<T, R, N>;
	}
}

const methods: Methods<KeyOf<Generator>> = [
	["cast", function cast() {return this;}]
];

addMethods("generator", Generator);
addMethods("generator", Generator, methods);