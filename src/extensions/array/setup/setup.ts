import {Tag}             from "#tags";
import {All}             from "@operators";
import {buildAddMethods} from "@utils";
import * as array        from "../operators";
import {ArrayInterfaces} from "./interface";

const addMethods = buildAddMethods<ArrayInterfaces<any>>(Object.assign({},
	{
		by          : All[Tag.InstanceSync].by        ,
		except      : All[Tag.InstanceSync].except    ,
		extend      : All[Tag.InstanceSync].extend    ,
		first       : All[Tag.InstanceSync].first     ,
		flatten     : All[Tag.InstanceSync].flatten   ,
		groupBy     : All[Tag.InstanceSync].groupBy   ,
		innerJoin   : All[Tag.InstanceSync].innerJoin ,
		intersect   : All[Tag.InstanceSync].intersect ,
		last        : All[Tag.InstanceSync].last      ,
		leftJoin    : All[Tag.InstanceSync].leftJoin  ,
		max         : All[Tag.InstanceSync].max       ,
		mean        : All[Tag.InstanceSync].mean      ,
		min         : All[Tag.InstanceSync].min       ,
		notUntil    : All[Tag.InstanceSync].notUntil  ,
		notWhile    : All[Tag.InstanceSync].notWhile  ,
		omit        : All[Tag.InstanceSync].omit      ,
		pick        : All[Tag.InstanceSync].pick      ,
		present     : All[Tag.InstanceSync].present   ,
		sortBy      : All[Tag.InstanceSync].sortBy    ,
		split       : All[Tag.InstanceSync].split     ,
		sum         : All[Tag.InstanceSync].sum       ,
		toMap       : All[Tag.InstanceSync].toMap     ,
		toRecord    : All[Tag.InstanceSync].toRecord  ,
		toSet       : All[Tag.InstanceSync].toSet     ,
		until       : All[Tag.InstanceSync].until     ,
		while       : All[Tag.InstanceSync].while     ,
		zip         : All[Tag.InstanceSync].zip       ,
		zipLongest  : All[Tag.InstanceSync].zipLongest,
	},

	array.ConcatFilterMap,

	array.ISlice,
	array.ISplice,

	{
		at(this: readonly any[], index: number) {
			if (index < 0)
				return this[index + this.length];
			return this[index];
		},
		cast<U>() {return this as U[];},
		peek(this: readonly any[]) {
			if (this.length)
				return this[this.length - 1];
		},

		ireduce: array.Reduce.reduce,
	}
));

Array.prototype.reduce = array.Reduce.reduce;

addMethods("array", Array);