import {Tag}                 from "#tags";
import {All}                 from "@operators";
import {UnionToIntersection} from "@utils";
import * as array            from "../operators";

type BaseInterfaces = Pick<All<Tag.InstanceSync>,
	| "by"
	| "except"
	| "extend"
	| "first"
	| "flatten"
	| "groupBy"
	| "innerJoin"
	| "intersect"
	| "last"
	| "leftJoin"
	| "max"
	| "mean"
	| "min"
	| "notUntil"
	| "notWhile"
	| "omit"
	| "pick"
	| "present"
	| "sortBy"
	| "split"
	| "sortBy"
	| "sum"
	| "toMap"
	| "toRecord"
	| "toSet"
	| "until"
	| "while"
	| "zip"
	| "zipLongest"
>;

export interface ArrayInterfaces<T> extends
	UnionToIntersection<typeof array[keyof typeof array]>,
	BaseInterfaces
{
	/**
	 * Returns the element found at the given index into this array, or `undefined` if `index`
	 * is beyond the bounds of the array.  Negative indices count backward from the end of the
	 * array.
	 */
	at(this: readonly T[], index: number): T | undefined;

	cast<U>(): U[];

	/**
	 * Returns the last element in the array.  This is similar to `Array.prototype.pop`, except
	 * that it doesn't remove the element from the array.
	 */
	peek(this: readonly T[]): T | undefined;

	ireduce: All<Tag.InstanceSync>["reduce"];
}