import {Tag} from "#tags";
import {All} from "@operators";

// Note this is slightly different from the standard splice operator in that it supports
// a negative start index.

const {splice} = All[Tag.StaticSync];

interface ISpliceDocs {
	/**
	 * Removes elements from an array and, if necessary, inserts new elements in their place.
	 *
	 * This function is similar to `Array.prototype.splice()` except that, instead of yielding the
	 * deleted elements, it yields the resulting array.
	 */
	isplice: any;
}

export interface ISplice extends ISpliceDocs {
	/**
	 * @param start The zero-based location in the array from which to start removing elements.
	 * @param deleteCount The number of elements to skip.
	 */
	isplice<I>(this: readonly I[], start: number, deleteCount?: number): Generator<I>;
	/**
	 * @param start The zero-based location in the array from which to start removing elements.
	 * @param deleteCount The number of elements to skip.
	 * @param items Elements to yield in place of the skipped elements.
	 */
	isplice<I>(
		this: readonly I[], start: number, deleteCount: number, ...items: I[]
	): Generator<I>;
}

export const ISplice: ISplice = {
	isplice(start, deleteCount, ...items) {
		if (start === undefined)
			start = 0;
		else if (start < 0) {
			if (start <= -this.length)
				start = 0;
			else
				start += this.length;
		}

		return splice(this, start, deleteCount!, ...items);
	}
};