import {Tag} from "#tags";
import {All} from "@operators";

// Note this is slightly different from the standard slice operator in that it supports
// negative indices.

const {slice} = All[Tag.StaticSync];

export interface ISlice {
	/**
	 * Yields a section of the array.
	 *
	 * This function is similar to `Array.prototype.slice()` except that it returns a generator
	 * instead of an array.
	 * @param start The index at which to start yielding items.  If this index is less than `0`,
	 * items are yielded starting at `array.length - start`.  If this index is equal to or greater
	 * than the number of items in the array, then no items are yielded.  If not given or
	 * `undefined`, items are yielded starting from the beginning of the array.
	 * @param end The index at which to stop yielding items.  If this index is less than or equal to
	 * `start`, then no items are yielded, unless this index is less than zero, and
	 * `end + array.length` is greater than start.  If not given or `undefined`, items are yielded
	 * until the end of the array.
	 */
	islice<I>(
		this   : readonly I[],
		start? : number | undefined,
		end?   : number | undefined,
	): Generator<I>;
}

export const ISlice: ISlice = {
	*islice(start?, end?) {
		if (start === undefined)
			start = 0;
		else if (start < 0) {
			if (start <= -this.length)
				start = 0;
			else
				start += this.length;
		}
		if (end !== undefined) {
			if (end < 0) {
				if (end <= -this.length)
					end = 0;
				else
					end += this.length;
			}
		}

		yield *slice(this, start, end);
	}
};