import {Tag} from "#tags";
import {All} from "@operators";

export interface ConcatFilterMap {
	iconcat : All<Tag.InstanceSync>["concat"];
	ifilter : All<Tag.InstanceSync>["filter"];
	imap    : All<Tag.InstanceSync>["map"];
}

export const ConcatFilterMap: ConcatFilterMap = {
	iconcat : All[Tag.InstanceSync].concat,
	ifilter : All[Tag.InstanceSync].filter,
	imap    : All[Tag.InstanceSync].map,
};