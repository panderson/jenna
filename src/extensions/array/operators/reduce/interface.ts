import {ReduceDocs} from "@operators/types/reduce/common";
import {
	Finalizer,
	Initializer,
	InitialValue,
	Reducer
}                   from "@operators/types/reduce/types";

// This is just like the sync iterable `reduce`, but the signatures that have a `params` parameter
// have a union type that will accept a callback function compatible with the built-in `reduce`
// method's callback function.  This is necessary to avoid an error saying "Interface 'T[]'
// incorrectly extends interface 'Reduce'...".

export type CallbackFn<T> =
	(previousValue: T, currentValue: T, currentIndex: number, array: T[]) => T;

export interface Reduce extends ReduceDocs {
	/**
	 * @param reduce A function that accepts up to three arguments.  It's called one time for each
	 * element, except for the first, which serves as the initial value of the accumulation.
	 * @returns The accumulated result, or `undefined` if the iterable is empty.
	 */
	reduce<T>(this: Iterable<T>, reduce: (prev: T, current: T, index: number) => T): T | undefined;

	/**
	 * @param reduce A function that accepts up to three arguments.  It's called one time for each
	 * element.
	 * @param initialValue The initial value to start the accumulation.
	 */
	reduce<T, U>(
		this         : Iterable<T>,
		reduce       : (prev: U, current: T, index: number) => U,
		initialValue : U,
	): T | U;

	/**
	 * @param params An object with a `reduce` property: a function that accepts up to three
	 * arguments.  It's called one time for each element, except for the first, which serves as the
	 * initial value of the accumulation.
	 * @returns The accumulated result, or `undefined` if the iterable is empty.
	 */
	reduce<T>(this: Iterable<T>, params: Reducer<T, T> | CallbackFn<T>): T | undefined;

	/**
	 * @param params An object with the following properties:
	 *
	 *   * `reduce`: A function that accepts up to three arguments.  It's called one time for each
	 *      element.
	 *
	 *   * `initialValue`: The initial value to start the accumulation.
	 */
	reduce<T, U>(this: Iterable<T>, params: Reducer<T, U> & InitialValue<U> | CallbackFn<T>): U;

	/**
	 * @param params An object with the following properties:
	 *
	 *   * `reduce`: A function that accepts up to three arguments.  It's called one time for each
	 *      element, except for the first, which is passed to the `initialize` function.
	 *
	 *   * `initialize`: A function called with the first item in this iterable (if there is one).
	 *     Its return value becomes the initial value of the accumulator.
	 * @returns The accumulated result, or `undefined` if the iterable is empty.
	 */
	reduce<T, U>(
		this: Iterable<T>, params: Reducer<T, U> & Initializer<T, U> | CallbackFn<T>
	): U | undefined;

	/**
	 * @param params An object with the following properties:
	 *
	 *   * `reduce`: A function that accepts up to three arguments.  It's called one time for each
	 *      element, except for the first, which serves as the initial value of the accumulation.
	 *
	 *   * `finalize`: A function called with the final accumulated result.  If this iterable was
	 *      empty, it will receive `undefined`.  Its return value will be the final result.
	 */
	reduce<T, U>(
		this: Iterable<T>, params: Reducer<T, T> & Finalizer<T | undefined, U> | CallbackFn<T>
	): U;

	/**
	 * @param params An object with the following properties:
	 *
	 *   * `reduce`: A function that accepts up to three arguments.  It's called one time for each
	 *      element.
	 *
	 *   * `initialValue`: The initial value to start the accumulation.
	 *
	 *   * `finalize`: A function called with the final accumulated result.  Its return value will
	 *     be the final result.
	 */
	reduce<T, U, V>(
		this: Iterable<T>, params: Reducer<T, U> & InitialValue<U> & Finalizer<U, V> | CallbackFn<T>
	): V;

	/**
	 * @param params An object with the following properties:
	 *
	 *   * `reduce`: A function that accepts up to three arguments.  It's called one time for each
	 *      element, except for the first, which is passed to the `initialize` function.
	 *
	 *   * `initialize`: A function called with the first item in this iterable (if there is one).
	 *     Its return value becomes the initial value of the accumulator.
	 *
	 *   * `finalize`: A function called with the final accumulated result.  If this iterable was
	 *      empty, it will receive `undefined`.  Its return value will be the final result.
	 */
	reduce<T, U, V>(
		this   : Iterable<T>,
		params : Reducer<T, U> & Initializer<T, U> & Finalizer<U | undefined, V> | CallbackFn<T>
	): V;
}