import {Tag}           from "#tags";
import {
	Finalize,
	Finalizer,
	Initializer,
	InitialValue,
	Reduce as ReduceFunc,
	Reducer,
}                      from "@operators/types/reduce/types";
import {All}           from "@operators";
import {
	CallbackFn,
	Reduce as Interface
}                      from "./interface";

const {reduce} = All[Tag.StaticSync];

const arrayReduce = Array.prototype.reduce;

export type Reduce = Interface;
export const Reduce: Reduce = {
	reduce<T, U, V>(
		this    : Iterable<T>,
		...args :
			| [reduce: ReduceFunc<T, T>]
			| [reduce: ReduceFunc<T, U>, initialValue: U]
			| [initialValue: U, reduce: ReduceFunc<T, U>, finalize?: Finalize<U, V>]
			| [params:
				| CallbackFn<T>
				| Reducer<T, T>
				| Reducer<T, U> & InitialValue<U>
				| Reducer<T, U> & Initializer<T, U>
				| Reducer<T, T> & Finalizer<T | undefined, U>
				| Reducer<T, U> & InitialValue<U> & Finalizer<U, V>
				| Reducer<T, U> & Initializer<T, U> & Finalizer<U | undefined, V>
			]
	) {
		if (typeof args[0] === "function")
			return arrayReduce.apply(this, args as any);
		return reduce(this, ...(args as [any]));
	}
};