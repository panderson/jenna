import "./setup";
import {AsyncIterable}   from "#types";
import {ArrayInterfaces} from "./setup";

declare global {
	interface Array<T>         extends ArrayInterfaces<T> {}
	interface ReadonlyArray<T> extends ArrayInterfaces<T> {}
	interface ArrayConstructor {
		fromAsync<T>(iterable: AsyncIterable<T>): Promise<T[]>;
	}
}

Array.fromAsync = async function fromAsync(iterable) {return AsyncIterable.toArray(iterable);}