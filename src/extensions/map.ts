import {Tag}                    from "#tags";
import {addMethods, Interfaces} from "#types/iterable/setup";
import {All}                    from "@operators";
import {appliable}              from "@utils/apply";

const {map} = All[Tag.StaticSync];

declare global {
	interface MapConstructor {
		/**
		 * Creates a new map with a single entry, identified by the given key and value.
		 * @example
		 * console.log(Map.create('a', 1));
		 * // Map(1) { 'a' => 1 }
		 *
		 * // Compare to:
		 * console.log(new Map([['a', 1]]));
		 * // Map(1) { 'a' => 1 }
		 */
		create<K, V>(key: K, value: V): Map<K, V>;

		/**
		 * Creates a new map with the given entries.  Equivalent to Map's constructor, but because
		 * it uses a rest parameter, allows one to elide the outer pair of square brackets.
		 * @example
		 * console.log(Map.create(['a', 1], ['b', 2]));
		 * // Map(2) { 'a' => 1, 'b' => 2 }

		 * // Compare to:
		 * console.log(new Map([['a', 1], ['b', 2]]));
		 * // Map(2) { 'a' => 1, 'b' => 2 }
		 */
		create<K, V>(...entries: readonly (readonly [K, V])[]): Map<K, V>;
	}

	interface Map<K, V> extends Omit<Interfaces, "forEach" | "extend"> {
		/**
		 * Gets the current value associated with the given key, or if one is not found, sets it to
		 * the given default value and returns that, instead.
		 * @param key The key of the value to get or set in the map.
		 * @param defaultValue A value to use as the default if the given key is not found in the
		 * map.  If this is a function, it will be passed the given key, and its return value will
		 * be used as the default value.
		 * @param storeFunction If `defaultValue` is a function and `storeFunction` is `true`, then
		 * the function will be used as the literal default value, instead of being called.
		 */
		default(key: K, defaultValue: V | ((key: K) => V), storeFunction?: boolean): V;

		keys    (): Generator<K>;
		values  (): Generator<V>;
		entries (): Generator<[K, V]>;

		/**
		 * An accessor equivalent to the built-in `entries()` method.
		 * @example
		 * console.log(Array.from(Map.create('a', 1).items));
		 * // [ [ 'a', 1 ] ]

		 * // Compare to:
		 * console.log(Array.from(Map.create('a', 1).entries()));
		 * // [ [ 'a', 1 ] ]
		 */
		readonly items: Generator<[K, V]>;

		cast<K2, V2>(): Map<K2, V2>;

		/**
		 * Creates a new map as a shallow copy of this map, with the addition of the given key and
		 * value.  The map's type can be widened using this method.
		 * @param key The new key to add to the map.
		 * @param value The new value to add to the map.
		 */
		extend<K2 = K, V2 = V>(key: K2, value: V2): Map<K | K2, V | V2>;
		/**
		 * Creates a new map as a shallow copy of this map, with the addition of the given entries.
		 * The map's type can be widened using this method.
		 * @param entries The new key/value pairs to add to the map.
		 */
		extend<K2 = K, V2 = V>(...entries: readonly (readonly [K2, V2])[]): Map<K | K2, V | V2>;

		/**
		 * Creates a new map by invoking the given selector function for each key/value pair of this
		 * map.
		 * @param selector A function that takes a key and its associated value as separate
		 * parameters and must return an array of two items, consisting of the key and value of the
		 * corresponding entry in the new map.
		 * @example
		 * const map = Map.create(['a', 1], ['b', 2]);
		 * const map2 = map.transform((key, value) => [`${key}2`, value * 2]);
		 * console.log(map2);
		 * // Map(2) { 'a2' => 2, 'b2' => 4 }
		 */
		transform<K2, V2>(selector: (key: K, value: V) => [K2, V2]): Map<K2, V2>;

		/**
		 * Looks up the current value associated with the given key and passes it, or `undefined`,
		 * to the given function.  Whatever the function returns becomes the new value associated
		 * with the key and is returned by `modify`.
		 * @param key The key of the value to modify.
		 * @param modify A function that takes the value currently associated value with `key`, as
		 * well as the value of `key` itself, and returns a new value to associated with with `key`.
		 * If the return value is `undefined`, then `key` is deleted from the map.
		 * @returns The value returned by the `modify` function.
		 */
		modify<R extends V | undefined>(key: K, modify: (value: V | undefined, key: K) => R): R;
	}
}

const isEntry = (value: unknown): value is [unknown, unknown] =>
	Array.isArray(value) && value.length === 2;

Map.create = (...args) => {
	switch (args.length) {
	case 0:
		return new Map();
	case 2:
		if (isEntry(args[0]))
			break;
		return new Map([args as [unknown, unknown]]);
	}

	return new Map(args as readonly (readonly [unknown, unknown])[]);
};

const {keys, values, entries} = Map.prototype;

const methods = {
	default<K, V>(
		this: Map<K, V>, key: K, defaultValue: V | ((key: K) => V), storeFunction = false
	): V {
		let rtn = this.get(key);
		if (rtn !== undefined)
			return rtn;
		if (!storeFunction && typeof defaultValue === "function")
			this.set(key, rtn = (<(key: K) => V>defaultValue)(key))
		else
			this.set(key, rtn = <V>defaultValue);
		return rtn;
	},

	*keys<K, V>(this: Map<K, V>): Generator<K> {
		for (const key of keys.call(this))
			yield key;
	},

	*values<K, V>(this: Map<K, V>): Generator<V> {
		for (const key of values.call(this))
			yield key;
	},

	*entries<K, V>(this: Map<K, V>): Generator<[K, V]> {
		for (const key of entries.call(this))
			yield key;
	},

	cast() {return this;},

	extend<K, V, K2 = K, V2 = V>(
		this: Map<K, V>,
		...args: [key: K2, value: V2] | [K2, V2][]
	): Map<K | K2, V | V2> {
		const copy = new Map<K | K2, V | V2>(this);
		switch (args.length) {
		case 0:
			return copy;
		case 2:
			if (isEntry(args[0]))
				break;
			copy.set(args[0], args[1] as V2);
			return copy;
		}

		for (const entry of args) {
			if (!isEntry(entry))
				throw new Error("Must provide exactly two elements, a key and a value.");
			copy.set(entry[0], entry[1]);
		}
		return copy;
	},

	transform<K, V, K2, V2>(this: Map<K, V>, fn: (key: K, value: V) => [K2, V2]) {
		return new Map(map(this.entries(), appliable(fn)));
	},

	modify<K, V, R extends V>(
		this   : Map<K, V>,
		key    : K,
		modify : (value: V | undefined, key: K) => R
	): R {
		const current  = this.get(key);
		const modified = modify(current, key);
		if (!Object.is(modified, current)) {
			if (modified === undefined)
				this.delete(key);
			else
				this.set(key, modified);
		}
		return modified;
	},
};

Object.assign(Map.prototype, methods);
addMethods("map", Map);

if (!("items" in Map.prototype)) {
	Object.defineProperty(Map.prototype, "items", {
		get: function() {
			return (this as Map<unknown, unknown>).entries();
		},
	});
}