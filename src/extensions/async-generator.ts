import {addMethods, AsyncInterfaces} from "#types/async-iterable/setup";
import {KeyOf, Methods}              from "@utils";

export const AsyncGeneratorFunction: AsyncGeneratorFunctionConstructor =
	Object.getPrototypeOf(async function*(){}).constructor;
export const AsyncGenerator: AsyncGeneratorFunction = AsyncGeneratorFunction.prototype;

declare global {
	interface AsyncGenerator<T = unknown, TReturn = any, TNext = unknown> extends AsyncInterfaces {
		cast<T, R = any, N = unknown>(): AsyncGenerator<T, R, N>;
	}
}

const methods: Methods<KeyOf<AsyncGenerator>> = [
	["cast", function cast() {return this}]
];

addMethods("asyncGenerator", AsyncGenerator);
addMethods("asyncGenerator", AsyncGenerator, methods);