import {TypedObject} from "#types";

declare global {
	export type TypedPropertyDescriptorMap<T> = {
		[K in keyof T]: TypedPropertyDescriptor<T[K]>;
	};

	interface ObjectConstructor {
		typed: TypedObject;

		recreate  : TypedObject["recreate"];
		transform : TypedObject["transform"];
		toMap     : TypedObject["toMap"];
	}
}

Object.typed = TypedObject;

Object.recreate  = TypedObject.recreate;
Object.transform = TypedObject.transform;
Object.toMap     = TypedObject.toMap;