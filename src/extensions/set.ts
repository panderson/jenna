import {addMethods, Interfaces} from "#types/iterable/setup";
import {KeyOf, Methods}         from "@utils";

declare global {
	interface Set<T> extends Omit<Interfaces, "forEach" | "extend"> {
		cast<U>(): Set<U>;

		/**
		 * Creates a new set as a shallow copy of this one, with the addition of the given value.
		 * The set's type can be widened using this method.
		 * @param value The new value to add to the set.
		 */
		extend<T2 = T>(...values: readonly T2[]): Set<T | T2>;
	}
}

const methods: Methods<KeyOf<Set<any>>> = [
	["cast", function cast() {return this;}],
	["extend", function extend(...values) {
		const copy = new Set(this);
		for (const value of values)
			copy.add(value);
		return copy;
	}],
];

addMethods("set", Set, methods);
addMethods("set", Set);