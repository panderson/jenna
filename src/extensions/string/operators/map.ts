export interface Map {
	/**
	 * Calls a selector function for each character in this string, and returns an iterable that
	 * yields the results.
	 * @param selector A function that accepts up to two arguments.  The `map` function calls the
	 * selector one time for each character in the string.
	 */
	map<T>(this: string, selector: (c: string, index: number) => T): Generator<T>;
}

export const Map: Map = {
	*map(selector) {
		let i = 0;
		for (const c of this)
			yield selector(c, i++);
	}
};