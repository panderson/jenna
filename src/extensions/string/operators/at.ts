export interface At {
	/**
	 * Returns the character found at the given index into this string, or `undefined` if `index` is
	 * beyond the length of the string.  Negative indices count backward from the end of the string.
	 */
	at(this: string, index: number): string | undefined;
}

export const At: At = {
	at(index) {
		if (index < 0)
			index += this.length;
		return this[index];
	}
};