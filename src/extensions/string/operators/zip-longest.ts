import {AsyncIterable} from "#types/async-iterable/class";
import {Matched}       from "@utils";

interface ZipLongestDocs {
	/**
	 * Iterates through this string and an iterable in tandem, calling the `resultSelector` function
	 * with each character and the corresponding element from the iterable and yielding its results,
	 * or yielding tuples containing the character and corresponding element if no result selector
	 * is given.
	 *
	 * Iteration continues until both of the iterables are exhausted.  If one is exhausted before
	 * the other, `undefined` will be given in its place.
	 */
	zipLongest: any;
}

export interface ZipLongest extends ZipLongestDocs {
	/**
	 * @param resultSelector A function that receives a character from this string and the
	 * corresponding element from the iterable, along with the current index.
	 */
	zipLongest<T, U = [string | undefined, T | undefined]>(
		this            : string,
		other           : Iterable<T>,
		resultSelector? : (a: string | undefined, b: T | undefined, index: number) => U
	): Generator<U>;

	/**
	 * @param resultSelector A function that receives a character from this string and the
	 * corresponding element from the iterable, along with the current index.
	 */
	zipLongest<T, U = [string | undefined, T | undefined]>(
		this            : string,
		other           : AnyIterable<T>,
		resultSelector? : (a: string | undefined, b: T | undefined, index: number) => U
	): AsyncGenerator<U>;
}

async function *asyncZipLongest<T, U>(
	str            : string,
	other          : AnyIterable<T>,
	resultSelector : (a: string | undefined, b: T | undefined, index: number) => U
): AsyncGenerator<U> {
	const it = Matched.iterator(other);

	let index = 0;
	for (; index < str.length; index++) {
		const step = await it.next();
		if (step.done) {
			for (; index < str.length; index++)
				yield resultSelector(str[index], undefined, index);
			break;
		}
		yield resultSelector(str[index], step.value, index);
	}
	while (true) {
		const step = await it.next();
		if (step.done)
			break;
		yield resultSelector(undefined, step.value, index++);
	}
}

function *syncZipLongest<T, U>(
	str            : string,
	other          : Iterable<T>,
	resultSelector : (a: string | undefined, b: T | undefined, index: number) => U
): Generator<U> {
	const it = other[Symbol.iterator]();

	let index = 0;
	for (; index < str.length; index++) {
		const step = it.next();
		if (step.done) {
			for (; index < str.length; index++)
				yield resultSelector(str[index], undefined, index);
			break;
		}
		yield resultSelector(str[index], step.value, index);
	}
	while (true) {
		const step = it.next();
		if (step.done)
			break;
		yield resultSelector(undefined, step.value, index++);
	}
}

export const ZipLongest: ZipLongest = {
	zipLongest<T, U>(
		this           : string,
		other          : AnyIterable<T>,
		resultSelector : (a: string | undefined, b: T | undefined, index: number) => U =
			(a, b) => [a, b] as any
	): any {
		if (AsyncIterable.is(other))
			return asyncZipLongest(this, other, resultSelector);
		return syncZipLongest(this, other, resultSelector);
	}
};