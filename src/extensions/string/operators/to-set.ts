export interface ToSet {
	/** Builds a set of the unique characters in this string. */
	toSet(this: string): Set<string>;
}

export const ToSet: ToSet = {toSet() {return new Set(this)}};