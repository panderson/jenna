import {AsyncIterable} from "#types/async-iterable/class";
import {Matched}       from "@utils";

interface ZipDocs {
	/**
	 * Iterates through this string and an iterable in tandem, calling the `resultSelector` function
	 * with each character and the corresponding element from the iterable and yielding its results,
	 * or yielding tuples containing the character and corresponding element if no result selector
	 * is given.
	 *
	 * Iteration stops when the end of either the string or the iterable is reached.
	 */
	zip: any;
}

export interface Zip extends ZipDocs {
	/**
	 * @param resultSelector A function that receives a character from this string and the
	 * corresponding element from the iterable, along with the current index.  Whatever it returns
	 * is yielded by `zip`.
	 */
	zip<T, U = [string, T]>(
		this: string, other: Iterable<T>, resultSelector?: (a: string, b: T, index: number) => U
	): Generator<U>;
	/**
	 * @param resultSelector A function that receives a character from this string and the
	 * corresponding element from the iterable, along with the current index.  Whatever it returns
	 * is yielded by `zip`.
	 */
	zip<T, U = [string, T]>(
		this            : string,
		other           : AnyIterable<T>,
		resultSelector? : (a: string, b: T, index: number) => U
	): AsyncGenerator<U>;
}

async function *asyncZip<T, U>(
	str            : string,
	other          : AnyIterable<T>,
	resultSelector : (a: string, b: T, index: number) => U
): AsyncGenerator<U> {
	const it = Matched.iterator(other);

	for (let index = 0; index < str.length; index++) {
		const step = await it.next();
		if (step.done)
			break;
		yield resultSelector(str[index], step.value, index);
	}
}

function *syncZip<T, U>(
	str            : string,
	other          : Iterable<T>,
	resultSelector : (a: string, b: T, index: number) => U
): Generator<U> {
	const it = other[Symbol.iterator]();

	for (let index = 0; index < str.length; index++) {
		const step = it.next();
		if (step.done)
			break;
		yield resultSelector(str[index], step.value, index);
	}
}

export const Zip: Zip = {
	zip<T, U>(
		this           : string,
		other          : AnyIterable<T>,
		resultSelector : (a: string, b: T, index: number) => U = (a, b) => [a, b] as any
	): any {
		if (AsyncIterable.is(other))
			return asyncZip(this, other, resultSelector);
		return syncZip(this, other, resultSelector);
	}
};