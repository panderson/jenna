import {Tag}           from "#tags";
import {AsyncIterable} from "#types/async-iterable/class";
import {All}           from "@operators";

const {join}            = All[Tag.StaticSync];
const {join: asyncJoin} = All[Tag.StaticAsync];

interface JoinDocs {
	/** Combines all elements of an iterable to a string, separated by this string. */
	join: any;
}

export interface Join extends JoinDocs {
	join<T>(this: string, iterable: Iterable<T>): string;
	join<T>(this: string, iterable: globalThis.AsyncIterable<T>): Promise<string>;
	join<T>(this: string, iterable: AnyIterable<T>): string | Promise<string>;
}

export const Join: Join = {
	join(this: string, iterable) {
		if (AsyncIterable.is(iterable))
			return asyncJoin(iterable, this);
		return join(iterable as any, this) as any;
	}
};