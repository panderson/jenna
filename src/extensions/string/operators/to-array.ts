export interface ToArray {
	/** Builds an array of individual characters from this string. */
	toArray(this: string): string[];
}

export const ToArray: ToArray = {toArray() {return this.split('')}};