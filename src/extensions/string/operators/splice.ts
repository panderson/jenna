export interface Splice {
	/**
	 * Creates a new copy of this string with a substring removed from it.
	 * @param start The zero-based location in the string from which to start removing characters.
	 * @param deleteCount The number of characters to omit.
	 */
	splice(this: string, start: number, deleteCount?: number): string;
	 /**
	  * Creates a new copy of this string with a substring removed from it, with a replacement string
	  * inserted in its place.
	  * @param start The zero-based location in the string from which to start removing characters.
	  * @param deleteCount The number of characters to omit.
	  * @param replacement The replacement string to insert in place of the removed substring.
	  */
	splice(this: string, start: number, deleteCount: number, replacement: string): string;
}

export const Splice: Splice = {
	splice (start, deleteCount?, replacement?) {
		if (start < 0)
			start = Math.max(0, start + this.length);
		else if (start === undefined)
			start = 0;
		return this.slice(0, start) + (replacement ?? "") + this.slice(start + (deleteCount ?? 0));
	}
};