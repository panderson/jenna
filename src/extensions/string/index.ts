import "./setup";
import {StringInterfaces} from "./setup";

declare global {
	interface String extends StringInterfaces {}
}