import {UnionToIntersection} from "@utils";
import * as op               from "../operators";

export interface StringInterfaces extends UnionToIntersection<typeof op[keyof typeof op]> {}