import {buildAddMethods}  from "@utils";
import * as op            from "../operators";
import {StringInterfaces} from "./interface";

const addMethods = buildAddMethods<StringInterfaces>(Object.assign({},
	op.At,
	op.Join,
	op.Map,
	op.Splice,
	op.ToArray,
	op.ToSet,
	op.Zip,
));

addMethods("string", String);