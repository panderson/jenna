let indent = 0;

console.open  = module => console.log(`${"    ".repeat(indent++)}> ${module.id.replace(/^\/home\/pete\/code\/www\/jenna\/dist\//, '')}`);
console.close = module => console.log(`${"    ".repeat(--indent)}< ${module.id.replace(/^\/home\/pete\/code\/www\/jenna\/dist\//, '')}`);

function register(extension: string) {
	const old = require.extensions[extension];
	if (!old)
		return;

	console.log(`registering extension: ${extension}`);
	require.extensions[extension] = (module, filename) => {
		console.open(module);
		try {
			return old(module, filename);
		} catch (E) {
			console.error("ERROR"); throw E;
		} finally {
			console.close(module);
		}
	};
}

console.log(require.extensions);

register(".js");
register(".d.ts");