declare module "console" {
	global {
		interface Console {
			open(module: NodeModule): void;
			close(module: NodeModule): void;
		}
	}
}