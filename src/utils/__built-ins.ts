import {IndexedMap} from "#types";

export const enabled = (globalThis as any).__BUILTINS__;

// This `MembersOf` type seems pretty redundant.  It seems it would return basically the same type
// as its generic type argument, and for most types, that's more or less true (although it does
// filter out members with Symbol and number keys).  Its main purpose is to disambiguate the type
// `keyof Array<T>`.  Because an array's keys are its elements as well as its members, weird things
// happen when you build a type based on `keyof Array<T>` (or variants thereof).
export type MembersOf<T> = {[K in keyof T as string & K]: T[K]};

type FunctionKeys<T> = {
	[K in keyof MembersOf<T>]: T[K] extends Function ? K : never
}[keyof MembersOf<T>];

type FunctionsOf<T> = {[K in FunctionKeys<T>]: T[K]};

// function functions<T>(prototype: T): FunctionsOf<T> {
// 	return Object.fromEntries(
// 		Object.keys(prototype)
// 		.map(name => {
// 			try {
// 				if (name === "constructor")
// 					return [name, undefined];
// 				return [name, prototype[name as keyof T]];
// 			} catch {
// 				return [name, undefined];
// 			}
// 		})
// 		.filter(([, value]) => typeof value === "function")
// 	);
// }

type Method         = (this: any, ...args: any[]) => any;
type Implementation = Record<string, Method>;

export type Types = "array" | "set" | "map" | "generator" | "asyncGenerator" | "string";

interface Type {
	name           : string;
	constructor    : Function;
	implementation : Implementation;
}

const typeMap = new IndexedMap<Types, Type>({
	onMissing(key) {throw new Error(`Couldn't find built-ins for '${key}'`);},
	onExists(key, existing, value) {
		throw new Error(
			`Name '${key}' was already used for constructor '${
				existing.constructor.name || existing.constructor
			}', and is now being used for constructor '${
				value.constructor.name || value.constructor
			}'`
		);
	}
});

export const builtIns = {
	get string        () {return typeMap.string   .implementation as FunctionsOf<String>;},
	get array         () {return typeMap.array    .implementation as FunctionsOf<Array<any>>;},
	get set           () {return typeMap.set      .implementation as FunctionsOf<Set<any>>;},
	get map           () {return typeMap.map      .implementation as FunctionsOf<Map<any, any>>;},
	get generator     () {return typeMap.generator.implementation as FunctionsOf<Generator>;},
	get asyncGenerator() {
		return typeMap.asyncGenerator.implementation as FunctionsOf<AsyncGenerator>;
	},
};