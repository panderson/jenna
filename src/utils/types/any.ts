export {}

declare global {
	type AnyIterable <T>                       = Iterable <T>       | AsyncIterable <T>;
	type AnyIterator <T, R = any, N = unknown> = Iterator <T, R, N> | AsyncIterator <T, R, N>;
	type AnyGenerator<T, R = any, N = unknown> = Generator<T, R, N> | AsyncGenerator<T, R, N>;
}