export namespace Matched {
	export type Iterator<I, R = any, N = undefined> =
		I extends AsyncIterable<infer U> ?       AsyncIterator<U, R, N> :
		I extends      Iterable<infer U> ? globalThis.Iterator<U, R, N> :
		never;

	export type Generator<I, G, R = any, N = unknown> =
		I extends    Iterable<any> ? globalThis.Generator<G, R, N>    :
		I extends AnyIterable<any> ? AsyncGenerator<Awaited<G>, R, N> :
		never;

	export function iterator<I extends AnyIterable<any>, R = any, N = unknown>(
		iterable: I
	): Iterator<I, R, N> {
		const asyncIterator = (iterable as any)[Symbol.asyncIterator];
		if (asyncIterator)
			return asyncIterator.call(iterable);
		return (iterable as any)[Symbol.iterator]();
	}
}