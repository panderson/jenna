// This is just to resolve a conflict between Jenna and Webpack.

// node_modules/webpack/types.d.ts:10963:2 - error TS2416: Property 'sort' in type 'SortableSet<T>' is not assignable to the same property in base type 'Set<T>'.
//   Type '() => SortableSet<T>' is not assignable to type '<I>(this: Iterable<I>, compare?: ((a: I, b: I) => number) | undefined) => I[]'.
//     Type 'SortableSet<T>' is missing the following properties from type 'I[]': length, pop, push, shift, and 13 more.

// 10963  sort(): SortableSet<T>;
//        ~~~~

// node_modules/webpack/types.d.ts:10963:2 - error TS2425: Class 'Set<T>' defines instance member property 'sort', but extended class 'SortableSet<T>' defines it as instance member function.

// 10963  sort(): SortableSet<T>;
//        ~~~~

declare module "webpack/types" {
	// interface SortableSet<T> extends Set<T> {
	// 	flashBang(): void;
	// 	sort(): SortableSet<T>;
	// }
	export class Set<T> extends globalThis.Set<T> {
		// @ts-ignore
		sort(): any;
	}
}

export {}