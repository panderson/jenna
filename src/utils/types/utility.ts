// https://stackoverflow.com/questions/50374908/transform-union-type-to-intersection-type
export type UnionToIntersection<U> =
	(U extends any ? (k: U) => void : never) extends ((k: infer I) => void) ? I : never;

export type StringKey<T> = T extends number ? `${T}` : T extends string ? T : never;
export type KeyOf<T>     = StringKey<keyof T>;

export type ValueOf<T, K extends keyof T = keyof T> = Required<T>[K];
export type ValuesOf<T, K extends [...(keyof T)[]] = (keyof T)[]> = {
	[P in keyof K]: K[P] extends keyof T ? ValueOf<T, K[P]> : never;
};

export type EntryOf<T, K extends keyof T = keyof T> = [K, ValueOf<T, K>];
export type EntriesOf<T, K extends [...(keyof T)[]] = (keyof T)[]> = {
	[P in keyof K]: K[P] extends keyof T ? EntryOf<T, K[P]> : never;
}

export type Func      = (...args: any[]) => any;
export type AsFunc<T> = T extends Func ? T : never;

export type IsTrue<I extends boolean, T = true, F = false> =
	[I] extends [true] ? [I] extends [never] ? F : T : F;

export type IsTuple<A extends readonly any[], T = true, F = false> =
	number extends A["length"] ? F : T;

export type HasElements<A extends readonly any[], T = true, F = false> =
	IsTuple<A, 0 extends A["length"] ? F : T, F>;

type UnasyncArray<A extends any[]> = A extends (infer E)[] ? UnasyncArg<E>[] : never;

type UnasyncArg<A>
	= A extends any[]
		? IsTuple<A> extends true ? UnasyncArgs<A> : UnasyncArray<A>
	: A extends string
		? A
	: A extends AnyGenerator<infer T, infer R, infer N>
		? Generator<T, R, N>
	: A extends AnyIterable<infer T>
		? Iterable<T>
	: A extends AnyIterator<infer T, infer R, infer N>
		? Iterator<T, R, N>
	: A extends Func
		? UnasyncFunc<A>
	: A;

export type UnasyncArgs<A extends readonly any[]> = {
	[K in keyof A]: UnasyncArg<A[K]>;
};

export type UnasyncFunc<F extends Func>
	= F extends (...args: infer A) => infer R
		? (...args: UnasyncArgs<A>) => Awaited<UnasyncArg<R>>
	: never;