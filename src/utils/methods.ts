import {KeyOf} from "./types";

export type Method                                    = (this: any, ...args: any[]) => any;
export type MethodEntry<K extends keyof any = string> = [K, Method];
export type Methods<K extends keyof any = string>     = MethodEntry<K>[];

export const patched  = new Map<Function, string>();
export const builtIns = new Map<string, Object>();

export function buildAddMethods<I extends Record<KeyOf<I>, Method>>(methods: I) {
	const all: Methods<KeyOf<I>> = Object.entries(methods) as any;

	return function addMethods<T extends I>(
		name        : string,
		constructor : Function,
		methods     : Methods<KeyOf<T>> = all,
	): void {
		if (!patched.has(constructor)) {
			if (builtIns.has(name)) {
				const existing = builtIns.get(name)!;
				throw new Error(`Name '${name}' already used for constructor '${
					existing.constructor.name || existing.constructor
				}', and now being used for constructor '${
					constructor.name || constructor
				}'`);
			}

			//console.log(`adding methods to ${name}: ${constructor}`);
			patched.set(constructor, name);
			builtIns.set(name, constructor.prototype);
		}

		const prototype = constructor.prototype as Record<KeyOf<T>, Method>;
		for (const [name, func] of methods) {
			if (!(name in prototype))
				prototype[name] = func;
		}
	}
}