export type Comparer<I> = (a: I, b: I) => number;

export const Comparer: Comparer<unknown> & {reverse: Comparer<unknown>} = Object.assign(
	function(a: unknown, b: unknown): number {
		if (a === undefined)
			return b === undefined ? 0 : -1;

		if (a === null)
			return b === null ? 0 : b === undefined ? 1 : -1;

		return b == undefined || b === null || a > b ? 1 : a < b ? -1 : 0;
	},
	{reverse: (a: unknown, b: unknown) => -Comparer(a, b)}
);

export function keySorter<I, K>(
	keySelector : (item: I) => K,
	comparer?   : Comparer<K> | undefined,
	descending  : boolean = false,
): Comparer<I> {
	if (!comparer)
		comparer = Comparer;

	if (descending)
		return (a, b) => -comparer!(keySelector(a), keySelector(b));

	return (a, b) => comparer!(keySelector(a), keySelector(b));
}

export function sortArray<I>(
	items    : I[],
	param?   : boolean | Comparer<I> | undefined,
	reverse? : boolean | undefined,
): void {
	if (!param)
		items.sort(Comparer);
	else if (param === true)
		items.sort(Comparer.reverse);
	else if (reverse)
		items.sort((a, b) => -param(a, b));
	else
		items.sort(param);
}