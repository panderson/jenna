import {Comparer as BaseComparer} from "./base";

export namespace AggregateSort {
	export type Comparer<K, I> = (groupKey: K, a: I, b: I) => number;

	export const Comparer: Comparer<unknown, unknown> & {
		reverse : Comparer<unknown, unknown>;
		wrap    : {
			<K, I>(groupKey: K, groupComparer: Comparer<K, I>): BaseComparer<I>;
			reverse<K, I>(groupKey: K, groupComparer: Comparer<K, I>): BaseComparer<I>;
		};
	} = Object.assign(
		function(_groupKey: unknown, a: unknown, b: unknown): number {
			if (a === undefined)
				return b === undefined ? 0 : -1;

			if (a === null)
				return b === null ? 0 : b === undefined ? 1 : -1;

			return b == undefined || b === null || a > b ? 1 : a < b ? -1 : 0;
		}, {
			reverse: (_: unknown, a: unknown, b: unknown) => -Comparer(_, a, b),
			wrap: Object.assign(
				<K, I>(groupKey: K, groupComparer: Comparer<K, I>): BaseComparer<I> =>
					(a, b) => groupComparer(groupKey, a, b),
				{
					reverse: <K, I>(groupKey: K, groupComparer: Comparer<K, I>): BaseComparer<I> =>
						(a, b) => -groupComparer(groupKey, a, b),
				}
			),
		}
	);

	export function keySorter<K, I, K2>(
		keySelector : (groupKey: K, item: I) => K2,
		comparer?   : Comparer<K, K2> | undefined,
		descending  : boolean = false,
	): Comparer<K, I> {
		if (!comparer)
			comparer = Comparer;

		if (descending)
			return (k, a, b) => -comparer!(k, keySelector(k, a), keySelector(k, b));

		return (k, a, b) => comparer!(k, keySelector(k, a), keySelector(k, b));
	}

	export function sortArray<K, I>(
		groupKey : K,
		items    : I[],
		param?   : boolean | Comparer<K, I> | undefined,
	): void {
		if (!param)
			items.sort(BaseComparer);
		else if (param === true)
			items.sort(BaseComparer.reverse);
		else
			items.sort(Comparer.wrap(groupKey, param));
	}
}