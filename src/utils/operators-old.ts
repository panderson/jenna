// import {Tag, $Tag, Tagged, Matched} from "./tags";
// import rx                  from "rx";
// // import {UnionToIntersection} from "./types";
// // import {All} from "@setup";

// // const rxAsyncFunc   = rx`/
// // 	^
// // 	(?:async\s*)?        # The function _may_ not actually be async.
// // 	(?:\bfunction\s*)?   # It may not be declared as a classic, top-level function.
// // /x`;

// // const rxResyncFunc  = rx`/${rxAsyncFunc}|\bawait\b\s*/g`;

// // const rxRsyncAnon   = rx`/
// // 	${rxAsyncFunc}
// // 	\*?\s*   # Skip past the generator symbol, if it has one,
// // 	(?:
// // 		\b\w+s*  # and capture the function's name,
// // 		(?=\()   # but only if it's a regular function.
// // 	)?  # If ^ doesn't match, it must be an arrow function of the form 'x=>...',
// // 	    # or an anonymous one of the form 'function*()...'.
// // 	|
// // 	\bawait\b\s*
// // /gx`;

// // const rxIsGenerator = /^(?:async)?\s*(?:function)?\s*\*/;
// // const rxIsArrowFunc = /^(?:async)?\s*(?:\([^)]*\)|\w+)\s*=>/;

// type Func = (...args: any[]) => any;

// type Obj<T> = {
// 	[K in keyof T]: Func;
// };

// type UnasyncArg<A>
// 	= A extends any[]
// 		? A
// 	: A extends AnyIterable<infer T>
// 		? Iterable<T>
// 	: A extends AnyIterator<infer T, infer R, infer N>
// 		? Iterator<T, R, N>
// 	: A extends AnyGenerator<infer T, infer R, infer N>
// 		? Generator<T, R, N>
// 	: A extends Func
// 		? UnasyncFunc<A>
// 	: A extends Obj<A>
// 		? UnasyncObj<A>
// 	: A;

// type UnasyncArgs<A> = {
// 	readonly [K in keyof A]: UnasyncArg<A[K]>;
// };

// type UnasyncFunc<F extends Func>
// 	= F extends (...args: infer A) => infer R
// 		? (...args: UnasyncArgs<A>) => Awaited<UnasyncArg<R>>
// 	: never;

// type UnasyncObj<O extends Obj<O>> = {
// 	[K in keyof O]: UnasyncFunc<O[K]>;
// };

// function matchedIterator<T>(iterable: Iterable<T>){return iterable[Symbol.iterator]()}
// // function syncParams(context: object | undefined){
// // 	const params = ["matchedIterator"];
// // 	const args: any[] = [matchedIterator];

// // 	if(context){
// // 		for(const [key, value] of Object.entries(context)){
// // 			params.push(key);
// // 			args.push(value);
// // 		}
// // 	}

// // 	return {params, args};
// // }

// // function syncFnText(fn: Function, params: string[], name?: string | undefined): string{
// // 	console.log(`\n======  ${fn.name || "<anonymous>"}  ======`);
// // 	let fnText = fn.toString().replace(/\bMatched\.iterator\b(?=\()/g, "matchedIterator");
// // 	console.log(fnText);
// // 	for(const param of params){
// // 		console.log(`  ==== param: ${param}  ====`);
// // 		fnText = fnText.replace(new RegExp(`\\b\\w+\\.${param}\\b`, "g"), param);
// // 		console.log(fnText);
// // 	}
// // 	if(name){
// // 		console.log(`  ====  name: ${name}  ====`);
// // 		fnText =
// // 			(rxIsGenerator.test(fnText) ? '*' : '') +
// // 			name +
// // 			(rxIsArrowFunc.test(fnText) ? ':' : '') +
// // 			fnText.replace(rxRsyncAnon, '');
// // 		console.log(fnText);
// // 		return fnText;
// // 	}
// // 	console.log(`  ====  ====`);
// // 	console.log(fnText.replace(rxResyncFunc, ''));
// // 	return fnText.replace(rxResyncFunc, '');
// // }

// // export function syncFunc<F extends Func>(fn: F, context?: object): UnasyncFunc<F>{
// // 	const {params, args} = syncParams(context);

// // 	const alias = fn.name ? undefined : 'x';
// // 	const fnText = syncFnText(fn, params, alias);
// // 	return Function(...params, `"use strict";return{${fnText}}`)(...args)[fn.name || alias!];
// // }

// // export function syncObj<O extends Obj<O>>(o: O, context?: object): UnasyncObj<O>{
// // 	const {params, args} = syncParams(context);
// // 	return Function(
// // 		...params,
// // 		`"use strict";return{${
// // 			Object.entries(o)
// // 			.map(([key, fn]) => syncFnText(fn as Function, params, key))
// // 			.join()
// // 		}}`
// // 	)(...args);
// // }

// export type Operator<$ extends $Tag> = {
// 	[T in Tag]: Tagged<T>[$];
// };

// function instanceFunc(fn: Function): Function{
// 	return Function(
// 		"fn",
// 		`"use strict";return{${fn.name}(){return fn(this,...arguments)}}`
// 	)(fn)[fn.name];
// }

// // function instance<$ extends $Tag, T extends Tag.Static>(
// // 	statics: Tagged<T>[$]
// // ): Tagged<Tag.ToInstance<T>>[$]{
// // 	return Object.fromEntries(
// // 		Object.entries(statics)
// // 		.map(([key, value]) => {
// // 			if(typeof value !== "function")
// // 				return [key, value];
// // 			return [key, instanceFunc(value)]
// // 		})
// // 	) as any;
// // }

// // interface Statics<$ extends $Tag>{
// // 	sync  : Tagged<Tag.StaticSync>[$];
// // 	async : Tagged<Tag.StaticAsync>[$];
// // }

// // function isStatics<$ extends $Tag>(o: object): o is Statics<$>{
// // 	return o && "sync" in o && "async" in o;
// // }

// // export function Operator<$ extends $Tag>(
// // 	staticAsync  : Tagged<Tag.StaticAsync>[$],
// // 	syncContext? : object,
// // ): Operator<$>;

// // export function Operator<$ extends $Tag>(statics: Statics<$>): Operator<$>;

// // export function Operator<$ extends $Tag>(
// // 	...args: [Tagged<Tag.StaticAsync>[$], object?] | [Statics<$>]
// // ): Operator<$>{
// // 	let statics: Statics<$>;
// // 	if(args.length === 1 && isStatics(args[0]))
// // 		statics = args[0];
// // 	else{
// // 		const async = args[0] as Tagged<Tag.StaticAsync>[$];
// // 		statics = {sync: syncObj(async as any, args[1]) as any, async};
// // 	}
// // 	return {
// // 		[Tag.StaticSync]    : statics.sync,
// // 		[Tag.StaticAsync]   : statics.async,
// // 		[Tag.InstanceSync]  : instance(statics.sync),
// // 		[Tag.InstanceAsync] : instance(statics.async),
// // 	};
// // }

// // const testObj = {
// // 	a: 1,
// // 	b: 'b',
// // 	c: true
// // };

// // let t1 = null as any as ValuesOf<typeof testObj, ['a', 'b', 'c']>;

// // type T1 = typeof t1;

// // type K1 = KeysOf<typeof testObj>;
// // type K2 = keyof typeof testObj;

// // const k: (keyof typeof testObj)[] = ['a', 'b', 'c'];

// // function one<K extends keyof typeof testObj>(k: K): typeof testObj[K] {
// // 	const rtn = testObj[k];
// // 	return rtn;
// // }

// // let a = one("b");

// // function many<K extends (keyof typeof testObj)[]>(...k: K): ValuesOf<typeof testObj, K>{
// // 	const rtn = k.map(one);
// // 	return rtn;
// // }

// // let b = many('a', 'b')

// // function foo<K extends (keyof typeof testObj)[]>(k: K): typeof testObj[K[number]][] {
// // 	const rtn = k.map(o => testObj[o]);
// // 	return rtn;
// // }


// // class TaggedFuncs<T extends Tag>{
// // 	constructor(readonly tag: T){}

// // 	one<O extends keyof All<T>>(op: O): All<T>[O] {
// // 		return All[this.tag][op];
// // 	}
// // 	many<O extends [...(keyof All<T>)[]]>(...ops: O): ValuesOf<All<T>, O> {
// // 		const all = All[this.tag];
// // 		return ops.map(o => all[o]) as any;
// // 	}
// // }

// type OpFuncs<T extends Tag, A extends readonly [...Operator<any>[]]> = {
// 	[O in keyof A]: T extends keyof A[O] ? ValueOf<A[O][T]> : never;
// };

// // type A = Operator<"Concat">[Tag.StaticSync];
// // type m = ValueOf<A>;

// // const tagged: Record<Tag, TaggedFuncs<Tag>> =
// // 	Object.fromEntries(Tag.All.map(tag => [tag, new TaggedFuncs(tag)])) as any;

// // function operator<$ extends $Tag>(statics: Tagged<Tag.StaticAsync>[$] | Statics<$>): Operator<$>{
// // 	if(!isStatics(statics))
// // 		statics = {sync: syncObj(statics as any) as any, async: statics};

// // 	return {
// // 		[Tag.StaticSync]    : statics.sync,
// // 		[Tag.StaticAsync]   : statics.async,
// // 		[Tag.InstanceSync]  : instance(statics.sync),
// // 		[Tag.InstanceAsync] : instance(statics.async),
// // 	};
// // }

// // interface NewerOperator<$ extends $Tag>{
// // 	(statics: Tagged<Tag.StaticAsync>[$] | Statics<$>): Operator<$>;

// // 	closure<A extends any[]>(
// // 		async : A,
// // 		sync  : UnasyncArgs<A>,
// // 		build : (...args: A) => Tagged<Tag.StaticAsync>[$],
// // 	): Operator<$>;

// // 	ops<A extends [...Operator<any>[]]>(
// // 		ops   : A,
// // 		async : (...args: OpFuncs<Tag.StaticAsync, A>) => Tagged<Tag.StaticAsync>[$],
// // 	): Operator<$>;
// // }

// // export function ops<$ extends $Tag, A extends readonly [...Operator<any>[]]>(
// // 	$: $,
// // 	ops: A,
// // 	async : (...args: OpFuncs<Tag.StaticAsync, A>) => ValueOf<Tagged<Tag.StaticAsync>[$]>,
// // ): Operator<$>{
// // 	const asyncArgs = ops.map(o => o[Tag.StaticAsync]);
// // 	type AA = typeof asyncArgs;
// // 	const aa: AA;
// // 	return null as any;
// // }

// // type T = Tagged.Function<Tag.StaticSync, "Concat" | "At">;
// // type U = ValueOf<Tagged<Tag.StaticSync>["Concat"]>;
// // type V = T extends U ? U extends T ? "same" : "greater" : U extends T ? "less" : "different";
// // type W = V

// // type F = keyof Tagged.Functions;

// // type T = keyof Tagged.Interface<Tag.StaticSync, $Tag>

// // type X = Tagged.Interface<Tag, "Concat">[""]

// // interface Statics2<$ extends $Tag>{
// // 	sync  : Tagged.Function<Tag.StaticSync,  $>;
// // 	async : Tagged.Function<Tag.StaticAsync, $>;
// // }

// // type Statics3<$ extends $Tag> = {
// // 	[K in keyof Tagged.Function<Tag, $>]: {
// // 		sync  : Tagged.Function<Tag.StaticSync,  $>;
// // 		async : Tagged.Function<Tag.StaticAsync, $>;
// // 	};
// // };

// // function isStatics2<$ extends $Tag>(o: any): o is Statics2<$>{
// // 	return o && "sync" in o && "async" in o;
// // }

// // function isStatics3<$ extends $Tag>(o: any): o is Statics3<$>{
// // 	if(!o || typeof o !== "object")
// // 		return false;

// // 	const values = Object.values(o);
// // 	return values.length === 1 && isStatics2(values[0]);
// // }

// // interface BuildOperator2<$ extends $Tag>{
// // 	(statics: ValueOf<Tagged<Tag.StaticAsync>[$]> | Statics2<$>): Operator<$>;

// // 	closure<A extends readonly any[]>(...async: A): (
// // 		sync  : UnasyncArgs<A>,
// // 		build : (...args: A) => ValueOf<Tagged<Tag.StaticAsync>[$]>,
// // 	) => Operator<$>;

// // 	ops<A extends readonly [...Operator<any>[]]>(...ops: A): (
// // 		async: (...args: OpFuncs<Tag.StaticAsync, A>) => ValueOf<Tagged<Tag.StaticAsync>[$]>
// // 	) => Operator<$>;
// // }

// // function operator2<$ extends $Tag>(
// // 	statics : /*Tagged<Tag.StaticAsync>[$]// | */Statics3<$>
// // ): Operator<$> {
// // 	statics;
// // 	return null as any;
// // 	// if(!isStatics3(statics))
// // 	// 	statics = {sync: syncFunc(statics as any), async: statics} as any;

// // 	// return {
// // 	// 	[Tag.StaticSync]    : {[name]: statics.sync},
// // 	// 	[Tag.StaticAsync]   : {[name]: statics.async},
// // 	// 	[Tag.InstanceSync]  : {[name]: instance(statics.sync)},
// // 	// 	[Tag.InstanceAsync] : {[name]: instance(statics.async)},
// // 	// } as any;
// // }

// // //operator2("concat")({sync(){}, async *async(iterable, others){return "" as any;}})
// // //if(x === ""
// // //operator2<"Concat">({concat: {}})
// // //operator<""

// // function operator3<$ extends $Tag>(): ${return null as any;}

// type Functions<T extends Tag = Tag, $ extends $Tag = $Tag> = {
// 	[K in $ as keyof Tagged<T>[K]]: {
// 		tag  : K extends $Tag ? K : never;
// 		func : ValueOf<Tagged<T>[K]> extends Func ? ValueOf<Tagged<T>[K]> : never;
// 	};
// };

// type TagOf<F extends keyof Functions> = Functions[F]["tag"];
// // type FuncOf<F extends keyof Functions, T extends Tag = Tag> =
// // 	Functions<T>[F]["func"];

// // type G<$ extends $Tag = $Tag, T extends Tag = Tag> = {
// // 	[$T in T]: {
// // 		[K in $ as keyof Tagged<$T>[K]]: ValueOf<Tagged<$T>[K]>;
// // 	};
// // }

// // type T = Functions<Tag.StaticSync, "Concat">//keyof UnionToIntersection<Tagged<Tag.StaticSync>[$Tag]>;
// // type U = Operator<"Concat">
// // type V = G<"Concat", Tag.StaticSync>;
// // type Z = T | U | V;

// type Statics2<F extends keyof Functions> = {
// 	sync  : ValueOf<Functions<Tag.StaticSync,  TagOf<F>>>["func"];
// 	async : ValueOf<Functions<Tag.StaticAsync, TagOf<F>>>["func"];
// };

// type AsyncFunc<F extends keyof Functions> = Statics2<F>["async"];

// // function isStatics2<$ extends $Tag>(o: object): o is Statics2<$>{
// // 	return o && typeof o === "object" && "sync" in o && "async" in o;
// // }

// // const x = operator1("at", {
// // 	async async(iterable, index){
// // 		if(index < 0)
// // 			return undefined;
// // 		const it = Matched.iterator(iterable);
// // 		while(true){
// // 			const entry = await it.next();
// // 			if(entry.done)
// // 				return undefined;
// // 			if(--index < 0)
// // 				return entry.value;
// // 		}
// // 	},
// // 	sync(iterable, index){
// // 		if(index < 0)
// // 			return undefined;
// // 		const it = iterable[Symbol.iterator]();
// // 		while(true){
// // 			const entry = it.next();
// // 			if(entry.done)
// // 				return undefined;
// // 			if(--index < 0)
// // 				return entry.value;
// // 		}
// // 	}
// // });


// type OperatorBuilder1<F extends keyof Functions, A extends readonly [...args: any[]]> =
// 	(...args: A) => Operator<TagOf<F>>;

// type IteratorBuilder<A extends readonly [...args: any[]], R> =
// 	(iterator: typeof Matched.iterator, ...args: A) => R;


// class Foo<F extends keyof Functions>{
// 	operator(statics: Statics2<F> | AsyncFunc<F>){
// 		statics; return null as any as Operator<TagOf<F>>;
// 	}

// 	closure<A extends readonly any[]>(f: F, ...async: A){
// 		f; async; return null as any as OperatorBuilder1<F, [
// 			sync  : UnasyncArgs<A>,
// 			build : (...args: A) => AsyncFunc<F>
// 		]>;
// 	}

// 	ops<A extends readonly Operator<any>[]>(f: F, ...ops: A){
// 		f; ops; return null as any as OperatorBuilder1<F, [
// 			async: (...args: OpFuncs<Tag.StaticAsync, A>) => AsyncFunc<F>
// 		]>;
// 	}

// 	iterator = new class Iterator{
// 		operator(statics: IteratorBuilder<[], Statics2<F> | AsyncFunc<F>>){
// 			statics; return null as any as Operator<TagOf<F>>;
// 		}

// 		closure<A extends readonly any[]>(f: F, ...async: A){
// 			f; async; return null as any as OperatorBuilder1<F, [
// 				sync  : UnasyncArgs<A>,
// 				build : IteratorBuilder<A, AsyncFunc<F>>
// 			]>;
// 		}

// 		ops<A extends readonly Operator<any>[]>(f: F, ...ops: A){
// 			f; ops; return null as any as OperatorBuilder1<F, [
// 				async: IteratorBuilder<OpFuncs<Tag.StaticAsync, A>, AsyncFunc<F>>
// 			]>;
// 		}
// 	}
// }

// type MainOperatorBuilder<F extends keyof Functions> = Foo<F>["operator"];

// interface OperatorBuilder<F extends keyof Functions>{
// 	main<A extends readonly [...args: any[]]>(...args: A): Operator<TagOf<F>>;

// 	operator(statics: Statics2<F> | AsyncFunc<F>): Operator<TagOf<F>>;
// 	closure<A extends readonly [...args: any[]]>(
// 		sync  : UnasyncArgs<A>,
// 		build : (...args: A) => AsyncFunc<F>
// 	): OperatorBuilder<F>["main"];
// 	//Operator: OperatorBuilder<F>["main"]<[f: F, statics: Statics2<F> | AsyncFunc<F>]>;
// }


// type Operator1<F extends keyof Functions> =
// 	OperatorBuilder<F, [f: F, statics: Statics2<F> | AsyncFunc<F>]>;

// type IteratorOperator<F extends keyof Functions> =
// 	OperatorBuilder<F, [f: F, statics: IteratorBuilder<[], Statics2<F> | AsyncFunc<F>>]>;

// type Closure<F extends keyof Functions, A extends readonly any[]> =
// 	OperatorBuilder<F, [sync: UnasyncArgs<A>, build: (...args: A) => AsyncFunc<F>]>;

// type IteratorClosure<F extends keyof Functions, A extends readonly any[]> =
// 	OperatorBuilder<F, [sync: UnasyncArgs<A>, build: IteratorBuilder<A, AsyncFunc<F>>]>;

// type Ops<F extends keyof Functions, A extends readonly [...Operator<any>[]]> =
// 	OperatorBuilder<F, [async: (...args: OpFuncs<Tag.StaticAsync, A>) => AsyncFunc<F>]>;

// type IteratorOps<F extends keyof Functions,

// function operator1<F extends keyof Functions>(
// 	f       : F,
// 	statics : Statics2<F> | AsyncFunc<F>//FuncOf<TagOf<F>, Tag.StaticAsync>,
// ): Operator<TagOf<F>>{
// 	const s: Statics2<F> = typeof statics === "function"
// 		? {sync: syncFunc(statics), async: statics} as any :
// 		statics;
// 	return {
// 		[Tag.StaticSync]    : {[f]: s.sync},
// 		[Tag.StaticAsync]   : {[f]: s.async},
// 		[Tag.InstanceSync]  : {[f]: instanceFunc(s.sync)},
// 		[Tag.InstanceAsync] : {[f]: instanceFunc(s.async)},
// 	} as any;
// }

// let rxMember = /^\s*\*?\s*\w+\s*\(/;

// export function syncFunc<F extends Func>(fn: F): UnasyncFunc<F>{
// 	let fnText =
// 		fn.toString()
// 		.replace(/\bMatched\.iterator\b(?=\()/g, "matchedIterator")
// 		.replace(/\ba(?:sync|wait)\s*/g, '');

// 	return Function(`"use strict";return ${rxMember.test(fnText) ? "function " : ''}${fnText}`)();
// }

// export function syncObj<O extends Obj<O>>(o: O, context?: object): UnasyncObj<O>{
// 	const {params, args} = syncParams(context);
// 	return Function(
// 		...params,
// 		`"use strict";return{${
// 			Object.entries(o)
// 			.map(([key, fn]) => syncFnText(fn as Function, params, key))
// 			.join()
// 		}}`
// 	)(...args);
// }

// const closure = <F extends keyof Functions, A extends readonly any[]>(
// 	f: F, ...async: A
// ) => (
// 	sync  : UnasyncArgs<A>,
// 	build : (...args: A) => AsyncFunc<F>,
// ) => operator1(f, {
// 	async : build(...async),
// 	sync  : syncFunc(build)(...sync),
// } as any);

// const ops = <F extends keyof Functions, A extends readonly [...Operator<any>[]]>(
// 	f: F, ...ops: A
// ) => (
// 	async: (...args: OpFuncs<Tag.StaticAsync, A>) => AsyncFunc<F>,
// ) => closure(
// 	f,
// 	...ops.map(o => Object.values(o[Tag.StaticAsync])[0])
// )(
// 	ops.map(o => Object.values(o[Tag.StaticSync])[0]),
// 	async as any
// );

// export const Operator = Object.assign(operator1, {closure, ops});

// // interface BuildOperator<$ extends $Tag>{
// // 	(statics: Tagged<Tag.StaticAsync>[$] | Statics<$>): Operator<$>;

// // 	closure<A extends readonly any[]>(...async: A): (
// // 		sync  : UnasyncArgs<A>,
// // 		build : (...args: A) => Tagged<Tag.StaticAsync>[$],
// // 	) => Operator<$>;

// // 	ops<A extends readonly [...Operator<any>[]]>(...ops: A): (
// // 		async: (...args: OpFuncs<Tag.StaticAsync, A>) => Tagged<Tag.StaticAsync>[$]
// // 	) => Operator<$>
// // }

// // const operator = (function operator<$ extends $Tag>(
// // 	statics: Tagged<Tag.StaticAsync>[$] | Statics<$>
// // ): Operator<$> {
// // 	if(!isStatics(statics))
// // 		statics = {sync: syncObj(statics as any) as any, async: statics};

// // 	return {
// // 		[Tag.StaticSync]    : statics.sync,
// // 		[Tag.StaticAsync]   : statics.async,
// // 		[Tag.InstanceSync]  : instance(statics.sync),
// // 		[Tag.InstanceAsync] : instance(statics.async),
// // 	};
// // }) as BuildOperator<$Tag>;

// // operator.closure = (...async) => (sync, build) => operator({
// // 	async : build(...async),
// // 	// @ts-ignore
// // 	sync  : syncObj(build(...sync))
// // });

// // operator.ops = (...ops) => async =>
// // 	operator.closure(
// // 		...ops.map(o => Object.values(o[Tag.StaticAsync])[0])
// // 	)(
// // 		ops.map(o => Object.values(o[Tag.StaticSync])[0]),
// // 		async as any
// // 	);

// // export const Operator = <$ extends $Tag>() => operator as any as BuildOperator<$>;

// // // function operator<$ extends $Tag>(_:$) {
// // // 	const rtn = Object.assign(
// // // 		function operator(statics: Tagged<Tag.StaticAsync>[$] | Statics<$>): Operator<$>{
// // // 			if(!isStatics(statics))
// // // 				statics = {sync: syncObj(statics as any) as any, async: statics};

// // // 			return {
// // // 				[Tag.StaticSync]    : statics.sync,
// // // 				[Tag.StaticAsync]   : statics.async,
// // // 				[Tag.InstanceSync]  : instance(statics.sync),
// // // 				[Tag.InstanceAsync] : instance(statics.async),
// // // 			};
// // // 		}, {
// // // 			closure: <A extends readonly any[]>(...async: A) => (
// // // 				sync  : UnasyncArgs<A>,
// // // 				build : (...args: A) => Tagged<Tag.StaticAsync>[$],
// // // 			): Operator<$> => rtn({
// // // 				async : build(...async),
// // // 				// @ts-ignore
// // // 				sync  : syncObj(build(...sync)) as any
// // // 			}),

// // // 			ops: <A extends readonly [...Operator<any>[]]>(...ops: A) => (
// // // 				async : (...args: OpFuncs<Tag.StaticAsync, A>) => Tagged<Tag.StaticAsync>[$],
// // // 			): Operator<$> => rtn.closure(...ops.map(o => Object.values(o[Tag.StaticAsync])[0]))(
// // // 				ops.map(o => Object.values(o[Tag.StaticSync])[0]) as any,
// // // 				async as any,
// // // 			),
// // // 		}
// // // 	);
// // // 	return rtn;
// // // }

// // // class $NewerOperator<$ extends $Tag>{
// // // 	closure<A extends any[]>(
// // // 		async : A,
// // // 		sync  : UnasyncArgs<A>,
// // // 		build : (...args: A) => Tagged<Tag.StaticAsync>[$],
// // // 	): Operator<$>{
// // // 		return operator({async: build(...async), sync: syncFunc(build)(...sync) as any});
// // // 	}

// // // 	ops<A extends Operator<any>[]>(
// // // 		ops   : A,
// // // 		async : (...args: A) => Tagged<Tag.StaticAsync>[$],
// // // 	): Operator<$>{
// // // 		const x = null as any as A[number][Tag.StaticAsync][keyof A[number][Tag.StaticAsync]];
// // // 		return operator({
// // // 			async : async(...(ops.map(o => o[Tag.StaticAsync]))),
// // // 			sync  : syncFunc(async)(...(ops.map(o => o[Tag.StaticSync])))
// // // 		});
// // // 	}
// // // }
// // // export const NewerOperator = operator as NewerOperator;
// // // Object.assign(operator, {
// // // 	closure: <$ extends $Tag, A extends any[]>(
// // // 		async : A,
// // // 		sync  : UnasyncArgs<A>,
// // // 		build : (...args: A) => Tagged<Tag.StaticAsync>[$],
// // // 	): Operator<$> => operator({async: build(...async), sync: syncFunc(build)(...sync) as any}),

// // // 	uses: <$ extends $Tag,
// // // });

// // // Object.assign({
// // // 	Operator<$ extends $Tag>(statics: Tagged<Tag.StaticAsync>[$] | Statics<$>): Operator<$>;
// // // 	Operator<$ extends $Tag, A extends any[]>
// // // })

// // // export function NewOperator<$ extends $Tag>(
// // // 	statics: Tagged<Tag.StaticAsync>[$] | Statics<$>
// // // ): Operator<$>;

// // // export function NewOperator<$ extends $Tag, A extends any[]>(
// // // 	async: A, sync: UnasyncArgs<A>, build: (...args: A) => Tagged<Tag.StaticAsync>[$]
// // // ): Operator<$>;

// // // export function NewOperator<$ extends $Tag, A extends any[] = any[]>(
// // // 	...args:
// // // 		| [Tagged<Tag.StaticAsync>[$] | Statics<$>]
// // // 		| [A, UnasyncArgs<A>, (...args: A) => Tagged<Tag.StaticAsync>[$]]
// // // ): Operator<$>{
// // // 	if(args.length === 1){
// // // 		let statics: Statics<$>;
// // // 		if(isStatics(args[0]))
// // // 			statics = args[0];
// // // 		else
// // // 			statics = {sync: syncObj(args[0] as any) as any, async: args[0]};
// // // 		return {
// // // 			[Tag.StaticSync]    : statics.sync,
// // // 			[Tag.StaticAsync]   : statics.async,
// // // 			[Tag.InstanceSync]  : instance(statics.sync),
// // // 			[Tag.InstanceAsync] : instance(statics.async),
// // // 		};
// // // 	}

// // // 	return NewOperator({async: args[2](...args[0]), sync: syncFunc(args[2])(...args[1]) as any});
// // // }