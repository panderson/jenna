export const syncIterator = <I extends Iterable<any>, R = any, N = unknown>(
	i: I
): Iterator<I, R, N> => i[Symbol.iterator]();