export type Appliable<F extends (...args: any) => any> =
	F extends (...args: infer A) => infer R
		? (args: A) => R : never;

export const appliable = <F extends (...args: any) => any>(fn: F): Appliable<F> =>
	fn.apply.bind(fn, null) as any;