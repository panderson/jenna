import {Tag}             from "#tags";
import {All}             from "@operators";
import {buildAddMethods} from "@utils";
import {AsyncIterable}   from "./class";
import {AsyncInterfaces} from "./interface";

export * from "./interface";

export const methods: AsyncInterfaces = Object.freeze(
	Object.assign({}, All[Tag.InstanceAsync])
);

export const addMethods = buildAddMethods(methods);

addMethods("AsyncIterable", AsyncIterable);
Object.assign(AsyncIterable, All[Tag.StaticAsync]);