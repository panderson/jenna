import {Tag} from "#tags";
import {All} from "@operators";

export interface AsyncInterfaces          extends All<Tag.InstanceAsync> {}
export interface AsyncIterableConstructor extends AsyncIterableBase, All<Tag.StaticAsync> {}

export interface AsyncIterableBase {
	/**
	 * Returns an empty iterable of the given type.
	 */
	empty<T>(): AsyncGenerator<T>;

	/** Casts an iterable object as an iterable, unwrapping promises. */
	from<T>(iterable: AnyIterable<Promise<T>>): AsyncIterable<T> & AsyncInterfaces;
	/**
	 * Casts an iterable object as an iterable.
	 * @param iterable An array, generator, or other iterable.
	 */
	from<T>(iterable: AnyIterable<T> | AnyIterator<T>): AsyncIterable<T> & AsyncInterfaces;
	/** Casts an array-like object as an iterable. */
	from<T>(arrayLike: ArrayLike<T>): AsyncIterable<T> & AsyncInterfaces;

	/** Determines whether the given object implements the async iterable protocol. */
	is(obj: any): obj is AsyncIterable<any>;

	/**
	 * Returns an iterable that starts at `0` and ends after yielding the given number of values.
	 * @param count The number of consecutive values to yield.
	 */
	range(count: number): AsyncGenerator<number>;
	/**
	 * Returns an iterable that starts at the given start value and ends after yielding the given
	 * number of values.
	 * @param start The first value to yield.
	 * @param stop  The total number of values to yield.
	 */
	range(start: number, count: number): AsyncGenerator<number>;

	/**
	 * Yields the given value the given number of times, or infinitely if `count` is omitted.
	 * @param element The value to repeat.
	 * @param count The number of times to yield the value.  If omitted, the value is yielded as
	 * long as the iterable is iterated.
	 */
	repeat<T>(element: T, count?: number): AsyncGenerator<T>;
}