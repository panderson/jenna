export const empty = <T>(): AsyncIterable<T> => ({
	[Symbol.asyncIterator]: () => ({
		next: (): Promise<IteratorResult<T, any>> => Promise.resolve({done: true, value: undefined})
	})
});

export const toAsync = <T>(iterable: Iterable<T>): AsyncIterable<T> => ({
	[Symbol.asyncIterator]() {
		const it = iterable[Symbol.iterator]();
		return {
			next: (): Promise<IteratorResult<T, any>> => Promise.resolve(it.next())
		};
	}
});

export function iterateArrayLike<T>(arrayLike: ArrayLike<T>): AsyncIterable<T> {
	let i = 0;
	return {
		[Symbol.asyncIterator]: () => ({
			next(): Promise<IteratorResult<T, any>> {
				if (i >= arrayLike.length)
					return Promise.resolve({done: true, value: undefined});
				return Promise.resolve({value: arrayLike[i++]});
			}
		})
	};
}

export const range = (start: number, end: number): AsyncIterable<number> => ({
	[Symbol.asyncIterator]: () => ({
		next(): Promise<IteratorResult<number, any>> {
			if (start >= end)
				return Promise.resolve({done: true, value: undefined});
			return Promise.resolve({value: start++});
		}
	})
});

export function repeat<T>(value: T, count?: number): AsyncIterable<T> {
	if (count === undefined) {
		return {
			[Symbol.asyncIterator]: () => ({
				next: (): Promise<IteratorResult<T, any>> => Promise.resolve({value})
			})
		};
	}
	return {
		[Symbol.asyncIterator]: () => ({
			next(): Promise<IteratorResult<T, any>> {
				if (count! > 0) {
					count!--;
					return Promise.resolve({value});
				}
				return Promise.resolve({done: true, value: undefined});
			}
		})
	}
}