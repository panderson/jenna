import {empty, iterateArrayLike, range, repeat, toAsync} from "./iterate";

const isAsync = <T>(obj: object): obj is globalThis.AsyncIterable<T> =>
	Symbol.asyncIterator in obj && typeof obj[Symbol.asyncIterator] === "function";

const isSync = <T>(obj: object): obj is Iterable<T> =>
	Symbol.iterator in obj && typeof obj[Symbol.iterator] === "function";

export class AsyncIterable<T> {
	constructor(iterable: globalThis.AsyncIterable<T> | AnyIterator<T>) {
		if ("next" in iterable && typeof iterable.next === "function") {
			Object.defineProperty(this, "next", {value: iterable.next.bind(iterable)});

			if ("return" in iterable && typeof iterable.return === "function")
				Object.defineProperty(this, "return", {value: iterable.return.bind(iterable)});

			if ("throw" in iterable && typeof iterable.throw === "function")
				Object.defineProperty(this, "throw", {value: iterable.throw.bind(iterable)});
		}

		if (isAsync(iterable)) {
			Object.defineProperty(
				this,
				Symbol.asyncIterator,
				{value: iterable[Symbol.asyncIterator].bind(iterable)}
			);
		} else if ("next" in iterable)
			Object.defineProperty(this, Symbol.asyncIterator, {value: () => this});
		else
			throw new Error("Given object isn't iterable.");
	}

	static empty<T>(): AsyncIterable<T> {
		return new AsyncIterable(empty());
	}

	static is(obj: unknown): obj is globalThis.AsyncIterable<unknown> {
		return typeof obj === "object" && !!obj && isAsync(obj);
	}

	static from<T>(
		iterable: AnyIterable<T> | AnyIterator<T> | ArrayLike<T> | string
	): AsyncIterable<T> {
		switch (typeof iterable) {
			case "string":
				return new AsyncIterable(iterateArrayLike(iterable));
			case "object":
				break;
			default:
				throw new Error("Given value isn't iterable.");
		}

		if ("length" in iterable)
			return new AsyncIterable(iterateArrayLike(iterable));

		if (isAsync(iterable))
			return new AsyncIterable(iterable);

		if (isSync(iterable))
			return new AsyncIterable(toAsync(iterable));

		return new AsyncIterable(iterable);
	}

	static range(count: number): AsyncIterable<number>;
	static range(start: number, count: number): AsyncIterable<number>;
	static range(param1: number, param2?: number): AsyncIterable<number> {
		if (arguments.length === 1)
			return new AsyncIterable(range(0, param1));
		return new AsyncIterable(range(param1, param1 + param2!));
	}

	static repeat<T>(element: T, count?: number): AsyncIterable<T> {
		return new AsyncIterable(repeat(element, count));
	}
}