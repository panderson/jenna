import {AsyncIterable as _AsyncIterable}           from "./class";
import {AsyncInterfaces, AsyncIterableConstructor} from "./interface";

import "./setup";

export type ExtendedAsyncIterable<T> = globalThis.AsyncIterable<T> & AsyncInterfaces;
export type ExtendedAsyncIterableIterator<T, R = any, N = undefined> =
	& ExtendedAsyncIterable<T>
	& AsyncIterator<T, R, N>;

export type ExtendedAsyncGenerator<T, R = any, N = unknown> =
	& AsyncGenerator<T, R, N>
	& AsyncInterfaces;

export type ExtendedAsyncIterableConstructor = AsyncIterableConstructor & {
	new <T>(iterable: globalThis.AsyncIterable<T>): ExtendedAsyncIterable<T>;
	new <T>(iterable: AsyncIterableIterator<T>): ExtendedAsyncIterableIterator<T>;

	new <T, R = any, N = undefined>(
		iterable: globalThis.AsyncIterable<T> & AsyncIterator<T, R, N>
	): ExtendedAsyncIterableIterator<T, R, N>;

	new <T, R = any, N = undefined>(
		iterator: AsyncIterator<T, R, N>
	): ExtendedAsyncIterableIterator<T, R, N>

	new <T, R = any, N = unknown>(
		generator: AsyncGenerator<T, R, N>
	): ExtendedAsyncGenerator<T, R, N>;
};

export const AsyncIterable = _AsyncIterable as ExtendedAsyncIterableConstructor;