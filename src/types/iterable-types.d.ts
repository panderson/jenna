interface Iterable<T> {
	[Symbol.iterator]<TReturn = any, TNext = unknown>(): Iterator<T, TReturn, TNext>;
}

interface AsyncIterable<T> {
	[Symbol.asyncIterator]<TReturn = any, TNext = unknown>(): AsyncIterator<T, TReturn, TNext>;
}