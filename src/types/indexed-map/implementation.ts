import {
	Arg1,
	Arg2,
	ForEachCallback,
	// IndexedMap as Interface,
	IndexedMapConstructor
}                           from "./interface";
import {
	existsHandler,
	getMap,
	missingHandler
}                           from "./utils";

const metadata = {
	raw     : Symbol(),
	keys    : Symbol(),
	values  : Symbol(),
	entries : Symbol(),
	size    : Symbol(),
	has     : Symbol(),
	clear   : Symbol(),
	forEach : Symbol(),
};

class CIndexedMap<Key extends string, Value> {
	static readonly raw     = metadata.raw;
	static readonly keys    = metadata.keys;
	static readonly values  = metadata.values;
	static readonly entries = metadata.entries;
	static readonly size    = metadata.size;
	static readonly has     = metadata.has;
	static readonly clear   = metadata.clear;
	static readonly forEach = metadata.forEach;

	constructor(arg1?: Arg1<Key, Value>, arg2?: Arg2<Key, Value>) {
		console.log("in the class constructor");
		const onMissing  = missingHandler(arg1, arg2);
		const onExists   = existsHandler(arg1, arg2);
		let   extensible = true;

		return new Proxy(getMap(arg1), {
			has(map, key) {
				return key === Symbol.iterator || key === "size" || map.has(key as Key);
			},
			ownKeys(map) {
				return Array.from(map.keys());
			},
			deleteProperty(map, key) {
				return map.delete(key as Key);
			},
			isExtensible() {return extensible;},
			preventExtensions() {
				extensible = false;
				return true;
			},
			get(map, key) {
				switch (key) {
					case Symbol.iterator    : return map[key as typeof Symbol.iterator];
					case metadata.clear     : return map.clear.bind(map);
					case metadata.entries   : return map.entries.bind(map);
					case metadata.forEach   :
						return (fn: ForEachCallback<Key, Value, true>) =>
							map.forEach((val, key) => fn(val as any, key, this as any));
					case metadata.has       : return map.has.bind(map);
					case metadata.keys      : return map.keys.bind(map);
					case metadata.raw       : return map;
					case metadata.size      : return map.size;
					case metadata.values    : return map.values.bind(map);
					case "constructor"      : return map.constructor;
					case "hasOwnProperty"   : return map.has.bind(map);
					case "toString"         : return Object.prototype.toString.bind(map);
					case "valueOf"          : return () => map;
				}
				if (typeof key === "symbol")
					return undefined;
				return map.get(key as Key) ?? onMissing(key as Key);
			},
			set (map, key, value) {
				if (key === Symbol.iterator || key === "size")
					return false;

				if (!extensible && !map.has(key as Key))
					return false;

				if (onExists && map.has(key as Key)) {
					const existing = map.get(key as Key);
					value = onExists(key as Key, existing, value);
					if (value === undefined)
						return false;
				}

				map.set(key as Key, value);
				return true;
			},
			defineProperty() {return false;},
			setPrototypeOf() {return false;},
			getOwnPropertyDescriptor(map, key) {
				if (key === Symbol.iterator || key === "size")
					return Object.getOwnPropertyDescriptor(map, key);

				if (!map.has(key as Key))
					return undefined;

				return {
					configurable : true,
					enumerable   : true,
					writable     : true,
					value        : map.get(key as Key),
				};
			},
			getPrototypeOf() {return null;},
		});
	}
}

export const IndexedMap: IndexedMapConstructor = CIndexedMap as any;

// export const IndexedMap: IndexedMapConstructor = Object.assign(
// 	function IxMap<Key extends string, Value>(
// 		arg1?: Arg1<Key, Value>,
// 		arg2?: Arg2<Key, Value>,
// 	): Interface<Key, Value> {
// 		const onMissing  = missingHandler(arg1, arg2);
// 		const onExists   = existsHandler(arg1, arg2);
// 		let   extensible = true;

// 		return new Proxy(getMap(arg1), {
// 			has(map, key) {
// 				return key === Symbol.iterator || key === "size" || map.has(key as Key);
// 			},
// 			ownKeys(map) {
// 				return map.keys().toArray();
// 			},
// 			deleteProperty(map, key) {
// 				return map.delete(key as Key);
// 			},
// 			isExtensible() {return extensible;},
// 			preventExtensions() {
// 				extensible = false;
// 				return true;
// 			},
// 			get(map, key) {
// 				switch (key) {
// 					case Symbol.iterator    : return map[key as typeof Symbol.iterator];
// 					case metadata.clear     : return map.clear.bind(map);
// 					case metadata.entries   : return map.entries.bind(map);
// 					case metadata.forEach   :
// 						return (fn: ForEachCallback<Key, Value, true>) =>
// 							map.forEach((val, key) => fn(val as any, key, this as any));
// 					case metadata.has       : return map.has.bind(map);
// 					case metadata.keys      : return map.keys.bind(map);
// 					case metadata.raw       : return map;
// 					case metadata.size      : return map.size;
// 					case metadata.values    : return map.values.bind(map);
// 					case "constructor"      : return map.constructor;
// 					case "hasOwnProperty"   : return map.has.bind(map);
// 					case "toString"         : return Object.prototype.toString.bind(map);
// 					case "valueOf"          : return () => map;
// 				}
// 				if (typeof key === "symbol")
// 					return undefined;
// 				return map.get(key as Key) ?? onMissing(key as Key);
// 			},
// 			set(map, key, value) {
// 				if (key === Symbol.iterator || key === "size")
// 					return false;

// 				if (!extensible && !map.has(key as Key))
// 					return false;

// 				if (onExists && map.has(key as Key)) {
// 					const existing = map.get(key as Key);
// 					value = onExists(key as Key, existing, value);
// 					if (value === undefined)
// 						return false;
// 				}

// 				map.set(key as Key, value);
// 				return true;
// 			},
// 			defineProperty() {return false;},
// 			setPrototypeOf() {return false;},
// 			getOwnPropertyDescriptor(map, key) {
// 				if (key === Symbol.iterator || key === "size")
// 					return Object.getOwnPropertyDescriptor(map, key);

// 				if (!map.has(key as Key))
// 					return undefined;

// 				return {
// 					configurable : true,
// 					enumerable   : true,
// 					writable     : true,
// 					value        : map.get(key as Key),
// 				};
// 			},
// 			getPrototypeOf() {return null;},
// 		}) as any;
// 	},
// 	metadata
// ) as any;