declare const raw     : unique symbol;
declare const keys    : unique symbol;
declare const values  : unique symbol;
declare const entries : unique symbol;
declare const size    : unique symbol;
declare const has     : unique symbol;
declare const clear   : unique symbol;
declare const forEach : unique symbol;

// interface Metadata {
// 	readonly raw     : typeof raw;
// 	readonly keys    : typeof keys;
// 	readonly values  : typeof values;
// 	readonly entries : typeof entries;
// 	readonly size    : typeof size;
// 	readonly has     : typeof has;
// 	readonly clear   : typeof clear;
// 	readonly forEach : typeof forEach;
// }

type CommonOptions<Key extends string, Value> =
	| {replaceable?: boolean}
	| {onExists(key: Key, existing: Value, newValue: Value): Value | undefined};

type NonNullOptions<Key extends string, Value> = (
	| {nullable?: false}
	| {onMissing(key: Key): Value}
) & CommonOptions<Key, Value>;

type NullableOptions<Key extends string, Value> = (
	| {nullable: true}
	| {onMissing(key: Key): Value | undefined}
) & CommonOptions<Key, Value>;

type DataOptions<Key extends string, Value> = (
	| {map?  : Map<Key, Value>}
	| {items : Iterable<[Key, Value]>}
);

type NonNullDataOptions<Key extends string, Value> =
	DataOptions<Key, Value> & NonNullOptions<Key, Value>;

type NullableDataOptions<Key extends string, Value> =
	DataOptions<Key, Value> & NullableOptions<Key, Value>;

export type Arg1<Key extends string, Value> =
	| Map<Key, Value>
	| Iterable<[Key, Value]>
	| NonNullDataOptions<Key, Value>
	| NullableDataOptions<Key, Value>;

export type Arg2<Key extends string, Value> =
	| NonNullOptions<Key, Value>
	| NullableOptions<Key, Value>;

export interface IndexedMapConstructor {
	readonly raw     : typeof raw;
	readonly keys    : typeof keys;
	readonly values  : typeof values;
	readonly entries : typeof entries;
	readonly size    : typeof size;
	readonly has     : typeof has;
	readonly clear   : typeof clear;
	readonly forEach : typeof forEach;

	new <Key extends string, Value>(
		options?: NonNullDataOptions<Key, Value>
	): IndexedMap<Key, Value, false>;

	new <Key extends string, Value>(
		options?: NullableDataOptions<Key, Value>
	): IndexedMap<Key, Value, true>;

	new <Key extends string, Value>(
		map      : Map<Key, Value>,
		options? : NonNullOptions<Key, Value>,
	): IndexedMap<Key, Value, false>;

	new <Key extends string, Value>(
		map      : Map<Key, Value>,
		options? : NullableOptions<Key, Value>,
	): IndexedMap<Key, Value, true>;

	new <Key extends string, Value>(
		items    : Iterable<[Key, Value]>,
		options? : NonNullOptions<Key, Value>,
	): IndexedMap<Key, Value, false>;

	new <Key extends string, Value>(
		items    : Iterable<[Key, Value]>,
		options? : NullableOptions<Key, Value>,
	): IndexedMap<Key, Value, true>;
}

export type ForEachCallback<Key extends string, Value, Nullable extends boolean> =
	(value: Value, key: Key, map: IndexedMap<Key, Value, Nullable>) => void;

export type ForEach<Key extends string, Value, Nullable extends boolean> =
	(callbackfn: ForEachCallback<Key, Value, Nullable>) => void;

export type IndexedMap<Key extends string, Value, Nullable extends boolean = false> = {
	[raw]     : Map<Key, Value>;
	[keys]    : Map<Key, Value>["keys"];
	[values]  : Map<Key, Value>["values"];
	[entries] : Map<Key, Value>["entries"];
	[size]    : Map<Key, Value>["size"];
	[has]     : Map<Key, Value>["has"];
	[clear]   : Map<Key, Value>["clear"];
	[forEach] : ForEach<Key, Value, Nullable>;
} & (
	Nullable extends true
	? {[key in Key]? : Value;}
	: {[key in Key]  : Value;}
);