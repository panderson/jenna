import {Arg1, Arg2} from "./interface";

export const isIterable = (o: object): o is Iterable<any> => Symbol.iterator in o;

export function keyNotFound<Key extends string>(key: Key): never {
	throw new Error(`Key not found: ${String(key)}`);
}

export function keyExists<Key extends string>(key: Key): never {
	throw new Error(`Key already exists: ${String(key)}`);
}

export const empty = () => undefined;

export function getMap<Key extends string, Value>(
	options: Arg1<Key, Value> | undefined
): Map<Key, Value> {
	if (options) {
		if (options instanceof Map)
			return options;
		if (isIterable(options))
			return new Map(options);
		if ("map" in options && options.map)
			return options.map;
		if ("items" in options)
			return new Map(options.items);
	}
	return new Map();
}

export function missingHandler<Key extends string>(
	arg1: Arg1<Key, any> | undefined,
	arg2: Arg2<Key, any> | undefined,
) {
	if (arg1) {
		if ("nullable" in arg1)
			return arg1.nullable ? empty : keyNotFound;
		if ("onMissing" in arg1)
			return arg1.onMissing;
	}
	if (arg2) {
		if ("nullable" in arg2)
			return arg2.nullable ? empty : keyNotFound;
		if ("onMissing" in arg2)
			return arg2.onMissing;
	}

	return keyNotFound;
};

export function existsHandler<Key extends string, Value>(
	arg1: Arg1<Key, Value> | undefined,
	arg2: Arg2<Key, Value> | undefined,
): ((key: Key, existing: Value, newValue: Value) => Value | undefined) | undefined {
	if (arg1) {
		if ("replaceable" in arg1)
			return arg1.replaceable ? undefined : keyExists;
		if ("onExists" in arg1)
			return arg1.onExists;
	}
	if (arg2) {
		if ("replaceable" in arg2)
			return arg2.replaceable ? undefined : keyExists;
		if ("onExists" in arg2)
			return arg2.onExists;
	}
	return undefined;
}