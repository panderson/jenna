export class DefaultMap<K, V> extends Map<K, V> {
	constructor(
		getDefault : (key: K) => V,
		entries?   : readonly (readonly [K, V])[] | null,
	);
	constructor(
		getDefault : (key: K) => V,
		entries    : Iterable<readonly [K, V]>,
	);
	constructor(
		private readonly getDefault : (key: K) => V,
		entries?                    : Iterable<readonly [K, V]> | null
	) {super(entries!);}

	get(key: K): V {
		let value = super.get(key);
		if (value !== void 0)
			return value;
		value = this.getDefault(key);
		super.set(key, value);
		return value;
	}
}