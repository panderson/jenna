import {Iterable}                   from "#types";
import {EntriesOf, KeyOf, ValuesOf} from "@utils";

export interface TypedObject {
	getOwnPropertyNames<T>(o: T): KeyOf<T>[];

	keys<T>   (o: T): KeyOf<T>[];
	values<T> (o: T): ValuesOf<T>;
	entries<T>(o: T): EntriesOf<T>;
	fromEntries<K extends keyof any, V>(entries: Iterable<readonly [K, V]>): Record<K, V>;

	create<T>(prototype: object, properties: TypedPropertyDescriptorMap<T>): T;

	recreate<T, U>(
		o  : T,
		fn : <K extends keyof T>(
			key      : K,
			property : TypedPropertyDescriptor<T[K]>
		) => [K, TypedPropertyDescriptor<U>]
	): Record<keyof T, U>;

	transform<T, K extends keyof any, V>(
		o  : T,
		fn : (key: KeyOf<T>, value: T[keyof T]) => readonly [K, V]
	): Record<K, V>;

	toMap<T extends {}, K = KeyOf<T>, V = T[keyof T]>(
		o          : T,
		transform? : (key: KeyOf<T>, value: T[keyof T]) => readonly [K, V]
	): Map<K, V>;
};

export const TypedObject = Object.freeze({
	getOwnPropertyNames : Object.getOwnPropertyNames,
	keys                : Object.keys,
	values              : Object.values,
	entries             : Object.entries,
	fromEntries         : Object.fromEntries,
	create              : Object.create,

	recreate<T, U>(
		o  : T,
		fn : <K extends keyof T>(
			key      : K,
			property : TypedPropertyDescriptor<T[K]>
		) => [K, TypedPropertyDescriptor<U>]
	): Record<keyof T, U> {
		return Object.create(
			Object.getPrototypeOf(o),
			Object.transform(Object.getOwnPropertyDescriptors(o), fn as any)
		) as any;
	},

	transform<T, K extends keyof any, V>(
		o  : T,
		fn : (key: KeyOf<T>, value: T[keyof T]) => readonly [K, V]
	): Record<K, V> {
		return TypedObject.fromEntries(
			Iterable.map(
				TypedObject.entries(o),
				([key, value]) => fn(key as any, value),
			)
		);
	},

	toMap<T extends {}, K = KeyOf<T>, V = T[keyof T]>(
		o          : T,
		transform? : (key: KeyOf<T>, value: T[keyof T]) => readonly [K, V]
	): Map<K, V> {
		if (transform) {
			return Iterable.toMap(
				Iterable.map(
					TypedObject.entries(o),
					([key, value]) => transform(key as any, value),
				)
			);
		}
		return Iterable.toMap(Object.entries(o)) as any;
	},
}) as TypedObject;