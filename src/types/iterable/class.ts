import {empty, iterateArrayLike, range, repeat} from "./iterate";

export class Iterable<T> {
	constructor(iterable: globalThis.Iterable<T> | Iterator<T>) {
		if ("next" in iterable && typeof iterable.next === "function") {
			Object.defineProperty(this, "next", {value: iterable.next.bind(iterable)});

			if ("return" in iterable && typeof iterable.return === "function")
				Object.defineProperty(this, "return", {value: iterable.return.bind(iterable)});

			if ("throw" in iterable && typeof iterable.throw === "function")
				Object.defineProperty(this, "throw", {value: iterable.throw.bind(iterable)});
		}

		if (Symbol.iterator in iterable && typeof iterable[Symbol.iterator] === "function") {
			Object.defineProperty(
				this,
				Symbol.iterator,
				{value: iterable[Symbol.iterator].bind(iterable)}
			);
		} else if ("next" in iterable)
			Object.defineProperty(this, Symbol.iterator, {value: () => this});
		else
			throw new Error("Given object isn't iterable.");
	}

	static empty<T>(): Iterable<T> {
		return new Iterable<T>(empty());
	}

	static is(obj: unknown): obj is globalThis.Iterable<unknown> {
		return (
			typeof obj === "object" &&
			!!obj &&
			Symbol.iterator in obj &&
			typeof obj[Symbol.iterator] === "function"
		);
	}

	static from<T>(
		iterable: globalThis.Iterable<T> | Iterator<T> | ArrayLike<T> | string
	): Iterable<T> {
		switch (typeof iterable) {
			case "string":
				return new Iterable(iterateArrayLike(iterable));
			case "object":
				break;
			default:
				throw new Error("Given value isn't iterable.");
		}

		if ("length" in iterable)
			return new Iterable(iterateArrayLike(iterable));

		return new Iterable(iterable);
	}

	static range(count: number): Iterable<number>;
	static range(start: number, count: number): Iterable<number>;
	static range(param1: number, param2?: number): Iterable<number> {
		if (arguments.length === 1)
			return new Iterable(range(0, param1));
		return new Iterable(range(param1, param1 + param2!));
	}

	static repeat<T>(element: T, count?: number): Iterable<T> {
		return new Iterable(repeat(element, count));
	}
}