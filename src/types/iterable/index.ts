import {Iterable as _Iterable}           from "./class";
import {Interfaces, IterableConstructor} from "./interface";

import "./setup";

export type ExtendedIterable<T> = globalThis.Iterable<T> & Interfaces;
export type ExtendedIterableIterator<T, R = any, N = undefined> =
	& ExtendedIterable<T>
	& Iterator<T, R, N>;

export type ExtendedGenerator<T, R = any, N = unknown> = Generator<T, R, N> & Interfaces;

export type ExtendedIterableConstructor = IterableConstructor & {
	new <T>(iterable: globalThis.Iterable<T>): ExtendedIterable<T>;
	new <T>(iterable: IterableIterator<T>): ExtendedIterableIterator<T>;

	new <T, R = any, N = undefined>(
		iterable: globalThis.Iterable<T> & Iterator<T, R, N>
	): ExtendedIterableIterator<T, R, N>;

	new <T, R = any, N = undefined>(
		iterator: Iterator<T, R, N>
	): ExtendedIterableIterator<T, R, N>;

	new <T, R = any, N = unknown>(
		generator: Generator<T, R, N>
	): ExtendedGenerator<T, R, N>;
};

export const Iterable = _Iterable as ExtendedIterableConstructor;