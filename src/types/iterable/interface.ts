import {Tag} from "#tags";
import {All} from "@operators";

export interface Interfaces          extends All<Tag.InstanceSync> {}
export interface IterableConstructor extends IterableBase, All<Tag.StaticSync> {}

export interface IterableBase {
	/**
	 * Returns an empty iterable of the given type.
	 */
	empty<T>(): Generator<T>;

	/**
	 * Ensures an iterable has instance methods from this library, wrapping it in a generator if
	 * necessary.
	 * @param iterable An array, generator, or other iterable.
	 */
	from<T>(iterable: Iterable<T> | Iterator<T>): Iterable<T> & Interfaces;
	/** Wraps an array-like object in a generator, with methods provided by this library. */
	from<T>(arrayLike: ArrayLike<T>): Iterable<T> & Interfaces;

	/** Determines whether the given object implements the iterable protocol. */
	is(obj: any): obj is Iterable<any>;

	/**
	 * Returns an iterable that starts at `0` and ends after yielding the given number of values.
	 * @param count The number of consecutive values to yield.
	 */
	range(count: number): Generator<number>;
	/**
	 * Returns an iterable that starts at the given start value and ends after yielding the given
	 * number of values.
	 * @param start The first value to yield.
	 * @param stop  The total number of values to yield.
	 */
	range(start: number, count: number): Generator<number>;

	/**
	 * Yields the given value the given number of times, or infinitely if `count` is omitted.
	 * @param element The value to repeat.
	 * @param count The number of times to yield the value.  If omitted, the value is yielded as
	 * long as the iterable is iterated.
	 */
	repeat<T>(element: T, count?: number): Generator<T>;
}