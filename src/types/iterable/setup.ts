import {Tag}             from "#tags";
import {All}             from "@operators";
import {buildAddMethods} from "@utils";
import {Iterable}        from "./class";
import {Interfaces}      from "./interface";

export * from "./interface";

export const methods: Interfaces = Object.freeze(
	Object.assign({}, All[Tag.InstanceSync])
);

export const addMethods = buildAddMethods(methods);

addMethods("Iterable", Iterable);
Object.assign(Iterable, All[Tag.StaticSync]);