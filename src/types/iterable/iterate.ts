export const empty = <T>(): Iterable<T> => ({
	[Symbol.iterator]: () => ({
		next: (): IteratorResult<T, any> => ({done: true, value: undefined})
	})
});

export function iterateArrayLike<T>(arrayLike: ArrayLike<T>): Iterable<T> {
	let i = 0;
	return {
		[Symbol.iterator]: () => ({
			next(): IteratorResult<T, any> {
				if (i >= arrayLike.length)
					return {done: true, value: undefined};
				return {value: arrayLike[i++]};
			}
		})
	};
}

export const range = (start: number, end: number): Iterable<number> => ({
	[Symbol.iterator]: () => ({
		next(): IteratorResult<number, any> {
			if (start >= end)
				return {done: true, value: undefined};
			return {value: start++};
		}
	})
});

export function repeat<T>(value: T, count?: number): Iterable<T> {
	if (count === undefined) {
		return {
			[Symbol.iterator]: () => ({
				next: (): IteratorResult<T, any> => ({value})
			})
		};
	}
	return {
		[Symbol.iterator]: () => ({
			next(): IteratorResult<T, any> {
				if (count! > 0) {
					count!--;
					return {value};
				}
				return {done: true, value: undefined};
			}
		})
	};
}