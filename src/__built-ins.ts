type Keys<T> = (keyof T)[];

export function keys<T>(prototype: T): Keys<T> {
	return <Keys<T>>Object.getOwnPropertyNames(prototype);
}

// This `MembersOf` type seems pretty redundant.  It seems it would return basically the same type
// as its generic type argument, and for most types, that's more or less true (although it does
// filter out members with Symbol and number keys).  Its main purpose is to disambiguate the type
// `keyof Array<T>`.  Because an array's keys are its elements as well as its members, weird things
// happen when you build a type based on `keyof Array<T>` (or variants thereof).
export type MembersOf<T> = {[K in keyof T as string & K]: T[K]};

type FunctionKeys<T> = {
	[K in keyof MembersOf<T>]: T[K] extends Function ? K : never
}[keyof MembersOf<T>];

// type FunctionsOf<T> = {[K in FunctionKeys<T>]: T[K]};
type FunctionMap<T> = Map<FunctionKeys<T>, Function>;

export function functions<T>(prototype: T): FunctionMap<T> {
	return new Map(
		keys(prototype)
		.map(name => {
			try {
				if (name === "constructor")
					return [name, undefined];
				return <any>[name, prototype[name]];
			} catch {
				return [name, undefined];
			}
		})
		.filter(([_, value]) => typeof value === "function")
	);
}

const __generator = <Generator>Object.getPrototypeOf(function*(){}());
const __asyncGenerator = <AsyncGenerator>Object.getPrototypeOf(async function*(){}());

export const builtIns = {
	Array          : undefined as any as FunctionMap<Array<any>>,
	Set            : undefined as any as FunctionMap<Set<any>>,
	Map            : undefined as any as FunctionMap<Map<any, any>>,
	Generator      : undefined as any as FunctionMap<typeof __generator>,
	AsyncGenerator : undefined as any as FunctionMap<typeof __asyncGenerator>,
	String         : undefined as any as FunctionMap<String>,
};