import {Tag}       from "#tags";
import {Operators} from "./types";
import {operators} from "./implementation";

export type All<T extends Tag> = Operators<T>;
export {operators as All};