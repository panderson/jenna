import {Tag, Tagged} from "#tags";

interface LastIndexDocs {
	/**
	 * Returns the index of the last occurrence of a specified value in an iterable, or -1 if it is
	 * not present.
	 *
	 * This function is similar to `Array.prototype.lastIndexOf()`.
	 */
	lastIndexOf: any;
}

interface LastIndexStatic<T extends Tag.Static> extends LastIndexDocs {
	/**
	 * @param iterable The iterable to search.  If the `maxIndex` is reached before the iterable is
	 * fully exhausted, iteration stops.
	 * @param element The value to locate.  Elements are compared using `===`.
	 * @param maxIndex The array index at which to stop searching.  If omitted, the search stops
	 * when the iterable is exhausted.
	 */
	lastIndexOf<I>(
		iterable  : Tagged.Iterable<T, I>,
		element   : I,
		maxIndex? : number
	): Tagged.Promise<T, number>;
}

interface LastIndexInstance<T extends Tag.Instance> extends LastIndexDocs {
	/**
	 * @param element The value to locate.  Elements are compared using `===`.
	 * @param maxIndex The array index at which to stop searching.  If omitted, the search stops
	 * when the iterable is exhausted.
	 */
	lastIndexOf<I>(
		this      : Tagged.Iterable<T, I>,
		element   : I,
		maxIndex? : number
	): Tagged.Promise<T, number>;
}

export type LastIndex<T extends Tag> =
	T extends Tag.Static
		? LastIndexStatic<T> :
	T extends Tag.Instance
		? LastIndexInstance<T> :
	never;