import {Tag, Tagged} from "#tags";

interface JoinDocs {
	/**
	 * Concatenates all the elements of an iterable to a string, separated by the specified
	 * separator string, if given.
	 *
	 * This is similar to `Array.prototype.join()` except that the default separator isn't a comma.
	 */
	join: any;
}

interface JoinStatic<T extends Tag.Static> extends JoinDocs {
	/**
	 * @param separator A string used to separate one element of the iterable from the next in the
	 * resulting string.  If omitted, the elements are joined without a separator.
	 */
	join<I>(iterable: Tagged.Iterable<T, I>, separator?: string): Tagged.Promise<T, string>;
}

interface JoinInstance<T extends Tag.Instance> extends JoinDocs {
	/**
	 * @param separator A string used to separate one element of the iterable from the next in the
	 * resulting string.  If omitted, the elements are joined without a separator.
	 */
	join<I>(this: Tagged.Iterable<T, I>, separator?: string): Tagged.Promise<T, string>
}

export type Join<T extends Tag> =
	T extends Tag.Static ? JoinStatic<T> : T extends Tag.Instance ? JoinInstance<T> : never;