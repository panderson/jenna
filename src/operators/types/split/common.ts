export interface SplitDocs {
	/**
	 * Splits an iterator into parts, analogously to `String.prototype.split`, but for any iterable.
	 *
	 * The `split` function returns a generator of arrays. Each item yielded by `split` is an array,
	 * whose elements are items from the original iterator that are in between separators.
	 *
	 * If a separator occurs at the beginning or end of the sequence, or if two separators appear
	 * consecutively, then an empty sequence will be yielded, unless `omitEmpty` is `true`.
	 */
	split: any;
}

export interface CommonOptions {
	/**
	 * The maximum number of parts in the resulting sequence. Specifying a limit of `n` will result
	 * in at most `n` generators being yielded. Note that this includes empty sequences due to
	 * consecutive, leading, or trailing separators, and also includes sequences consisting of the
	 * separators themselves.
	 *
	 * If `limit` is not given, or is less than `0`, then the number of parts will not be limited.
	 *
	 * @default - No limit
	 */
	limit?         : number;

	/**
	 * Whether or not to yield parts consisting of the separator itself. One of the following
	 * values:
	 *
	 *   * **`false`** - Don't yield separators.
	 *   * **`true`** - Yield additional arrays in between parts, each consisting of a single value,
	 *     the separator itself.
	 *   * **`"end"`** - Include the separators at the ends of the parts that precede them.
	 *   * **`"start"`** - Include the separators at the beginnings of the parts that follow them.
	 *
	 * @example
	 * #### `false`
	 * ```ts
	 * Array.from(Iterable.split("abc-123--xxx-", '-').map(p => p.join('')))
	 * // [ 'abc', '123', '', 'xxx', '' ]
	 * ```
	 *
	 * @example
	 * #### `true`
	 * ```ts
	 * Array.from(
	 *     Iterable.split(
	 *         "abc-123--xxx-",
	 *         {separator: '-', keepSeparator: true}
	 *     )
	 *     .map(p => p.join(''))
	 * )
	 * // [ 'abc', '-', '123', '-', '', '-', 'xxx', '-', '' ]
	 * ```
	 *
	 * @example
	 * #### `"end"`
	 * ```ts
	 * Array.from(
	 *     Iterable.split(
	 *         "abc-123--xxx-",
	 *         {separator: '-', keepSeparator: "end"}
	 *     )
	 *     .map(p => p.join(''))
	 * )
	 * // [ 'abc-', '123-', '-', 'xxx-', '' ]
	 * ```
	 *
	 * @example
	 * #### `"start"`
	 * ```ts
	 * Array.from(
	 *     Iterable.split(
	 *         "abc-123--xxx-",
	 *         {separator: '-', keepSeparator: "start"}
	 *     )
	 *     .map(p => p.join(''))
	 * )
	 * // [ 'abc', '-123', '-', '-xxx', '-' ]
	 * ```
	 *
	 * @default false
	 */
	keepSeparator? : boolean | "end" | "start";

	/**
	 * Whether to omit empty parts. Empty parts can result if separators occur at the beginning or
	 * end of the original iterator, or if consecutive separators are found with no intervening
	 * elements.
	 *
	 * @example
	 * #### `false`
	 * ```ts
	 * Array.from(Iterable.split("-abc--123-", '-').map(p => p.join('')))
	 * // [ '', 'abc', '', '123', '' ]
	 * ```
	 *
	 * @example
	 * #### `true`
	 * ```ts
	 * Array.from(Iterable.split("-abc--123-", {separator: '-', omitEmpty: true}))
	 * // [ 'abc', '123' ]
	 * ```
	 */
	omitEmpty?     : boolean;
};

export type SeparatorFunc<I> = (element: I, index: number) => boolean;

export interface Separator<I> {
	/** The separator in between parts.  Elements will be compared to this value using `===`. */
	separator: I;
}

export interface ShouldSplit<I> {
	/**
	 * A function that accepts up to two arguments.  The first is the current element from the
	 * source sequence, and the second is the 0-based index of that element.  If `shouldSplit`
	 * returns `true`, then the element will be considered a separator and will end the current part
	 * and begin a new one.  If `keepSeparator` is `true`, this separator element itself will be
	 * yielded as a one-element part.
	 */
	shouldSplit : SeparatorFunc<I>;
}

export type SeparatorOptions<I>   = Separator<I> & CommonOptions;
export type ShouldSplitOptions<I> = ShouldSplit<I> & CommonOptions;

export type SplitOptions<I> = SeparatorOptions<I> | ShouldSplitOptions<I>;