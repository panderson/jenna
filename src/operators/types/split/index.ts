import {Tag, Tagged}                            from "#tags";
import {SeparatorFunc, SplitDocs, SplitOptions} from "./common";

interface SplitStatic<T extends Tag.Static> extends SplitDocs {
	/**
	 * @param iterable The source iterable to split.
	 * @param separator The separator in between parts, to which elements will be compared using the
	 * `===` operator.
	 *
	 * _or_
	 *
	 * A function that will be passed an element and the 0-based index of that element and which
	 * should return a boolean value determining whether or not that element should be considered
	 * to be a separator.
	 * @param limit The maximum number of parts in the resulting sequence. Specifying a limit of
	 * `n` will result in at most `n` generators being yielded. Note that this includes empty
	 * sequences due to consecutive, leading, or trailing separators, and also includes sequences
	 * consisting of the separators themselves.
	 *
	 * If `limit` is not given, or is less than `0`, then the number of parts will not be limited.
	 *
	 * default: No limit
	 */
	split<I, R = any, N = unknown>(
		iterable  : Tagged.Iterable<T, I>,
		separator : I | SeparatorFunc<I>,
		limit?    : number,
	): Tagged.Result.Generator<T, I[], R | undefined, N>;
	split<I, R = any, N = unknown>(
		iterable : Tagged.Iterable<T, I>,
		options  : SplitOptions<I>,
	): Tagged.Result.Generator<T, I[], R | undefined, N>;
}

interface SplitInstance<T extends Tag.Instance> extends SplitDocs {
	/**
	 * @param separator The separator in between parts, to which elements will be compared using the
	 * `===` operator.
	 *
	 * _or_
	 *
	 * A function that will be passed an element and the 0-based index of that element and which
	 * should return a boolean value determining whether or not that element should be considered
	 * to be a separator.
	 * @param limit The maximum number of parts in the resulting sequence. Specifying a limit of
	 * `n` will result in at most `n` generators being yielded. Note that this includes empty
	 * sequences due to consecutive, leading, or trailing separators, and also includes sequences
	 * consisting of the separators themselves.
	 *
	 * If `limit` is not given, or is less than `0`, then the number of parts will not be limited.
	 *
	 * default: No limit
	 */
	split<I, R = any, N = unknown>(
		this      : Tagged.Iterable<T, I>,
		separator : I | SeparatorFunc<I>,
		limit?    : number,
	): Tagged.Result.Generator<T, I[], R | undefined, N>;
	split<I, R = any, N = unknown>(
		this    : Tagged.Iterable<T, I>,
		options : SplitOptions<I>,
	): Tagged.Result.Generator<T, I[], R | undefined, N>;
}

export type Split<T extends Tag> =
	T extends Tag.Static ? SplitStatic<T> : T extends Tag.Instance ? SplitInstance<T> : never;