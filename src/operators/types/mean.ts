import {Tag, Tagged} from "#tags";

interface MeanDocs {
	/**
	 * Returns the sum of the elements of the iterable divided by their count.  If the iterable is
	 * empty, returns `undefined`.
	 */
	mean: any;
}

interface MeanStatic<T extends Tag.Static> extends MeanDocs {
	mean<I>(iterable: Tagged.Iterable<T, I>): Tagged.Promise<T, number | undefined>;
}

interface MeanInstance<T extends Tag.Instance> extends MeanDocs {
	mean<I>(this: Tagged.Iterable<T, I>): Tagged.Promise<T, number | undefined>;
}

export type Mean<T extends Tag> =
	T extends Tag.Static ? MeanStatic<T> : T extends Tag.Instance ? MeanInstance<T> : never;