import {Tag, Tagged} from "#tags";

interface WhileDocs {
	/** Yields items of the given iterable as long as a predicate returns `true`. */
	while: any;
}

interface WhileStatic<T extends Tag.Static> extends WhileDocs {
	/**
	 * @param iterable The iterable whose items are to be yielded.
	 * @param predicate A function that receives items from the iterable and returns a value
	 * indicating whether or not iteration should continue.  As long as it returns a truthy value,
	 * items will be yielded and iteration will continue.  The first time it returns a falsey value,
	 * that item will not be yielded and iteration will cease.
	 */
	while<I, R = any, N = unknown>(
		iterable  : Tagged.Iterable<T, I>,
		predicate : (item: I) => boolean
	): Tagged.Result.Generator<T, I, R | undefined, N>;
}

interface WhileInstance<T extends Tag.Instance> extends WhileDocs {
	/**
	 * @param predicate A function that receives items from the iterable and returns a value
	 * indicating whether or not iteration should continue.  As long as it returns a truthy value,
	 * items will be yielded and iteration will continue.  The first time it returns a falsey value,
	 * that item will not be yielded and iteration will cease.
	 */
	while<I, R = any, N = unknown>(
		this      : Tagged.Iterable<T, I>,
		predicate : (item: I) => boolean
	): Tagged.Result.Generator<T, I, R | undefined, N>;
}

export type While<T extends Tag> =
	T extends Tag.Static ? WhileStatic<T> : T extends Tag.Instance ? WhileInstance<T> : never;