import {Tag, Tagged} from "#tags";
import {Comparer}    from "@utils";

interface SortByDocs {
	/**
	 * Returns an array built from the given iterable and sorted by a key selected from each item.
	 *
	 * This function is similar to `Array.prototype.sort()`, but returns a new array.
	 */
	sortBy: any;
}

interface SortByStatic<T extends Tag.Static> extends SortByDocs {
	/**
	 * @param iterable The iterable to sort.
	 * @param keySelector A function used to select a key from each item.  The iterable is
	 * sorted based on a strict ordered comparison of these keys, using the default comparer as
	 * described for the `sort` function.
	 * @param reverse Whether to sort in reverse order.  The default value is `false`.
	 */
	sortBy<I, K>(
		iterable    : Tagged.Iterable<T, I>,
		keySelector : (item: I) => K,
		reverse?    : boolean | undefined,
	): Tagged.Promise<T, I[]>;

	/**
	 * @param iterable The iterable to sort.
	 * @param keySelector A function used to select a key from each item.  The iterable is
	 * sorted based on a strict ordered comparison of these keys.
	 * @param comparer A function used to compare keys, similar to the item compare function in
	 * the `sort` function, except that it receives keys instead of the original items.
	 * @param reverse Whether to sort in reverse order.  The default value is `false`.
	 */
	sortBy<I, K>(
		iterable    : Tagged.Iterable<T, I>,
		keySelector : (item: I) => K,
		comparer    : Comparer<K>,
		reverse?    : boolean | undefined,
	): Tagged.Promise<T, I[]>;
}

interface SortByInstance<T extends Tag.Instance> extends SortByDocs{
	/**
	 * Sorts the items of this iterable by a key selected from each item.
	 * @param keySelector A function used to select a key from each item.  The iterable is
	 * sorted based on a strict ordered comparison of these keys, using the default comparer as
	 * described for the `sort` method.
	 * @param reverse Whether to sort in reverse order.  The default value is `false`.
	 */
	sortBy<I, K>(
		this        : Tagged.Iterable<T, I>,
		keySelector : (item: I) => K,
		reverse?    : boolean | undefined,
	): Tagged.Promise<T, I[]>;

	/**
	 * Sorts the items of this iterable by a key selected from each item.
	 * @param keySelector A function used to select a key from each item.  The iterable is
	 * sorted based on a strict ordered comparison of these keys.
	 * @param comparer A function used to compare keys, similar to the item compare function in
	 * the `sort` method, except that it receives keys instead of the original items.
	 * @param reverse Whether to sort in reverse order.  The default value is `false`.
	 */
	sortBy<I, K>(
		this        : Tagged.Iterable<T, I>,
		keySelector : (item: I) => K,
		comparer    : Comparer<K>,
		reverse?    : boolean | undefined,
	): Tagged.Promise<T, I[]>;
}

export type SortBy<T extends Tag> =
	T extends Tag.Static ? SortByStatic<T> : T extends Tag.Instance ? SortByInstance<T> : never;