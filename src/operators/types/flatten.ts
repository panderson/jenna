import {Tag, Tagged} from "#tags";

interface FlattenDocs {
	/**
	 * Calls a selector function for each element of an iterable, then flattens the result into a
	 * new one-dimensional iterable.
	 *
	 * This is similar to `Array.prototype.flatMap()`.
	 */
	flatten: any;

	/*
	 * TODO:
	 *
	 *   * Rename to `flatMap` for parity with `Array.prototype.flatMap()`?
	 *   * Add `flat` with a depth parameter for parity with `Array.prototype.flat()`?
	 */
}

interface FlattenStatic<T extends Tag.Static> extends FlattenDocs {
	/**
	 * @param iterable The iterable to flatten.
	 * @param selector A function that takes up to two arguments and returns an iterable object.
	 * `flatten` calls this function for each element in the source iterable and yields each element
	 * in each resulting iterable.
	 */
	flatten<I, J, R = any, N = unknown>(
		iterable : Tagged.Iterable<T, I>,
		selector : (item: I, index: number) => Tagged.Iterable<T, J>,
	): Tagged.Result.Generator<T, J, R, N>;

	/**
	 * Flattens an iterable of iterables into a new one-dimensional iterable.
	 *
	 * This is similar to `Array.prototype.flat()`.
	 * @param iterable The iterable to flatten.
	 */
	flatten<I extends Iterable<any>, R = any, N = unknown>(
		iterable: Tagged.Iterable<T, I>
	): Tagged.Result.Generator<T, I extends Iterable<infer J> ? J : never, R, N>;
}

interface FlattenInstance<T extends Tag.Instance> extends FlattenDocs {
	/**
	 * @param selector A function that takes up to two arguments and returns an iterable object.
	 * `flatten` calls this function for each element in the source iterable and yields each element
	 * in each resulting iterable.
	 */
	flatten<I, J, R = any, N = unknown>(
		this     : Tagged.Iterable<T, I>,
		selector : (item: I, index: number) => Tagged.Iterable<T, J>,
	): Tagged.Result.Generator<T, J, R, N>;

	/**
	 * Flattens an iterable of iterables into a new one-dimensional iterable.
	 *
	 * This is similar to `Array.prototype.flat()`.
	 */
	flatten<I extends Iterable<any>, R = any, N = unknown>(
		this: Tagged.Iterable<T, I>
	): Tagged.Result.Generator<T, I extends Iterable<infer J> ? J : never, R, N>;
}

export type Flatten<T extends Tag> =
	T extends Tag.Static ? FlattenStatic<T> : T extends Tag.Instance ? FlattenInstance<T> : never;