import {Tag, Tagged} from "#tags";

interface IntersectDocs {
	/** Yields only the elements of a given iterable also found in another given iterable. */
	intersect: any;
}

interface IntersectStatic<T extends Tag.Static> extends IntersectDocs {
	/**
	 * @param iterable The source iterable whose items are to be yielded if found in the other
	 * iterable.
	 * @param other The other iterable. Only items from the source iterable that are also found in
	 * this iterable will be yielded.
	 */
	intersect<I, R = any, N = unknown>(
		iterable : Tagged.Iterable<T, I>,
		other    : Tagged.Iterable<T, I>,
	): Tagged.Result.Generator<T, I, R, N>;
}

interface IntersectInstance<T extends Tag.Instance> extends IntersectDocs {
	/**
	 * @param other The other iterable. Only items from the source iterable that are also found in
	 * this iterable will be yielded.
	 */
	intersect<I, R = any, N = unknown>(
		this  : Tagged.Iterable<T, I>,
		other : Tagged.Iterable<T, I>,
	): Tagged.Result.Generator<T, I, R, N>;
}

export type Intersect<T extends Tag> =
	T extends Tag.Static
		? IntersectStatic<T> :
	T extends Tag.Instance
		? IntersectInstance<T> :
	never;