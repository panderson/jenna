import {Tag, Tagged} from "#tags";

interface SumDocs {
	/**
	 * Adds all the elements of the iterable and returns the result.  If the iterable is empty,
	 * returns `undefined`.
	 */
	sum: any;
}

interface SumStatic<T extends Tag.Static> extends SumDocs {
	sum<I>(iterable: Tagged.Iterable<T, I>): Tagged.Promise<T, I | undefined>;
}

interface SumInstance<T extends Tag.Instance> extends SumDocs {
	sum<I>(this: Tagged.Iterable<T, I>): Tagged.Promise<T, I | undefined>;
}

export type Sum<T extends Tag> =
	T extends Tag.Static ? SumStatic<T> : T extends Tag.Instance ? SumInstance<T> : never;