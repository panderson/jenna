import {Tag} from "#tags";

interface EveryDocs {
	/**
	 * Determines whether all members of the iterable satisfy the specified test.
	 *
	 * This function is similar to `Array.prototype.every()` except that the predicate doesn't
	 * receive a reference to the iterable, and there is no `thisArg` parameter.  If you want a
	 * `this` argument for the predicate, you can bind one before passing it.
	 */
	every: any;
}

// Because the async interfaces can't have the type predicate, and because the signature of the type
// predicate and the regular function are effectively identical, we can't use the extending trick we
// use elsewhere, or we lose the type information from the base interface.  So in this file, we have
// to repeat everything a bunch.  At least it's not too much.

interface EveryStaticAsync extends EveryDocs {
	/**
	 * @param iterable The iterable to be tested.
	 * @param predicate A function that accepts up to two arguments.  The `every` function calls the
	 * predicate for each element in the iterable until the predicate returns a value coercible to
	 * the Boolean value `false`, or until the iterable is exhausted.
	 */
	every<I>(
		iterable  : AnyIterable<I>,
		predicate : (value: I, index: number) => boolean
	): Promise<boolean>;
}
interface EveryStaticSync  extends EveryDocs {
	/**
	 * @param iterable The iterable to be tested.
	 * @param predicate A function that accepts up to two arguments.  The `every` function calls the
	 * predicate for each element in the iterable until the predicate returns a value coercible to
	 * the Boolean value `false`, or until the iterable is exhausted.
	 */
	every<I>(
		iterable  : Iterable<I>,
		predicate : (value: I, index: number) => boolean
	): boolean;
	/**
	 * @param iterable The iterable to be tested.
	 * @param predicate A function that accepts up to two arguments.  The `every` function calls the
	 * predicate for each element in the iterable until the predicate returns a value coercible to
	 * the Boolean value `false`, or until the iterable is exhausted.
	 */
	every<I, J extends I>(
		iterable  : Iterable<I>,
		predicate : (value: I, index: number) => value is J
	): iterable is Iterable<J>;
}

interface EveryInstanceAsync extends EveryDocs {
	/**
	 * @param predicate A function that accepts up to two arguments.  The `every` function calls the
	 * predicate for each element in the iterable until the predicate returns a value coercible to
	 * the Boolean value `false`, or until the iterable is exhausted.
	 */
	every<I>(
		this      : AnyIterable<I>,
		predicate : (value: I, index: number) => boolean
	): boolean;
}
interface EveryInstanceSync  extends EveryDocs {
	/**
	 * @param predicate A function that accepts up to two arguments.  The `every` function calls the
	 * predicate for each element in the iterable until the predicate returns a value coercible to
	 * the Boolean value `false`, or until the iterable is exhausted.
	 */
	every<I>(
		this      : Iterable<I>,
		predicate : (value: I, index: number) => boolean
	): boolean;
	/**
	 * @param predicate A function that accepts up to two arguments.  The `every` function calls the
	 * predicate for each element in the iterable until the predicate returns a value coercible to
	 * the Boolean value `false`, or until the iterable is exhausted.
	 */
	every<I, J extends I>(
		this      : Iterable<I>,
		predicate : (value: I, index: number) => value is J
	): this is Iterable<J>;
}

export type Every<T extends Tag> =
	T extends Tag.StaticAsync   ? EveryStaticAsync   :
	T extends Tag.StaticSync    ? EveryStaticSync    :
	T extends Tag.InstanceAsync ? EveryInstanceAsync :
	T extends Tag.InstanceSync  ? EveryInstanceSync  :
	never;