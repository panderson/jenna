import {Tag, Tagged} from "#tags";

interface FilterDocs {
	/**
	 * Yields the elements of an iterator that meet the specified condition.
	 *
	 * This function is similar to `Array.prototype.filter()` except that the predicate doesn't
	 * receive a reference to the iterable, and there is no `thisArg` parameter.  If you want a
	 * `this` argument for the predicate, you can bind one before passing it.
	 */
	filter: any;
}

interface FilterStatic<T extends Tag.Static> extends FilterDocs {
	/**
	 * @param iterable The iterable to filter.
	 * @param predicate A function that accepts up to two arguments.  The `filter` function calls
	 * the predicate one time for each element in the iterator.
	 */
	filter<I, J extends I, R = any, N = unknown>(
		iterable  : Tagged.Iterable<T, I>,
		predicate : (value: I, index: number) => value is J
	): Tagged.Result.Generator<T, J, R, N>;
	/**
	 * @param iterable The iterable to filter.
	 * @param predicate A function that accepts up to two arguments.  The `filter` function calls
	 * the predicate one time for each element in the iterator.
	 */
	filter<I, R = any, N = unknown>(
		iterable  : Tagged.Iterable<T, I>,
		predicate : (value: I, index: number) => boolean
	): Tagged.Result.Generator<T, I, R, N>;
}

interface FilterInstance<T extends Tag.Instance> extends FilterDocs {
	/**
	 * @param predicate A function that accepts up to two arguments.  The `filter` function calls
	 * the predicate one time for each element in the iterator.
	 */
	filter<I, J extends I, R = any, N = unknown>(
		this      : Tagged.Iterable<T, I>,
		predicate : (value: I, index: number) => value is J
	): Tagged.Result.Generator<T, J, R, N>;
	/**
	 * @param predicate A function that accepts up to two arguments.  The `filter` function calls
	 * the predicate one time for each element in the iterator.
	 */
	filter<I, R = any, N = unknown>(
		this      : Tagged.Iterable<T, I>,
		predicate : (value: I, index: number) => boolean
	): Tagged.Result.Generator<T, I, R, N>;
}

export type Filter<T extends Tag> =
	T extends Tag.Static ? FilterStatic<T> : T extends Tag.Instance ? FilterInstance<T> : never;