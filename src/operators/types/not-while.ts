import {Tag, Tagged} from "#tags";

interface NotWhileDocs {
	/** Yields items of the given iterable once a predicate returns `true`. */
	notWhile: any;
}

interface NotWhileStatic<T extends Tag.Static> extends NotWhileDocs {
	/**
	 * @param iterable The iterable whose items are to be yielded.
	 * @param predicate A function that receives items from the iterable and returns a value
	 * indicating whether or not yielding should begin.  As long as it returns a truthy value, no
	 * items will be yielded.  The first time it returns a falsey value, that item and all the
	 * remaining items in the iterable will be yielded, and the predicate will not be called again.
	 */
	notWhile<I, R = any, N = unknown>(
		iterable  : Tagged.Iterable<T, I>,
		predicate : (item: I) => boolean
	): Tagged.Result.Generator<T, I, R, N | undefined>;
}

interface NotWhileInstance<T extends Tag.Instance> extends NotWhileDocs {
	/**
	 * @param predicate A function that receives items from the iterable and returns a value
	 * indicating whether or not yielding should begin.  As long as it returns a truthy value, no
	 * items will be yielded.  The first time it returns a falsey value, that item and all the
	 * remaining items in the iterable will be yielded, and the predicate will not be called again.
	 */
	notWhile<I, R = any, N = unknown>(
		this      : Tagged.Iterable<T, I>,
		predicate : (item: I) => boolean
	): Tagged.Result.Generator<T, I, R, N | undefined>;
}

export type NotWhile<T extends Tag> =
	T extends Tag.Static ? NotWhileStatic<T> : T extends Tag.Instance ? NotWhileInstance<T> : never;