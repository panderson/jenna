import {Tag, Tagged} from "#tags";
import {ReduceDocs}  from "./common";
import {
	Finalize,
	Finalizer,
	Initializer,
	InitialValue,
	Reduce,
	Reducer,
}                    from "./types";

export interface ReduceInstance<T extends Tag.Instance> extends ReduceDocs {
	// Overload 1
	/**
	 * @param reduce A function that accepts up to three arguments.  It's called one time for each
	 * element in the iterable, except for the first element, which serves as the initial value of
	 * the accumulation.
	 * @returns The accumulated result, or `undefined` if the iterable is empty.
	 */
	reduce<I>(
		this   : Tagged.Iterable<T, I>,
		reduce : Reduce<I, I>,
	): Tagged.Promise<T, I | undefined>;

	// Overload 2
	/**
	 * @param reduce A function that accepts up to three arguments.  It's called one time for each
	 * element in the iterable.
	 * @param initialValue The initial value to start the accumulation.
	 */
	reduce<I, R>(
		this         : Tagged.Iterable<T, I>,
		reduce       : Reduce<I, R>,
		initialValue : R,
	): Tagged.Promise<T, I | R>;

	// Overload 3
	/**
	 * @param params An object with a `reduce` property: a function that accepts up to three
	 * arguments.  It's called one time for each element in the iterable, except for the first
	 * element, which serves as the initial value of the accumulation.
	 * @returns The accumulated result, or `undefined` if the iterable is empty.
	 */
	reduce<I>(
		this   : Tagged.Iterable<T, I>,
		params : Reducer<I, I>,
	): Tagged.Promise<T, I | undefined>;

	// Overload 4
	/**
	 * @param params An object with the following properties:
	 *
	 *   * `reduce`: A function that accepts up to three arguments.  It's called one time for each
	 *      element in the iterable.
	 *
	 *   * `initialValue`: The initial value to start the accumulation.
	 */
	reduce<I, R>(
		this   : Tagged.Iterable<T, I>,
		params : Reducer<I, R> & InitialValue<R>,
	): Tagged.Promise<T, R>;

	// Overload 5
	/**
	 * @param params An object with the following properties:
	 *
	 *   * `reduce`: A function that accepts up to three arguments.  It's called one time for each
	 *      element in the iterable, except for the first element, which is passed to the
	 *      `initialize` function.
	 *
	 *   * `initialize`: A function called with the first item in the iterable (if there is one).
	 *     Its return value becomes the initial value of the accumulator.
	 * @returns The accumulated result, or `undefined` if the iterable is empty.
	 */
	reduce<I, R>(
		this   : Tagged.Iterable<T, I>,
		params : Reducer<I, R> & Initializer<I, R>,
	): Tagged.Promise<T, R | undefined>;

	// Overload 6
	/**
	 * @param params An object with the following properties:
	 *
	 *   * `reduce`: A function that accepts up to three arguments.  It's called one time for each
	 *      element in the iterable, except for the first element, which serves as the initial value
	 *      of the accumulation.
	 *
	 *   * `finalize`: A function called with the final accumulated result.  If the iterable was
	 *      empty, it will receive `undefined`.  Its return value will be the final result.
	 */
	reduce<I, R>(
		this   : Tagged.Iterable<T, I>,
		params : Reducer<I, I> & Finalizer<I | undefined, R>,
	): Tagged.Promise<T, R>;

	// Overload 7
	/**
	 * @param params An object with the following properties:
	 *
	 *   * `reduce`: A function that accepts up to three arguments.  It's called one time for each
	 *      element in the iterable.
	 *
	 *   * `initialValue`: The initial value to start the accumulation.
	 *
	 *   * `finalize`: A function called with the final accumulated result.  Its return value will
	 *     be the final result.
	 */
	reduce<I, R, V>(
		this   : Tagged.Iterable<T, I>,
		params : Reducer<I, R> & InitialValue<R> & Finalizer<R, V>,
	): Tagged.Promise<T, V>;

	// Overload 8
	/**
	 * @param params An object with the following properties:
	 *
	 *   * `reduce`: A function that accepts up to three arguments.  It's called one time for each
	 *      element in the iterable, except for the first element, which is passed to the
	 *      `initialize` function.
	 *
	 *   * `initialize`: A function called with the first item in the iterable (if there is one).
	 *     Its return value becomes the initial value of the accumulator.
	 *
	 *   * `finalize`: A function called with the final accumulated result.  If the iterable was
	 *      empty, it will receive `undefined`.  Its return value will be the final result.
	 */
	reduce<I, R, V>(
		this   : Tagged.Iterable<T, I>,
		params : Reducer<I, R> & Initializer<I, R> & Finalizer<R | undefined, V>,
	): Tagged.Promise<T, V>;

	// Overload 9
	/**
	 * @param initialValue The initial value to start the accumulation.
	 * @param reduce A function that accepts up to three arguments.  It's called one time for each
	 * element in the iterable.
	 */
	reduce<I, R>(
		this         : Tagged.Iterable<T, I>,
		initialValue : R,
		reduce       : Reduce<I, R>,
	): Tagged.Promise<T, R>;

	// Overload 10
	/**
	 * @param initialValue The initial value to start the accumulation.
	 * @param reduce A function that accepts up to three arguments.  It's called one time for each
	 * element in the iterable.
	 * @param finalize A function called with the final accumulated result.  Its return value will
	 * be the final result.
	 */
	reduce<I, R, V>(
		this         : Tagged.Iterable<T, I>,
		initialValue : R,
		reduce       : Reduce<I, R>,
		finalize     : Finalize<R, V>,
	): Tagged.Promise<T, V>;
}