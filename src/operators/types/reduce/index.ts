import {Tag}            from "#tags";
import {ReduceInstance} from "./instance";
import {ReduceStatic}   from "./static";

export type Reduce<T extends Tag> =
	T extends Tag.Static ? ReduceStatic<T> : T extends Tag.Instance ? ReduceInstance<T> : never;