export interface Initialize  <T, U> {(firstItem: T): U};
export interface Reduce      <T, U> {(prev: U, current: T, index: number): U};
export interface Finalize    <T, U> {(result: T): U};

export interface Reducer     <T, U> {reduce       : Reduce<T, U>};
export interface Initializer <T, U> {initialize   : Initialize<T, U>};
export interface InitialValue<T>    {initialValue : T};
export interface Finalizer   <T, U> {finalize     : Finalize<T, U>};