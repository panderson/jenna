export interface ReduceDocs {
	/**
	 * Calls the specified callback for all the elements in an iterable.  The return value of the
	 * callback is the accumulated result, and is provided as an argument in the next call.
	 *
	 * This function is similar to `Array.prototype.reduce()`, but the callback doesn't receive a
	 * reference to the iterable.
	 */
	reduce: any;
}