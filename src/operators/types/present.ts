import {Tag, Tagged} from "#tags";

interface PresentDocs {
	/** Yields the elements of an iterator that are not `null` or `undefined`. */
	present: any;
}

interface PresentStatic<T extends Tag.Static> extends PresentDocs {
	present<I, R = any, N = unknown>(
		iterable: Tagged.Iterable<T, I>
	): Tagged.Result.Generator<T, NonNullable<I>, R, N>;
}

interface PresentInstance<T extends Tag.Instance> extends PresentDocs {
	present<I, R = any, N = unknown>(
		this: Tagged.Iterable<T, I>
	): Tagged.Result.Generator<T, NonNullable<I>, R, N>;
}

export type Present<T extends Tag> =
	T extends Tag.Static ? PresentStatic<T> : T extends Tag.Instance ? PresentInstance<T> : never;