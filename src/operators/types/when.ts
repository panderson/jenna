import {Tag, Tagged} from "#tags";

interface WhenDocs {
	/**
	 * Passes values to an iterator's `next()` function based on the result of a selector on each
	 * yielded value.
	 * @param selector A function that accepts up to three arguments.  The `when` function calls
	 * this selector one time for each element in the iterator, passing it:
	 *
	 *   * `value` : the value from the iterator
	 *   * `index` : the 0-based index of this step through the iterator
	 *   * `next`  : the `next` value passed by the consumer of the `when` function, if any
	 *
	 * Whatever value the selector function returns is passed to the iterator's `next()` function to
	 * retrieve the next item.
	 * @returns An iterable iterator yielding the items retrieved from the original iterator.  Each
	 * item is yielded before being passed to the selector.
	 */
	when: any;
}

interface WhenStatic<T extends Tag.Static> extends WhenDocs {
	/**
	 * @param selector A function that receives items from the iterable a returns a value to be
	 * passed to the call to `next()` to retrieve the next item.
	 */
	when<I, J, R = any, N = unknown>(
		iterable : Tagged.Iterable<T, I>,
		selector : (value: I, index: number, next: N | undefined) => J,
	): Tagged.Result.Generator<T, I, R, N | undefined>;
}

interface WhenInstance<T extends Tag.Instance> extends WhenDocs {
	/**
	 * @param selector A function that receives items from the iterable a returns a value to be
	 * passed to the call to `next()` to retrieve the next item.
	 */
	when<I, J, R = any, N = unknown>(
		this     : Tagged.Iterable<T, I>,
		selector : (value: I, index: number, next: N | undefined) => J,
	): Tagged.Result.Generator<T, I, R, N | undefined>;
}

export type When<T extends Tag> =
	T extends Tag.Static ? WhenStatic<T> : T extends Tag.Instance ? WhenInstance<T> : never;