import {Tag, Tagged} from "#tags";

interface InnerJoinDocs {
	/** Finds all items from `rightIterable` with keys matching those from `leftIterable`. */
	innerJoin: any;
}

interface InnerJoinStatic<T extends Tag.Static> extends InnerJoinDocs {
	/**
	 * @param leftIterable The left-side iterable of the inner join operation.
	 * @param rightIterable The right-side iterable of the inner join operation.
	 * @param leftKeySelector A function that receives items from `leftIterable` and returns keys
	 * that are expected to match keys from `rightIterable`.
	 * @param rightKeySelector A function that receives items from `rightIterable` and returns keys that are expected to
	 * match keys from `leftIterable`.
	 * @param resultSelector A function that receives an item from `leftIterable`, an item from `rightIterable`, and the
	 * key they both share.  Whatever it returns is yielded by `innerJoin`.
	 *
	 * For any particular key, if no matching items are found in `rightIterable`, the result selector will not be called
	 * for that key and no result will be yielded.
	 */
	innerJoin<I, J, K, V = [I, J, K]>(
		leftIterable     : Tagged.Iterable<T, I>,
		rightIterable    : Tagged.Iterable<T, J>,
		leftKeySelector  : (item: I, index: number) => K,
		rightKeySelector : (item: J, index: number) => K,
		resultSelector?  : (left: I, right: J, key: K) => V
	): Tagged.Result.Generator<T, V>;
}

interface InnerJoinInstance<T extends Tag.Instance> extends InnerJoinDocs {
	/**
	 * @param rightIterable The right-side iterable of the inner join operation.
	 * @param leftKeySelector A function that receives items from `leftIterable` and returns keys
	 * that are expected to match keys from `rightIterable`.
	 * @param rightKeySelector A function that receives items from `rightIterable` and returns keys that are expected to
	 * match keys from `leftIterable`.
	 * @param resultSelector A function that receives an item from `leftIterable`, an item from `rightIterable`, and the
	 * key they both share.  Whatever it returns is yielded by `innerJoin`.
	 *
	 * If this function is omitted, a tuple of what would have been its parameters will be yielded.
	 *
	 * For any particular key, if no matching items are found in `rightIterable`, the result selector will not be called
	 * for that key and no result will be yielded.
	 */
	innerJoin<I, J, K, V = [I, J, K]>(
		this             : Tagged.Iterable<T, I>,
		rightIterable    : Tagged.Iterable<T, J>,
		leftKeySelector  : (item: I, index: number) => K,
		rightKeySelector : (item: J, index: number) => K,
		resultSelector?  : (left: I, right: J, key: K) => V
	): Tagged.Result.Generator<T, V>;
}

export type InnerJoin<T extends Tag> =
	T extends Tag.Static
		? InnerJoinStatic<T> :
	T extends Tag.Instance
		? InnerJoinInstance<T> :
	never;