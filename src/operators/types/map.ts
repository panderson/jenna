import {Tag, Tagged} from "#tags";

interface MapDocs {
	/**
	 * Calls a selector function for each element of an iterable, and returns an iterable that
	 * yields the results.
	 *
	 * This function is similar to `Array.prototype.map()` except that the selector doesn't
	 * receive a reference to the iterable, and there is no `thisArg` parameter.  If you want a
	 * `this` argument for the selector, you can bind one before passing it.
	 */
	map: any;
}

interface MapStatic<T extends Tag.Static> extends MapDocs {
	/**
	 * @param selector A function that accepts up to two arguments.  The `map` function calls the
	 * selector one time for each element in the iterable.
	 */
	map<I, U, R = any, N = unknown>(
		iterable : Tagged.Iterable<T, I>,
		selector : (value: I, index: number) => U,
	): Tagged.Result.Generator<T, U, R, N>;
}

interface MapInstance<T extends Tag.Instance> extends MapDocs {
	/**
	 * @param selector A function that accepts up to two arguments.  The `map` function calls the
	 * selector one time for each element in the iterable.
	 */
	map<I, U, R = any, N = unknown>(
		this     : Tagged.Iterable<T, I>,
		selector : (value: I, index: number) => U,
	): Tagged.Result.Generator<T, U, R, N>;
}

export type Map<T extends Tag> =
	T extends Tag.Static ? MapStatic<T> : T extends Tag.Instance ? MapInstance<T> : never;