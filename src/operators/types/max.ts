import {Tag, Tagged} from "#tags";

interface MaxDocs {
	/**
	 * Returns the maximum valued element of an iterable, or `undefined` if the iterable is empty.
	 */
	max: any;
}

interface MaxStatic<T extends Tag.Static> extends MaxDocs {
	max<I>(iterable: Tagged.Iterable<T, I>): Tagged.Promise<T, I | undefined>;
}

interface MaxInstance<T extends Tag.Instance> extends MaxDocs {
	max<I>(this: Tagged.Iterable<T, I>): Tagged.Promise<T, I | undefined>;
}

export type Max<T extends Tag> =
	T extends Tag.Static ? MaxStatic<T> : T extends Tag.Instance ? MaxInstance<T> : never;