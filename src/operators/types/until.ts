import {Tag, Tagged} from "#tags";

interface UntilDocs {
	/** Yields items of the given iterable until a predicate returns `true`. */
	until: any;
}

interface UntilStatic<T extends Tag.Static> extends UntilDocs {
	/**
	 * @param iterable The iterable whose items are to be yielded.
	 * @param predicate A function that receives items from the iterable and returns a value
	 * indicating whether or not iteration should stop.  As long as it returns a falsey value, items
	 * will be yielded and iteration will continue.  The first time it returns a truthy value, that
	 * item will not be yielded and iteration will cease.
	 */
	until<I, R = any, N = unknown>(
		iterable  : Tagged.Iterable<T, I>,
		predicate : (item: I) => boolean
	): Tagged.Result.Generator<T, I, R | undefined, N>;
}

interface UntilInstance<T extends Tag.Instance> extends UntilDocs {
	/**
	 * @param predicate A function that receives items from the iterable and returns a value
	 * indicating whether or not iteration should stop.  As long as it returns a falsey value, items
	 * will be yielded and iteration will continue.  The first time it returns a truthy value, that
	 * item will not be yielded and iteration will cease.
	 */
	until<I, R = any, N = unknown>(
		this      : Tagged.Iterable<T, I>,
		predicate : (item: I) => boolean
	): Tagged.Result.Generator<T, I, R | undefined, N>;
}

export type Until<T extends Tag> =
	T extends Tag.Static ? UntilStatic<T> : T extends Tag.Instance ? UntilInstance<T> : never;