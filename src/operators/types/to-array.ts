import {Tag, Tagged} from "#tags";

interface ToArrayDocs {
	/** Materializes the given iterable to an array. */
	toArray: any;
}

interface ToArrayStatic<T extends Tag.Static> extends ToArrayDocs {
	toArray<I>(iterable: Tagged.Iterable<T, I>): Tagged.Promise<T, I[]>;
}

interface ToArrayInstance<T extends Tag.Instance> extends ToArrayDocs {
	toArray<I>(this: Tagged.Iterable<T, I>): Tagged.Promise<T, I[]>;
}

export type ToArray<T extends Tag> =
	T extends Tag.Static ? ToArrayStatic<T> : T extends Tag.Instance ? ToArrayInstance<T> : never;