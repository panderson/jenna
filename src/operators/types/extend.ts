import {Tag, Tagged} from "#tags";

interface ExtendDocs {
	/**
	 * Similar to `Iterable.concat`, except that it appends individual elements.  The following
	 * usage of `extend`:
	 *
	 * ```ts
	 * Iterable.extend(iterable, element1, element2)
	 * ```
	 *
	 * is equivalent to this usage of `concat`:
	 *
	 * ```ts
	 * Iterable.concat(iterable, [element1, element2])
	 * ```
	 *
	 * See also `Map.extend` and `Set.extend`, where `extend` offers uniquely convenient
	 * functionality.
	 */
	extend: any;
}

interface ExtendStatic<T extends Tag.Static> extends ExtendDocs {
	/**
	 * @param iterable The first iterable.
	 * @param elements Additional elements to yield after the end of the first iterable.
	 */
	extend<I, J, R = any, N = unknown>(
		iterable    : Tagged.Iterable<T, I>,
		...elements : J[]
	): Tagged.Result.Generator<T, I | J, R, N>;
}

interface ExtendInstance<T extends Tag.Instance> extends ExtendDocs {
	/** @param elements Additional elements to yield after the end of this iterable. */
	 extend<I, J, R = any, N = unknown>(
		this        : Tagged.Iterable<T, I>,
		...elements : J[]
	): Tagged.Result.Generator<T, I | J, R, N>;
}

export type Extend<T extends Tag> =
	T extends Tag.Static ? ExtendStatic<T> : T extends Tag.Instance ? ExtendInstance<T> : never;