import {Tag, Tagged} from "#tags";

interface GroupByDocs {
	/** Generates a map from the elements of an iterator. */
	groupBy: any;
}

interface GroupByStatic<T extends Tag.Static> extends GroupByDocs {
	/**
	 * @param keySelector A function that takes an element from the iterator and returns a key value to associate with
	 * it. The map will contain lists of elements grouped by these key values.
	 * @param elementSelector A function that takes an element from the iterator and returns a value to be used in its
	 * place in the resulting map.
	 */
	groupBy<I, K, V = I>(
		iterable         : Tagged.Iterable<T, I>,
		keySelector      : (item: I, index: number) => K,
		elementSelector? : (item: I, index: number, key: K) => V,
	): Tagged.Promise<T, Map<K, V[]>>;
}

interface GroupByInstance<T extends Tag.Instance> extends GroupByDocs {
	/**
	 * @param keySelector A function that takes an element from the iterator and returns a key value to associate with
	 * it. The map will contain lists of elements grouped by these key values.
	 * @param elementSelector A function that takes an element from the iterator and returns a value to be used in its
	 * place in the resulting map.
	 */
	groupBy<I, K, V = I>(
		this             : Tagged.Iterable<T, I>,
		keySelector      : (item: I, index: number) => K,
		elementSelector? : (item: I, index: number, key: K) => V,
	): Tagged.Promise<T, Map<K, V[]>>;
}

export type GroupBy<T extends Tag> =
	T extends Tag.Static ? GroupByStatic<T> : T extends Tag.Instance ? GroupByInstance<T> : never;