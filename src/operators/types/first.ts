import {Tag, Tagged} from "#tags";

interface FirstDocs {
	/** Returns the first element of an iterable, or `undefined` if the iterable is empty. */
	first: any;
}
interface FirstStatic<T extends Tag.Static> extends FirstDocs {
	first<I>(iterable: Tagged.Iterable<T, I>): Tagged.Promise<T, I | undefined>;
}

interface FirstInstance<T extends Tag.Instance> extends FirstDocs {
	first<I>(this: Tagged.Iterable<T, I>): Tagged.Promise<T, I | undefined>;
}

export type First<T extends Tag> =
	T extends Tag.Static ? FirstStatic<T> : T extends Tag.Instance ? FirstInstance<T> : never;