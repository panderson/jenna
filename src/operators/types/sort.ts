import {Tag, Tagged} from "#tags";
import {Comparer}    from "@utils";

interface SortDocs {
	/**
	 * Returns an array built from the given iterable and sorted by the given compare function.
	 *
	 * This function is similar to `Array.prototype.sort()`, but returns a new array.
	 */
	sort: any;
}

interface SortStatic<T extends Tag.Static> extends SortDocs {
	/**
	 * Sorts the given iterable using the default comparer, which uses `>` and `<` operators to
	 * compare items, and treats `null` as lower than all other values except `undefined`, which is
	 * considered even lower.
	 * @param iterable The iterable to sort.
	 * @param reverse Whether to sort in reverse order.  The default value is `false`.
	 */
	 sort<I>(
		iterable : Tagged.Iterable<T, I>,
		reverse? : boolean | undefined,
	): Tagged.Promise<T, I[]>;

	/**
	 * @param iterable The iterable to sort.
	 * @param comparer A function used to determine the order of the elements.  It is expected to
	 * return a negative value if its first argument is less than its second argument, zero if
	 * they're equal, and a positive value otherwise.
	 *
	 * @example
	 * ```ts
	 * Iterable.sort(getNumbers(), (a, b) => a - b)
	 * ```
	 */
	sort<I>(
		iterable : Tagged.Iterable<T, I>,
		comparer : Comparer<I>,
		reverse? : boolean | undefined,
	): Tagged.Promise<T, I[]>;
}

interface SortInstance<T extends Tag.Instance> extends SortDocs {
	/**
	 * Sorts this iterable using the default comparer, which uses `>` and `<` operators to compare
	 * items, and treats `null` as lower than all other values except `undefined`, which is
	 * considered even lower.
	 * @param reverse Whether to sort in reverse order.  The default value is `false`.
	 */
	 sort<I>(
		this     : Tagged.Iterable<T, I>,
		reverse? : boolean | undefined,
	): Tagged.Promise<T, I[]>;

	/**
	 * @param comparer A function used to determine the order of the elements.  It is expected to
	 * return a negative value if its first argument is less than its second argument, zero if
	 * they're equal, and a positive value otherwise.
	 *
	 * @example
	 * ```ts
	 * getNumbers().sort((a, b) => a - b)
	 * ```
	 */
	sort<I>(
		this     : Tagged.Iterable<T, I>,
		comparer : Comparer<I>,
		reverse? : boolean | undefined,
	): Tagged.Promise<T, I[]>;
}

export type Sort<T extends Tag> =
	T extends Tag.Static ? SortStatic<T> : T extends Tag.Instance ? SortInstance<T> : never;