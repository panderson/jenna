import {Tag, Tagged} from "#tags";

interface CountDocs {
	/** Returns the count of elements in the iterable. */
	 count: any;
}
interface CountStatic<T extends Tag.Static> extends CountDocs {
	count<I>(iterable: Tagged.Iterable<T, I>): Tagged.Promise<T, number>;
}

interface CountInstance<T extends Tag.Instance> extends CountDocs {
	count<I>(this: Tagged.Iterable<T, I>): Tagged.Promise<T, number>;
}

export type Count<T extends Tag> =
	T extends Tag.Static ? CountStatic<T> : T extends Tag.Instance ? CountInstance<T> : never;