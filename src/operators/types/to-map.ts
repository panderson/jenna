import {Tag, Tagged} from "#tags";

interface ToMapDocs {
	/** Materializes the given iterable to a map. */
	toMap: any;
}

interface ToMapStatic<T extends Tag.Static> extends ToMapDocs {
	/** @param iterable The iterable to materialize to a map. */
	toMap<K, V>(iterable: Tagged.Iterable<T, readonly [K, V]>): Tagged.Promise<T, Map<K, V>>;

	/**
	 * @param iterable The iterable to materialize to a map.
	 * @param keySelector A function that receives each item, along with its index, and returns a
	 * key to be associated with the item in the map. If a previous item is already associated with
	 * the given key, it will be replaced in the map.
	 */
	toMap<I, K>(
		iterable    : Tagged.Iterable<T, I>,
		keySelector : (item: I, index: number) => K
	): Tagged.Promise<T, Map<K, I>>;

	/**
	 * @param iterable The iterable to materialize to a map.
	 * @param keySelector A function that receives each item, along with its index, and returns a
	 * key to be associated with the value in the map. If a previous value is already associated with
	 * the given key, it will be replaced in the map.
	 * @param valueSelector A function that receives each item, the key generated for it, and the
	 * item's index, and returns a value to be associated with the key in the map.
	 */
	toMap<I, K, V>(
		iterable      : Tagged.Iterable<T, I>,
		keySelector   : (item: I, index: number) => K,
		valueSelector : (item: I, key: K, index: number) => V,
	): Tagged.Promise<T, Map<K, V>>;
}

interface ToMapInstance<T extends Tag.Instance> extends ToMapDocs {
	toMap<K, V>(this: Tagged.Iterable<T, readonly [K, V]>): Tagged.Promise<T, Map<K, V>>;

	/**
	 * @param keySelector A function that receives each item, along with its index, and returns a
	 * key to be associated with the item in the map. If a previous item is already associated with
	 * the given key, it will be replaced in the map.
	 */
	toMap<I, K>(
		this        : Tagged.Iterable<T, I>,
		keySelector : (item: I, index: number) => K
	): Tagged.Promise<T, Map<K, I>>;

	/**
	 * @param keySelector A function that receives each item, along with its index, and returns a
	 * key to be associated with the value in the map. If a previous value is already associated with
	 * the given key, it will be replaced in the map.
	 * @param valueSelector A function that receives each item, the key generated for it, and the
	 * item's index, and returns a value to be associated with the key in the map.
	 */
	toMap<I, K, V>(
		this          : Tagged.Iterable<T, I>,
		keySelector   : (item: I, index: number) => K,
		valueSelector : (item: I, key: K, index: number) => V
	): Tagged.Promise<T, Map<K, V>>;
}

export type ToMap<T extends Tag> =
	T extends Tag.Static ? ToMapStatic<T> : T extends Tag.Instance ? ToMapInstance<T> : never;