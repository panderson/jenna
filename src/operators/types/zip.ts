import {Tag, Tagged} from "#tags";

interface ZipDocs {
	/**
	 * Iterates through two iterables in tandem, calling the `resultSelector` function for each pair
	 * of elements and yielding its results, or yielding tuples containing the elements from each
	 * iterable if no result selector is given.
	 *
	 * Iteration stops when either of the iterables is exhausted.
	 */
	zip: any;
}

interface ZipStatic<T extends Tag.Static> extends ZipDocs {
	/**
	 * @param resultSelector A function that receives an element from each iterable and the current
	 * element index.  Whatever it returns is yielded by `zip`.
	 */
	zip<I, J, R = [I, J]>(
		iterable        : Tagged.Iterable<T, I>,
		other           : Tagged.Iterable<T, J>,
		resultSelector? : (a: I, b: J, index: number) => R
	): Tagged.Result.Generator<T, R>;
}

interface ZipInstance<T extends Tag.Instance> extends ZipDocs {
	zip<I, J, R = [I, J]>(
		this            : Tagged.Iterable<T, I>,
		other           : Tagged.Iterable<T, J>,
		resultSelector? : (a: I, b: J, index: number) => R
	): Tagged.Result.Generator<T, R>;
}

export type Zip<T extends Tag> =
	T extends Tag.Static ? ZipStatic<T> : T extends Tag.Instance ? ZipInstance<T> : never;