import {Tag, Tagged} from "#tags";

interface SliceDocs {
	/**
	 * Yields a section of the iterable.
	 *
	 * This function is similar to `Array.prototype.slice()` except that negative indices for
	 * `start` and `end` aren't supported, since the length of an iterable isn't known in advance.
	 */
	slice: any;
}

interface SliceStatic<T extends Tag.Static> extends SliceDocs {
	/**
	 * @param iterable The iterable to be sliced.
	 * @param start The index at which to start yielding items.
	 *
	 * If this value is negative, then the end of the iterable is found and items are yielded
	 * beginning at `-start` before the end. If `-start` is greater than the number of items in the
	 * iterable, then yielding start at the beginning of the iterable.
	 *
	 * If this index is equal to or greater than the number of items in the iterable, then no items
	 * are yielded.
	 *
	 * If not given or `undefined`, items are yielded starting from the beginning of the iterable.
	 * @param end The index at which to stop yielding items.
	 *
	 * If this index is less than or equal to `start`, then no items are yielded.
	 *
	 * If this index is equal to or greater than the number of items in the iterable, or if it's not
	 * given or `undefined`, then items are yielded until the iterable is exhausted.
	 *
	 * If this value is negative, then the end of the iterable is found and items are yielded until
	 * `-end` before the end. If `-end` is equal to or less than the starting index, then no items
	 * are yielded.
	 */
	slice<I, R = any, N = unknown>(
		iterable : Tagged.Iterable<T, I>,
		start?   : number | undefined,
		end?     : number | undefined,
	): Tagged.Result.Generator<T, I, R | undefined, N>;
}

interface SliceInstance<T extends Tag.Instance> extends SliceDocs {
	/**
	 * @param start The index at which to start yielding items.
	 *
	 * If this value is negative, then the end of the iterable is found and items are yielded
	 * beginning at `-start` before the end. If `-start` is greater than the number of items in the
	 * iterable, then yielding start at the beginning of the iterable.
	 *
	 * If this index is equal to or greater than the number of items in the iterable, then no items
	 * are yielded.
	 *
	 * If not given or `undefined`, items are yielded starting from the beginning of the iterable.
	 * @param end The index at which to stop yielding items.
	 *
	 * If this index is less than or equal to `start`, then no items are yielded.
	 *
	 * If this index is equal to or greater than the number of items in the iterable, or if it's not
	 * given or `undefined`, then items are yielded until the iterable is exhausted.
	 *
	 * If this value is negative, then the end of the iterable is found and items are yielded until
	 * `-end` before the end. If `-end` is equal to or less than the starting index, then no items
	 * are yielded.
	 */
	slice<I, R = any, N = unknown>(
		this   : Tagged.Iterable<T, I>,
		start? : number | undefined,
		end?   : number | undefined,
	): Tagged.Result.Generator<T, I, R | undefined, N>;
}

export type Slice<T extends Tag> =
	T extends Tag.Static ? SliceStatic<T> : T extends Tag.Instance ? SliceInstance<T> : never;