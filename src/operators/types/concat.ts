import {Tag, Tagged} from "#tags";

interface ConcatDocs {
	/**
	 * Combines two or more iterables.
	 *
	 * This function is similar to `Array.prototype.concat()` except that additional items must be
	 * iterables and not elements.
	 */
	concat: any;
}

interface ConcatStatic<T extends Tag.Static> extends ConcatDocs {
	/**
	 * @param iterable The first iterable.
	 * @param others Additional iterables to yield after the end of the first.
	 */
	concat<I, R = any, N = unknown>(
		iterable  : Tagged.Iterable<T, I>,
		...others : Tagged.Iterable<T, I>[]
	): Tagged.Result.Generator<T, I, R, N>;

	/**
	 * @param iterable The first iterable.
	 * @param others Additional iterables to yield after the end of the first.
	 */
	 concat<I, J, R = any, N = unknown>(
		iterable  : Tagged.Iterable<T, I>,
		...others : Tagged.Iterable<T, J>[]
	): Tagged.Result.Generator<T, I | J, R, N>;
}

interface ConcatInstance<T extends Tag.Instance> extends ConcatDocs {
	/** @param others Additional iterables to yield after the end of this one. */
	concat<I, R = any, N = unknown>(
		this      : Tagged.Iterable<T, I>,
		...others : Tagged.Iterable<T, I>[]
	): Tagged.Result.Generator<T, I, R, N>;

	/** @param others Additional iterables to yield after the end of this one. */
	concat<I, J, R = any, N = unknown>(
		this      : Tagged.Iterable<T, I>,
		...others : Tagged.Iterable<T, J>[]
	): Tagged.Result.Generator<T, I | J, R, N>;
}

export type Concat<T extends Tag> =
	T extends Tag.Static ? ConcatStatic<T> : T extends Tag.Instance ? ConcatInstance<T> : never;