import {Tag, Tagged} from "#tags";

interface ToSetDocs {
	/** Materializes the given iterable to a set. */
	toSet: any;
}

interface ToSetStatic<T extends Tag.Static> extends ToSetDocs {
	toSet<I>(iterable: Tagged.Iterable<T, I>): Tagged.Promise<T, Set<I>>;
}

interface ToSetInstance<T extends Tag.Instance> extends ToSetDocs {
	toSet<I>(this: Tagged.Iterable<T, I>): Tagged.Promise<T, Set<I>>;
}

export type ToSet<T extends Tag> =
	T extends Tag.Static ? ToSetStatic<T> : T extends Tag.Instance ? ToSetInstance<T> : never;