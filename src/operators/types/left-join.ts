import {Tag, Tagged} from "#tags";

interface LeftJoinDocs {
	/** Finds all items from `rightIterable` with keys matching those from `leftIterable`. */
	leftJoin: any;
}

interface LeftJoinStatic<T extends Tag.Static> extends LeftJoinDocs {
	/**
	 * @param leftIterable The left-side iterable of the left join operation.
	 * @param rightIterable The right-side iterable of the left join operation.
	 * @param leftKeySelector A function that receives items from `leftIterable` and returns keys
	 * that are expected to match keys from `rightIterable`.
	 * @param rightKeySelector  A function that receives items from `rightIterable` and returns keys
	 * that are expected to match keys from `leftIterable`.
	 * @param resultSelector A function that receives an item from `leftIterable`, an item from
	 * `rightIterable` (or `undefined` if there isn't one), and the key they both share.  Whatever
	 * it returns is yielded by `leftJoin`.
	 *
	 * If this function is omitted, a tuple of what would have been its parameters will be yielded.
	 */
	leftJoin<I, J, K, V = [I, J | undefined, K]>(
		leftIterable     : Tagged.Iterable<T, I>,
		rightIterable    : Tagged.Iterable<T, J>,
		leftKeySelector  : (item: I, index: number) => K,
		rightKeySelector : (item: J, index: number) => K,
		resultSelector?  : (left: I, right: J | undefined, key: K) => V
	): Tagged.Result.Generator<T, V>;
}

interface LeftJoinInstance<T extends Tag.Instance> extends LeftJoinDocs {
	/**
	 * @param rightIterable The right-side iterable of the left join operation.
	 * @param leftKeySelector A function that receives items from `leftIterable` and returns keys
	 * that are expected to match keys from `rightIterable`.
	 * @param rightKeySelector  A function that receives items from `rightIterable` and returns keys
	 * that are expected to match keys from `leftIterable`.
	 * @param resultSelector A function that receives an item from `leftIterable`, an item from
	 * `rightIterable` (or `undefined` if there isn't one), and the key they both share.  Whatever
	 * it returns is yielded by `leftJoin`.
	 *
	 * If this function is omitted, a tuple of what would have been its parameters will be yielded.
	 */
	leftJoin<I, J, K, V = [I, J | undefined, K]>(
		this             : Tagged.Iterable<T, I>,
		rightIterable    : Tagged.Iterable<T, J>,
		leftKeySelector  : (item: I, index: number) => K,
		rightKeySelector : (item: J, index: number) => K,
		resultSelector?  : (left: I, right: J | undefined, key: K) => V
	): Tagged.Result.Generator<T, V>;
}

export type LeftJoin<T extends Tag> =
	T extends Tag.Static ? LeftJoinStatic<T> : T extends Tag.Instance ? LeftJoinInstance<T> : never;