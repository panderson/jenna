import {Tag, Tagged} from "#tags";

interface ToRecordDocs {
	/** Materializes the given iterable to a record-like object. */
	toRecord: any;
}

interface ToRecordStatic<T extends Tag.Static> extends ToRecordDocs {
	toRecord<K extends keyof any, V>(
		iterable: Tagged.Iterable<T, readonly [K, V]>
	): Tagged.Promise<T, Record<K, V>>;

	toRecord<I, K extends keyof any>(
		iterable    : Tagged.Iterable<T, I>,
		keySelector : (item: I) => K,
	): Tagged.Promise<T, Record<K, I>>;

	toRecord<I, R extends Record<any, any>>(
		iterable      : Tagged.Iterable<T, I>,
		keySelector   : (item: I) => keyof R,
		valueSelector : <K extends keyof R>(item: I, key: K) => R[K],
	): Tagged.Promise<T, R>;
}

interface ToRecordInstance<T extends Tag.Instance> extends ToRecordDocs {
	toRecord<K extends keyof any, V>(
		this: Tagged.Iterable<T, readonly [K, V]>
	): Tagged.Promise<T, Record<K, V>>;

	toRecord<I, K extends keyof any>(
		this        : Tagged.Iterable<T, I>,
		keySelector : (item: I) => K,
	): Tagged.Promise<T, Record<K, I>>;

	toRecord<I, R extends Record<any, any>>(
		this          : Tagged.Iterable<T, I>,
		keySelector   : (item: I) => keyof R,
		valueSelector : <K extends keyof R>(key: K, item: I) => R[K],
	): Tagged.Promise<T, R>;
}

export type ToRecord<T extends Tag> =
	T extends Tag.Static ? ToRecordStatic<T> : T extends Tag.Instance ? ToRecordInstance<T> : never;