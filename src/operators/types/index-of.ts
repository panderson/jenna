import {Tag, Tagged} from "#tags";

interface IndexOfDocs {
	/**
	 * Returns the index of the first occurrence of a value in an iterable, or -1 if it is not
	 * present.
	 *
	 * This function is similar to `Array.prototype.indexOf()`.
	 */
	indexOf: any
}

interface IndexOfStatic<T extends Tag.Static> extends IndexOfDocs {
	/**
	 * @param iterable The iterable to search.  If the element is found before the iterable is fully exhausted,
	 * iteration stops.
	 * @param element The value to locate.  Elements are compared using `===`.
	 * @param minIndex The iterable index at which to begin the search.  If `minIndex` is omitted or less than `0`, the
	 * search starts at the first element.
	 */
	indexOf<I>(
		iterable  : Tagged.Iterable<T, I>,
		element   : I,
		minIndex? : number
	): Tagged.Promise<T, number>;
}

interface IndexOfInstance<T extends Tag.Instance> extends IndexOfDocs {
	/**
	 * @param element The value to locate.  Elements are compared using `===`.
	 * @param minIndex The iterable index at which to begin the search.  If `minIndex` is omitted or less than `0`, the
	 * search starts at the first element.
	 */
	indexOf<I>(
		this      : Tagged.Iterable<T, I>,
		element   : I,
		minIndex? : number
	): Tagged.Promise<T, number>;
}

export type IndexOf<T extends Tag> =
	T extends Tag.Static ? IndexOfStatic<T> : T extends Tag.Instance ? IndexOfInstance<T> : never;