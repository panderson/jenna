import {Tag, Tagged} from "#tags";

interface ReverseDocs {
	/**
	 * Returns an array containing the elements of a given iterable in reverse.
	 *
	 * This function is similar to `Array.prototype.reverse()` except that it returns a new array.
	 */
	reverse: any;
}

interface ReverseStatic<T extends Tag.Static> extends ReverseDocs {
	reverse<I>(iterable: Tagged.Iterable<T, I>): Tagged.Promise<T, I[]>;
}

interface ReverseInstance<T extends Tag.Instance> extends ReverseDocs {
	reverse<I>(this: Tagged.Iterable<T, I>): Tagged.Promise<T, I[]>;
}

export type Reverse<T extends Tag> =
	T extends Tag.Static ? ReverseStatic<T> : T extends Tag.Instance ? ReverseInstance<T> : never;