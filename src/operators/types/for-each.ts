import {Tag, Tagged} from "#tags";

interface ForEachDocs {
	/**
	 * Performs the specified action for each element in an iterable.
	 *
	 * This function is similar to `Array.prototype.forEach()` except that the callback doesn't
	 * receive a reference to the iterable, and there is no `thisArg` parameter.  If you want a
	 * `this` argument for the callback, you can bind one before passing it.
	 */
	forEach: any;
}

interface ForEachStatic<T extends Tag.Static> extends ForEachDocs {
	/**
	 * @param callback A function that accepts up to two arguments.  `forEach` calls the callback
	 * one time for each element in the iterable.
	 */
	forEach<I, R = any, N = unknown>(
		iterable : Tagged.Iterable<T, I>,
		callback : (value: I, index: number) => N
	): Tagged.Promise<T, R>;
}

interface ForEachInstance<T extends Tag.Instance> extends ForEachDocs {
	/**
	 * @param callback A function that accepts up to two arguments.  `forEach` calls the callback
	 * one time for each element in the iterable.
	 */
	forEach<I, R = any, N = unknown>(
		this     : Tagged.Iterable<T, I>,
		callback : (value: I, index: number) => N
	): Tagged.Promise<T, R>;
}

export type ForEach<T extends Tag> =
	T extends Tag.Static ? ForEachStatic<T> : T extends Tag.Instance ? ForEachInstance<T> : never;