import {Tag, Tagged} from "#tags";

interface OmitDocs {
	/** Yields only the elements whose keys don't match those given by another iterable. */
	omit: any;
}

interface OmitStatic<T extends Tag.Static> extends OmitDocs {
	/**
	 * @param keySelector A function that receives an element from the iterable and returns its key.
	 * @param keys The keys of the items from the iterable to skip when yielding the result.
	 */
	omit<I, K, R = any, N = unknown>(
		iterable    : Tagged.Iterable<T, I>,
		keySelector : (item: I) => K,
		keys        : Tagged.Iterable<T, K>
	): Tagged.Result.Generator<T, I, R, N>;
}

interface OmitInstance<T extends Tag.Instance> extends OmitDocs {
	/**
	 * @param keySelector A function that receives an element from the iterable and returns its key.
	 * @param keys The keys of the items from the iterable to yield in the result.
	 */
	omit<I, K, R = any, N = unknown>(
		this        : Tagged.Iterable<T, I>,
		keySelector : (item: I) => K,
		keys        : Tagged.Iterable<T, K>
	): Tagged.Result.Generator<T, I, R, N>;
}

export type Omit<T extends Tag> =
	T extends Tag.Static
		? OmitStatic<T> :
	T extends Tag.Instance
		? OmitInstance<T> :
	never;