import {Tag, Tagged} from "#tags";

interface AtDocs {
	/**
	 * Returns the element found at the given index into the iterable. If `index` is negative, then
	 * returns the item found `-index` items from the end of the iterable. For instance, an index of
	 * `-1` returns the last item in the iterable, and an index of `-2` returns the second from the
	 * last item in the iterable, etc.
	 *
	 * If `index` is equal to or greater the number of elements in the iterable, or if `index` is
	 * negative and `-index` is greater than the number of elements in the iterable, then `at`
	 * returns `undefined`.
	 */
	at: any;
}

interface AtStatic<T extends Tag.Static> extends AtDocs {
	at<I>(iterable: Tagged.Iterable<T, I>, index: number): Tagged.Promise<T, I | undefined>;
}

interface AtInstance<T extends Tag.Instance> extends AtDocs {
	at<I>(this: Tagged.Iterable<T, I>, index: number): Tagged.Promise<T, I | undefined>;
}

export type At<T extends Tag> =
	T extends Tag.Static ? AtStatic<T> : T extends Tag.Instance ? AtInstance<T> : never;