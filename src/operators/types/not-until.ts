import {Tag, Tagged} from "#tags";

interface NotUntilDocs {
	/** Yields items of the given iterable once a predicate returns `false`. */
	notUntil: any;
}

interface NotUntilStatic<T extends Tag.Static> extends NotUntilDocs {
	/**
	 * @param iterable The iterable whose items are to be yielded.
	 * @param predicate A function that receives items from the iterable and returns a value
	 * indicating whether or not yielding should begin.  As long as it returns a falsey value, no
	 * items will be yielded.  The first time it returns a truthy value, that item and all the
	 * remaining items in the iterable will be yielded, and the predicate will not be called again.
	 */
	notUntil<I, R = any, N = unknown>(
		iterable  : Tagged.Iterable<T, I>,
		predicate : (item: I) => boolean
	): Tagged.Result.Generator<T, I, R, N | undefined>;
}

interface NotUntilInstance<T extends Tag.Instance> extends NotUntilDocs {
	/**
	 * @param predicate A function that receives items from the iterable and returns a value
	 * indicating whether or not yielding should begin.  As long as it returns a falsey value, no
	 * items will be yielded.  The first time it returns a truthy value, that item and all the
	 * remaining items in the iterable will be yielded, and the predicate will not be called again.
	 */
	notUntil<I, R = any, N = unknown>(
		this      : Tagged.Iterable<T, I>,
		predicate : (item: I) => boolean
	): Tagged.Result.Generator<T, I, R, N | undefined>;
}

export type NotUntil<T extends Tag> =
	T extends Tag.Static ? NotUntilStatic<T> : T extends Tag.Instance ? NotUntilInstance<T> : never;