import {Tag, Tagged} from "#tags";

interface FindDocs {
	/**
	 * Returns the value of the first element in the iteration where `predicate` returns `true`.
	 * Returns `undefined` if such element is not found.
	 *
	 * This function is similar to `Array.prototype.find()` except that the predicate doesn't
	 * receive a reference to the iterable, and there is no `thisArg` parameter.  If you want a
	 * `this` argument for the predicate, you can bind one before passing it.
	 */
	find: any;
}
interface FindStatic<T extends Tag.Static> extends FindDocs {
	/**
	 * @param predicate A function that accepts up to two arguments.  The `find` function calls the
	 * predicate once for each element of the iterable, in ascending order, until it finds one where
	 * the predicate returns `true`.  If such an element is found, `find` immediately returns that
	 * element value.  Otherwise, when the iterable is exhausted, `find` returns `undefined`.
	 */
	find<I, J extends I>(
		iterable  : Tagged.Iterable<T, I>,
		predicate : (value: I, index: number) => value is J
	): Tagged.Promise<T, J | undefined>;

	/**
	 * @param predicate A function that accepts up to two arguments.  The `find` function calls the
	 * predicate once for each element of the iterable, in ascending order, until it finds one where
	 * the predicate returns `true`.  If such an element is found, `find` immediately returns that
	 * element value.  Otherwise, when the iterable is exhausted, `find` returns `undefined`.
	 */
	find<I>(
		iterable  : Tagged.Iterable<T, I>,
		predicate : (value: I, index: number) => boolean
	): Tagged.Promise<T, I | undefined>;
}

interface FindInstance<T extends Tag.Instance> extends FindDocs {
	/**
	 * @param predicate A function that accepts up to two arguments.  The `find` function calls the
	 * predicate once for each element of the iterable, in ascending order, until it finds one where
	 * the predicate returns `true`.  If such an element is found, `find` immediately returns that
	 * element value.  Otherwise, when the iterable is exhausted, `find` returns `undefined`.
	 */
	find<I, J extends I>(
		this      : Tagged.Iterable<T, I>,
		predicate : (value: I, index: number) => value is J
	): Tagged.Promise<T, J | undefined>;
	/**
	 * @param predicate A function that accepts up to two arguments.  The `find` function calls the
	 * predicate once for each element of the iterable, in ascending order, until it finds one where
	 * the predicate returns `true`.  If such an element is found, `find` immediately returns that
	 * element value.  Otherwise, when the iterable is exhausted, `find` returns `undefined`.
	 */
	find<I>(
		this      : Tagged.Iterable<T, I>,
		predicate : (value: I, index: number) => boolean
	): Tagged.Promise<T, I | undefined>;
}

export type Find<T extends Tag> =
	T extends Tag.Static ? FindStatic<T> : T extends Tag.Instance ? FindInstance<T> : never;