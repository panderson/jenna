import {Tag, Tagged} from "#tags";

interface FindIndexDocs {
	/**
	 * Returns the index of the first element in the iterable where `predicate` is `true`, or `-1`
	 * otherwise.
	 *
	 * This function is similar to `Array.prototype.findIndex()` except that the predicate doesn't
	 * receive a reference to the iterable, and there is no `thisArg` parameter.  If you want a
	 * `this` argument for the predicate, you can bind one before passing it.
	 */
	findIndex: any;
}
interface FindIndexStatic<T extends Tag.Static> extends FindIndexDocs {
	/**
	 * @param predicate This function is called once for each element of the iterable until it
	 * returns `true` or the iterable is exhausted.  If the predicate returns `true`, `findIndex`
	 * immediately returns that element index.  Otherwise, it returns `-1`.
	 */
	findIndex<I>(
		iterable  : Tagged.Iterable<T, I>,
		predicate : (value: I, index: number) => boolean
	): Tagged.Promise<T, number>;
}

interface FindIndexInstance<T extends Tag.Instance> extends FindIndexDocs {
	/**
	 * @param predicate This function is called once for each element of the iterable until it
	 * returns `true` or the iterable is exhausted.  If the predicate returns `true`, `findIndex`
	 * immediately returns that element index.  Otherwise, it returns `-1`.
	 */
	findIndex<I>(
		this      : Tagged.Iterable<T, I>,
		predicate : (value: I, index: number) => boolean
	): Tagged.Promise<T, number>;
}

export type FindIndex<T extends Tag> =
	T extends Tag.Static
		? FindIndexStatic<T> :
	T extends Tag.Instance
		? FindIndexInstance<T> :
	never;