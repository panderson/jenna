import {Tag, Tagged} from "#tags";

interface SomeDocs {
	/**
	 * Determines whether any items in the iterable pass the given predicate.  If no predicate is
	 * given, returns true if there are any items in the iterable.
	 *
	 * This function is similar to `Array.prototype.some()` except that the predicate is optional,
	 * the predicate doesn't receive a reference to the iterable, and there is no `thisArg`
	 * parameter.  If you want a `this` argument for the predicate, you can bind one before passing
	 * it.
	 */
	some: any;
}

interface SomeStatic<T extends Tag.Static> extends SomeDocs {
	/**
	 * @param iterable The iterable to be tested.
	 * @param predicate A function that accepts up to two arguments.  The `some` function calls the
	 * predicate for each element in the iterable until the predicate returns a value coercible to
	 * the Boolean value `true`, or until the iterable is exhausted.
	 */
	some<I>(
		iterable   : Tagged.Iterable<T, I>,
		predicate? : ((value: I, index: number) => boolean) | undefined
	): Tagged.Promise<T, boolean>;
}

interface SomeInstance<T extends Tag.Instance> extends SomeDocs {
	/**
	 * @param predicate A function that accepts up to two arguments.  The `some` function calls the
	 * predicate for each element in the iterable until the predicate returns a value coercible to
	 * the Boolean value `true`, or until the iterable is exhausted.
	 */
	some<I>(
		this       : Tagged.Iterable<T, I>,
		predicate? : ((value: I, index: number) => boolean) | undefined
	): Tagged.Promise<T, boolean>;
}

export type Some<T extends Tag> =
	T extends Tag.Static ? SomeStatic<T> : T extends Tag.Instance ? SomeInstance<T> : never;