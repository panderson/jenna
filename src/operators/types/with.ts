import {Tag, Tagged} from "#tags";

interface WithDocs {
	/**
	 * Returns an iterable with the same values as an existing iterable except for the value at the
	 * given index, which is replaced by the new given value.
	 *
	 * This function is similar to `Array.prototype.with()` except that a selector function can be
	 * used to choose the new value based on the current value.
	 */
	with: any;
}

interface WithStatic<T extends Tag.Static> extends WithDocs {
	with<I, J = I, R = any, N = unknown>(
		iterable : Tagged.Iterable<T, I>,
		index    : number,
		value    : J | ((currentValue: I) => J),
	): Tagged.Result.Generator<T, I | J, R, N>;
}

interface WithInstance<T extends Tag.Instance> extends WithDocs {
	with<I, J = I, R = any, N = unknown>(
		this  : Tagged.Iterable<T, I>,
		index : number,
		value : J | ((currentValue: I) => J),
	): Tagged.Result.Generator<T, I | J, R, N>;
}

export type With<T extends Tag> =
	T extends Tag.Static ? WithStatic<T> : T extends Tag.Instance ? WithInstance<T> : never;