import {Tag, Tagged} from "#tags";

interface ExceptDocs {
	/** Yields all elements of the given iterable not found in another given iterable. */
	except: any
}

interface ExceptStatic<T extends Tag.Static> extends ExceptDocs {
	/**
	 * @param iterable The source iterable whose items are to be yielded if not found in the other
	 * iterable.
	 * @param other The other iterable. Only items from the source iterable that are not found in
	 * this iterable will be yielded.
	 */
	except<I, R = any, N = unknown>(
		iterable : Tagged.Iterable<T, I>,
		other    : Tagged.Iterable<T, I>
	): Tagged.Result.Generator<T, I, R, N>;
}

interface ExceptInstance<T extends Tag.Instance> extends ExceptDocs {
	/**
	 * @param other The other iterable. Only items from the source iterable that are not found in
	 * this iterable will be yielded.
	 */
	except<I, R = any, N = unknown>(
		this  : Tagged.Iterable<T, I>,
		other : Tagged.Iterable<T, I>
	): Tagged.Result.Generator<T, I, R, N>;
}

export type Except<T extends Tag> =
	T extends Tag.Static ? ExceptStatic<T> : T extends Tag.Instance ? ExceptInstance<T> : never;