import {Tag, Tagged} from "#tags";

interface MinDocs {
	/**
	 * Returns the minimum valued element of an iterable, or `undefined` if the iterable is empty.
	 */
	min: any;
}
interface MinStatic<T extends Tag.Static> extends MinDocs {
	min<I>(iterable: Tagged.Iterable<T, I>): Tagged.Promise<T, I | undefined>;
}

interface MinInstance<T extends Tag.Instance> extends MinDocs {
	min<I>(this: Tagged.Iterable<T, I>): Tagged.Promise<T, I | undefined>;
}

export type Min<T extends Tag> =
	T extends Tag.Static ? MinStatic<T> : T extends Tag.Instance ? MinInstance<T> : never;