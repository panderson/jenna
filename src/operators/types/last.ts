import {Tag, Tagged} from "#tags";

interface LastDocs {
	/** Returns the last element of an iterable, or `undefined` if the iterable is empty. */
	last: any;
}

interface LastStatic<T extends Tag.Static> extends LastDocs {
	/** @param iterable The iterable whose last item is to be returned. */
	last<I>(iterable: Tagged.Iterable<T, I>): Tagged.Promise<T, I | undefined>;
}

interface LastInstance<T extends Tag.Instance> extends LastDocs {
	last<I>(this: Tagged.Iterable<T, I>): Tagged.Promise<T, I | undefined>;
}

export type Last<T extends Tag> =
	T extends Tag.Static ? LastStatic<T> : T extends Tag.Instance ? LastInstance<T> : never;