import {Tag, Tagged} from "#tags";

interface IncludesDocs {
	/** Determines whether a certain element can be found in the iterable. */
	includes: any;
}

interface IncludesStatic<T extends Tag.Static> extends IncludesDocs {
	/**
	 * @param element The element to search for.
	 * @param minIndex The iterable index at which to begin the search.  If `minIndex` is omitted or
	 * less than `0`, the search starts at the first element.
	 */
	includes<I>(
		iterable  : Tagged.Iterable<T, I>,
		element   : I,
		minIndex? : number
	): Tagged.Promise<T, boolean>;
}

interface IncludesInstance<T extends Tag.Instance> extends IncludesDocs {
	/**
	 * @param element The element to search for.
	 * @param minIndex The iterable index at which to begin the search.  If `minIndex` is omitted or
	 * less than `0`, the search starts at the first element.
	 */
	includes<I>(
		this      : Tagged.Iterable<T, I>,
		element   : I,
		minIndex? : number
	): Tagged.Promise<T, boolean>;
}

export type Includes<T extends Tag> =
	T extends Tag.Static ? IncludesStatic<T> : T extends Tag.Instance ? IncludesInstance<T> : never;