import {Tag, Tagged} from "#tags";

interface SpliceDocs {
	/**
	 * Skips elements from an iterable and, if necessary, inserts new elements in their place.
	 *
	 * This function is similar to `Array.prototype.splice()` except that, instead of yielding the
	 * deleted elements, it yields the resulting iterable.
	 */
	splice: any;
}

interface SpliceStatic<T extends Tag.Static> extends SpliceDocs {
	/**
	 * @param iterable The iterable to splice.
	 * @param start The zero-based location in the iterable from which to start skipping elements.
	 * If negative, then the position counting backward from the end of the iterable is found. If
	 * not given, then no elements are skipped. However, `undefined` is treated like `0`, and
	 * elements will be skipped starting from the beginning of the iterable.
	 * @param deleteCount The number of elements to skip. If not given, then all remaining elements
	 * from the starting position will be skipped. However, `undefined` is treated like `0`, and no
	 * elements will be skipped.
	 * @param newItems Elements to yield in place of the skipped elements.
	 */
	splice<I, J = I, R = any, N = unknown>(
		iterable     : Tagged.Iterable<T, I>,
		start?       : number | undefined,
		deleteCount? : number | undefined,
		...newItems  : J[]
	): Tagged.Result.Generator<T, I | J, R, N>;
}

interface SpliceInstance<T extends Tag.Instance> extends SpliceDocs {
	/**
	 * @param start The zero-based location in the iterable from which to start skipping elements.
	 * If negative, then the position counting backward from the end of the iterable is found. If
	 * not given, then no elements are skipped. However, `undefined` is treated like `0`, and
	 * elements will be skipped starting from the beginning of the iterable.
	 * @param deleteCount The number of elements to skip. If not given, then all remaining elements
	 * from the starting position will be skipped. However, `undefined` is treated like `0`, and no
	 * elements will be skipped.
	 * @param newItems Elements to yield in place of the skipped elements.
	 */
	splice<I, J = I, R = any, N = unknown>(
		this         : Tagged.Iterable<T, I>,
		start?       : number | undefined,
		deleteCount? : number | undefined,
		...newItems  : J[]
	): Tagged.Result.Generator<T, I | J, R, N>;
}

export type Splice<T extends Tag> =
	T extends Tag.Static ? SpliceStatic<T> : T extends Tag.Instance ? SpliceInstance<T> : never;