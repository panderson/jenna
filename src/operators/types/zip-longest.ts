import {Tag, Tagged} from "#tags";

interface ZipLongestDocs {
	/**
	 * Iterates through two iterables in tandem, calling the `resultSelector` function for each pair
	 * of elements and yielding its results, or yielding tuples containing the elements from each
	 * iterable if no result selector is given.
	 *
	 * Iteration continues until both of the iterables are exhausted.
	 */
	zipLongest: any;
}

interface ZipLongestStatic<T extends Tag.Static> extends ZipLongestDocs {
	/**
	 * @param resultSelector A function that receives an element from each iterable and the current
	 * element index.  If one of the iterables is already exhausted, the function will receive
	 * `undefined` in place of its element.
	 */
	zipLongest<I, J, R = [I | undefined, J | undefined]>(
		iterable        : Tagged.Iterable<T, I>,
		other           : Tagged.Iterable<T, J>,
		resultSelector? : (a: I | undefined, b: J | undefined, index: number) => R
	): Tagged.Result.Generator<T, R>;
}

interface ZipLongestInstance<T extends Tag.Instance> extends ZipLongestDocs {
	zipLongest<I, J, R = [I | undefined, J | undefined]>(
		this            : Tagged.Iterable<T, I>,
		other           : Tagged.Iterable<T, J>,
		resultSelector? : (a: I | undefined, b: J | undefined, index: number) => R
	): Tagged.Result.Generator<T, R>;
}

export type ZipLongest<T extends Tag> =
	T extends Tag.Static
		? ZipLongestStatic<T> :
	T extends Tag.Instance
		? ZipLongestInstance<T> :
	never;