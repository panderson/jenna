import {Tag, Tagged} from "#tags";
import {AggregateBy} from "@aggregates/types";

interface ByDocs {
	/** Provides a set of aggregate functions on keyed groups from an iterable. */
	by: any;
}

interface ByStatic<T extends Tag.Static> extends ByDocs {
	by<I, K = I>(
		iterable     : Tagged.Iterable<T, I>,
		keySelector? : ((item: I) => K) | undefined,
	): AggregateBy<T, K, I>;
}

interface ByInstance<T extends Tag.Instance> extends ByDocs {
	by<I, K = I>(
		this         : Tagged.Iterable<T, I>,
		keySelector? : ((item: I) => K) | undefined,
	): AggregateBy<T, K, I>;
}

export type By<T extends Tag> =
	T extends Tag.Static ? ByStatic<T> : T extends Tag.Instance ? ByInstance<T> : never;