import {Tag, Tagged} from "#tags";

interface PickDocs {
	/** Yields only the elements whose keys match those given by another iterable. */
	pick: any;
}

interface PickStatic<T extends Tag.Static> extends PickDocs {
	/**
	 * @param keySelector A function that receives an element from the iterable and returns its key.
	 * @param keys The keys of the items from the iterable to yield in the result.
	 */
	pick<I, K, R = any, N = unknown>(
		iterable    : Tagged.Iterable<T, I>,
		keySelector : (item: I) => K,
		keys        : Tagged.Iterable<T, K>
	): Tagged.Result.Generator<T, I, R, N>;
}

interface PickInstance<T extends Tag.Instance> extends PickDocs {
	/**
	 * @param keySelector A function that receives an element from the iterable and returns its key.
	 * @param keys The keys of the items from the iterable to yield in the result.
	 */
	pick<I, K, R = any, N = unknown>(
		this        : Tagged.Iterable<T, I>,
		keySelector : (item: I) => K,
		keys        : Tagged.Iterable<T, K>
	): Tagged.Result.Generator<T, I, R, N>;
}

export type Pick<T extends Tag> =
	T extends Tag.Static
		? PickStatic<T> :
	T extends Tag.Instance
		? PickInstance<T> :
	never;