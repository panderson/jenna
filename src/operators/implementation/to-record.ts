import {operator} from "../setup";

export const toRecord = operator("toRecord", {iterator: true}, iterator =>
	async function toRecord<T, K extends keyof any, V>(
		iterable       : AnyIterable<any>,
		keySelector?   : (item: T) => K,
		valueSelector? : (item: T, key: K) => V,
	): Promise<any> {
		const rtn = {} as any;
		const it  = iterator(iterable);

		try {
			let step = await it.next();

			if (!keySelector) {
				while (!step.done) {
					const [key, value] = step.value as any;
					rtn[key] = value;
					step = await it.next();
				}
			} else if (!valueSelector) {
				while (!step.done) {
					rtn[keySelector(step.value)] = step.value;
					step = await it.next();
				}
			} else {
				while (!step.done) {
					const key = keySelector(step.value);
					rtn[key] = valueSelector(step.value, key);
					step = await it.next();
				}
			}

			return rtn;
		} catch (E) {
			it.throw?.(E);
			throw E;
		}
	}
);