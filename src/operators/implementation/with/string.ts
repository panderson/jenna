export async function *string<J = string>(
	string   : string,
	index    : number,
	selector : (currentValue: string) => J,
): AsyncGenerator<string | J, any, never> {
	const {length} = string;

	if (!length)
		throw new RangeError("Cannot replace a value in an empty string.");

	if (index < 0)
		index += length;

	if (index < 0 || index >= length)
		throw new RangeError(`index must be between -${length} and ${length - 1}.`);

	const result = string.slice(0, index) + selector(string[index]) + string.slice(index + 1);
	yield *result;
}