import {Matched} from "@utils";

export async function *negative<I, J = I>(
	iterator : typeof Matched.iterator,
	iterable : AnyIterable<I>,
	index    : number,
	selector : (currentValue: I) => J,
): AsyncGenerator<I | J, any, never> {
	const it   = iterator(iterable);
	let   step = await it.next();

	try {
		if (step.done)
			throw new RangeError("Cannot replace a value in an empty sequence.");

		const buffer = new Array(-index);
		let   offset = 0;

		while (offset < -index) {
			buffer[offset++] = step.value;
			step = await it.next();

			if (step.done) {
				if (offset < -index) {
					throw new RangeError(
						`Cannot replace a value at index ${index} in sequence of length ${offset}.`
					);
				}

				yield selector(buffer[0]);

				for (let i = 1; i < offset; i++)
					yield buffer[i];

				return step.value;
			}
		}

		offset = 0;

		do {
			yield buffer[offset];
			buffer[offset++] = step.value;
			if (offset === -index)
				offset = 0;
			step = await it.next();
		} while (!step.done);

		yield selector(buffer[offset]);

		for (let i = offset + 1; i < -index; i++)
			yield buffer[i];

		for (let i = 0; i < offset; i++)
			yield buffer[i];

		return step.value;
	} finally {
		if (!step.done)
			await it.return?.();
	}
}