import {Matched} from "@utils";

export async function *positive<I, J = I>(
	iterator : typeof Matched.iterator,
	iterable : AnyIterable<I>,
	index    : number,
	selector : (currentValue: I) => J,
): AsyncGenerator<I | J, any, never> {
	const it   = iterator(iterable);
	let   step = await it.next();

	try {
		if (step.done)
			throw new RangeError("Cannot replace a value in an empty sequence.");

		let i = 0;

		while (i++ !== index) {
			step = await it.next(yield step.value);

			if (step.done) {
				throw new RangeError(
					`Cannot replace a value at index ${index} in sequence of length ${i}.`
				);
			}
		}

		step = await it.next(yield selector(step.value));

		while (!step.done)
			step = await it.next(yield step.value);

		return step.value;
	} finally {
		if (!step.done)
			await it.return?.();
	}
}