import {operator} from "@operators/setup";
import {syncFunc} from "@operators/setup/generate";
import {array}    from "./array";
import {negative} from "./negative";
import {positive} from "./positive";
import {string}   from "./string";

const async   = [array, negative, positive, string] as const;
const sync    = async.map(syncFunc) as any;
const closure = {async, sync};

const with_ = operator(
	"with",
	{iterator: true, wrap: true, closure},
	(iterator, array, negative, positive, string) => function with_<I, J = I>(
		iterable : AnyIterable<I>,
		index    : number,
		value    : J | ((currentValue: I) => J),
	): AsyncGenerator<I | J> {
		const selector: (currentValue: I) => J =
			typeof value === "function" ? value as any : () => value;

		if (Array.isArray(iterable))
			return array(iterable, index, selector);

		if (typeof iterable === "string")
			return string(iterable, index, selector as any) as any;

		return (index < 0 ? negative : positive)(iterator, iterable, index, selector);
	}
);

export {with_ as with};