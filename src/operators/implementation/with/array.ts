export async function *array<I, J = I>(
	array    : I[],
	index    : number,
	selector : (currentValue: I) => J,
): AsyncGenerator<I | J, any, never> {
	const {length} = array;

	if (!length)
		throw new RangeError("Cannot replace a value in an empty array.");

	if (index < 0)
		index += length;

	if (index < 0 || index >= length)
		throw new RangeError(`index must be between -${length} and ${length - 1}.`);

	const copy = [...array] as Array<I | J>;
	copy[index] = selector(array[index]);
	yield *copy;
}