import {operator} from "../setup";
import {groupBy}  from "./group-by";

export const leftJoin = operator("leftJoin", {ops: [groupBy], wrap: true}, groupBy =>
	async function *leftJoin(
		leftIterable,
		rightIterable,
		leftKeySelector,
		rightKeySelector,
		resultSelector
	) {
		if (!resultSelector)
			resultSelector = (left, right, key) => [left, right, key] as any;

		const lookup = await groupBy(rightIterable, rightKeySelector);

		let index = 0;
		for await (const value of leftIterable) {
			const key = leftKeySelector(value, index++);
			for (const u of lookup.get(key) ?? [undefined])
				yield resultSelector(value, u, key);
		}
	}
);