import {operator} from "../setup";
import {concat}   from "./concat";

export const extend = operator("extend", {ops: [concat]},
	concat => (iterable, ...elements) => concat(iterable, elements) as any
);