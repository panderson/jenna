import {operator}     from "@operators/setup";
import {syncFunc}     from "@operators/setup/generate";
import {SplitOptions} from "@operators/types/split/common";
import {getParts}     from "./get-part";
import {options}      from "./options";

const async   = [options, getParts] as const;
const sync    = [options, syncFunc(getParts)] as const;
const closure = {async, sync};

export const split = operator("split", {closure, iterator: true, wrap: true},
	(iterator, options, getParts) => async function *split<I, R = any, N = unknown>(
		iterable : AnyIterable<I>,
		...args  : [separator: I, limit?: number] | [options: SplitOptions<I>]
	): AsyncGenerator<I[], R | void, N> {
		const opt = options(args);

		if (opt.limit === 0)
			return;

		const it = iterator(iterable);
		let step: IteratorResult<I[]>;

		try {
			const parts = getParts(it, opt);
			step = await parts.next();

			while (!step.done) {
				const next = yield step.value;

				if (--opt.limit === 0)
					return;

				step = await parts.next(next);
			}

			return step.value;
		} finally {
			if (!step!?.done)
				await it.return?.();
		}
	}
);