import {SeparatorOptions, ShouldSplitOptions, SplitOptions} from "@operators/types/split/common";

const shouldSplit = <I>(options: ShouldSplitOptions<I>): Required<ShouldSplitOptions<I>> => ({
	shouldSplit   : options.shouldSplit,
	limit         : Math.floor(options.limit ?? -1),
	keepSeparator : options.keepSeparator ?? false,
	omitEmpty     : options.omitEmpty     ?? false,
});

function separator<I>(options: SeparatorOptions<I>): Required<ShouldSplitOptions<I>> {
	const separator = options.separator;
	return shouldSplit({...options, shouldSplit: e => e === separator});
}

export function options<I>(
	args: [separator: I, limit?: number] | [options: SplitOptions<I>]
): Required<ShouldSplitOptions<I>> {
	if (typeof args[0] === "function")
		return shouldSplit({shouldSplit: args[0] as any, limit: args[1]});

	if (args.length === 1) {
		if (typeof args[0] === "object" && args[0]) {
			if ("shouldSplit" in args[0])
				return shouldSplit(args[0]);
			if ("separator" in args[0])
				return separator(args[0]);
		}
		return separator({separator: args[0]});
	}
	return separator({separator: args[0], limit: args[1]});
}