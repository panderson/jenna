import {ShouldSplitOptions} from "@operators/types/split/common";

export async function *getParts<I, R = any, N = unknown>(
	iterator : AnyIterator<I>,
	opt      : Required<ShouldSplitOptions<I>>,
): AsyncGenerator<I[], R | void, N> {
	let part: I[] = [];
	let step      = await iterator.next();
	let index     = 0;
	let separator = false;

	while (!step.done) {
		if (opt.shouldSplit(step.value, index++)) {
			switch (opt.keepSeparator) {
			case true:
				if (!opt.omitEmpty || part.length)
					yield part;

				step = await iterator.next(yield [step.value]);
				break;
			case "start":
				if (opt.omitEmpty && !part.length) {
					part = [step.value];
					step = await iterator.next();
				} else {
					const next = yield part;
					part       = [step.value];
					step       = await iterator.next(next);
				}
				continue;
			case "end":
				part.push(step.value);
				step = await iterator.next(yield part);
				break;
			default: // Don't yield separator.
				if (opt.omitEmpty && !part.length)
					step = await iterator.next();
				else
					step = await iterator.next(yield part);
				break;
			}

			part      = [];
			separator = true;
			continue;
		}

		part.push(step.value);
		step      = await iterator.next();
		separator = false;
	}

	if (part.length || separator && !opt.omitEmpty)
		yield part;

	return step.value;
}