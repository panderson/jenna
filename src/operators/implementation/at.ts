import {operator} from "../setup";

export const at = operator("at", async function at(iterable, index) {
	if (Array.isArray(iterable) || typeof iterable === "string") {
		if (index < 0)
			index += iterable.length;
		return iterable[index];
	}

	if (index < 0) {
		if (index === -1) {
			let last;

			for await (last of iterable) {
				// Do nothing;
			}

			return last;
		}

		const buffer = new Array(-index);
		let   offset = 0;

		for await (const item of iterable) {
			buffer[offset++] = item;
			if (offset === -index)
				offset = 0;
		}

		return buffer[offset];
	}

	for await (const item of iterable) {
		if (index-- === 0)
			return item;
	}

	return undefined;
});