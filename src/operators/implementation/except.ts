import {operator} from "../setup";
import {omit}     from "./omit";

export const except = operator("except", {ops: [omit]},
	omit => (iterable, other) => omit(iterable, x => x, other) as any
);