import {operator} from "../setup";

export const some = operator("some", {iterator: true}, iterator =>
	async function some(iterable, predicate) {
		if (!iterable)
			return false;

		const it   = iterator(iterable);
		let   step = await it.next();

		try {
			if (!predicate)
				return !step.done;

			let i = 0;

			while (!step.done) {
				if (predicate(step.value, i++))
					return true;

				step = await it.next();
			}

			return false;
		} finally {
			if (!step.done)
				await it.return?.();
		}
	}
);