import {operator} from "../setup";

export const count = operator("count", {iterator: true}, iterator =>
	async function count(iterable) {
		if (Array.isArray(iterable) || typeof iterable === "string")
			return iterable.length;

		let count = 0;
		const it = iterator(iterable);
		while (!(await it.next()).done)
			count++;
		return count;
	}
);