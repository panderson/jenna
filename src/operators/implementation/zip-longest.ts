import {operator} from "../setup";

export const zipLongest = operator("zipLongest", {iterator: true, wrap: true}, iterator =>
	async function *zipLongest(iterable, other, resultSelector) {
		if (!resultSelector)
			resultSelector = (a, b) => [a, b] as any;

		const ita = iterator(iterable);
		let doneA = false;

		try {
			const itb = iterator(other);
			let doneB = false;

			try {

				for (let index = 0; ; index++) {
					let stepA = await ita.next();
					let stepB = await itb.next();

					if (stepA.done) {
						doneA = true;
						while (!stepB.done) {
							yield resultSelector(undefined, stepB.value, index++);
							stepB = await itb.next();
						}
						break;
					}

					if (stepB.done) {
						doneB = true;
						do {
							yield resultSelector(stepA.value, undefined, index++);
							stepA = await ita.next();
						} while (!stepA.done);
						break;
					}

					yield resultSelector(stepA.value, stepB.value, index);
				}
			} catch (E) {
				if (!doneB)
					itb.throw?.(E);
				throw E;
			}
		} catch (E) {
			if (!doneA)
				ita.throw?.(E);
			throw E;
		}
	}
);