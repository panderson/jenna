import {operator} from "../setup";
import {map}      from "./map";
import {sum}      from "./sum";

export const mean = operator("mean", {ops: [map, sum] as const},
	(map, sum) => async function mean(iterable) {
		let count = 0;
		const counter = map(iterable, t => {count++; return t;});
		const total   = await sum(counter);
		if (!count)
			return undefined;
		return (total as any) / count;
	}
);