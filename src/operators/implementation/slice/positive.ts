import {Matched} from "@src/utils";

export async function *positive<I>(
	iterator : typeof Matched.iterator,
	iterable : AnyIterable<I>,
	start?   : number,
	end?     : number,
): AsyncGenerator<I, any, never> {
	if (start === undefined)
		start = 0;

	if (end !== undefined && end <= start)
		return;

	const it   = iterator(iterable);
	let   step = await it.next();

	try {
		let i = start;

		while (true) {
			if (step.done)
				return step.value;
			if (!start--)
				break;
			step = await it.next();
		}

		if (end === undefined) {
			while (!step.done)
				step = await it.next(yield step.value);
		} else {
			while (!step.done) {
				const next: any = yield step.value;

				if (++i >= end)
					return;

				step = await it.next(next);
			}
		}

		return step.value;
	} finally {
		if (!step.done)
			await it.return?.();
	}
}