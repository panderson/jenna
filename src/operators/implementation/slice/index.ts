import {operator} from "@operators/setup";
import {syncFunc} from "@operators/setup/generate";
import {array}    from "./array";
import {negative} from "./negative";
import {positive} from "./positive";
import {string}   from "./string";

const async   = [array, string, positive, negative.async] as const;
const sync    = [syncFunc(array), syncFunc(string), syncFunc(positive), negative.sync] as any;
const closure = {async, sync};

export const slice = operator(
	"slice",
	{iterator: true, wrap: true, closure},
	(iterator, array, string, positive, negative) => function slice(iterable, start?, end?) {
		if (Array.isArray(iterable))
			return array(iterable, start, end);

		if (typeof iterable === "string")
			return string(iterable, start, end);

		if (start === undefined || start >= 0) {
			if (end === undefined || end >= 0)
				return positive(iterator, iterable, start, end);
			return negative.end(iterator, iterable, start, end);
		}
		if (end === undefined)
			return negative.noEnd(iterator, iterable, start);
		if (end >= 0)
			return negative.start(iterator, iterable, start, end);
		return negative.both(iterator, iterable, start, end);
	}
);