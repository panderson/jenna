import {Matched} from "@src/utils";

// positive start, negative end

export async function *end<I>(
	iterator : typeof Matched.iterator,
	iterable : AnyIterable<I>,
	start    : number | undefined,
	end      : number,
): AsyncGenerator<I, any, never> {
	if (start === undefined)
		start = 0;

	const size   = -end;
	const buffer = new Array(size);
	let   offset = 0;
	let   index  = end;
	const it     = iterator(iterable);
	let   step   = await it.next();

	try {
		while (!step.done) {
			if (++index > start)
				yield buffer[offset];

			buffer[offset++] = step.value;
			if (offset === size)
				offset = 0;

			step = await it.next();
		}

		return step.value;
	} finally {
		if (!step.done)
			await it.return?.();
	}
}