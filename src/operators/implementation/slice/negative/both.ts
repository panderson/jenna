import {Matched} from "@src/utils";

export async function *both<I>(
	iterator : typeof Matched.iterator,
	iterable : AnyIterable<I>,
	start    : number,
	end      : number,
): AsyncGenerator<I, any, never> {
	if (end <= start)
		return;

	const size   = -start;
	const buffer = new Array(size);
	let   offset = 0;
	let   count  = 0;
	const it     = iterator(iterable);
	let   step   = await it.next();

	try {
		while (!step.done) {
			count++;
			buffer[offset++] = step.value;
			if (offset === size)
				offset = 0;

			step = await it.next();
		}

		if (count < size) {
			start = Math.max(0, start + count);
			end   = Math.max(0, end + count);
			while (start < end)
				yield buffer[start++];

			return step.value;
		}

		count = Math.min(end - start, count - Math.max(start + count, 0));

		for (let i = offset; i < size && count--; i++)
			yield buffer[i];

		for (let i = 0; i < offset && count-- > 0; i++)
			yield buffer[i];

		return step.value;
	} finally {
		if (!step.done)
			await it.return?.();
	}
}