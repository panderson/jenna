import {Matched} from "@src/utils";

// negative start, positive end

export async function *start<I>(
	iterator : typeof Matched.iterator,
	iterable : AnyIterable<I>,
	start    : number,
	end      : number,
): AsyncGenerator<I, any, never> {
	const size   = -start;
	const buffer = new Array(size);
	let   offset = 0;
	let   count  = 0;
	const limit  = end - start;
	const it     = iterator(iterable);
	let   step   = await it.next();

	try {
		while (!step.done) {
			if (++count >= limit)
				return;

			buffer[offset++] = step.value;
			if (offset === size)
				offset = 0;

			step = await it.next();
		}

		if (count < size)
			offset = 0;

		start = Math.max(start + count, 0);
		count = Math.min(end, count) - start;

		for (let i = offset; i < size && count--; i++)
			yield buffer[i];

		for (let i = 0; i < offset && count-- > 0; i++)
			yield buffer[i];

		return step.value;
	} finally {
		if (!step.done)
			await it.return?.();
	}
}