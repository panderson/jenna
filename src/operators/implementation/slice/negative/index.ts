import {syncFunc} from "@operators/setup/generate";
import {both}     from "./both";
import {end}      from "./end";
import {noEnd}    from "./no-end";
import {start}    from "./start";

const async = {noEnd, start, end, both};
const sync  = {
	noEnd : syncFunc(noEnd),
	start : syncFunc(start),
	end   : syncFunc(end),
	both  : syncFunc(both),
};

export const negative = {async, sync};