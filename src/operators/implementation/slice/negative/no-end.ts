import {Matched} from "@src/utils";

export async function *noEnd<I>(
	iterator : typeof Matched.iterator,
	iterable : AnyIterable<I>,
	start    : number,
): AsyncGenerator<I, any, never> {
	const size    = -start;
	const buffer  = new Array(size);
	let   offset  = 0;
	let   wrapped = false;
	const it      = iterator(iterable);
	let   step    = await it.next();

	try {
		while (!step.done) {
			buffer[offset++] = step.value;
			if (offset === size) {
				offset = 0;
				wrapped = true;
			}

			step = await it.next();
		}

		if (wrapped) {
			for (let i = offset; i < size; i++)
				yield buffer[i];
		}

		for (let i = 0; i < offset; i++)
			yield buffer[i];

		return step.value;
	} finally {
		if (!step.done)
			await it.return?.();
	}
}