export async function *array<I>(
	array  : I[],
	start? : number,
	end?   : number,
): AsyncGenerator<I, any, never> {
	const {length} = array;

	if (start === undefined)
		start = 0;
	else if (start < 0)
		start = Math.max(0, start + length);

	if (end === undefined)
		end = length;
	else if (end < 0)
		end += length;
	else if (end > length)
		end = length;

	while (start < end)
		yield array[start++];
}