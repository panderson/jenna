export async function *string(
	string : string,
	start? : number,
	end?   : number,
): AsyncGenerator<string, any, never> {
	yield *string.slice(start, end);
}