import {operator} from "../setup";

export const first = operator("first", async function first(iterable) {
	for await (const item of iterable)
		return item;
	return undefined;
});