import {operator}         from "../../setup";
import {AggregateByAsync} from "./async";
import {AggregateBySync}  from "./sync";

export const by = operator("by", {async: AggregateByAsync, sync: AggregateBySync});