import {Tag}                    from "#tags";
import {aggregates, Aggregates} from "@aggregates";

class AggregateBy<K, I> implements AsyncIterable<[K, I]> {
	constructor(
		private readonly iterable    : AnyIterable<I>,
		private readonly keySelector : (item: I) => K,
	) { }

	async *[Symbol.asyncIterator](): AsyncGenerator<[K, I]> {
		for await (const item of this.iterable)
			yield [this.keySelector(item), item];
	}

	get items() { return this; }
}

Object.assign(AggregateBy.prototype, aggregates[Tag.InstanceAsync]);


export type AggregateByAsync<K, I> = AggregateBy<K, I> & Aggregates<Tag.InstanceAsync, K, I>;
export const AggregateByAsync = <I, K = I>(
	iterable     : AnyIterable<I>,
	keySelector? : ((item: I) => K) | undefined,
): AggregateByAsync<K, I> => new AggregateBy(iterable, keySelector ?? (i => i as any)) as any;