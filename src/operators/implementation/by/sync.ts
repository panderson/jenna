import {Tag}                    from "#tags";
import {aggregates, Aggregates} from "@aggregates";

class AggregateBy<K, I> implements Iterable<[K, I]> {
	constructor(
		private readonly iterable    : Iterable<I>,
		private readonly keySelector : (item: I) => K,
	) { }

	*[Symbol.iterator](): Generator<[K, I]> {
		for (const item of this.iterable)
			yield [this.keySelector(item), item];
	}

	get items() { return this; }
}

Object.assign(AggregateBy.prototype, aggregates[Tag.InstanceSync]);


export type AggregateBySync<K, I> = AggregateBy<K, I> & Aggregates<Tag.InstanceSync, K, I>;
export const AggregateBySync = <I, K = I>(
	iterable     : Iterable<I>,
	keySelector? : ((item: I) => K) | undefined,
): AggregateBySync<K, I> => new AggregateBy(iterable, keySelector ?? (i => i as any)) as any;