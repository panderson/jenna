import {operator} from "../setup";

export const toMap = operator("toMap", {iterator: true}, iterator => async function toMap<T, K, V>(
	iterable       : AnyIterable<T>,
	keySelector?   : (item: T, index: number) => K,
	valueSelector? : (item: T, key: K, index: number) => V,
) {
	const rtn  = new Map();
	const it   = iterator(iterable);
	let   step = await it.next();

	try {
		if (!keySelector) {
			while (!step.done) {
				const [key, value] = step.value as any;
				rtn.set(key, value);
				step = await it.next();
			}
		} else if (!valueSelector) {
			let index = 0;
			while (!step.done) {
				rtn.set(keySelector(step.value, index++), step.value);
				step = await it.next();
			}
		} else {
			let index = 0;
			while (!step.done) {
				const key = keySelector(step.value, index);
				rtn.set(key, valueSelector(step.value, key, index++));
				step = await it.next();
			}
		}

		return rtn;
	} finally {
		if (!step.done)
			await it.return?.();
	}
});