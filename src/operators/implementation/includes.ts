import {operator} from "../setup";
import {indexOf}  from "./index-of";

export const includes = operator("includes", {ops: [indexOf]},
	indexOf => async (it, e, min) => await indexOf(it, e, min) >= 0
);