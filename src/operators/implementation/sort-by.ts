import {keySorter, sortArray} from "@utils";
import {operator}             from "../setup";
import {toArray}              from "./to-array";

const closure = [sortArray, keySorter] as const;

export const sortBy = operator("sortBy", {closure, ops: [toArray]},
	(sortArray, keySorter, toArray) => async function sortBy(iterable, keySelector, ...args) {
		const [comparer, reverse] = args.length === 2 || typeof args[0] !== "boolean"
			? args : [undefined, args[0]];

		const array = await toArray(iterable);
		sortArray(array, keySorter(keySelector, comparer as any, reverse as any));
		return array;
	}
);