import {operator} from "../setup";

export const groupBy = operator("groupBy", async function groupBy<T, K, U>(
	iterable        : AnyIterable<T>,
	keySelector     : (item: T, index: number) => K,
	elementSelector : (item: T, index: number, key: K) => U = x => x as any,
): Promise<Map<K, U[]>> {
	const map = new Map<K, U[]>();

	let index = 0;
	for await (const item of iterable) {
		const key   = keySelector(item, index);
		const list  = map.get(key);
		const value = elementSelector(item, index++, key);

		if (list)
			list.push(value);
		else
			map.set(key, [value]);
	}

	return map;
});