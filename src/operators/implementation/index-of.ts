import {operator} from "../setup";

export const indexOf = operator("indexOf", {iterator: true}, iterator =>
	async function indexOf(iterable, element, minIndex) {
		let i = -1;
		if (minIndex && minIndex > 0) {
			const it = iterator(iterable);

			while (++i < minIndex) {
				if ((await it.next()).done)
					return -1;
			}

			while (true) {
				const step = await it.next();
				if (step.done)
					return -1;

				if (step.value === element) {
					await it.return?.();
					return i;
				}

				i++;
			}
		}

		for await (const value of iterable) {
			i++;
			if (value === element)
				return i;
		}
		return -1;
	}
);