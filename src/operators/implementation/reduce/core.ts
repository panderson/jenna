import {Initialize, Reduce} from "@operators/types/reduce/types";
import {Matched}            from "@utils";

export const implementation = (iterator: typeof Matched.iterator) => ({
	async 1<T>(
		iterable : AnyIterable<T>,
		reduce   : Reduce<T, T>,
	): Promise<T | undefined> {
		const it   = iterator(iterable);
		let   step = await it.next();

		try {
			if (step.done)
				return undefined;

			let result = step.value;
			for (let i = 1; ; i++) {
				step = await it.next();
				if (step.done)
					return result;
				result = reduce(result, step.value, i);
			}
		} finally {
			if (!step.done)
				await it.return?.();
		}
	},

	async 2<T, U>(
		iterable     : AnyIterable<T>,
		reduce       : Reduce<T, U>,
		initialValue : U,
	): Promise<U> {
		let result = initialValue;
		let i = 0;
		for await (const value of iterable)
			result = reduce(result, value, i++);
		return result;
	},

	async 3<T, U>(
		iterable   : AnyIterable<T>,
		reduce     : Reduce<T, U>,
		initialize : Initialize<T, U>,
	): Promise<U | undefined> {
		const it   = iterator(iterable);
		let   step = await it.next();

		try {
			if (step.done)
				return undefined;

			let result = initialize(step.value);
			for (let i = 1; ; i++) {
				step = await it.next();
				if (step.done)
					return result;
				result = reduce(result, step.value, i);
			}
		} finally {
			if (!step.done)
				await it.return?.();
		}
	}
});