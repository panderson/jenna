import {Matched, syncIterator} from "@utils";
import {
	Finalize,
	Finalizer,
	Initializer,
	InitialValue,
	Reduce,
	Reducer,
}                              from "@operators/types/reduce/types";
import {implementation}        from "./core";
import {operator}              from "@operators/setup";
import {syncFunc}              from "@operators/setup/generate";

type Params<T, U, V> =
	| Reducer<T, T>
	| Reducer<T, U> & InitialValue<U>
	| Reducer<T, U> & Initializer<T, U>
	| Reducer<T, T> & Finalizer<T | undefined, U>
	| Reducer<T, U> & InitialValue<U> & Finalizer<U, V>
	| Reducer<T, U> & Initializer<T, U> & Finalizer<U | undefined, V>;

const async   = [implementation(Matched.iterator)];
const sync    = [syncFunc(implementation)(syncIterator)];
const closure = {async, sync};

export const reduce = operator("reduce", {closure},
	implementation => async function reduce<T, U, V>(
		iterable : AnyIterable<T>,
		...args  :
			| [reduce: Reduce<T, T>]
			| [reduce: Reduce<T, U>, initialValue: U]
			| [initialValue: U, reduce: Reduce<T, U>, finalize?: Finalize<U, V>]
			| [params: Params<T, U, V>]
	): Promise<T | U | V | undefined> {
		if (typeof args[0] === "function") {
			if (args.length === 1)
				return await implementation[1](iterable, args[0]);

			return await implementation[2](iterable, args[0] as Reduce<T, U>, args[1] as U);
		}

		let result;

		if (typeof args[1] === "function") {
			const [initialValue, reduce, finalize] = args;
			result = await implementation[2](iterable, reduce as any, initialValue);
			return finalize ? finalize(result as any) : result as any;
		}

		const [params] = args as [Params<T, U, V>];

		if (!("reduce" in params))
			throw new Error("At minimum, a reduce function must be given.")

		if ("initialValue" in params) {
			if ("initialize" in params)
				throw new Error("An initialize function and an initialValue cannot both be given.");

			result = await implementation[2](iterable, params.reduce, params.initialValue);
		} else if ("initialize" in params)
			result = await implementation[3](iterable, params.reduce, params.initialize);
		else
			result = await implementation[1](iterable, params.reduce);

		return "finalize" in params ? params.finalize(result as any) : result;
	}
);