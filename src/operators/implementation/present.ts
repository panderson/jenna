import {operator} from "../setup";

export const present = operator("present", {iterator: true, wrap: true}, iterator =>
	async function *present<T, R = any, N = unknown>(
		iterable: AnyIterable<T>
	): AsyncGenerator<NonNullable<T>, R, N> {
		const it   = iterator(iterable);
		let   step = await it.next();

		try {
			while (!step.done) {
				const item = await step.value;
				const n = item === undefined || item === null ? undefined : yield item!;
				step = await it.next(await n);
			}

			return step.value;
		} finally {
			if (!step.done)
				await it.return?.();
		}
	}
);