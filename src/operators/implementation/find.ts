import {operator} from "../setup";

export const find = operator("find", async function find<T>(
	iterable  : AnyIterable<T>,
	predicate : (value: T, index: number) => boolean
) {
	let i = 0;
	for await (const value of iterable) {
		if (predicate(value, i++))
			return value;
	}
	return undefined;
});