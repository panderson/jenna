import {operator} from "../setup";
import {pick}     from "./pick";

export const intersect = operator("intersect", {ops: [pick]},
	pick => (iterable, other) => pick(iterable, x=> x, other) as any
);