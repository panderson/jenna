import {operator} from "../setup";

export const toSet = operator("toSet", {
	sync  : iterable => new Set(iterable),
	async : async function toSet<T>(iterable: AnyIterable<T>) {
		const rtn = new Set<T>();
		for await (const item of iterable)
			rtn.add(item);
		return rtn;
	}
});