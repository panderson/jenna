import {operator} from "../setup";

export const last = operator("last", async function last(iterable) {
	if (Array.isArray(iterable) || typeof iterable === "string")
		return iterable[iterable.length - 1];

	let last = undefined;
	for await (const item of iterable)
		last = item;
	return last;
});