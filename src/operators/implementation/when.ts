import {operator} from "../setup";

export const when = operator("when", {iterator: true, wrap: true}, iterator =>
	async function *when(iterable, selector) {
		const it = iterator(iterable);

		try {
			let step = await it.next();

			for (let index = 0; !step.done; index++)
				step = await it.next(selector(step.value, index, (yield step.value) as any));

			return step.value;
		} catch (E) {
			it.throw?.(E);
			throw E;
		}
	}
);