import {operator} from "../setup";

export const filter = operator("filter", {wrap: true, iterator: true}, iterator =>
	async function *filter<T>(
		iterable  : AnyIterable<T>,
		predicate : (value: T, index: number) => boolean
	) {
		const it = iterator(iterable);
		let step = await it.next();
		let i    = 0;

		try {
			while (!step.done)
				step = await it.next(predicate(step.value, i++) ? yield step.value : undefined);

			return step.value;
		} finally {
			if (!step.done)
				await it.return?.();
		}
	}
);