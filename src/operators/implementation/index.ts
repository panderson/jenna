import {Tag}       from "#tags";
import {Operators} from "../types";
import * as all    from "./@all";

type OperatorImplementations = {
	[$ in Tag]: Operators<$>;
};

export const operators: OperatorImplementations =
	Object.fromEntries(
		Tag.All.map(tag => [
			tag,
			Object.fromEntries(
				Object.entries(all)
				.map(([name, operator]) => [name, operator[tag]])
			)
		])
	) as any;