import {operator} from "../setup";
import {toSet}    from "./to-set";

export const omit = operator("omit", {ops: [toSet], iterator: true, wrap: true},
	(iterator, toSet) => async function *omit(iterable, keySelector, keys) {
		const set  = keys instanceof Set ? keys : await toSet(keys);
		const it   = iterator(iterable);
		let   step = await it.next();

		try {
			while (!step.done) {
				step = await it.next(
					set.has(keySelector(step.value)) ? undefined : yield step.value
				);
			}

			return step.value;
		} finally {
			if (!step.done)
				await it.return?.();
		}
	}
);