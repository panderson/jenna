import {operator} from "../setup";

export const concat = operator("concat", {iterator: true, wrap: true}, iterator =>
	async function *concat<I, J>(iterable: AnyIterable<I>, ...others: AnyIterable<I | J>[]) {
		let it: AnyIterator<I | J> = iterator(iterable);
		let step                   = await it.next();

		try {
			while (!step.done)
				step = await it.next(yield step.value);

			for (const other of others) {
				it = iterator(other);
				step = await it.next();
				while (!step.done)
					step = await it.next(yield step.value);
			}

			return step.value;
		} finally {
			if (!step.done)
				await it.return?.();
		}
	}
);