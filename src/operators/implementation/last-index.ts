import {operator} from "../setup";

export const lastIndexOf = operator("lastIndexOf",
	async function lastIndexOf(iterable, element, maxIndex) {
		let i     = -1;
		let found = -1;

		if (maxIndex !== undefined) {
			for await (const value of iterable) {
				i++;
				if (i > maxIndex)
					return found;
				if (value === element)
					found = i;
			}
		} else {
			for await (const value of iterable) {
				i++;
				if (value === element)
					found = i;
			}
		}
		return found;
	}
);