import {operator} from "../setup";
import {toArray}  from "./to-array";

export const reverse = operator("reverse", {ops: [toArray]},
	toArray => async(iterable) => (await toArray(iterable)).reverse()
);