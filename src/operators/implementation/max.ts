import {operator} from "../setup";
import {reduce}   from "./reduce";

export const max = operator("max", {ops: [reduce]},
	reduce => iterable => reduce(iterable, (a, b) => a > b ? a : b)
);