import {Matched} from "@src/utils";

export async function *negative<I, J = I>(
	iterator    : typeof Matched.iterator,
	iterable    : AnyIterable<I>,
	start       : number,
	deleteCount : number,
	newItems    : J[],
): AsyncGenerator<I | J, any, never> {
	const it   = iterator(iterable);
	let   step = await it.next();

	try {
		const buffer = new Array(-start);
		let   offset = 0;

		while (!step.done && offset < -start) {
			buffer[offset++] = step.value;
			step = await it.next();
		}

		if (step.done) {
			start  = -offset;
			offset = 0;
		} else {
			offset = 0;
			while (!step.done) {
				yield buffer[offset];
				buffer[offset++] = step.value;
				if (offset === -start)
					offset = 0;

				step = await it.next();
			}
		}

		yield *newItems;

		for (let i = offset + Math.max(0, deleteCount); i < -start; i++)
			yield buffer[i];

		for (let i = 0; i < offset; i++)
			yield buffer[i];

		return step.value;
	} finally {
		if (!step.done)
			await it.return?.();
	}
}