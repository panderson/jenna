export async function *all<I, J = I>(
	iterable : AnyIterable<I>,
): AsyncGenerator<I | J, any, never> {
	yield *iterable;
}