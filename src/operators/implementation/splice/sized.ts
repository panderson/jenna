export async function *sized<I extends readonly any[] | string, J = I>(
	array        : I,
	start        : number,
	deleteCount  : number,
	newItems     : J[]
): AsyncGenerator<(I extends ReadonlyArray<infer U> ? U : string) | J, any, never> {
	const {length} = array;

	if (start < 0) {
		start += length;
		if (start < 0)
			start = 0;
	} else if (start >= length) {
		yield *array;
		yield *newItems;
		return;
	}

	for (let i = 0; i < start; i++)
		yield array[i];

	yield *newItems;

	for (let i = start + Math.max(0, deleteCount); i < length; i++)
		yield array[i];
}