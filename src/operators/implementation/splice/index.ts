import {operator} from "@operators/setup";
import {syncFunc} from "@operators/setup/generate";
import {all}      from "./all";
import {negative} from "./negative";
import {positive} from "./positive";
import {sized}    from "./sized";

function readNumber(num: number | undefined): number {
	if (num === undefined || Number.isNaN(num) || num === -Infinity)
		return 0;
	return num;
}

const async = [all, sized, positive, negative, readNumber] as const;
const sync  = [
	syncFunc(all),
	syncFunc(sized),
	syncFunc(positive),
	syncFunc(negative),
	readNumber,
] as const;

const closure = {async, sync};


export const splice = operator(
	"splice",
	{iterator: true, wrap: true, closure},
	(iterator, all, sized, positive, negative, readNumber) => function splice(iterable, ...args) {
		if (!args.length)
			return all(iterable);

		const start       = readNumber(args.shift() as number);
		const deleteCount = args.length ? readNumber(args.shift() as number) : Infinity;

		if (Array.isArray(iterable) || typeof iterable === "string")
			return sized(iterable, start, deleteCount, args);

		if (start < 0)
			return negative(iterator, iterable, start, deleteCount, args);

		return positive(iterator, iterable, start, deleteCount, args);
	}
);