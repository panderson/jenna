import {Matched} from "@src/utils";

export async function *positive<I, J = I>(
	iterator    : typeof Matched.iterator,
	iterable    : AnyIterable<I>,
	start       : number,
	deleteCount : number,
	newItems    : J[],
): AsyncGenerator<I | J, any, never> {
	const it   = iterator(iterable);
	let   step = await it.next();

	try {
		while (!step.done && start--)
			step = await it.next(yield step.value);

		yield *newItems;

		while (!step.done && deleteCount-- > 0)
			step = await it.next();

		while (!step.done)
			step = await it.next(yield step.value);

		return step.value;
	} finally {
		if (!step.done)
			await it.return?.();
	}
}