import {operator} from "../setup";

export const until = operator("until", {iterator: true, wrap: true}, iterator =>
	async function *until(iterable, predicate) {
		const it   = iterator(iterable);
		let   step = await it.next();

		try {
			while (!step.done) {
				if (predicate(step.value))
					return undefined;

				step = await it.next(yield step.value);
			}

			return step.value;
		} finally {
			if (!step.done)
				await it.return?.();
		}
	}
);