import {operator} from "../setup";

export const map = operator("map", {iterator: true, wrap: true}, iterator =>
	async function *map(iterable, selector) {
		const it   = iterator(iterable);
		let   step = await it.next();
		let   i    = 0;

		try {
			while (!step.done)
				step = await it.next(yield selector(step.value, i++));

			return step.value;
		} finally {
			if (!step.done)
				await it.return?.();
		}
	}
);