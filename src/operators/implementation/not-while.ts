import {operator} from "../setup";

export const notWhile = operator("notWhile", {iterator: true, wrap: true}, iterator =>
	async function *notWhile(iterable, predicate) {
		const it   = iterator(iterable);
		let   step = await it.next();

		try {
			while (!step.done && predicate(step.value))
				step = await it.next();

			while (!step.done)
				step = await it.next(yield step.value);

			return step.value;
		} finally {
			if (!step.done)
				await it.return?.();
		}
	}
);