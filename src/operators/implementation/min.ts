import {operator} from "../setup";
import {reduce}   from "./reduce";

export const min = operator("min", {ops: [reduce]},
	reduce => iterable => reduce(iterable, (a, b) => a < b ? a : b)
);