import {operator} from "../setup";
import {reduce}   from "./reduce";

export const sum = operator("sum", {ops: [reduce]},
	reduce => iterable => reduce(iterable, (a, b) => (a as any) + b)
);