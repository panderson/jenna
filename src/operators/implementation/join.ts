import {operator} from "../setup";
import {map}      from "./map";
import {reduce}   from "./reduce";

export const join = operator("join", {ops: [map, reduce] as const},
	(map, reduce) => async(iterable, separator = '') =>
		await reduce(map(iterable, String), (a, b) => a + separator + b) ?? ''
);