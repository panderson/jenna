import {operator} from "../setup";

export const toArray = operator("toArray", {
	sync  : Array.from.bind(Array),
	async : async function toArray(iterable) {
		const rtn = [];
		for await (const item of iterable)
			rtn.push(item);
		return rtn;
	}
});