import {operator} from "../setup";

export const every = operator("every", async function every(iterable, predicate) {
	let i = 0;
	for await (const value of iterable) {
		if (!predicate(value, i++))
			return false;
	}
	return true;
});