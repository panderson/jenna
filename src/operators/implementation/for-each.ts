import {operator} from "../setup";

export const forEach = operator("forEach", {iterator: true}, iterator =>
	async function forEach(iterable, callback) {
		const it   = iterator(iterable);
		let   step = await it.next();
		let   i    = 0;

		try {
			while (!step.done)
				step = await it.next(callback(step.value, i++));

			return step.value;
		} finally {
			if (!step.done)
				await it.return?.();
		}
	}
);