import {operator} from "../setup";

export const flatten = operator("flatten", {wrap: true, iterator: true}, iterator =>
	async function *flatten<I, J, R = any, N = unknown>(
		iterable  : AnyIterable<I>,
		selector? : (item: I, index: number) => AnyIterable<J>
	): AsyncGenerator<any, R, N> {
		if (!selector)
			selector = x => x as unknown as AnyIterable<J>;

		const outerIt   = iterator(iterable);
		let   i         = 0;
		let   outerStep = await outerIt.next();

		try {
			while (!outerStep.done) {
				const innerIt   = iterator(selector(outerStep.value, i++));
				let   innerStep = await innerIt.next();
				let   next      : unknown;

				try {
					while (!innerStep.done) {
						next = yield innerStep.value;
						innerStep = await innerIt.next(next);
					}

					outerStep = await outerIt.next(next);
				} finally {
					if (!innerStep.done)
						await innerIt.return?.();
				}
			}

			return outerStep.value;
		} finally {
			if (!outerStep.done)
				await outerIt.return?.();
		}
	}
);