import {sortArray} from "@utils";
import {operator}  from "../setup";
import {toArray}   from "./to-array";

const closure = [sortArray] as const;

export const sort = operator("sort", {closure, ops: [toArray]},
	(sortArray, toArray) => async function sort(iterable, param, reverse?) {
		const array = await toArray(iterable);
		sortArray(array, param, reverse as any);
		return array;
	}
);