import {operator} from "../setup";

export const findIndex = operator("findIndex", async function findIndex(iterable, predicate) {
	let i = -1;
	for await (const value of iterable) {
		if (predicate(value, ++i))
			return i;
	}
	return -1;
});