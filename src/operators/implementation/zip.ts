import {operator} from "../setup";

export const zip = operator("zip", {iterator: true, wrap: true}, iterator =>
	async function *zip(iterable, other, resultSelector) {
		if (!resultSelector)
			resultSelector = (a, b) => [a, b] as any;

		const ita = iterator(iterable);

		try {
			const itb = iterator(other);

			try {
				for (let index = 0; ; index++) {
					const stepA = await ita.next();
					if (stepA.done)
						break;
					const stepB = await itb.next();
					if (stepB.done)
						break;
					yield resultSelector(stepA.value, stepB.value, index);
				}
			} catch (E) {
				itb.throw?.(E);
				throw E;
			}
		} catch (E) {
			ita.throw?.(E);
			throw E;
		}
	}
);