import {Tag}                          from "#tags";
import {Operators}                    from "@operators/types";
import {IsTrue, Matched, UnasyncArgs} from "@utils";

export type OperatorKey = keyof Operators<Tag>;

export type Implementation<K extends OperatorKey, T extends Tag> = Operators<T>[K];

export interface Statics<K extends OperatorKey> {
	sync  : Implementation<K, Tag.StaticSync>;
	async : Implementation<K, Tag.StaticAsync>;
}

export type OpFuncs<O extends readonly OperatorKey[]> = {
	[K in keyof O]: Implementation<O[K], Tag.StaticAsync>;
}

export type Operator<K extends OperatorKey> = {
	[T in Tag]: Implementation<K, T>;
};

export type Template<K extends OperatorKey> = Statics<K> | Implementation<K, Tag.StaticAsync>;

type IteratorParam<I extends boolean> = IsTrue<I, [typeof Matched.iterator], []>;
type Args<I extends boolean, A extends readonly any[], O extends readonly OperatorKey[]> =
	[...IteratorParam<I>, ...A, ...OpFuncs<O>];

export type Builder<
	R,
	I extends boolean                = false,
	A extends readonly any[]         = [],
	O extends readonly OperatorKey[] = [],
> = (...args: Args<I, A, O>) => R;

export interface Options<
	I extends boolean                = false,
	A extends readonly any[]         = [],
	O extends readonly OperatorKey[] = [],
> {
	ops?      : {[N in keyof O]: Operator<O[N]>};
	iterator? : I;
	wrap?     : boolean;
	closure?  : A | {
		async : A;
		sync  : UnasyncArgs<A>;
	};
}