import {Tag}                           from "#tags";
import {Matched}                       from "@utils";
import {Builder, OperatorKey, Options} from "./types";

/**
 * See [TypeScript issue 17002][1].  Warning: it's a deep rabbit hole.
 *
 * [1]: https://github.com/microsoft/TypeScript/issues/17002
 */

const isArray = Array.isArray as
	<A extends readonly any[]>(
		closure: NonNullable<Options<false, A, []>["closure"]>
	) => closure is A;

export function buildImplementation<
	R,
	I extends boolean                = false,
	A extends readonly any[]         = [],
	O extends readonly OperatorKey[] = [],
>(
	options : Omit<Options<I, A, O>, "wrap">,
	builder : Builder<R, I, A, O>,
	sync?   : boolean,
): R {
	const {iterator, closure, ops} = options;
	const params = [];

	if (iterator)
		params.push(Matched.iterator);

	if (closure)
		params.push(...isArray(closure) ? closure : sync ? closure.sync : closure.async);

	if (ops) {
		const tag = sync ? Tag.StaticSync : Tag.StaticAsync;
		params.push(...ops.map(o => o[tag]));
	}

	return builder(...params as any);
}