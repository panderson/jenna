import {AsyncIterable}     from "#types/async-iterable/class";
import {Iterable}          from "#types/iterable/class";
import {Func, UnasyncFunc} from "@utils";

const rxMember = /^\s*\*?\s*(?!function\b)\w+\s*\(/;

export const instanceFunc = (name: string, fn: Function) => Function(
	"fn",
	`"use strict";return{${name}(){return fn(this,...arguments)}}`
)(fn)[name];

export function syncFunc<F extends Func>(fn: F): UnasyncFunc<F> {
	let fnText = fn.toString().replace(/\ba(?:sync|wait)\s*/g, '');
	if (rxMember.test(fnText))
		fnText = "function " + fnText;
	return Function(`"use strict";return ${fnText}`)();
}

export const wrapIterable = {
	async : <F extends Func>(name: string, fn: F): F => Function(
		"fn", "AsyncIterable",
		`"use strict";return{${name}(){return AsyncIterable.from(fn(...arguments))}}`
	)(fn, AsyncIterable)[name],

	sync  : <F extends Func>(name: string, fn: F): F => Function(
		"fn", "Iterable",
		`"use strict";return{${name}(){return Iterable.from(fn(...arguments))}}`
	)(fn, Iterable)[name],
};