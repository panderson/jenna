import {Tag}                                                               from "#tags";
import {instanceFunc, syncFunc, wrapIterable}                              from "./generate";
import {Builder, OperatorKey, Implementation, Operator, Options, Template} from "./types";
import {buildImplementation} from "./utils";

export function operator<K extends OperatorKey>(
	operator : K,
	template : Template<K>,
): Operator<K>;

export function operator<
	K extends OperatorKey,
	I extends boolean                = false,
	A extends readonly any[]         = [],
	O extends readonly OperatorKey[] = [],
>(
	operator : K,
	options  : Options<I, A, O>,
	build    : Builder<Template<K>, I, A, O>,
): Operator<K>;

export function operator<
	K extends OperatorKey,
	I extends boolean                = false,
	A extends readonly any[]         = [],
	O extends readonly OperatorKey[] = [],
>(
	operator : K,
	...args  : [Template<K>] | [Options<I, A, O>, Builder<Template<K>, I, A, O>]
): Operator<K> {
	let async : Implementation<K, Tag.StaticAsync>;
	let sync  : Implementation<K, Tag.StaticSync>;

	if (args.length === 1) {
		const [template] = args;
		if (typeof template === "object")
			({async, sync} = template);
		else {
			async = template;
			sync  = syncFunc(template) as any;
		}
	} else {
		const [{wrap, ...options}, build] = args;
		const implementation = buildImplementation(options, build);

		if (typeof implementation === "object")
			({async, sync} = implementation);
		else {
			async = implementation;
			sync  = buildImplementation(options, syncFunc(build) as any, true);
		}

		if (wrap) {
			async = wrapIterable.async(operator, async);
			sync  = wrapIterable.sync(operator, sync);
		}
	}

	return {
		[Tag.StaticSync]    : sync,
		[Tag.StaticAsync]   : async,
		[Tag.InstanceSync]  : instanceFunc(operator, sync),
		[Tag.InstanceAsync] : instanceFunc(operator, async),
	};
}