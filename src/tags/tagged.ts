import {
	ExtendedAsyncGenerator,
	ExtendedAsyncIterable,
	ExtendedAsyncIterableIterator,
	ExtendedGenerator,
	ExtendedIterable,
	ExtendedIterableIterator,
}            from "#types";
import {Tag} from "./tag";

declare module "#tags" {
	export namespace Tagged {
		export type Iterable<T extends Tag, I> =
			T extends Tag.Sync  ? globalThis.Iterable<I> :
			T extends Tag.Async ?         AnyIterable<I> :
			never;

		export type Iterator<T extends Tag, I, R = any, N = undefined> =
			T extends Tag.Sync   ? globalThis.Iterator<I, R, N> :
			T extends Tag.Async  ?         AnyIterator<I, R, N> :
			never;

		export type Promise<T extends Tag, P> =
			T extends Tag.Sync  ? P :
			T extends Tag.Async ? globalThis.Promise<P> :
			never;

		export type Generator<T extends Tag, G, R = any, N = unknown> =
			T extends Tag.Sync  ? globalThis.Generator<G, R, N> :
			T extends Tag.Async ?         AnyGenerator<G, R, N> :
			never;

		export namespace Result {
			export type Iterable<T extends Tag, I> =
				T extends Tag.Sync  ?      ExtendedIterable<I> :
				T extends Tag.Async ? ExtendedAsyncIterable<I> :
				never;

			export type Iterator<T extends Tag, I, R = any, N = undefined> =
				T extends Tag.Sync   ?      ExtendedIterableIterator<I, R, N> :
				T extends Tag.Async  ? ExtendedAsyncIterableIterator<I, R, N> :
				never;

			export type Generator<T extends Tag, G, R = any, N = unknown> =
				T extends Tag.Sync  ?      ExtendedGenerator<G, R, N> :
				T extends Tag.Async ? ExtendedAsyncGenerator<G, R, N> :
				never;
		}
	}
}