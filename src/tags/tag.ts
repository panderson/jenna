export const Tag = {
	StaticSync    : "Static-Sync"    as const,
	StaticAsync   : "Static-Async"   as const,
	InstanceSync  : "Instance-Sync"  as const,
	InstanceAsync : "Instance-Async" as const,
	All           : undefined as any as readonly Tag[],
	Static        : undefined as any as readonly Tag.Static[],
	Instance      : undefined as any as readonly Tag.Instance[],
	Sync          : undefined as any as readonly Tag.Sync[],
	Async         : undefined as any as readonly Tag.Async[],
};

export namespace Tag {
	export type StaticSync    = "Static-Sync";
	export type StaticAsync   = "Static-Async";
	export type InstanceSync  = "Instance-Sync";
	export type InstanceAsync = "Instance-Async";

	export type Sync        = StaticSync   | InstanceSync;
	export type Async       = StaticAsync  | InstanceAsync;
	export type Static      = StaticSync   | StaticAsync;
	export type Instance    = InstanceSync | InstanceAsync;

	export type ToSync    <T extends Tag> = T extends Static ? StaticSync   : InstanceSync;
	export type ToAsync   <T extends Tag> = T extends Static ? StaticAsync  : InstanceAsync;
	export type ToStatic  <T extends Tag> = T extends Sync   ? StaticSync   : StaticAsync;
	export type ToInstance<T extends Tag> = T extends Sync   ? InstanceSync : InstanceAsync;
}

export type Tag = Tag.Sync | Tag.Async;

Tag.All      = [Tag.StaticSync, Tag.StaticAsync, Tag.InstanceSync, Tag.InstanceAsync] as const;
Tag.Static   = [Tag.StaticSync, Tag.StaticAsync] as const;
Tag.Instance = [Tag.InstanceSync, Tag.InstanceAsync] as const;
Tag.Sync     = [Tag.StaticSync, Tag.InstanceSync] as const;
Tag.Async    = [Tag.StaticAsync, Tag.InstanceAsync] as const;